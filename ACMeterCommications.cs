﻿// ACMeterCommications.cs
//
// Define a set of routines that will commicate with the AC Meter.
// Currently the AC meter is a CU-3
//
// defined functions:
//      Initialize
//      Close
//      GetData
//

using System;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;
using System.Threading;
using System.Diagnostics;

namespace SensorManufSY2
{
    public sealed class ACMeterCommications : IDisposable
    {
        /************************************/
        /*      constants                   */
        /************************************/
        private const int CMD_RCV_DELAY = 100;         // time in milliseconds to delay after sending the command to start looking for the response
        private const int CMD_RESPONSE_TIMEOUT = 2000;    // max time in milliseconds to wait for the command response

        /************************************/
        /*          defines              */
        /************************************/
        private static int NUMBER_OF_BYTES_IN_CALIIBRATION_BUFFER = 48;  // 4 * NUMBER_OF_CAL_VALUES
        private static int NUMBER_OF_CAL_VALUES = 12;                     // the number of cal values in the buffer
        private static int NUMBER_OF_BYTES_IN_MANUFACTURING_BUFFER = 128;

        //        static int CU_NUM_BYTES_COMMAND = 6;
        static int CU_NUM_BYTES_RESPONSE_BUFFER = 6;


        /************************************/
        /*         CU Commands variables         */
        /************************************/
        byte[] CU_responseBuffer = new byte[CU_NUM_BYTES_RESPONSE_BUFFER];

        byte[] rd_status = { 0x00, 0x00, 0x00, 0x00, 0x00, 0 };                 // 0x0000   read
        byte[] rd_SU_watchdog_control = { 0x00, 0x00, 0x01, 0xFE, 0xED, 0 };    // 0x0001   read
        byte[] wr_SU_watchdog_control = { 0x20, 0x00, 0x01, 0xBE, 0xEF, 0 };    // 0x0001   write
        byte[] relay_on = { 0x20, 0x00, 0x13, 0x00, 0x01, 0 };                  // 0x0013   write
        byte[] relay_off = { 0x20, 0x00, 0x13, 0x00, 0x00, 0 };                 // 0x0013   write
        byte[] rd_energy = { 0x00, 0x00, 0x15, 0x00, 0x00, 0 };                 // 0x0015   read
        byte[] wr_SU_reboot = { 0x00, 0x00, 0x17, 0x00, 0x00, 0 };              // 0x0017   read
        byte[] rd_version = { 0x00, 0xFF, 0x02, 0x00, 0x00, 0 };                // 0x0002   read
        byte[] rd_milliWattHoursLower = { 0x00, 0x00, 0x19, 0x00, 0x00, 0 };    // 0x0019   read
        byte[] rd_milliWattHoursUpper = { 0x00, 0x00, 0x1A, 0x00, 0x00, 0 };    // 0x001A   read
        byte[] rd_WattHoursLower = { 0x00, 0x00, 0x1B, 0x00, 0x00, 0 };         // 0x001B   read
        byte[] rd_WattHoursUpper = { 0x00, 0x00, 0x1C, 0x00, 0x00, 0 };         // 0x001C   read
        byte[] rd_RMS_Voltage = { 0x00, 0x00, 0x1D, 0x00, 0x00, 0 };            // 0x001D   read
        byte[] rd_RMS_Current = { 0x00, 0x00, 0x1E, 0x00, 0x00, 0 };            // 0x001E   read
        byte[] rd_PowerFactor = { 0x00, 0x00, 0x1F, 0x00, 0x00, 0 };            // 0x001F   read
        byte[] rd_RMS_Power = { 0x00, 0x00, 0x20, 0x00, 0x00, 0 };              // 0x0020   read
        byte[] rd_temperature = { 0x00, 0x00, 0x21, 0x00, 0x00, 0 };            // 0x0021   read
        byte[] rd_line_frequency = { 0x00, 0x00, 0x22, 0x00, 0x00, 0 };         // 0x0022   read
        byte[] rd_bd_version = { 0x00, 0xFF, 0x07, 0x00, 0x00, 0 };             // 0xFF07   read
        byte[] rd_fw_build_number = { 0x00, 0xFF, 0x09, 0x00, 0x00, 0 };        // 0xFF09   read
        byte[] rd_security_bit = { 0x00, 0xFF, 0x0A, 0x00, 0x00, 0 };           // 0xFF0A   read
        byte[] wr_tempcal_data = { 0x20, 0xFF, 0x11, 0x00, 0x00, 0 };           // 0xFF11   write
        byte[] wr_perform_tempcal = { 0x20, 0xff, 0x13, 0x00, 0x00, 0 };        // 0xFF13   write
        byte[] rd_tempcal_data = { 0x00, 0xff, 0x14, 0x00, 0x00, 0 };           // 0xFF14   read
        byte[] rd_versionRequest = { 0, 0xff, 0x20, 0, 0, 0 };                  // 0xFF20   read
        byte[] rd_mfg_data = { 0x00, 0xFF, 0x80, 0x00, 0x00, 0 };
        byte[] wr_mfg_data = { 0x20, 0xFF, 0x80, 0x00, 0x00, 0 };
        byte[] rd_cal_data = { 0x00, 0xFE, 0x80, 0x00, 0x00, 0 };
        byte[] wr_cal_data = { 0x20, 0xFE, 0x80, 0x00, 0x00, 0 };
        byte[] wr_mfg_flash_segment = { 0x20, 0xFF, 0x08, 0x00, 0x00, 0 };
        byte[] rd_mfg_flash_segment = { 0x00, 0xFF, 0x08, 0x01, 0x00, 0 };
        byte[] rd_msg_bad_crc = { 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF };


        /***************************************
        /*  storage
        /********************************/

        // Calibration addresses and values.
        // The following 3 items are positioned matched.

        //FE00 - CAL IA 60hz msb  - default is 9571
        //FE03 – CAL IA 60hz lsb
        //FE04 - CAL VA 60hz msb – default is  17109
        //FE07 – CAL VA 60hz lsb
        //FE08 – Phase Adjust 60hz msb – default is 0
        //FE0B – Phase Adjust 60hz lsb
        //FE0C – Temperature 60hz msb – default is 16385
        //FE0F – Temperature 60hz lsb
        //FE10 – close relay delay msb  - 60 hz – default is 1270
        //FE13 – close relay delay lsb
        //FE14 – open relay delay msb  - 60 hz – default is 2310
        //FE17 – open relay delay lsb
        //FE18 – close relay delay msb  - 50 hz – default is 1830
        //FE1B– close relay delay lsb
        //FE1C– open relay delay msb  - 50 hz – default is 3100
        //FE1F – open relay delay lsb
        //FE20 - CAL IA 50hz msb  - default is 9571
        //FE23 – CAL IA 50hz lsb
        //FE24 - CAL VA 50hz msb – default is  17109
        //FE27 – CAL VA 50hz lsb
        //FE28– Phase Adjust50hz msb – default is 0
        //FE2b – Phase Adjust50hz lsb
        //FE2C – Temperature 50hz msb – default is 16385
        //FE2F – Temperature 50hz lsb

        private UInt32[] CalValues = new UInt32[NUMBER_OF_CAL_VALUES];

        private enum CalAddresses
        {
            IA_GAIN_60_HZ = 0xFE00,
            VA_GAIN_60_HZ = 0xFE04,
            PHASE_ADJ_GAIN_60_HZ = 0xFE08,
            TEMPERATURE_GAIN_60_HZ = 0xFE0C,
            RELAY_CLOSE_60_HZ_TIMER = 0xFE10,
            RELAY_OPEN_60_HZ_TIMER = 0xFE14,
            RELAY_CLOSE_50_HZ_TIMER = 0xFE18,
            RELAY_OPEN_50_HZ_TIMER = 0xFE1C,
            IA_GAIN_50_HZ = 0xFE20,
            VA_GAIN_50_HZ = 0xFE24,
            PHASE_ADJ_GAIN_50_HZ = 0xFE28,
            TEMPERATURE_GAIN_50_HZ = 0xFE2C
        }

        private string[] CalLabels =
        {
            "IA_GAIN_60_HZ",
            "VA_GAIN_60_HZ",
            "PHASE_ADJ_GAIN_60_HZ",
            "TEMPERATURE_GAIN_60_HZ",
            "RELAY_CLOSE_60_HZ_TIMER",
            "RELAY_OPEN_60_HZ_TIMER",
            "RELAY_CLOSE_50_HZ_TIMER",
            "RELAY_OPEN_50_HZ_TIMER",
            "IA_GAIN_50_HZ",
            "VA_GAIN_50_HZ",
            "PHASE_ADJ_GAIN_50_HZ",
            "TEMPERATURE_GAIN_50_HZ"
        };

        //----------------------------------------
        // serial port values
        //----------------------------------------
        private readonly SerialPort CUSerialPort;            // dut serial port
        //private string CUSerialBuffer = string.Empty;      // buffer of incoming serial data
        private string ConfigCUComPort = null;             // com port to use. in the form of COMx
        //private double ConfigCurrentOffset = 0;             // current used by the CU
        //public double configCurrentOffset { get { return ConfigCurrentOffset; } }
        bool disposed = false;
        public bool IsACMeterInitialized { get { return CUSerialPort.IsOpen; } }


        byte[] inv = new byte[6];
        byte[] outv = new byte[6];

        byte[] mfgdata = new byte[6];
        byte[] mfgctrl = new byte[6];

        byte[] ManufacturingDataBuffer = new byte[NUMBER_OF_BYTES_IN_MANUFACTURING_BUFFER];
        byte[] CalibrationDataBuffer = new byte[NUMBER_OF_BYTES_IN_CALIIBRATION_BUFFER];

        public ACMeterCommications()
        {
            CUSerialPort = new SerialPort();
        }

        //---------------------------------------------
        //  private functions
        //--------------------------------------------
        private byte CrcCalc(byte[] Msg)
        {
            int len, k;
            int crc = 0;
            const byte poly = 0x07;
            for (len = 0; len < 5; len++)
            {
                crc = crc ^ Msg[len];
                for (k = 0; k < 8; k++)
                {
                    if ((crc & 0x80) != 0)
                    {
                        crc <<= 1;
                        crc ^= poly;
                    }
                    else
                    {
                        crc <<= 1;
                    }
                }
            }
            return ((byte)crc);
        }

        //--------------------------------------
        //  public functions
        //--------------------------------------
        /*******************************************/
        /*       Init the CU COM Port       */
        /*******************************************/
        //--------------------------
        // Sets up and opens the dut serial port.
        // Uses the configUUTComPort value for the port.
        // If the value is "", just returns.
        //
        // excpetions:
        //          PortNotFound - configured port not in computer ports
        //          Excpetion
        //          SerialPort contruct exceptions
        //          SerialPort.Open exceptions
        public void Open(string comport)
        {
            Trace.WriteLine(DateTime.Now.ToString("HH:mm:ss.fff") + " - AC Meter port open called");
            if (this.disposed)
            {
                throw new ObjectDisposedException(this.GetType().Name, "Cannot use a disposed object.");
            }

            if (comport == "")      // if the comport is not defined
                return;

            string[] ports = SerialPort.GetPortNames();         // get list of serial ports

            // see if this port is in the list of ports on the computer
            bool found = false;
            foreach (string i in ports)
            {
                if (i.Equals(comport))         // if found a match
                {
                    found = true;
                    break;
                }
            }
            if (!found)
            {       // port not found
                string msg = string.Format("Configured port({0}) not valid on this system", comport);
                throw new Exception(msg);
            }

            // port valid on this system, so try to open
            try
            {
                if (!CUSerialPort.IsOpen)
                {
                    ConfigCUComPort = comport;
                    CUSerialPort.PortName = comport;
                    CUSerialPort.BaudRate = 19200;
                    CUSerialPort.Parity = Parity.None;
                    CUSerialPort.StopBits = StopBits.One;
                    CUSerialPort.DataBits = 8;
                    CUSerialPort.ReadBufferSize = CU_NUM_BYTES_RESPONSE_BUFFER * 20;
                    CUSerialPort.Handshake = Handshake.None;
                    CUSerialPort.ReadTimeout = 500;
                    CUSerialPort.WriteTimeout = 500;
                    CUSerialPort.ErrorReceived += new SerialErrorReceivedEventHandler(ErrorReceived);
                    CUSerialPort.Open();           // open the serial port
                    disposed = false;
                }
            }
            catch (Exception ex)
            {
                Trace.WriteLine(string.Format("{1} - AC Meter Error opening {0} : {2}", comport, DateTime.Now.ToString("HH:mm:ss.fff"), ex.Message));
                throw new Exception(string.Format("Error opening {0}", comport), ex);
            }


            //
            // to save time, set up the CRCs for commands that are constant.
            //  initialize packets
            //
            rd_versionRequest[5] = CrcCalc(rd_versionRequest);
            //	        rd_versionReply[5]=CrcCalc(rd_versionReply);
            rd_status[5] = CrcCalc(rd_status);
            rd_energy[5] = CrcCalc(rd_energy);
            rd_msg_bad_crc[5] = 0xff;
            inv[5] = CrcCalc(inv);
            //	        outv[0]=0x20; outv[1]=0; outv[2]=0x11; outv[3]=0; outv[4]=0x80;
            //	        mfgdata[0]=0x00; mfgdata[1]=0xFF; mfgdata[2]=0x80; mfgdata[3]=0x00; mfgdata[4]=0x00;
            //	        mfgctrl[0]=0x20; mfgctrl[1]=0xff; mfgctrl[2]=0x01; mfgctrl[3]=0; mfgctrl[4]=0;  // default read mode
            relay_on[5] = CrcCalc(relay_on);
            relay_off[5] = CrcCalc(relay_off);
            rd_SU_watchdog_control[5] = CrcCalc(rd_SU_watchdog_control);
            wr_SU_watchdog_control[5] = CrcCalc(wr_SU_watchdog_control);
            wr_SU_reboot[5] = CrcCalc(wr_SU_reboot);
            rd_version[5] = CrcCalc(rd_version);
            rd_bd_version[5] = CrcCalc(rd_bd_version);
            // writes ram buffer to flash
            wr_mfg_flash_segment[5] = CrcCalc(wr_mfg_flash_segment);
            // reads from flash to  ram buffer 
            rd_mfg_flash_segment[5] = CrcCalc(rd_mfg_flash_segment);
            rd_fw_build_number[5] = CrcCalc(rd_fw_build_number);
            rd_security_bit[5] = CrcCalc(rd_security_bit);
            //reads/writes to ram buffer of mfg data
            rd_mfg_data[5] = CrcCalc(rd_mfg_data);
            wr_mfg_data[5] = CrcCalc(wr_mfg_data);
            //reads/writes to ram buffer of calibration data
            rd_cal_data[5] = CrcCalc(rd_cal_data);
            wr_cal_data[5] = CrcCalc(wr_cal_data);
            rd_milliWattHoursLower[5] = CrcCalc(rd_milliWattHoursLower);
            rd_milliWattHoursUpper[5] = CrcCalc(rd_milliWattHoursUpper);
            rd_WattHoursLower[5] = CrcCalc(rd_WattHoursLower);
            rd_WattHoursUpper[5] = CrcCalc(rd_WattHoursUpper);
            rd_RMS_Voltage[5] = CrcCalc(rd_RMS_Voltage);
            rd_RMS_Current[5] = CrcCalc(rd_RMS_Current);
            rd_PowerFactor[5] = CrcCalc(rd_PowerFactor);
            rd_RMS_Power[5] = CrcCalc(rd_RMS_Power);
            rd_temperature[5] = CrcCalc(rd_temperature);
            rd_line_frequency[5] = CrcCalc(rd_line_frequency);
            // temp cal
            rd_tempcal_data[5] = CrcCalc(rd_tempcal_data);
            Trace.WriteLine(DateTime.Now.ToString("HH:mm:ss.fff") + "AC Meter port opened");
        }

        // //-------------------
        //// exception PortNotFound 
        //// Will cause a exception when the configured port is not found in the
        //// list of avaialibe ports
        //[Serializable]
        //public class PortNotFound : ApplicationException
        //{
        //    public string[] ComputerPorts { get; set; }                 // list of ports availible ports
        //    public string RequestedPort { get; set; }

        //    public PortNotFound() { }
        //    public PortNotFound(string message, string[] comPorts, string rqsPort)
        //        : base(message)
        //    {
        //        ComputerPorts = comPorts;
        //        RequestedPort = rqsPort;
        //    }
        //    public PortNotFound(string message, Exception inner) : base(message, inner) { }
        //    protected PortNotFound(
        //      System.Runtime.Serialization.SerializationInfo info,
        //      System.Runtime.Serialization.StreamingContext context)
        //        : base(info, context) { }
        //}

        /************************************/
        /*         Close COM Port           */
        /************************************/
        public bool Close()
        {
            Trace.WriteLine(DateTime.Now.ToString("HH:mm:ss.fff") + " - AC Meter port close called");
            if (this.disposed)
            {
                throw new ObjectDisposedException(this.GetType().Name, "Cannot use a disposed object.");
            }
            try
            {
                if (CUSerialPort != null)
                {
                    CUSerialPort.DiscardInBuffer();
                    if (CUSerialPort.IsOpen)
                        CUSerialPort.Close();
                }
                Trace.WriteLine(DateTime.Now.ToString("HH:mm:ss.fff") + " - AC Meter port closed");
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public void DiscardAllBuffers()
        {
            if (this.disposed)
            {
                throw new ObjectDisposedException(this.GetType().Name, "Cannot use a disposed object.");
            }
            CUSerialPort.DiscardOutBuffer();
            CUSerialPort.DiscardInBuffer();
            Task.Delay(1000);
        }

        public void Dispose()
        {
            if (!this.disposed)
            {
                CUSerialPort.Close();
                CUSerialPort.Dispose();
                GC.SuppressFinalize(this);
                disposed = true;
            }
        }

        //protected virtual void Dispose(bool disposing)
        //{
        //    //if (!disposed)
        //    //{
        //    //    if (disposing)
        //    //    {
        //    //        //if (serialPort != null)
        //    //        //    if (serialPort.IsOpen)
        //    //        //        serialPort.Close();
        //    //        serialPort.Dispose();
        //    Close();
        //    disposed = true;
        //    //    }
        //    //}
        //}

        private void ErrorReceived(object sender, SerialErrorReceivedEventArgs e)
        {
            Trace.WriteLine(DateTime.Now.ToString("HH:mm:ss.fff") + " - AC port error: " + e.EventType.ToString());
        }


        //***************************
        // Read the power meter data without correcting the data
        //***************************
        public int ReadRaw(ref double watts, ref double current, ref double voltage, ref double powerfactor, ref string reportmsg)
        {
            int status = (int)ErrorCodes.Program.Success;
            if (this.disposed)
            {
                throw new ObjectDisposedException(this.GetType().Name, "Cannot use a disposed object.");
            }

            watts = 0;
            current = 0;
            voltage = 0;
            powerfactor = 0;
            reportmsg = string.Empty;

            if (!IsACMeterInitialized)
            {
                reportmsg = "AC Power Meter not initialized";
                status = (int)ErrorCodes.System.InstrumentNotSetUp;
            }
            else
            {
                status = get_cu_AC_PowerFactor(ref powerfactor, ref reportmsg);
                if (status == (int)ErrorCodes.Program.Success)
                {
                    status = get_cu_AC_RMS_Current(ref current, ref reportmsg);
                }
                if (status == (int)ErrorCodes.Program.Success)
                {
                    status = get_cu_AC_RMS_Voltage(ref voltage, ref reportmsg);
                }
                if (status == (int)ErrorCodes.Program.Success)
                {
                    status = get_cu_AC_RMS_Watts(ref watts, ref reportmsg);
                }
            }

            return status;
        }

        //***************************
        // Read the power meter. The retured data will take into account the current used by the cu.
        //
        //  current = current - offsetcurrent
        //  The only error returned by the get_ function is a system error.
        //***************************
        public int Read(ref double watts, ref double current, ref double voltage, ref double powerfactor, ref string reportmsg)
        {
            int status = (int)ErrorCodes.Program.Success;
            if (this.disposed)
            {
                throw new ObjectDisposedException(this.GetType().Name, "Cannot use a disposed object.");
            }

            watts = 0;
            current = 0;
            voltage = 0;
            powerfactor = 0;
            reportmsg = string.Empty;

            if (!IsACMeterInitialized)
            {
                reportmsg = "AC Power Meter not initialized";
                status = (int)ErrorCodes.System.InstrumentNotSetUp;
            }
            else
            {
                status = get_cu_AC_PowerFactor(ref powerfactor, ref reportmsg);
                powerfactor = powerfactor / 1000;                               // get it to 0 to 1 range
                if (status == (int)ErrorCodes.Program.Success)
                {
                    status = get_cu_AC_RMS_Current(ref current, ref reportmsg);
                }
                if (status == (int)ErrorCodes.Program.Success)
                {
                    status = get_cu_AC_RMS_Voltage(ref voltage, ref reportmsg);
                }
                if (status == (int)ErrorCodes.Program.Success)
                {
                    status = get_cu_AC_RMS_Watts(ref watts, ref reportmsg);
                }
            }

            return status;
        }

        //***********************************************
        //  Read watts
        //  The only error returned by the get_ function is a system error.
        //***********************************************
        public int ReadWatts(ref double watts, ref string reportmsg)
        {
            int status;
            if (this.disposed)
            {
                throw new ObjectDisposedException(this.GetType().Name, "Cannot use a disposed object.");
            }

            if (!IsACMeterInitialized)
            {
                reportmsg = "AC Power Meter not initialized";
                status = (int)ErrorCodes.System.InstrumentNotSetUp;
            }
            else
            {
                status = get_cu_AC_RMS_Watts(ref watts, ref reportmsg);
            }
            return status;
        }

        //***********************************************
        //  Read volts
        //  The only error returned by the get_ function is a system error.
        //***********************************************
        public int ReadVoltage(ref double voltage, ref string reportmsg)
        {
            int status;
            if (this.disposed)
            {
                throw new ObjectDisposedException(this.GetType().Name, "Cannot use a disposed object.");
            }

            if (!IsACMeterInitialized)
            {
                reportmsg = "AC Power Meter not initialized";
                status = (int)ErrorCodes.System.InstrumentNotSetUp;
            }
            else
            {
                status = get_cu_AC_RMS_Voltage(ref voltage, ref reportmsg);
            }
            return status;
        }

        public int ReadVersion(ref string version, ref string outmsg)
        {
            return get_cu_firmware_build_number(ref version, ref outmsg);
        }

        //--------------------------------------
        //  private functions
        //--------------------------------------

        /*********************************************************************************************************/
        /***********************************    CU  COMM   PORT   ************************************************/
        /*********************************************************************************************************/


        /**************************************/
        /* send command to CU com port 
        // Sends the command and waits for the response.
        //
        // throw the following expections:
        //      Sending Command error
        //      Not enough characters received in time
        //      Did not get a ACK
        /***************************************/
        /// <summary>
        /// Sends the command array passed and waits for the response. 
        /// Will throw exceptions for the following:
        ///     Sending Command error
        ///     Not enough characters received in time
        ///     Did not get a ACK
        /// </summary>
        /// <param name="CU_command_array">byte[6] - data to send. The CRC will be calcuated and put in the last byte</param>
        /// <param name="timeoutms">int - max time in ms for the received to complete</param>
        /// <param name="rcvDelayms">int - delay in ms to wait after the send to receive</param>
        /// <param name="CU_response_array">byte[6] - data received</param>
        private void send_CU_com_port(byte[] CU_command_array, int timeoutms, int rcvDelayms, ref byte[] CU_response_array)
        {
            bool rtnbad = false;
            int timesSent = 0;
            int x;
            byte rtncrc;

            CU_command_array[5] = CrcCalc(CU_command_array);    // add the CRC to the command
            Trace.WriteLine(DateTime.Now.ToString("HH:mm:ss.fff") + "Command write start");

            do
            {
                rtnbad = false;
                if (CUSerialPort.BytesToRead != 0)      // if there is stuff in the input buffer
                {
                    CUSerialPort.DiscardInBuffer();     // clear out any old stuff
                    CUSerialPort.DiscardOutBuffer();
                    Trace.WriteLine("Junk in buffer. len=" + CUSerialPort.BytesToRead);
                    delay(1000);
                }
                try
                {
                    //CUSerialPort.Write(CU_command_array, 0, 6);
                    //while (CUSerialPort.BytesToWrite > 0)
                    //{
                    //    delay(5);
                    //}
                    //delay(5);
                    for (x = 0; x < 6; x++)
                    {
                        CUSerialPort.Write(CU_command_array, x, 1);
                        Thread.Sleep(1);
                    }
                }
                catch (Exception ex)    // send error
                {
                    Trace.WriteLine(DateTime.Now.ToString("HH:mm:ss.fff") + ":AC:Error doing cmd write: " + ex.Message);
                    throw new Exception("Sending command error. " + ex.Message);
                }

                delay(rcvDelayms);
                Trace.WriteLine(DateTime.Now.ToString("HH:mm:ss.fff") + "Command read start");
                x = CUReadResponse(ref CU_response_array, timeoutms);   // get the response
                Trace.WriteLine(DateTime.Now.ToString("HH:mm:ss.fff") + "Command read done");
                rtncrc = CrcCalc(CU_response_array);
                if (x < 6)             // if the return is bad
                {
                    rtnbad = true;
                    Trace.WriteLine(string.Format("{13}:AC:Not enough characters received in time. number of recived characters={12}  " +
                                                    "snd={0:X2}:{1:X2}:{2:X2}:{3:X2}:{4:X2}:{5:X2}  " +
                                                    "rcv={6:X2}:{7:X2}:{8:X2}:{9:X2}:{10:X2}:{11:X2}",
                                                        CU_command_array[0], CU_command_array[1], CU_command_array[2],
                                                        CU_command_array[3], CU_command_array[4], CU_command_array[5],
                                                        CU_response_array[0], CU_response_array[1], CU_response_array[2],
                                                        CU_response_array[3], CU_response_array[4], CU_response_array[5],
                                                        x, DateTime.Now.ToString("HH:mm:ss.fff")));
                    //CUSerialPort.DiscardInBuffer();
                    //CUSerialPort.DiscardOutBuffer();
                    //Thread.Sleep(1000);
                    //CUSerialPort.Close();
                    //Thread.Sleep(1000);
                    //CUSerialPort.Open();
                    //Thread.Sleep(1000);
                    //Trace.WriteLine(string.Format("{0}:Reopened AC Meter com port", DateTime.Now.ToString()));
                }
                else if (rtncrc != CU_response_array[5])
                {
                    rtnbad = true;
                    Trace.WriteLine(string.Format("{12}:PM:CRCs do not match  " +
                                                    "snd={0:X2}:{1:X2}:{2:X2}:{3:X2}:{4:X2}:{5:X2}  " +
                                                    "rcv={6:X2}:{7:X2}:{8:X2}:{9:X2}:{10:X2}:{11:X2}",
                                                        CU_command_array[0], CU_command_array[1], CU_command_array[2],
                                                        CU_command_array[3], CU_command_array[4], CU_command_array[5],
                                                        CU_response_array[0], CU_response_array[1], CU_response_array[2],
                                                        CU_response_array[3], CU_response_array[4], CU_response_array[5],
                                                        DateTime.Now.ToString("HH:mm:ss.fff")));
                }
                else if ((CU_response_array[0] & 0x87) != 0x80)      // only care about the reply and error bits
                {
                    rtnbad = true;
                    Trace.WriteLine(string.Format("{12}:AC:Did not get a ACK. snd={0:X2}:{1:X2}:{2:X2}:{3:X2}:{4:X2}:{5:X2}  " +
                                                    "rcv={6:X2}:{7:X2}:{8:X2}:{9:X2}:{10:X2}:{11:X2}",
                                                        CU_command_array[0], CU_command_array[1], CU_command_array[2],
                                                        CU_command_array[3], CU_command_array[4], CU_command_array[5],
                                                        CU_response_array[0], CU_response_array[1], CU_response_array[2],
                                                        CU_response_array[3], CU_response_array[4], CU_response_array[5],
                                                        DateTime.Now.ToString("HH:mm:ss.fff")));
                }
            } while (rtnbad && (timesSent++ <= 2));
            Trace.Flush();

            if (x != 6)
                throw new Exception(string.Format("Not enough characters received in time. number of recived characters={12}" +
                                                    "snd={0:X2}:{1:X2}:{2:X2}:{3:X2}:{4:X2}:{5:X2}\n" +
                                                    "rcv={6:X2}:{7:X2}:{8:X2}:{9:X2}:{10:X2}:{11:X2}",
                                                        CU_command_array[0], CU_command_array[1], CU_command_array[2],
                                                        CU_command_array[3], CU_command_array[4], CU_command_array[5],
                                                        CU_response_array[0], CU_response_array[1], CU_response_array[2],
                                                        CU_response_array[3], CU_response_array[4], CU_response_array[5], x));

            if (rtncrc != CU_response_array[5])
            {
                throw new Exception(string.Format("CRCs do not match  " +
                                                "snd={0:X2}:{1:X2}:{2:X2}:{3:X2}:{4:X2}:{5:X2}  " +
                                                "rcv={6:X2}:{7:X2}:{8:X2}:{9:X2}:{10:X2}:{11:X2}",
                                                    CU_command_array[0], CU_command_array[1], CU_command_array[2],
                                                    CU_command_array[3], CU_command_array[4], CU_command_array[5],
                                                    CU_response_array[0], CU_response_array[1], CU_response_array[2],
                                                    CU_response_array[3], CU_response_array[4], CU_response_array[5]));
            }

            if ((CU_response_array[0] & 0x87) != 0x80)      // only care about the reply and error bits
                throw new Exception(string.Format("Did not get a ACK. snd={0:X2}:{1:X2}:{2:X2}:{3:X2}:{4:X2}:{5:X2}\n" +
                                                    "rcv={6:X2}:{7:X2}:{8:X2}:{9:X2}:{10:X2}:{11:X2}",
                                                        CU_command_array[0], CU_command_array[1], CU_command_array[2],
                                                        CU_command_array[3], CU_command_array[4], CU_command_array[5],
                                                        CU_response_array[0], CU_response_array[1], CU_response_array[2],
                                                        CU_response_array[3], CU_response_array[4], CU_response_array[5]));
            Trace.Flush();
        }

        //************************************************************
        // Method: CUReadResponse
        //  Reads the serial input buffer for a command response.
        // 
        // params:  ref byte[] returnedLine - the 6 retured bytes.
        //          int timeoutms - max number of milliseconds to wait for the line
        // return:
        //      number of bytes found.
        // Exceptions:
        //      Exception
        //*******************************************************
        private int CUReadResponse(ref byte[] returnedLine, int timeoutms)
        {
            DateTime endTime;
            int status = 0;

            endTime = DateTime.Now.AddMilliseconds(timeoutms);
            while ((CUSerialPort.BytesToRead < 6) && (endTime > DateTime.Now))
            {
                //Task.Delay(10);
            }

            if (CUSerialPort.BytesToRead > 6)
            {
                Trace.WriteLine(string.Format("{0}:AC:Recived more than 6 char. #={1}", DateTime.Now.ToString("HH:mm:ss.fff"), CUSerialPort.BytesToRead));
                status = 6;
            }
            else
                status = CUSerialPort.BytesToRead;
            returnedLine[0] = 0;
            returnedLine[1] = 0;
            returnedLine[2] = 0;
            returnedLine[3] = 0;
            returnedLine[4] = 0;
            returnedLine[5] = 0;
            for (int i = 0; i < status; i++)
            {
                returnedLine[i] = (byte)CUSerialPort.ReadByte();
            }
            return status;
        }


        //------------------------------------------------
        //  CU commands
        //---------------------------------------


        /************************************/
        /*   get cu_security_bit            */
        /* bit 6 is set for security        */
        /************************************/
        static byte CU_SECURITY_BIT_MASK = 0x40;  // bit 6 in SRF 0xB2 is set when the chip is properly programmed.

        private int get_cu_security_bit(out byte securitybyte, out string reportStatus)
        {
            byte[] cmdresponse = { 0, 0, 0, 0, 0, 0 };

            securitybyte = 0;
            reportStatus = "";

            try
            {
                send_CU_com_port(rd_security_bit, CMD_RESPONSE_TIMEOUT, CMD_RCV_DELAY, ref cmdresponse);
            }
            catch (Exception ex)    // there was an issue with the send or receive
            {
                reportStatus = ex.Message;
                return (int)ErrorCodes.Program.FailedToPerformCUFunction;
            }
            // response 0 - ctrl
            // response 1 - addr
            // response 2 - addr
            // response 3 - not used
            // respones 4 - security bit
            // response 5 - crc

            // check SFR 0xB2 to make sure the security bit (bit 6) is set.
            // isolate just bit 6.
            securitybyte = cmdresponse[4];

            if ((securitybyte & CU_SECURITY_BIT_MASK) != CU_SECURITY_BIT_MASK)
                securitybyte = 0;
            else
                securitybyte = CU_SECURITY_BIT_MASK;

            return (int)ErrorCodes.Program.Success;
        }



        /************************************/
        /*   get cu_firmware_build_number    /
        /************************************/

        private int get_cu_firmware_build_number(ref string fwVersion, ref string reportMsg)
        {
            byte[] cmdresponse = { 0, 0, 0, 0, 0, 0 };

            fwVersion = "";
            reportMsg = string.Empty;

            try
            {
                send_CU_com_port(rd_fw_build_number, CMD_RESPONSE_TIMEOUT, CMD_RCV_DELAY, ref cmdresponse);
                fwVersion = cmdresponse[3].ToString() + "." + cmdresponse[4].ToString();
                return (int)ErrorCodes.Program.Success;
            }
            catch (Exception ex)    // there was an issue with the send or receive
            {
                reportMsg = "Error doing 'get_cu_firmware_build_number' function. Error message: " + ex.Message;
                return (int)ErrorCodes.Program.FailedToPerformCUFunction;
            }
        }

        /************************************/
        /*   get cu_AC_RMS_Voltage           /
        /************************************/

        private int get_cu_AC_RMS_Voltage(ref double voltage, ref string reportMsg)
        {
            byte[] cmdresponse = { 0, 0, 0, 0, 0, 0 };
            voltage = 0;
            reportMsg = string.Empty;

            try
            {
                Trace.WriteLine("read volts");
                send_CU_com_port(rd_RMS_Voltage, CMD_RESPONSE_TIMEOUT, CMD_RCV_DELAY, ref cmdresponse);
                voltage = ((cmdresponse[3] * 256) + cmdresponse[4]) / 10.0;
                return (int)ErrorCodes.Program.Success;
            }
            catch (Exception ex)    // there was an issue with the send or receive
            {
                reportMsg = "Error doing pm 'get_cu_AC_RMS_Voltage' functions. Error message: " + ex.Message;
                return (int)ErrorCodes.System.ACMeterReadError;
            }
        }

        /************************************/
        /*   get cu_AC_RMS_Current  ma       /
        /************************************/

        private int get_cu_AC_RMS_Current(ref double voltage, ref string reportMsg)
        {
            byte[] cmdresponse = { 0, 0, 0, 0, 0, 0 };

            voltage = 0;
            reportMsg = string.Empty;

            try
            {
                Trace.WriteLine("read current");
                send_CU_com_port(rd_RMS_Current, CMD_RESPONSE_TIMEOUT, CMD_RCV_DELAY, ref cmdresponse);
                voltage = (cmdresponse[3] * 256) + cmdresponse[4];
                return (int)ErrorCodes.Program.Success;
            }
            catch (Exception ex)    // there was an issue with the send or receive
            {
                reportMsg = "Error doing pm 'get_cu_AC_RMS_Current' functions. Error message: " + ex.Message;
                return (int)ErrorCodes.System.ACMeterReadError;
            }
        }


        /************************************/
        /*  get cu_AC_RMS_Watts              /
        /************************************/
        private int get_cu_AC_RMS_Watts(ref double voltage, ref string reportMsg)
        {
            byte[] cmdresponse = { 0, 0, 0, 0, 0, 0 };

            voltage = 0;
            reportMsg = string.Empty;

            try
            {
                Trace.WriteLine("read watts");
                send_CU_com_port(rd_RMS_Power, CMD_RESPONSE_TIMEOUT, CMD_RCV_DELAY, ref cmdresponse);
                voltage = ((cmdresponse[3] * 256) + cmdresponse[4]) / 10.0;
                return (int)ErrorCodes.Program.Success;
            }
            catch (Exception ex)    // there was an issue with the send or receive
            {
                reportMsg = "Error doing pm 'get_cu_AC_RMS_Power' functions. Error message: " + ex.Message;
                return (int)ErrorCodes.System.ACMeterReadError;
            }
        }

        /************************************/
        /*  get cu_AC_PowerFactor            /
        /************************************/
        private int get_cu_AC_PowerFactor(ref double voltage, ref string reportMsg)
        {
            byte[] cmdresponse = { 0, 0, 0, 0, 0, 0 };

            voltage = 0;
            reportMsg = string.Empty;

            try
            {
                Trace.WriteLine("read PF");
                send_CU_com_port(rd_PowerFactor, CMD_RESPONSE_TIMEOUT, CMD_RCV_DELAY, ref cmdresponse);
                voltage = ((cmdresponse[3] * 256) + cmdresponse[4]) / 1000.0;
                return (int)ErrorCodes.Program.Success;
            }
            catch (Exception ex)    // there was an issue with the send or receive
            {
                reportMsg = "Error doing pm 'get_PowerFactor' functions. Error message: " + ex.Message;
                return (int)ErrorCodes.System.ACMeterReadError;
            }
        }

        /************************************/
        /*  get cu_Temperature               /
        /************************************/
        private int get_cu_Temperature(ref double voltage, ref string reportMsg)
        {
            byte[] cmdresponse = { 0, 0, 0, 0, 0, 0 };

            voltage = 0;
            reportMsg = string.Empty;

            try
            {
                send_CU_com_port(rd_temperature, CMD_RESPONSE_TIMEOUT, CMD_RCV_DELAY, ref cmdresponse);
                voltage = ((cmdresponse[3] * 256) + cmdresponse[4]) / 10.0;
                return (int)ErrorCodes.Program.Success;
            }
            catch (Exception ex)    // there was an issue with the send or receive
            {
                reportMsg = "Error doing 'get_cu_Temperature' functions. Error message: " + ex.Message;
                return (int)ErrorCodes.Program.FailedToPerformCUFunction;
            }
        }


        /************************************/
        /*   Perform CU_S_RELAY_ON           /
        /************************************/

        private int perform_cu_s_relay_on(ref string reportMsg)
        {
            byte[] cmdresponse = { 0, 0, 0, 0, 0, 0 };

            try
            {
                send_CU_com_port(relay_on, CMD_RESPONSE_TIMEOUT, CMD_RCV_DELAY, ref cmdresponse);
                return (int)ErrorCodes.Program.Success;
            }
            catch (Exception ex)    // there was an issue with the send or receive
            {
                reportMsg = "Error doing 'perform_cu_s_relay_on' functions. Error message: " + ex.Message;
                return (int)ErrorCodes.Program.FailedToPerformCUFunction;
            }
        }


        /************************************/
        /*   Perform CU_S_RELAY_OFF          /
        /************************************/

        private int perform_cu_s_relay_off(ref string reportMsg)
        {
            byte[] cmdresponse = { 0, 0, 0, 0, 0, 0 };

            try
            {
                send_CU_com_port(relay_off, CMD_RESPONSE_TIMEOUT, CMD_RCV_DELAY, ref cmdresponse);
                return (int)ErrorCodes.Program.Success;
            }
            catch (Exception ex)    // there was an issue with the send or receive
            {
                reportMsg = "Error doing 'perform_cu_s_relay_off' functions. Error message: " + ex.Message;
                return (int)ErrorCodes.Program.FailedToPerformCUFunction;
            }
        }

        /************************************/
        /*   Perform wr_SU_reboot            /
        /************************************/

        private int perform_wr_SU_reboot(ref string reportMsg)
        {
            byte[] cmdresponse = { 0, 0, 0, 0, 0, 0 };

            try
            {
                send_CU_com_port(wr_SU_reboot, CMD_RESPONSE_TIMEOUT, CMD_RCV_DELAY, ref cmdresponse);
                return (int)ErrorCodes.Program.Success;
            }
            catch (Exception ex)    // there was an issue with the send or receive
            {
                reportMsg = "Error doing 'perform_wr_SU_reboot' functions. Error message: " + ex.Message;
                return (int)ErrorCodes.Program.FailedToPerformCUFunction;
            }
        }

        /************************************/
        /*   Perform CU_S_POWER_CYCLE_SU     /
        /************************************/

        private int perform_cu_s_power_cycle(ref string reportMsg)
        {
            byte[] cmdresponse = { 0, 0, 0, 0, 0, 0 };

            try
            {
                send_CU_com_port(wr_SU_reboot, CMD_RESPONSE_TIMEOUT, CMD_RCV_DELAY, ref cmdresponse);
                return (int)ErrorCodes.Program.Success;
            }
            catch (Exception ex)    // there was an issue with the send or receive
            {
                reportMsg = "Error doing 'perform_cu_s_power_cycle' functions. Error message: " + ex.Message;
                return (int)ErrorCodes.Program.FailedToPerformCUFunction;
            }
        }


        /*****************************************/
        /*   read_manufacturing_data_string     /
        /*****************************************/
        /// <summary>
        /// Will read the manufacting data from ram to the manufacturingDataBuffer.
        /// </summary>
        /// <returns>byte[NUMBER_OF_BYTES_IN_MANUFACTURING_BUFFER] - bytes read from cu ram.</returns>
        private byte[] read_manufacturing_data_string_from_ram()
        {
            byte[] cmdresponse = { 0, 0, 0, 0, 0, 0 };
            byte[] ManufacturingDataBuffer = new byte[NUMBER_OF_BYTES_IN_MANUFACTURING_BUFFER];


            int loop2;
            rd_mfg_data[2] = 0x80;  // starting address

            for (loop2 = 0; (loop2 < NUMBER_OF_BYTES_IN_MANUFACTURING_BUFFER); loop2++)
            {
                try
                {
                    //                    send_CU_com_port(rd_mfg_data, 1000, 60, ref cmdresponse);
                    send_CU_com_port(rd_mfg_data, CMD_RESPONSE_TIMEOUT, CMD_RCV_DELAY, ref cmdresponse);
                }
                catch (Exception ex)    // there was an issue with the send or receive
                {
                    throw new Exception(string.Format("Error doing 'read_manufacturing_data_string_from_ram' function, loop {0}. Error message: {1}",
                        loop2, ex.Message));
                }
                ManufacturingDataBuffer[loop2] = cmdresponse[4];
                rd_mfg_data[2] += 1;
            }

            return ManufacturingDataBuffer;
        }

        /// <summary>
        /// Will parse the data that is in the buffer to the strings. If a item is not found, the string
        /// is set to empty.
        /// The buffer contains data fields that have tags.
        ///     --field---              --tag--
        ///     PCBA Serial  number	    #
        ///     PCBA Part  number	    :
        ///     HLA Serial  number	    !
        ///     HLA Part  number	    “
        ///     HLA Model  number	    &
        /// Each field is terminated with a \n. The buffer is terminated with a null character.
        /// The first 8 characters in the buffer is the CU2 ID string.
        /// </summary>
        /// <param name="pcbaSN"></param>
        /// <param name="pcbaPN"></param>
        /// <param name="hlaSN"></param>
        /// <param name="hlaPN"></param>
        /// <param name="hlaModel"></param>
        /// <param name="ManufacturingDataBuffer"></param>
        private void parse_manufacturing_data_buffer(ref string pcbaSN, ref string pcbaPN, ref string hlaSN,
            ref string hlaPN, ref string hlaModel, ref string cu2ID, byte[] ManufacturingDataBuffer)
        {
            string ManufacturingDataString;
            string[] manValues;

            pcbaSN = string.Empty;
            pcbaPN = string.Empty;
            hlaSN = string.Empty;
            hlaPN = string.Empty;
            hlaModel = string.Empty;
            cu2ID = string.Empty;

            ManufacturingDataString = ASCIIEncoding.ASCII.GetString(ManufacturingDataBuffer);
            cu2ID = ManufacturingDataString.Substring(0, 8);
            manValues = ManufacturingDataString.Split('\n');
            foreach (var item in manValues)
            {
                switch (item.Substring(0, 1))
                {
                    case "#":
                        pcbaSN = item.Substring(1);
                        break;

                    case ":":
                        pcbaPN = item.Substring(1);
                        break;

                    case "!":
                        hlaSN = item.Substring(1);
                        break;

                    case "\"":
                        hlaPN = item.Substring(1);
                        break;

                    case "&":
                        hlaModel = item.Substring(1);
                        break;
                    case "\0":      // ignore 
                        break;

                }
            }
        }

        /// <summary>
        /// This will construct the manufacturing data buffer. It will use the passed buffer and delete, replace, or insert
        /// the new data. If a string is passed as a empty string, then that data is unchanged.
        /// The buffer contains data fields that have tags.
        ///     --field---              --tag--
        ///     PCBA Serial  number	    #
        ///     PCBA Part  number	    :
        ///     HLA Serial  number	    !
        ///     HLA Part  number	    “
        ///     HLA Model  number	    &
        /// Each field is terminated with a \n. The buffer is terminated with a null character.
        ///  If the string is null
        ///     nothing is done with the buffer
        ///  If the string has data
        ///     the string is looked for in the buffer
        ///         if nothing is found, the string is inserted
        ///         if something is found, the string is replaced
        /// </summary>
        /// <param name="pcbaSN"></param>
        /// <param name="pcbaPN"></param>
        /// <param name="hlaSN"></param>
        /// <param name="hlaPN"></param>
        /// <param name="hlaModel"></param>
        /// <param name="ManufacturingDataBuffer"></param>
        private void update_manufacturing_data_buffer(string pcbaSN, string pcbaPN, string hlaSN, string hlaPN, string hlaModel,
            ref byte[] ManufacturingDataBuffer)
        {
            int startindex = 0;
            string ManufacturingDataString;

            ManufacturingDataString = ASCIIEncoding.ASCII.GetString(ManufacturingDataBuffer);

            if (pcbaSN == null)     // do not update, get the current value
            {
                // look for PCBA Serial number
                startindex = ManufacturingDataString.IndexOf('#') + 1;
                if (startindex != 0)
                    pcbaSN = ManufacturingDataString.Substring(startindex, ManufacturingDataString.IndexOf('\n', startindex));
                else
                    pcbaSN = "";
            }

            if (pcbaPN == null)     // do not update, get the current value
            {
                // look for PCBA Part number
                startindex = ManufacturingDataString.IndexOf(':') + 1;
                if (startindex != 0)
                    pcbaPN = ManufacturingDataString.Substring(startindex, ManufacturingDataString.IndexOf('\n', startindex));
                else
                    pcbaPN = "";
            }

            if (hlaSN == null)     // do not update, get the current value
            {
                // look for HLA Serial number
                startindex = ManufacturingDataString.IndexOf('!') + 1;
                if (startindex != 0)
                    hlaSN = ManufacturingDataString.Substring(startindex, ManufacturingDataString.IndexOf('\n', startindex));
                else
                    hlaSN = "";
            }

            if (hlaPN == null)
            {
                // look for HLA Part number
                startindex = ManufacturingDataString.IndexOf('“') + 1;
                if (startindex != 0)
                    hlaPN = ManufacturingDataString.Substring(startindex, ManufacturingDataString.IndexOf('\n', startindex));
                else
                    hlaPN = "";
            }

            if (hlaModel == null)
            {
                // look for HLA Model number
                startindex = ManufacturingDataString.IndexOf('&') + 1;
                if (startindex != 0)
                    hlaModel = ManufacturingDataString.Substring(startindex, ManufacturingDataString.IndexOf('\n', startindex));
                else
                    hlaModel = "";
            }

            ManufacturingDataString = "";       // clear out the old data

            if (pcbaSN != "")
            {
                ManufacturingDataString = pcbaSN.Substring(7, 8) + "\n";
                ManufacturingDataString += "#" + pcbaSN + "\n";
            }
            if (pcbaPN != "")
                ManufacturingDataString += ":" + pcbaPN + "\n";
            if (hlaSN != "")
                ManufacturingDataString += "!" + hlaSN + "\n";
            if (hlaPN != "")
                ManufacturingDataString += "“" + hlaPN + "\n";
            if (hlaModel != "")
                ManufacturingDataString += "&" + hlaModel + "\n";

            ManufacturingDataBuffer = Encoding.ASCII.GetBytes(ManufacturingDataString);
            Array.Resize<byte>(ref ManufacturingDataBuffer, NUMBER_OF_BYTES_IN_MANUFACTURING_BUFFER);
            for (int i = ManufacturingDataString.Length; i < NUMBER_OF_BYTES_IN_MANUFACTURING_BUFFER; i++)
            {
                ManufacturingDataBuffer[i] = 0;
            }

        }

        /// <summary>
        /// Write the ManufacturingDataBuffer to ram. will only write up to the first null character.
        /// </summary>
        /// <param name="ManufacturingDataBuffer"></param>
        /// <param name="reportStatus"></param>
        /// <returns></returns>
        private int write_manufacturing_data_string_to_ram(byte[] ManufacturingDataBuffer, out string reportStatus)
        {
            int loop2;
            byte[] cmdresponse = { 0, 0, 0, 0, 0, 0 };

            wr_mfg_data[2] = 0x80;   // set the starting address in the ram flash buffer
            reportStatus = "";

            for (loop2 = 0; (ManufacturingDataBuffer[loop2] != 0); loop2++)
            {
                wr_mfg_data[4] = ManufacturingDataBuffer[loop2];  // get the data to write
                try
                {
                    send_CU_com_port(wr_mfg_data, CMD_RESPONSE_TIMEOUT, CMD_RCV_DELAY, ref cmdresponse);        // write the data to ram flash buffer
                }
                catch (Exception ex)    // there was an issue with the send or receive
                {
                    reportStatus = string.Format("Error doing 'write_manufacturing_data_string_to_ram' function, loop {0}. Error message: {1}", loop2, ex.Message);
                    return (int)ErrorCodes.Program.FailedToPerformCUFunction;
                }
                wr_mfg_data[2] += 1;// increment the address
            }
            // write out the null character to end the string
            wr_mfg_data[4] = 0;  // get the data to write
            try
            {
                send_CU_com_port(wr_mfg_data, CMD_RESPONSE_TIMEOUT, CMD_RCV_DELAY, ref cmdresponse);        // write the data to ram flash buffer
            }
            catch (Exception ex)    // there was an issue with the send or receive
            {
                reportStatus = string.Format("Error doing 'write_manufacturing_data_string_to_ram' function, loop {0}. Error message: {1}", loop2, ex.Message);
                return (int)ErrorCodes.Program.FailedToPerformCUFunction;
            }

            return (int)ErrorCodes.Program.Success;
        }

        /// <summary>
        /// Reads the calibration data from ram to the buffer array
        /// </summary>
        /// <param name="CalibrationDataBuffer"></param>
        /// <param name="reportStatus"></param>
        /// <returns></returns>
        private byte[] read_calibration_data_from_ram()
        {
            byte[] cmdresponse = { 0, 0, 0, 0, 0, 0 };
            byte[] calibrationDataBuffer = new byte[NUMBER_OF_BYTES_IN_CALIIBRATION_BUFFER];

            int loop2;
            rd_cal_data[2] = 0x80;  // starting address

            for (loop2 = 0; (loop2 < NUMBER_OF_BYTES_IN_CALIIBRATION_BUFFER); loop2++)
            {
                try
                {
                    send_CU_com_port(rd_cal_data, CMD_RESPONSE_TIMEOUT, CMD_RCV_DELAY, ref cmdresponse);
                    //                    send_CU_com_port(rd_cal_data, 1000, 60, ref cmdresponse);
                    calibrationDataBuffer[loop2] = cmdresponse[4];
                }
                catch (Exception ex)    // there was an issue with the send or receive
                {
                    throw new Exception(string.Format("Error doing 'read_calibration_data_from_ram' function, loop {0}. Error message: {1}", loop2, ex.Message));
                }
                rd_cal_data[2] += 1;
            }
            return calibrationDataBuffer;
        }

        /********************************************************/
        /*   write_calibration_data_to_ram     /
        /*******************************************************/

        private int write_calibration_data_to_ram(byte[] CalibrationDataBuffer, ref string reportMsg)
        {
            int loop2;
            byte[] cmdresponse = { 0, 0, 0, 0, 0, 0 };

            reportMsg = string.Empty;

            wr_cal_data[2] = 0x80;   // set the starting address in the ram flash buffer

            for (loop2 = 0; (loop2 < NUMBER_OF_BYTES_IN_CALIIBRATION_BUFFER); loop2++)
            {
                wr_cal_data[4] = CalibrationDataBuffer[loop2];  // get the data to write
                try
                {
                    send_CU_com_port(wr_cal_data, CMD_RESPONSE_TIMEOUT, CMD_RCV_DELAY, ref cmdresponse);
                    //                    send_CU_com_port(wr_cal_data, 1000, 50, ref cmdresponse);
                    wr_cal_data[2] += 1;// increment the address
                }
                catch (Exception ex)
                {
                    reportMsg = string.Format("Error doing 'write_calibration_data_to_ram' function, loop {0}. Error message: {1}",
                        loop2, ex.Message);
                    return (int)ErrorCodes.Program.FailedToPerformCUFunction;
                }
            }
            return (int)ErrorCodes.Program.Success;
        }

        /// <summary>
        /// Will parse out the buffer into the array.
        /// </summary>
        /// <param name="CalibrationDataBuffer"></param>
        /// <param name="calvalues"></param>
        /// <returns></returns>
        private void parse_calibration_data_buffer(byte[] calibrationDataBuffer, ref UInt32[] Calvalues)
        {
            int loop;

            for (loop = 0; loop < NUMBER_OF_CAL_VALUES; loop++)
            {
                Calvalues[loop] = (uint)(calibrationDataBuffer[loop * 4] * 0x1000000) +
                                (uint)(calibrationDataBuffer[(loop * 4) + 1] * 0x010000) +
                                (uint)(calibrationDataBuffer[(loop * 4) + 2] * 0x00100) +
                                (uint)(calibrationDataBuffer[(loop * 4) + 3]);
            }
        }

        /// <summary>
        /// Will update the CalibrationDataBuffer from the CalValue array.
        /// </summary>
        /// <param name="CalibrationDataBuffer">byte[NUMBER_OF_BYTES_IN_CALIIBRATION_BUFFER] - array to put data in.
        /// Unused bytes are filled with 0x00</param>
        /// <param name="calvalues">UInit32[NUMBER_OF_CAL_VALUES] - cal values to insert in buffer.</param>
        private void update_calibration_data_buffer(ref byte[] CalibrationDataBuffer, UInt32[] calvalues)
        {
            int offset = 0;

            int loop;
            for (loop = 0; loop < NUMBER_OF_BYTES_IN_CALIIBRATION_BUFFER; loop++)
                CalibrationDataBuffer[loop] = (byte)'\0';

            for (loop = 0; loop < NUMBER_OF_CAL_VALUES; loop++)
            {
                CalibrationDataBuffer[(loop * 4) + offset] = (byte)(calvalues[loop] >> 24);
                CalibrationDataBuffer[(loop * 4) + offset + 1] = (byte)((calvalues[loop] & 0xff000) >> 16);
                CalibrationDataBuffer[(loop * 4) + offset + 2] = (byte)((calvalues[loop] & 0xff00) >> 8);
                CalibrationDataBuffer[(loop * 4) + offset + 3] = (byte)(calvalues[loop] & 0xff);
            }

        }

        /************************************/
        /*        write_ram_to_flash        */
        /************************************/

        private int write_ram_to_flash(ref string reportMsg)
        {
            byte[] cmdresponse = { 0, 0, 0, 0, 0, 0 };
            reportMsg = string.Empty;

            try
            {
                send_CU_com_port(wr_mfg_flash_segment, CMD_RESPONSE_TIMEOUT, CMD_RCV_DELAY, ref cmdresponse);
            }
            catch (Exception ex)    // there was an issue with the send or receive
            {
                reportMsg = "Error doing 'write_ram_to_flash' function. Error message: " + ex.Message;
                return (int)ErrorCodes.Program.FailedToPerformCUFunction;
            }
            Thread.Sleep(5000);
            return (int)ErrorCodes.Program.Success;
        }

        /************************************/
        /*        read_flash_to_ram        */
        /************************************/

        private void read_flash_to_ram()
        {
            byte[] cmdresponse = { 0, 0, 0, 0, 0, 0 };

            try
            {
                send_CU_com_port(rd_mfg_flash_segment, CMD_RESPONSE_TIMEOUT, CMD_RCV_DELAY, ref cmdresponse);
            }
            catch (Exception ex)    // there was an issue with the send or receive
            {
                throw new Exception("Error doing 'read_flash_to_ram' function. Error message: " + ex.Message);
            }
        }

        /// <summary>
        /// Will write the temp cal value, perform the cal, and read back the cal value. 
        /// If the value read back does not match what was wrote, false is returned.
        /// </summary>
        /// <param name="calvalue"></param>
        /// <param name="returnvalue"></param>
        /// <returns></returns>
        private int performTempCal(Int32 calvalue, out Int32 returnvalue, ref string reportMsg)
        {
            byte[] cmdresponse = { 0, 0, 0, 0, 0, 0 };

            wr_tempcal_data[4] = (byte)calvalue;    // store the cal value
            wr_tempcal_data[3] = (byte)(calvalue / 256);
            returnvalue = 0;

            try
            {
                send_CU_com_port(wr_tempcal_data, CMD_RESPONSE_TIMEOUT, CMD_RCV_DELAY, ref cmdresponse);
            }
            catch (Exception ex)    // there was an issue with the send or receive
            {
                reportMsg = "Error doing 'wr_tempcal_data' function. Error message: " + ex.Message;
                return (int)ErrorCodes.Program.FailedToPerformCUFunction;
            }
            Thread.Sleep(100);

            // preform the calibration
            try
            {
                send_CU_com_port(wr_perform_tempcal, CMD_RESPONSE_TIMEOUT, CMD_RCV_DELAY, ref cmdresponse);
            }
            catch (Exception ex)    // there was an issue with the send or receive
            {
                reportMsg = "Error doing 'wr_perform_tempcal' function. Error message: " + ex.Message;
                return (int)ErrorCodes.Program.FailedToPerformCUFunction;
            }
            Thread.Sleep(1000);

            // read back the value
            try
            {
                send_CU_com_port(rd_tempcal_data, CMD_RESPONSE_TIMEOUT, CMD_RCV_DELAY, ref cmdresponse);
            }
            catch (Exception ex)    // there was an issue with the send or receive
            {
                reportMsg = "Error doing 'rd_cal_data' function. Error message: " + ex.Message;
                return (int)ErrorCodes.Program.FailedToPerformCUFunction;
            }

            returnvalue = (Int32)(cmdresponse[4] + (cmdresponse[3] * 256));
            if (returnvalue != calvalue)
                return (int)ErrorCodes.Program.ValueOutsideOfLimit;
            else
                return (int)ErrorCodes.Program.Success;
        }


        //------------------------------------------
        //      utils
        //-----------------------------------------
        private void delay(int ms)
        {
            DateTime endTime = new DateTime();
            endTime = DateTime.Now.AddMilliseconds(ms);

            while (DateTime.Now < endTime)
            {

            }
        }
    }

}