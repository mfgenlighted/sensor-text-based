﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SensorManufSY2
{
    public partial class Banner : Form
    {
        FixtureFunctions lclFixture;

        public Banner(FixtureFunctions fixture)
        {
            InitializeComponent();
            lclFixture = fixture;
            timer1.Enabled = true;
        }

        public string SN
        {
            set { sn.Text = value.ToString(); }
        }

        public string MAC
        {
            set { mac.Text = value.ToString(); }
        }

        public string PFText
        {
            set { pfText.Text = value.ToString(); }
        }

        public string LastFailCode
        {
            set { textLastFailCode.Text = value.ToString(); }
        }

        public string FirstFailCode
        {
            set { textFirstFailCode.Text = value.ToString(); }
        }

        private void checkBoxButton(object sender, EventArgs e)
        {
            if (lclFixture.IsBoxButtonPushed())
            {
                DialogResult = System.Windows.Forms.DialogResult.OK;
                timer1.Enabled = false;
                return;
            }
        }

    }
}
