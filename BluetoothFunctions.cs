﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.IO.Ports;
using System.Threading;
using System.Diagnostics;
using System.Text.RegularExpressions;

using LimitsDatabase;
using FixtureDatabase;

namespace SensorManufSY2
{

    class BluetoothFunctions
    {
        public List<string> rntDevices = new List<string>();
        public List<string> rntDevicesOldFormat = new List<string>();
        public List<int> rntRSSI = new List<int>();
        public List<DateTime> timeStamps = new List<DateTime>();
         
        public string FailMessage = string.Empty;
        public int FailErrorCode = (int)ErrorCodes.Program.Success;
        public string BluetoothPort;
        public string BLETagPort;
        public SensorConsole consoleSerialPort;

        private SerialPort serialPort;
        private List<byte[]> rcvdEvents = new List<byte[]>();
        private enum RcvStates { startbyte, typebyte, payloadbyte, payload };
        private RcvStates rcvState = RcvStates.startbyte;
        public string tagMac = string.Empty;
        public string tagVer = string.Empty;

        private byte[] inputBuffer = new byte[1024];
        private int inputBufferIndex = 0;
        private int payloadLength = 0;
        private int packetCount = 0;
        private object fileLock = new object();
        private object fileLock2 = new object();

        //------------------------------------
        //  bluetooth commands
        //------------------------------------
        private const string cmd_loadBLEFromSPI = "p ble-spi 1";             // loads the BLE chip with the image from SPI
        private const string cmd_startBLEScan = "p ble-mode 1";              // start the BLE scan
        private const string cmd_readBLEScanData = "d ble-scan";            // get the result of the scan
        private const string cmd_startBLEAdvert = "p ble-mode 2";           // starts the BLE advertise. 2.9.9 on
        private const string cmd_bleNoCodeLoad = "p ble-spi 0";             // will make the ble not load code from spi. 3.0.5 on
        private const string cmd_bleReboot = "p reboot ble";                // reboot the ble
        private const string cmd_dutReboot = "p reboot";                    // reboot the dut
        private const string cmd_getVer = "d ver";                          // get firmware version

        private const int bleRebootDelaySec = 5;                               // default delay after the p reboot-spi command

        public BluetoothFunctions()
        {
            BluetoothPort = string.Empty;
            BLETagPort = string.Empty;
        }

        public BluetoothFunctions(string blePort, string bleTagPort, SensorConsole comPort)
        {
            BluetoothPort = blePort;
            BLETagPort = bleTagPort;
            consoleSerialPort = comPort;
        }

        /// <summary>
        /// Initialize the bluetooth dongle.
        /// </summary>
        /// <remarks>
        /// Will do the following steps:
        ///     Send Reset
        ///     Wait for Reset completed response
        ///     Send GAP Init
        ///     Wait for Process command complete    
        ///     Wait for GAP Init done
        /// 
        /// 
        ///     Send Extension Overlapped Processing
        ///     Wait for OverLapped Processing complete response
        ///     Send GAP Set Parameter - TGAP_CONN_EST_INT_MIN
        ///     Wait for Process command complete
        ///     Send GAP set Parameter - TGAP_CONN_EST_INT_MAX
        ///     Wait for Process command complete
        ///     Send GAP set Parameter - TGAP_CONN_EST_SUPERV_TIMEOUT
        ///     Wait for Process command complete
        ///     Send GAP Set Parameter - TGAP_CONN_EST_LATENCY     
        ///     Wait for Process command complete
        /// </remarks>
        /// <param name="message"></param>
        /// <returns></returns>
        public bool dongleInit(ref string message)
        {
            bool status = true;
            string msg = string.Empty;

            byte[] readData = new byte[1024];
            byte[] resetCmd = {0x01, 0x1D, 0xFC, 0x01, 0x01};
            byte[] resetCmdDone = {0x04, 0xFF, 0x05, 0x1D, 0x04, 0x00, 0x1D, 0xFC};
            byte[] gapDeviceInitcmd = {0x01, 0x00, 0xFE, 0x26,                                  // GAP_DeviceInit
	                                0x08,			                                            // profileRole: GAP_PROFILE_CENTRAL
	                                0x05,			                                            // maxScanResponses 
	                                0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	                                0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	                        //IRK
	                                0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	                                0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	                        // CSRK
	                                0x01, 0x00, 0x00, 0x00 };                                   // signCounter
            byte[] gapDeviceInitDoneHd = { 0x04, 0xFF, 0x2C, 0x00, 0x06 };       // first part of init response. next byte should be status = 0x00
            byte[] extOverlappedProcessingCmd =
                                    {0x01, 0x1E, 0xFC, 0x01, 0x01};         // EXtension Overlapped Processing = HCI_EXT_ENABLE_OVERLAPPED_PROCESSING
            byte[] extOverlappedCmdComplete = {0x04, 0xFF, 0x05, 0x1E, 0x04, 0x00, 0x1E, 0xFC}; // extension overoverlapped response
            byte[] gapSetParameter1cmd =
                                    {0x01, 0x30, 0xFE, 0x03, 0x15, 0x06, 0x00};     // GAP Set Parameter - TGAP_CONN_EST_INT_MIN
            byte[] cmdProcessed = { 0x04, 0xFF, 0x06, 0x7F, 0x06, 0x00, 0x30, 0xFE, 0x00 };     // command processed
            byte[] gapSetParameter2cmd =
                        {0x01, 0x30, 0xFE, 0x03, 0x16, 0x06, 0x00};     // GAP Set Parameter - TGAP_CONN_EST_INT_MAX
            byte[] gapSetParameter3cmd =
                        {0x01, 0x30, 0xFE, 0x03, 0x19, 0x32, 0x00};     // GAP Set Parameter - TGAP_CONN_EST_SUPERV_TIMEOUT
            byte[] gapSetParameter4cmd =
                        {0x01, 0x30, 0xFE, 0x03, 0x1A, 0x00, 0x00};     // GAP Set Parameter - TGAP_CONN_EST_LATENCY

            message = string.Empty;
            try
            {

                //sendDongleCmd(resetCmd, 0, resetCmd.Length);
                //if ((status = readDongleEvent(1000, ref readData)))     // if found a packet
                //    if (!responseCheck(resetCmdDone, readData))         // see if it was was expected
                //        status = false;
 
                // send Gap Devide Init commmand 
                sendDongleCmd(gapDeviceInitcmd, 0, gapDeviceInitcmd.Length);
                status = readDongleEvent(5000, ref readData);
                status = readDongleEvent(5000, ref readData);

                //// send Overlapped command
                //sendDongleCmd(extOverlappedProcessingCmd, 0, extOverlappedProcessingCmd.Length);
                //status = readDongleEvent(1000, ref readData);

                //// send gap parameters
                //sendDongleCmd(gapSetParameter1cmd, 0, gapSetParameter1cmd.Length);
                //status = readDongleEvent(1000, ref readData);
                //sendDongleCmd(gapSetParameter2cmd, 0, gapSetParameter2cmd.Length);
                //status = readDongleEvent(1000, ref readData);
                //sendDongleCmd(gapSetParameter3cmd, 0, gapSetParameter3cmd.Length);
                //status = readDongleEvent(1000, ref readData);
                //sendDongleCmd(gapSetParameter4cmd, 0, gapSetParameter4cmd.Length);
                //status = readDongleEvent(1000, ref readData);

            }
            catch (Exception ex)
            {
                message = "Error setting up BLE: " + ex.Message;
                status = false;
            }
            Thread.Sleep(1000);
            return status;
        }

        /// <summary>
        /// Checks the response.
        /// </summary>
        /// <param name="goodResponse"></param>
        /// <param name="readData"></param>
        /// <returns></returns>
        private bool responseCheck(byte[] goodResponse, byte[] readData)
        {
            int i;
            for (i = 0; i < goodResponse.Length; i++)
            {
                if (goodResponse[i] != readData[i])
                    return false;
            }
            return true;
        }

        /// <summary>
        /// Will look for bluetooth devices using the TI dongle. Displays the results.
        /// </summary>
        /// <param name="timeoutSec"></param>
        /// <returns></returns>
        public bool FindBluetoothDevices(int timeoutSec)
        {
            string outmsg = string.Empty;

            UpdateStatusWindow("Opening port");
            if (OpenBLEMonitor(ref outmsg) != (int)ErrorCodes.Program.Success)
            {
                UpdateStatusWindow("Error opening the BLE Monitor port\n");
                return false;
            }
            //dongleInit(ref outmsg);

            UpdateStatusWindow("start TI dongle scan");
            if (!scan(timeoutSec, string.Empty))
            {
                UpdateStatusWindow("Error scaning: " + FailMessage + "\n");
                CloseBLEMonitor();
                return false;
            }
            else
            {
                UpdateStatusWindow("Number of units found = " + rntDevices.Count + "\n");
                if (rntDevices.Count > 0)
                {
                    int i = 0;
                    foreach (var item in rntDevices)
                    {
                        UpdateStatusWindow("Device = " + item);
                        UpdateStatusWindow("   RSSI: " + rntRSSI[i++] + "\n");
                    }
                }
            }
            CloseBLEMonitor();
            return true;  // get everybody
        }

        /// <summary>
        /// Will use the DUT to find bluetooth devices advertising. The lists rntDevices and rntRSSI
        /// will have the result data.
        /// </summary>
        /// <remarks>
        /// The return data will look like:
        /// d ble-scan
        ///     Scan result cnt = 4
        ///     debug scan result sequence number = 1
        ///     debug scan stats: advert=107, end=30
        ///      0: hb 0 mo 1  68 54 F5 F8 F9 18   15 [   0   0   0 ]
        ///      1: hb 1 mo 0  68 54 F5 F8 F9 30   32 [  -6   0  72 ]
        ///      2: hb 1 mo 0  68 54 F5 F8 F8 9D   33 [   6  -2 -59 ]
        ///      3: hb 1 mo 0  68 54 F5 F8 F9 15   33 [  55  20 -14 ]
        ///     DVTMAN> 
        /// </remarks>
        /// <param name="timeoutSec"></param>
        /// <returns></returns>
        public bool DutFindBluetoothDevices(int timeoutSec, int resetDelaySec)
        {
            bool status = true;

            UpdateStatusWindow("start DUT scan");
            if (!DutScan(timeoutSec, resetDelaySec, string.Empty))
            {
                UpdateStatusWindow("Error scaning: " + FailMessage + "\n");
                return false;
            }
            else
            {
                UpdateStatusWindow("Number of units found = " + rntDevices.Count + "\n");
                if (rntDevices.Count > 0)
                {
                    int i = 0;
                    foreach (var item in rntDevices)
                    {
                        UpdateStatusWindow("Device = " + item);
                        UpdateStatusWindow("   RSSI: " + rntRSSI[i++] + "\n");
                    }
                }
            }
            return status;
        }

        //--------------------------
        //      Methods - Tests

        /// <summary>
        /// Will issue the Bluetooth change over command
        /// </summary>
        /// <param name="testParams">test parameters</param>
        /// <param name="testDB">result database object</param>
        /// <param name="outmsg">error results of test</param>
        /// <returns>Error codes</returns>
        public int BLETest_DoLoad(LimitsData.stepsItem testParams, ResultData testDB, ref string outmsg)
        {
            int status = (int)ErrorCodes.Program.Success;
            
            outmsg = string.Empty;
            testDB.CreateStepRecord(testParams.name, testParams.step_number);

            if (DoBLEDoLoad2())
            {
                testDB.AddStepResult("PASS", "", (int)ErrorCodes.Program.Success);
            }
            else
            {
                if (FailErrorCode < (int)ErrorCodes.System.SystemErrorCodes)
                    FailErrorCode = FailErrorCode + (testParams.step_number * 100);
                testDB.AddStepResult("FAIL", FailMessage, FailErrorCode);
            }
            return status;
        }

        /// <summary>
        /// Look for the uut advertize mac address. ver=1 -> pre 2.9.9 ver=2 -> 2.9.9 and on
        /// </summary>
        /// <param name="testParams"></param>
        /// <param name="testDB"></param>
        /// <param name="uutData"></param>
        /// <param name="outmsg"></param>
        /// <returns></returns>
        public int BLETest_LookForAdvert(int ver, LimitsData.stepsItem testParams, ResultData testDB, MainWindow.uutdata uutData, ref string outmsg)
        {
            int status = (int)ErrorCodes.Program.Success;
            bool scanStatus;
            bool startStatus;
            int i = 0;
            int resetDelay = bleRebootDelaySec;             // delay after the p reset-ble before looking. If set to 0, no BLE reset is done.
            int retrys = 0;
            int timeoutSec = 10;
            int maxretrys = 5;
            int foundRSSI = 0;

            // get the test constants
            foreach (var item in testParams.parameters)
            {
                if (item.name.ToUpper() == "TIMEOUTSEC")
                {
                    timeoutSec = Convert.ToInt32(item.value);
                }
                if (item.name.ToUpper() == "RETRYS")
                {
                    maxretrys = Convert.ToInt32(item.value);
                }
                if (item.name.ToUpper() == "RESETDELAYSEC")
                {
                    resetDelay = Convert.ToInt32(item.value);
                }
            }

            if ((FailErrorCode = OpenBLEMonitor(ref FailMessage)) != (int)ErrorCodes.Program.Success)
                return FailErrorCode;

            // serial port is good and open. Setup the result database for the test
            testDB.CreateStepRecord(testParams.name, testParams.step_number);

            if (ver == 1)       // pre 2.9.9, need to change over to start
                startStatus = DoBLEDoLoad();         // load and assumes advert start automatic
            else
            {
                startStatus = true;             // assume commands get sent correctly

                // if the ResetDelaySec constant is set to 0, then do not reset the BLE
                if (resetDelay == 0)
                {
                    // Reset the BLE. It has issues going between rcv and tx tests
                    // make sure that the reload flag is NOT set
                    if (!consoleSerialPort.WriteLine(cmd_bleNoCodeLoad, 2000))
                    {

                        FailMessage = "Error sending '" + cmd_bleNoCodeLoad + "' command to DUT.";
                        FailErrorCode = (int)ErrorCodes.Program.DUTComError;
                        startStatus = false;
                    }
                    // wait for the prompt from the command
                    if (startStatus)
                    {
                        if (!consoleSerialPort.WaitForDVTMANPrompt(10000, out outmsg))
                        {
                            FailMessage = "Error waiting for prompt after '" + cmd_bleNoCodeLoad + "' command to DUT.";
                            FailErrorCode = (int)ErrorCodes.Program.DUTComError;
                            startStatus = false;
                        }
                    }
                    if (startStatus)
                    {
                        if (!consoleSerialPort.WriteLine(cmd_bleReboot, 2000))
                        {
                            FailMessage = "Error sending '" + cmd_bleReboot + "' command to DUT.";
                            FailErrorCode = (int)ErrorCodes.Program.DUTComError;
                            startStatus = false;
                        }
                    }
                    // wait for the prompt from the command
                    if (startStatus)
                    {
                        if (!consoleSerialPort.WaitForDVTMANPrompt(10000, out outmsg))
                        {
                            FailMessage = "Error waiting for prompt after '" + cmd_bleReboot + "' command to DUT.";
                            FailErrorCode = (int)ErrorCodes.Program.DUTComError;
                            startStatus = false;
                        }
                    }

                    if (startStatus)
                    {
                        Thread.Sleep(resetDelay * 1000);         // give the BLE time to reboot.
                    }
                }

                if (startStatus)
                {
                    // start the advert
                    if (!consoleSerialPort.WriteLine(cmd_startBLEAdvert, 2000))
                    {
                        FailMessage = "Error sending '" + cmd_startBLEAdvert + "' command.";
                        FailErrorCode = (int)ErrorCodes.Program.DutUnexpectedCmdReturn;
                        startStatus = false;
                    }
                }

                if (startStatus)
                {
                    // wait for prompt from command
                    if (!consoleSerialPort.WaitForDVTMANPrompt(10000, out outmsg))
                    {
                        FailMessage = "Error waiting for the  '" + cmd_startBLEAdvert + "' reponse prompt.";
                        FailErrorCode = (int)ErrorCodes.Program.DutUnexpectedCmdReturn;
                        startStatus = false;
                    }
                }
            }

            if (!startStatus)         // if there is a error doing the start
            {
                if (FailErrorCode < (int)ErrorCodes.System.SystemErrorCodes)
                    FailErrorCode = FailErrorCode + (testParams.step_number * 100);
                testDB.AddStepResult("FAIL", FailMessage, FailErrorCode);
                CloseBLEMonitor();
                return FailErrorCode;
            }

            // start was issued so the bluetooth should be advertising now.
            do
            {
                UpdateStatusWindow("Start scan\n");
                try         // this is added to try to find out why this test kicks out sometimes
                {
                    scanStatus = scan(timeoutSec, uutData.mac.Substring(4));
                }
                catch (Exception ex)
                {
                    FailErrorCode = (int)ErrorCodes.System.FixtureError;
                    FailMessage = "Error in bluetooth scan. " + ex.ToString();
                    scanStatus = false;
                }
                if (scanStatus)             // if items found
                {
                    status = (int)ErrorCodes.Program.ValueOutsideOfLimit;       // assume not found
                    outmsg = "Device mac " + uutData.mac + " not found.";
                    i = 0;
                    foundRSSI = 0;
                    foreach (var item in rntDevices)
                    {
                        // format of the item is <lenght><string>, but do not know what the 1st character of the string is, so I ignor it.
                        UpdateStatusWindow((i + 1) + ":read ->" + item + "\tRSSI -> " + rntRSSI[i] + "\n");
                        if (item.ToUpper().Contains(uutData.mac.ToUpper().Substring(4))) // if the uut mac found
                        {
                            UpdateStatusWindow("    Found unit\n");
                            status = (int)ErrorCodes.Program.Success;
                            outmsg = string.Empty;
                            foundRSSI = rntRSSI[i];
                            break;
                        }
                        i++;
                    }
                    if (status != (int)ErrorCodes.Program.Success)
                    {
                        //status = (int)ErrorCodes.Program.ValueOutsideOfLimit;       // assume not found
                        //outmsg = "Device mac " + uutData.mac + " not found.";
                        //i = 0;
                        //foundRSSI = 0;
                        //UpdateStatusWindow("Unit not found in scanned list.\nLook for old format\n");
                        //foreach (var item in rntDevicesOldFormat)
                        //{
                        //    // format of the item is <lenght><string>, but do not know what the 1st character of the string is, so I ignor it.
                        //    UpdateStatusWindow((i + 1) + ":read ->" + item + "\tRSSI -> " + rntRSSI[i] + "\n");
                        //    if (item.Contains(uutData.mac.Substring(4))) // if the uut mac found
                        //    {
                        //        UpdateStatusWindow("    Found unit\n");
                        //        status = (int)ErrorCodes.Program.Success;
                        //        outmsg = string.Empty;
                        //        foundRSSI = rntRSSI[i];
                        //        break;
                        //    }
                        //    i++;
                        //}
                        //if (status != (int)ErrorCodes.Program.Success)      // if not even in old format
                        //{
                            UpdateStatusWindow("Unit not found in scanned list.\n");

                            status = (int)ErrorCodes.Program.AdvertNotFound;
                            retrys++;
                            if (retrys < maxretrys)
                                UpdateStatusWindow("Retry " + retrys + "\n");
                        //}
                    }
                }
                else        // else nothing was found
                {
                    UpdateStatusWindow("Scan fail: " + FailMessage + "\n");
                    outmsg = FailMessage;
                    status = FailErrorCode;
                    retrys++;
                    if (retrys < maxretrys)
                        UpdateStatusWindow("Retry " + retrys + "\n");
                }
                
            } while ( (status != (int)ErrorCodes.Program.Success) & (retrys < maxretrys));
            testDB.AddStepRef("BLETest_LookForAdvert_tries", retrys.ToString(), "", "", "");

            if (status == (int)ErrorCodes.Program.Success)      // the right unit was found
            {
                if (MainWindow.CheckResultValue(foundRSSI, testParams.limits[0], ref outmsg))
                {
                    status = (int)ErrorCodes.Program.Success;           // so set as pass
                    outmsg = string.Empty;
                }
                else            // outside of the RSSI reading
                {
                    status = (int)ErrorCodes.Program.ValueOutsideOfLimit + (testParams.step_number * 100);
                    outmsg = "RSSI(" + foundRSSI + ") outside of limits.";
                }
                testDB.AddStepResult("RSSI", (status == (int)ErrorCodes.Program.Success) ? "PASS" : "FAIL", testParams.limits[0].operation,
                Convert.ToDouble(testParams.limits[0].value1), Convert.ToDouble(testParams.limits[0].value2), foundRSSI, "RSSI", "N0", outmsg, status);
            }
            else
            {
                if (status < (int)ErrorCodes.System.SystemErrorCodes)
                    status = FailErrorCode + (testParams.step_number * 100);
                testDB.AddStepResult("FAIL", outmsg, status);
            }

            CloseBLEMonitor();

            return status;
        }

        /// <summary>
        /// Look for the devices adverts
        /// </summary>
        /// <param name="testParams"></param>
        /// <param name="testDB"></param>
        /// <param name="uutData"></param>
        /// <param name="outmsg"></param>
        /// <returns></returns>
        public int BLETest_DutLookForAdvert(LimitsData.stepsItem testParams, ResultData testDB, ref string outmsg)
        {
            int status = (int)ErrorCodes.Program.Success;
            bool scanStatus;
            int i = 0;
            int resetDelaySec = bleRebootDelaySec;         // time delay p reset-ble after
            int retrys = 0;
            int timeoutSec = 2;                             // time between starting the scan and asking for results
            int maxretrys = 2;
            int foundRSSI = 0;

            // get the test constants
            foreach (var item in testParams.parameters)
            {
                if (item.name.ToUpper() == "TIMEOUTSEC")
                {
                    timeoutSec = Convert.ToInt32(item.value);
                }
                if (item.name.ToUpper() == "RETRYS")
                {
                    maxretrys = Convert.ToInt32(item.value);
                }
                if (item.name.ToUpper() == "RESETDELAYSEC")
                {
                    resetDelaySec = Convert.ToInt32(item.value);
                }
            }


            // serial port is good and open. Setup the result database for the test
            testDB.CreateStepRecord(testParams.name, testParams.step_number);


            // device should be advertising now.
            UpdateStatusWindow("Tag mac = " + tagMac + "\n");
            do
            {
                UpdateStatusWindow("Start scan\n");
                try         // this is added to try to find out why this test kicks out sometimes
                {
                    scanStatus = DutScan(timeoutSec, resetDelaySec, tagMac);
                }
                catch (Exception ex)
                {
                    FailErrorCode = (int)ErrorCodes.System.FixtureError;
                    FailMessage = "Error in bluetooth scan. " + ex.ToString();
                    scanStatus = false;
                }
                if (scanStatus)             // if items found
                {
                    status = (int)ErrorCodes.Program.ValueOutsideOfLimit;       // assume not found
                    outmsg = "Device mac " + tagMac + " not found.";
                    i = 0;
                    foundRSSI = 0;
                    foreach (var item in rntDevices)
                    {
                        // format of the item is <lenght><string>, but do not know what the 1st character of the string is, so I ignor it.
                        UpdateStatusWindow((i + 1) + ":read ->" + item + "\tRSSI -> " + rntRSSI[i] + "\n");
                        if (item.ToUpper().Contains(tagMac)) // if the uut mac found
                        {
                            UpdateStatusWindow("    Found unit\n");
                            status = (int)ErrorCodes.Program.Success;
                            outmsg = string.Empty;
                            foundRSSI = rntRSSI[i];
                            break;
                        }
                        i++;
                    }
                    if (status != (int)ErrorCodes.Program.Success)
                    {
                        UpdateStatusWindow("Unit not found in scanned list.\n");

                            status = (int)ErrorCodes.Program.AdvertNotFound;
                            retrys++;
                            if (retrys < maxretrys)
                                UpdateStatusWindow("Retry " + retrys + "\n");
                    }
                }
                else        // else nothing was found
                {
                    UpdateStatusWindow("Scan fail: " + FailMessage + "\n");
                    outmsg = FailMessage;
                    status = FailErrorCode;
                    retrys++;
                    if (retrys < maxretrys)
                        UpdateStatusWindow("Retry " + retrys + "\n");
                }

            } while ((status != (int)ErrorCodes.Program.Success) & (retrys < maxretrys));
            testDB.AddStepRef("BLETest_DutLookForAdvert_tries", retrys.ToString(), "", "", "");

            if (status == (int)ErrorCodes.Program.Success)      // the right unit was found
            {
                if (MainWindow.CheckResultValue(foundRSSI, testParams.limits[0], ref outmsg))
                {
                    status = (int)ErrorCodes.Program.Success;           // so set as pass
                    outmsg = string.Empty;
                }
                else            // outside of the RSSI reading
                {
                    status = (int)ErrorCodes.Program.ValueOutsideOfLimit + (testParams.step_number * 100);
                    outmsg = "RSSI(" + foundRSSI + ") outside of limits.";
                }
                testDB.AddStepResult("RSSI", (status == (int)ErrorCodes.Program.Success) ? "PASS" : "FAIL", testParams.limits[0].operation,
                Convert.ToDouble(testParams.limits[0].value1), Convert.ToDouble(testParams.limits[0].value2), foundRSSI, "RSSI", "N0", outmsg, status);
            }
            else
            {
                if (status < (int)ErrorCodes.System.SystemErrorCodes)
                    status = FailErrorCode + (testParams.step_number * 100);
                testDB.AddStepResult("FAIL", outmsg, status);
            }


            return status;
        }

        /// <summary>
        /// Will initialize the tag radio
        /// </summary>
        /// <returns></returns>
        public int DoInitTag()
        {
            string msg = string.Empty;
            SerialPort serialPort = null;
            try
            {
                // set up the TAG to transmit
                // Open the bluetooth dongle
                if (BLETagPort == string.Empty)      // error if not configured
                {
                    FailMessage = "System Error: BLE Tag com port has not been set up.";
                    FailErrorCode = (int)ErrorCodes.System.InstrumentNotSetUp;
                    return FailErrorCode;
                }
                if (checkForSerialPort(BLETagPort))  // if the port is a valid port on the system
                {
                    // port valid on this system, so try to open
                    try
                    {
                        serialPort = new SerialPort(BLETagPort, 115200);
                        serialPort.ReadTimeout = 2000;
                        serialPort.WriteTimeout = 2000;
                        serialPort.DtrEnable = false;
                        serialPort.Handshake = Handshake.None;
                        serialPort.Parity = Parity.None;
                        serialPort.Open();           // open the serial port
                    }
                    catch (Exception ex)
                    {
                        FailMessage = string.Format("Error opening {0}. {1}", BLETagPort, ex.Message);
                        FailErrorCode = (int)ErrorCodes.System.ComponetInitFail;
                        return FailErrorCode;
                    }
                }
                else                // serial port not a valid port on the system
                {
                    FailMessage = "Tag serial port " + BLETagPort + " is not a valid com port on this computer";
                    FailErrorCode = (int)ErrorCodes.System.ComponetInitFail;
                    return FailErrorCode;
                }

                Thread.Sleep(100);

                serialPort.DiscardInBuffer();

                // not doing the reboot. operates only in the mode 5
                ////serialPort.WriteLine("p reboot ble");
                //foreach (var item in "p reboot ble\n")        // slow down the writes
                //{
                //    serialPort.Write(item.ToString());
                //    Thread.Sleep(1);
                //}
                //msg = serialPort.ReadLine();                  // should be the echo
                //msg = serialPort.ReadTo("SU>");                  // should be prompt
                //Thread.Sleep(5000);                             // time for the ble to reboot
                //serialPort.WriteLine("d mac");          // get the mac of the tag
                foreach (var item in "d mac\n")        // slow down the writes
                {
                    serialPort.Write(item.ToString());
                    Thread.Sleep(1);
                }
                msg = serialPort.ReadLine();                  // should be the echo
                tagMac = serialPort.ReadLine();                  // should be the mac
                msg = serialPort.ReadTo("SU>");                  // should be prompt
                //serialPort.WriteLine("p ble-mode 5");   // turn on tag advertise
                foreach (var item in "p ble-mode 5\n")        // slow down the writes
                {
                    serialPort.Write(item.ToString());
                    Thread.Sleep(1);
                }
                msg = serialPort.ReadLine();                    // should be the echo
                msg = serialPort.ReadTo("SU>");                 // should be prompt
                // process the MAC from 00:00:xx:xx:xx:xx:xx:xx to xxxxxxxxxxxx
                tagMac = tagMac.Replace("00:00:", "");
                tagMac = tagMac.Replace(":", "");
                tagMac = tagMac.Substring(4, 12);               // make sure only the mac. get rid of MAC= and cr
                tagMac = tagMac.ToUpper();                      // and all upper case

                serialPort.WriteLine(cmd_getVer);
                msg = serialPort.ReadLine();                    // should be the echo
                tagVer = serialPort.ReadTo("SU>");                 // should be prompt

                serialPort.Close();                             // do not need it open any more
            }
            catch (Exception ex)
            {
                FailErrorCode = (int)ErrorCodes.System.FixtureError;
                FailMessage = "Error setting up TAG. " + ex.ToString();
            }

            return FailErrorCode;
        }

        //----------------------
        //      Methods - functions

        /// <summary>
        /// Will open the ble monitor device
        /// </summary>
        /// <param name="outmsg"></param>
        /// <returns></returns>
        public int OpenBLEMonitor(ref string outmsg)
        {
            string message = string.Empty;

                        // Open the bluetooth dongle
            if (BluetoothPort == string.Empty)      // error if not configured
            {
                outmsg = "System Error: Bluetooth com port has not been set up.";
                return (int)ErrorCodes.System.InstrumentNotSetUp;
            }
            if (checkForSerialPort(BluetoothPort))  // if the port is a valid port on the system
            {
                // port valid on this system, so try to open
                try
                {
                    serialPort = new SerialPort(BluetoothPort, 115200);
                    serialPort.ReadTimeout = 2000;
                    serialPort.WriteTimeout = 2000;
                    serialPort.DtrEnable = false;
                    serialPort.Handshake = Handshake.None;
                    serialPort.Parity = Parity.None;
                    serialPort.DataReceived += new SerialDataReceivedEventHandler(serialDataReceived);
                    serialPort.Open();           // open the serial port
                }
                catch (Exception ex)
                {
                    outmsg = string.Format("Error opening {0}. {1}", BluetoothPort, ex.Message);
                    return (int)ErrorCodes.System.ComponetInitFail;
                }
            }
            else                // serial port not a valid port on the system
            {
                outmsg = "Dut serial port " + BluetoothPort + " is not a valid com port on this computer";
                return (int)ErrorCodes.System.ComponetInitFail;
            }

            Thread.Sleep(100);
            //dongleInit(ref message);
            return (int)ErrorCodes.Program.Success;
        }

        /// Will open the ble tag
        /// </summary>
        /// <param name="outmsg"></param>
        /// <returns></returns>
        //public int OpenBLETag(ref string outmsg, SerialPort tagSerialPort)
        //{
        //    string message = string.Empty;

        //    // Open the bluetooth dongle
        //    if (BLETagPort == string.Empty)      // error if not configured
        //    {
        //        outmsg = "System Error: BLE Tag com port has not been set up.";
        //        return (int)ErrorCodes.System.InstrumentNotSetUp;
        //    }
        //    if (checkForSerialPort(BLETagPort))  // if the port is a valid port on the system
        //    {
        //        // port valid on this system, so try to open
        //        try
        //        {
        //            tagSerialPort = new SerialPort(BLETagPort, 115200);
        //            tagSerialPort.ReadTimeout = 2000;
        //            tagSerialPort.WriteTimeout = 2000;
        //            tagSerialPort.DtrEnable = false;
        //            tagSerialPort.Handshake = Handshake.None;
        //            tagSerialPort.Parity = Parity.None;
        //            tagSerialPort.Open();           // open the serial port
        //        }
        //        catch (Exception ex)
        //        {
        //            outmsg = string.Format("Error opening {0}. {1}", BLETagPort, ex.Message);
        //            return (int)ErrorCodes.System.ComponetInitFail;
        //        }
        //    }
        //    else                // serial port not a valid port on the system
        //    {
        //        outmsg = "Dut serial port " + BLETagPort + " is not a valid com port on this computer";
        //        return (int)ErrorCodes.System.ComponetInitFail;
        //    }

        //    Thread.Sleep(100);
        //    //dongleInit(ref message);
        //    Debug.WriteLine("ble dongle port open");
        //    return (int)ErrorCodes.Program.Success;
        //}


        //-------------------------------------
        // Method: serialDataReceived
        //  StatusUpdated handler for the bluetooth serial port
        //
        void serialDataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            SerialPort sp = (SerialPort)sender;

            while (sp.BytesToRead != 0)
	        {
                switch (rcvState)
	            {
                    case RcvStates.startbyte:
                        if (sp.ReadByte() == 0x04)              // if it is the start of a event
                        {
                            rcvState = RcvStates.typebyte;
                            inputBuffer[0] = 0x04;
                            packetCount++;
                        }
                        break;

                    case RcvStates.typebyte:
                        inputBuffer[1] = (byte)sp.ReadByte();
                        rcvState = RcvStates.payloadbyte;
                        break;

                    case RcvStates.payloadbyte:
                        payloadLength = sp.ReadByte();
                        inputBuffer[2] =(byte) payloadLength;
                        inputBufferIndex = 2;
                        rcvState = RcvStates.payload;
                        break;

                    case RcvStates.payload:
                        inputBufferIndex++;
                        inputBuffer[inputBufferIndex] = (byte)sp.ReadByte();
                        if (inputBufferIndex == (payloadLength + 2))
                        {
                            rcvState = RcvStates.startbyte;
                            lock (fileLock)
                            {
                                rcvdEvents.Add(inputBuffer);
                            }

                        }
                        break;

                    default:
                        break;
	            }
	        }
        }


        /// <summary>
        /// Closes the BLE monitor port
        /// </summary>
        public void CloseBLEMonitor()
        {
             serialPort.Close();
            serialPort.Dispose();
        }

        /// <summary>
        /// Will verify that the com port assigned to the BLE is present.
        /// </summary>
        /// <returns>true = present
        /// false = see FailMessage and FailErrorCode</returns>
        public bool VerifyBLEPresent()
        {
            FailMessage = string.Empty;
            FailErrorCode = (int)ErrorCodes.Program.Success;

            if (BluetoothPort == string.Empty)
            {
                FailMessage = "Bluetooth com port has not been defined. Set the property BluetoothPort.";
                FailErrorCode = (int)ErrorCodes.System.ComponetInitFail;
                return false;
            }
            if (checkForSerialPort(BluetoothPort))
                return true;
            else
            {
                FailMessage = "Bluetooth com port " + BluetoothPort + " not found.";
                FailErrorCode = (int)ErrorCodes.System.ComponetInitFail;
                return false;
            }

        }

        /// <summary>
        /// Will set the ble to load from the spi image 2, and do the load.
        /// for pre 2.9.9 the advertise started when the code was loaded.
        /// </summary>
        /// <returns></returns>
        public bool DoBLEDoLoad()
        {
            bool status = true;             // set to start pass
            string outmsg = string.Empty;

            // will set the flash to make the start command load from ble image 2
            if (!consoleSerialPort.WriteLine(cmd_loadBLEFromSPI, 2000))
            {
                FailMessage = "Error sending '" + cmd_loadBLEFromSPI + "' command.";
                FailErrorCode = (int)ErrorCodes.Program.DutUnexpectedCmdReturn;
                return false;
            }
            if (!consoleSerialPort.WaitForDVTMANPrompt(10000, out outmsg))
            {
                FailMessage = "Error waiting for the  '" + cmd_loadBLEFromSPI + "' reponse prompt.";
                FailErrorCode = (int)ErrorCodes.Program.DutUnexpectedCmdReturn;
                return false;
            }

            // start(let it finish booting up) which will load from flash
            if (!consoleSerialPort.WriteLine("p ble-start", 2000))
            {
                FailMessage = "Error sending 'p ble-start' command.";
                FailErrorCode = (int)ErrorCodes.Program.DutUnexpectedCmdReturn;
                return false;
            }
            if (!consoleSerialPort.WaitForDVTMANPrompt(2000, out outmsg))
            {
                FailMessage = "Error waiting for the  'p ble-start' reponse prompt.";
                FailErrorCode = (int)ErrorCodes.Program.DutUnexpectedCmdReturn;
                return false;
            }
            Thread.Sleep(100);          // give time for command to do its thing


            return status;
        }

        /// <summary>
        /// Will set the ble to load from the spi image 2, and do the load.
        /// for 3.0.0 or better code.
        /// </summary>
        /// <remarks>
        /// This just starts the load. Need to check 10 to 20 seconds latter to see
        /// if the load is OK. When BLE version shows up, it is loaded.
        /// </remarks>
        /// <returns></returns>
        public bool DoBLEDoLoad2()
        {
            bool status = true;             // set to start pass
            string outmsg = string.Empty;

            // will set the flash to make the start command load from ble image 2
            if (!consoleSerialPort.WriteLine(cmd_loadBLEFromSPI, 2000))
            {
                FailMessage = "Error sending '" + cmd_loadBLEFromSPI + "' command.";
                FailErrorCode = (int)ErrorCodes.Program.DutUnexpectedCmdReturn;
                return false;
            }
            if (!consoleSerialPort.WaitForDVTMANPrompt(10000, out outmsg))
            {
                FailMessage = "Error waiting for the  '" + cmd_loadBLEFromSPI + "' reponse prompt.";
                FailErrorCode = (int)ErrorCodes.Program.DutUnexpectedCmdReturn;
                return false;
            }

            // reboot the ble to cause it to load from SPI
            if (!consoleSerialPort.WriteLine(cmd_bleReboot, 2000))
            {
                FailMessage = "Error sending '" + cmd_bleReboot + "' command.";
                FailErrorCode = (int)ErrorCodes.Program.DutUnexpectedCmdReturn;
                return false;
            }
            if (!consoleSerialPort.WaitForDVTMANPrompt(2000, out outmsg))
            {
                FailMessage = "Error waiting for the  '" + cmd_bleReboot + "' reponse prompt.";
                FailErrorCode = (int)ErrorCodes.Program.DutUnexpectedCmdReturn;
                return false;
            }

            return status;
        }


        /// <summary>
        /// Will start the advertise. Assumes that the firmware has been loaded with DoBLEDoLoad.
        /// </summary>
        /// <returns></returns>
        public bool DoBLEAdvertise()
        {
            bool status = true;             // set to advertise pass
            string outmsg = string.Empty;

            if (!consoleSerialPort.WriteLine(cmd_startBLEAdvert, 1000))
            {
                FailMessage = "Error sending '" + cmd_startBLEAdvert + "' command.";
                FailErrorCode = (int)ErrorCodes.Program.DutUnexpectedCmdReturn;
                return false;
            }

            if (!consoleSerialPort.WaitForDVTMANPrompt(2000, out outmsg))
            {
                FailMessage = "Error waiting for the  '" + cmd_startBLEAdvert + "' reponse prompt.";
                FailErrorCode = (int)ErrorCodes.Program.DutUnexpectedCmdReturn;
                return false;
            }

            return status;
        }

        /// <summary>
        /// scan the the bluetooth for devices. Sets FoundDevices and FoundRSSI lists with 
        /// found items. serialPort must be open already.
        /// </summary>
        /// <remarks>
        ///     Send GAP device disovery request
        ///         01 04 FE 03 03 01 00
        ///     Get HCI Ext Status Event with status present
        ///         04 FF responseLength 06 7F status opCode -- --
        ///         status = 0 -> command sent OK
        ///     Get GAP Device Info Event(one for each device found)
        ///         04 FF responseLenght 0D 06 status eventtype addresstype addr rssi datalen datafield
        ///     Get GAP device Discovery Event - all devices have been found
        ///         04 FF responselenght 01 06 status numDevs array of {eventType, addrType, addr}
        /// </remarks>
        /// <returns>true = success
        /// false = fail. See FailMessage for failure</returns>
        private bool scan(int eventTimeoutSec, string idToSearchFor)
        {

            byte[] discoverCommand = { 0x01, 0x04, 0xfe, 0x03, 0x03, 0x01, 0x00 };
            byte[] discoverCancelCommand = { 0x01, 0x05, 0xFE, 0x00 };
            byte[] readData = new byte[1024];
            byte[] addressBytesR = new byte[6];      // copy of array that has address
            byte[] addressBytes = new byte[6];      // copy of array that has address
            string addressStringD = string.Empty;     // copy of string that has address in right order
            string addressString = string.Empty;     // copy of string that has address in right order, no -s
            bool status = true;                     // at least one unit found
            bool doneFound = false;                 // end of discovery found
            bool eventFound = false;                // at least one event was found
            bool idFound = false;                   // if the idToSearchFor was found
            bool serialPortOpened = false;          // set to true if had to open the port
            string outmsg = string.Empty;
            DateTime endTime = DateTime.Now.AddSeconds(eventTimeoutSec);

            if (serialPort == null)     // if the serial port has not been opened yet
            {
                serialPortOpened = true;
                if ((FailErrorCode = OpenBLEMonitor(ref FailMessage)) != (int)ErrorCodes.Program.Success)  // if problem opening port
                    return false;
            }

            if (!serialPort.IsOpen)     // if the serial port has not been opened yet
            {
                serialPortOpened = true;
                if ((FailErrorCode = OpenBLEMonitor(ref FailMessage)) != (int)ErrorCodes.Program.Success)  // if problem opening port
                    return false;
            }

            serialPort.DiscardInBuffer();
            sendDongleCmd(discoverCommand, 0, 7);        // send the discover command

            // now get the device data
            status = false;                 // all data not read yet
            rntRSSI.Clear();
            rntDevices.Clear();
            do
	        {
                if (rcvdEvents.Count != 0)
                {
                    eventFound = true;

                    //lock (fileLock2)
                    //{
                    //    readData = rcvdEvents.First();
                    //    rcvdEvents.RemoveAt(0);
                    //}
                    readDongleEvent(1000, ref readData);
                    if ((readData[3] == 0x0d) & (readData[4] == 0x06))  // if info event
                    {
                        //if (readData[6] == 4)       // only look at type 4
                        {
                            Array.Copy(readData, 8, addressBytes, 0, 6);           //get the address
                            Array.Reverse(addressBytes);                            // get in right order
                            addressStringD = BitConverter.ToString(addressBytes);    // convert to hex string
                            addressString = string.Empty;
                            for (int i = 0; i < addressStringD.Length; i++)
                            {
                                if (addressStringD[i] != '-')
                                 addressString = addressString + addressStringD[i];
                            }
                            rntDevices.Add(addressString);
                            rntRSSI.Add((sbyte)readData[14]);
                            status = true;      // units have been found
                            // if a id has been pasted
                            if (idToSearchFor != string.Empty)
                            {       // see if this is the id
                                if (rntDevices.Last().Contains(idToSearchFor))
                                {
                                    idFound = true;
                                }

                            }
                        }
                    }
                    else if ((readData[3] == 0x01) & (readData[4] == 0x06))     // if end discovery event
                    {
                        doneFound = true;
                    }
                }

	        } while ((DateTime.Now < endTime) & !doneFound & !idFound);

            if (!doneFound & eventFound)     // cancel if end discovery was not found
                sendDongleCmd(discoverCancelCommand, 0, discoverCancelCommand.Length);        // send the cancel command

            if (!eventFound)   // no events were found
            {
                FailMessage = "ble dongle response timeout";
                FailErrorCode = (int)ErrorCodes.Program.AdvertNotFound;
            }
            else if (status)       // event happened and found at least one unit
            {
                FailMessage = string.Empty;
                FailErrorCode = (int)ErrorCodes.Program.Success;
            }
            else        // event happend, but not units found
            {
                FailMessage = "No units found";
                FailErrorCode = (int)ErrorCodes.Program.ValueOutsideOfLimit;
            }

            Thread.Sleep(1000);
            if (serialPortOpened)   // close port if opened by us
                CloseBLEMonitor();
            return status;
        }

        /// <summary>
        /// Will use the DUT to find bluetooth devices advertising. The lists rntDevices and rntRSSI
        /// will have the result data.
        /// </summary>
        /// <remarks>
        /// The return data will look like:
        /// d ble-scan
        ///     Scan result cnt = 4
        ///     debug scan result sequence number = 1
        ///     debug scan stats: advert=107, end=30
        ///      0: hb 0 mo 1  68 54 F5 F8 F9 18   15 [   0   0   0 ]
        ///      1: hb 1 mo 0  68 54 F5 F8 F9 30   32 [  -6   0  72 ]
        ///      2: hb 1 mo 0  68 54 F5 F8 F8 9D   33 [   6  -2 -59 ]
        ///      3: hb 1 mo 0  68 54 F5 F8 F9 15   33 [  55  20 -14 ]
        ///     DVTMAN> 
        /// </remarks>
        /// <param name="timeoutSec"></param>
        /// <returns></returns>
        public bool DutScan(int eventTimeoutSec, int resetDelaySec, string idToSearchFor)
        {
            bool status = false;
            string msg;
            Match match;
            Match count;
            string matchLine;
            int start;
            bool idFound = false;
            DateTime endTime = DateTime.Now.AddSeconds(eventTimeoutSec);

            rntDevices.Clear();
            rntRSSI.Clear();
            consoleSerialPort.ClearBuffers();
            idFound = false;

            do
            {
                idFound = false;
                // make sure that the reload flag is NOT set
                if (!consoleSerialPort.WriteLine(cmd_bleNoCodeLoad, 2000))
                {

                    FailMessage = "Error sending '" + cmd_bleNoCodeLoad + "' command to DUT.";
                    FailErrorCode = (int)ErrorCodes.Program.DUTComError;
                    return false;
                }
                // wait for the prompt from the command
                if (!consoleSerialPort.WaitForDVTMANPrompt(10000, out msg))
                {
                    FailMessage = "Error waiting for prompt after '" + cmd_bleNoCodeLoad + "' command to DUT.";
                    FailErrorCode = (int)ErrorCodes.Program.DUTComError;
                    return false;
                }

                // Reset the BLE. It has issues going between rcv and tx tests
                // if time is set to 0, do not do a reboot
                if (resetDelaySec != 0)
                {
                    if (!consoleSerialPort.WriteLine(cmd_bleReboot, 2000))
                    {
                        FailMessage = "Error sending '" + cmd_bleReboot + "' command to DUT.";
                        FailErrorCode = (int)ErrorCodes.Program.DUTComError;
                        return false;
                    }
                    // wait for the prompt from the command
                    if (!consoleSerialPort.WaitForDVTMANPrompt(10000, out msg))
                    {
                        FailMessage = "Error waiting for prompt after '" + cmd_bleReboot + "' command to DUT.";
                        FailErrorCode = (int)ErrorCodes.Program.DUTComError;
                        return false;
                    }
                    Thread.Sleep(resetDelaySec * 1000);         // give the BLE time to reboot.
                }

                // start the DUT BLE scan
                if (!consoleSerialPort.WriteLine(cmd_startBLEScan, 2000))
                {
                    FailMessage = "Error sending '" + cmd_startBLEScan + "' command to DUT.";
                    FailErrorCode = (int)ErrorCodes.Program.DUTComError;
                    return false;
                }
                // wait for the prompt from the command
                if (!consoleSerialPort.WaitForDVTMANPrompt(2000, out msg))
                {
                    FailMessage = "Error waiting for prompt after '" + cmd_startBLEScan + "' command to DUT.";
                    FailErrorCode = (int)ErrorCodes.Program.DUTComError;
                    return false;
                }
                // wait for the timeout to let the scan happen
                Thread.Sleep(eventTimeoutSec * 1000);
                // now look for the MAC
                if (!consoleSerialPort.WriteLine(cmd_readBLEScanData, 2000))
                {
                    FailMessage = "Error sending '" + cmd_readBLEScanData + "' command to DUT.";
                    FailErrorCode = (int)ErrorCodes.Program.DUTComError;
                    return false;
                }
                // wait for the prompt from the command
                if (!consoleSerialPort.WaitForDVTMANPrompt(2000, out msg))
                {
                    FailMessage = "Error waiting for prompt after '" + cmd_readBLEScanData + "' command to DUT.";
                    FailErrorCode = (int)ErrorCodes.Program.DUTComError;
                    return false;
                }
                // now have the data in msg. so put it into the lists
                // look if any devices are found. 'Scan result cnt = ' value
                match = Regex.Match(msg, @"Scan result cnt = \d+");
                if (match.Success)          // found the scan message
                {
                    count = Regex.Match(match.Value, @"\d+");
                    if (count.Value == "0")       // if nothing found
                        status = false;
                    else
                    {
                        status = true;                                          // found at least one unit
                        // have data. get it into the lists
                        for (int i = 0; i < Convert.ToInt32(count.Value); i++)
                        {
                            start = msg.IndexOf(string.Format("{0}: ", i));     // look for line with mac
                            matchLine = msg.Substring(start + 14, 17);          // get the mac
                            if (matchLine.ToUpper() == idToSearchFor)
                                idFound = true;
                            rntDevices.Add(matchLine.Replace(" ", ""));         // save in list with spaces removed
                            matchLine = msg.Substring(start + 34, 2);           // get RSSI difference
                            rntRSSI.Add(Convert.ToInt32(matchLine));            // save in list
                        }
                    }
                }
            } while ((DateTime.Now < endTime) & !idFound);

            if (status)       // event happened and found at least one unit
            {
                FailMessage = string.Empty;
                FailErrorCode = (int)ErrorCodes.Program.Success;
            }
            else        // event happend, but not units found
            {
                FailMessage = "No units found";
                FailErrorCode = (int)ErrorCodes.Program.ValueOutsideOfLimit;
            }

            return status;
        }


        //---------------------
        //  utilities
        //-------------------

        private bool checkForSerialPort(string comPort)
        {
            string[] ports = SerialPort.GetPortNames();         // get list of serial ports
            // see if this port is in the list of ports on the computer
            foreach (string i in ports)
            {
                if (i.Equals(comPort))         // if found a match
                    return true;
            }
            return false;
        }

        /// <summary>
        /// Read a event response from ble dongle.
        /// </summary>
        /// <remarks>
        /// general format of a event
        ///     04 code responseLenght Opcode 0 Opcode 1 status data
        /// </remarks>
        /// <param name="timeoutMS"></param>
        /// <returns></returns>
        private bool readDongleEvent(int timeoutMS, ref byte[] packet)
        {
            bool status = false;        // have not gotten a event yet
            DateTime endTime = new DateTime();
            string displayString = string.Empty;

            //serialPort.DiscardInBuffer();
            endTime = DateTime.Now.AddMilliseconds(timeoutMS);      // absolute time for timeout
            do                                                      // loop to wait for event and process it
            {
                //Thread.Sleep(10);
                if (rcvdEvents.Count != 0)                  // if the data has been recived
                {
                    lock (fileLock2)
                    {
                        packet = rcvdEvents.First();
                        rcvdEvents.RemoveAt(0);
                        status = true;
                    }
                }
            } while (!status & (DateTime.Now < endTime));

            return status;
        }

        private void sendDongleCmd(byte[] cmd, int startIndex, int len)
        {
            string displayString = string.Empty;
            int i = 0;

            serialPort.Write(cmd, startIndex, len);
            for (i = 0; i < len; i++)
            {
                displayString = displayString + cmd[i].ToString("X2") + " ";
            }
        }

        //------------------------
        // events
        //  Use UpdateStatusWindow(msg) to display a message on the
        //  MainWindow status window.
        //------------------------
        public event EventHandler<StatusUpdatedEventArgs>
            StatusUpdated;

        protected virtual void
            OnStatusUpdated(StatusUpdatedEventArgs e)
        {
            if (StatusUpdated != null)
                StatusUpdated(this, e);
        }

        // call these functions to update the main status window
        private void UpdateStatusWindow(string msg)
        {
            string newStatus = msg;
            StatusUpdatedEventArgs nuea = new StatusUpdatedEventArgs();
            nuea.statusText = msg;
            OnStatusUpdated(nuea);
        }
        private void UpdateStatusWindow(string msg, StatusType status)
        {
            string newStatus = msg;
            StatusUpdatedEventArgs nuea = new StatusUpdatedEventArgs();
            nuea.statusText = msg;
            nuea.statusType = status;
            OnStatusUpdated(nuea);
        }

    }
}
