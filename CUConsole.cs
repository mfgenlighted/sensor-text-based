﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.IO.Ports;
using System.Threading;
using System.Diagnostics;

namespace SensorManufSY2
{
    public class CUConsole : IDisposable
    {

        //public bool ConfigDisplaySendChar { get; set; }
        //public bool ConfigDisplayRcvChar { get; set; }

        //----------------------------------
        //  events for the BaseStream version of the serial handling
        //---------------------------------
        public delegate void RecieveDataHandler(byte[] data);
        public event RecieveDataHandler RecieveData;

        public delegate void ClosedHandler();
        public event ClosedHandler Closed;

        public delegate void SerialPortErrorHandler(IOException exception);
        public event SerialPortErrorHandler SerialPortError;

        // Error Codes
        public const int  ERR_CUPortNotOpen  = -1;          // CU port is not open
        public const int ERR_CUSendError = -2;                // error sending data
        public const int ERR_CURcvError = -3;               // error recievieng data
        public const int ERR_CUHasNotBeenInitialize = -4;       // cu port has not been intitialized


        //------------------------
        // events
        //  Use UpdateStatusWindow(msg) to display a message on the
        //  MainWindow status window.
        //------------------------
        public event EventHandler<StatusUpdatedEventArgs> StatusUpdated;

        protected virtual void OnStatusUpdated(StatusUpdatedEventArgs e)
        {
            if (StatusUpdated != null)
                StatusUpdated(this, e);
        }

        // call these functions to update the main status window
        private void UpdateStatusWindow(string msg)
        {
            string newStatus = msg;
            StatusUpdatedEventArgs nuea = new StatusUpdatedEventArgs();
            nuea.statusText = msg;
            OnStatusUpdated(nuea);
        }
        private void UpdateStatusWindow(string msg, StatusType status)
        {
            string newStatus = msg;
            StatusUpdatedEventArgs nuea = new StatusUpdatedEventArgs();
            nuea.statusText = msg;
            nuea.statusType = status;
            OnStatusUpdated(nuea);
        }


        private bool disposed = false;
        //----------------------------------------
        // serial port values
        //----------------------------------------
        private SerialPort serialPort = null;            // CU serial port
        private string comPort = string.Empty;
        private string serialBuffer = string.Empty;      // buffer of incoming serial data
        private string readBuffer = string.Empty;        // buffer read on one event
        private string dumpFileName = string.Empty;
        private object fileLock = new object();
        private object readLock = new object();

        private bool displaySendChar = false;
        private bool displayRcvChar = false;

        private byte[] buffer = new byte[4096];          // data buffer for BaseStream version
        private bool stopping = false;                  // BaseStream version, set to true to initiate a close

        public bool DisplaySendChar { get { return displaySendChar; } set { displaySendChar = value; } }
        public bool DisplayRcvChar { get { return displayRcvChar; } set { displayRcvChar = value; } }


        public bool UseOldMethod = true;                               // true=use serialDataReceived event handler
                                                                        // false=BaseStream method(default)

        public bool IsOpen { get { if (serialPort == null) return false; return serialPort.IsOpen; } }

        object locker = new object();

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            //if (!disposed)
            //{
            //    if (disposing)
            //    {
            //        if (serialPort != null)
            //            serialPort.Close();
            //        disposed = true;
            //    }
            //}
            Close();
            disposed = true;
        }


        //--------------------------
        // Sets up and opens the CU serial port.
        // Uses the configCUComPort value for the port.
        //
        // excpetions:
        //          PortNotFound - configured port not in computer ports
        //          Excpetion
        //          SerialPort contruct exceptions
        //          SerialPort.Open exceptions
        public void Open(string comPort)
        {
            bool found = true;

            if (serialPort != null)                        // if already opne
            {
                serialPort.Close();                         // close it
                serialPort.Dispose();                       // and get rid of it
                this.comPort = string.Empty;
            }


            Trace.Write("Open cu port");
            if (comPort == string.Empty)                        // if it has not been defined
                throw new Exception("Error: CU com port has not been defined. ReadStationConfigFile must be run.");
            found = CheckForSerialPort(comPort);
            Trace.Write("..port found");
            if (found)
            {
                // port valid on this system, so try to open
                try
                {
                    serialPort = new SerialPort(comPort, 19200);
                    serialPort.ReadTimeout = 500;
                    serialPort.WriteTimeout = 500;
                    serialPort.DtrEnable = false;
                    serialPort.Handshake = Handshake.None;
                    serialPort.Parity = Parity.None;
                    if (UseOldMethod)
                        serialPort.DataReceived += new SerialDataReceivedEventHandler(SerialDataReceived);
                    serialPort.Open();           // open the serial port
                    disposed = false;
                    Trace.WriteLine("..opened");

                    if (!UseOldMethod)
                        serialPort.BaseStream.BeginRead(buffer, 0, buffer.Length, EndRead, null);

                }
                catch (Exception ex)
                {
                    Trace.WriteLine("..error opening: " + ex.Message);
                    throw new Exception(string.Format("Error opening {0}", comPort), ex);
                }
            }
            else
                throw new Exception("CU serial port " + comPort + " is not a valid com port on this computer");
        }

        public bool CheckForSerialPort(string comPort)
        {
            string[] ports = SerialPort.GetPortNames();         // get list of serial ports
            // see if this port is in the list of ports on the computer
            foreach (string i in ports)
            {
                if (i.Equals(comPort))         // if found a match
                    return true;
            }
            return false;
        }

        //-----------------
        // Close the CU serial port
        public void Close()
        {
            if (this.disposed)
            {
                throw new ObjectDisposedException(this.GetType().Name, "Cannot use a disposed object.");
            }
            if (UseOldMethod)
            {
                try
                {
                    if (serialPort != null)
                    {
                        if (serialPort.IsOpen)
                            serialPort.Close();
                        serialPort.Dispose();
                        Trace.WriteLine("CU port closed");
                    }

                }
                catch (Exception ex)
                {
                    UpdateStatusWindow("Error while closing CU console port. IsOpen = " + serialPort.IsOpen + "\n" + ex.Message + "\n", StatusType.failed);
                }
            }
            else
            {
                stopping = true;
                Thread.Sleep(100);
                if (serialPort.IsOpen)
                    serialPort.Close();
                Closed?.Invoke();
                serialPort.Dispose();
            }
        }


        //------------------------
        // Clear the cu serial buffers, both serial driver and serial in buffer.
        // Exceptions:
        //      SerialPort.DiscardInBuffers construct exceptions
        //      SerialPort.DiscardOutBuffers construct exceptions
        public void ClearSerialBuffers()
        {
            try
            {
                serialPort.DiscardInBuffer();
                serialPort.DiscardOutBuffer();
                serialBuffer = string.Empty;
            }
            catch (Exception ex)
            {
                throw new Exception("Error clearing CU serial buffers", ex);
            }
        }

        public void SetDumpFileName(string directory, string baseTitle)
        {
            if (!Directory.Exists(directory))
                Directory.CreateDirectory(directory);

            dumpFileName = directory + @"\cu" + baseTitle;
        }

        public void EndDump()
        {
            dumpFileName = string.Empty;
        }

        /// <summary>
        /// Wait for CU version requests and send response.
        /// </summary>
        /// <remarks>
        /// Will wait for a CU version request and send it. Will wait up to 200ms to
        /// see if a new request comes. If so, will send the response again. If not,
        /// returns.
        /// Error Codes
        /// 
        /// </remarks>
        /// <param name="errorCode"></param>
        /// <param name="errorMsg"></param>
        /// <returns></returns>
        public bool RespondToCUTest(byte[] responseArray, ref int errorCode, ref string errorMsg)
        {
            int sendTries = 0;              // number of time tried to send response
            DateTime startTime;             // time started looking a responding
            string rcvString = string.Empty;

            errorCode = 0;                  // start out assuming it will work
            errorMsg = string.Empty;

            if (serialPort == null)
            {
                errorCode = ERR_CUHasNotBeenInitialize;
                errorMsg = "CU port has not been initialized.";
                return false;
            }
            if (!serialPort.IsOpen)
            {
                errorCode = ERR_CUPortNotOpen; ;
                errorMsg = "CU port is not open.";
                return false;
            }

            // port is open and initialized. so start the fun
            startTime = DateTime.Now;
            do
            {
                if (ReadChars(ref rcvString, 6, 100))      // if found 6 characters
                {
                    try
                    {
                        serialPort.Write(responseArray, 0, 6);
                    }
                    catch (Exception ex)
                    {
                        errorCode = ERR_CUSendError;
                        errorMsg = "Error sending response. " + ex.Message;
                        return false;
                    }
                }
                else            // problem with the read
                {
                    errorCode = ERR_CURcvError;
                    errorMsg = "Did not get 6 characters.";
                    return false;
                }
                // keep tring if it has not responded 9 time and less than 1 second has elapsed
            } while (((DateTime.Now - startTime).Milliseconds < 1000) & (sendTries++ < 9));   

            return true;
        }

        //-------------------------------
        // CUWriteChars
        ///<summary>Will send the characters to CU port.</summary>
        ///<param name="charsToSend">byte[] - Array of characters to send.</param>
        ///<param name="numToSend">int - Number of characters to send.</param>
        ///<returns>bool - true = characters were sent.</returns>
        ///
        public bool WriteChars(byte[] charsToSend, int numToSend)
        {
            string rtnstring = string.Empty;
            string smsg;

            try
            {
                serialPort.Write(charsToSend, 0, numToSend);
                //byte[] sendchar = new byte[1];
                //for (int i = 0; i < numToSend; i++)
                //{
                //    sendchar[0] = charsToSend[i];
                //    serialPort.Write(sendchar, 0, 1);
                //    Thread.Sleep(5);
                //}
                if (displaySendChar)         // if display window is enabled
                {
                    smsg = DateTime.Now.ToString("HH:mm:ss.fff") + "-> CU command sent: " + BitConverter.ToString(charsToSend) + "\n";
                    UpdateStatusWindow(smsg, StatusType.cuSend);
                }
                //if (dumpFileName != string.Empty)
                //{
                //    lock (fileLock)
                //    {
                //        using (System.IO.StreamWriter file = new System.IO.StreamWriter(dumpFileName, true))
                //        {
                //            smsg = DateTime.Now.ToString("HH:mm:ss.fff") + "-> CU command sent: " + BitConverter.ToString(charsToSend) + "\n";
                //            file.Write("\n" + smsg);
                //        }
                //    }
                //}
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        //--------------------------------------------
        // Method: CUReadChars
        /// <summary>
        ///  Reads the serial input buffer till the number of characters are found.
        ///  If the number of characters asked for are not found, false is returned.
        /// </summary>
        ///
        ///<param name="rcvCount">int - Number of characters to look for</param>
        ///<param name="returnedLine">ref string - characters found. Will be a empty string
        ///if at least rcvCount characters are not found</param>
        ///<param name="timeoutms">int - Number of ms to wait for the characters</param>
        ///<returns>bool - true = at least rcvCount characters are found and returned in returnedLine
        ///                 false = not enough characters were found in time. Empty string returned.</returns>
        public bool ReadChars(ref string returnedLine, int rcvCount, int timeoutms)
        {
            DateTime endTime;
            bool status = true;

            endTime = DateTime.Now.AddMilliseconds(timeoutms);
            while ((serialBuffer.Length < rcvCount) && (endTime > DateTime.Now))
            {
                Thread.Sleep(5);
            }
            if (serialBuffer.Length >= rcvCount)     // if found the right number of  charc
            {
                lock(readLock)
                {
                    returnedLine = serialBuffer.Substring(0, rcvCount);
                    if (serialBuffer.Length > rcvCount)
                        serialBuffer = serialBuffer.Substring(rcvCount + 1, serialBuffer.Length - rcvCount - 1);
                    else
                        serialBuffer = string.Empty;
                }
            }
            else
            {
                returnedLine = string.Empty;
                status = false;
            }

            return status;
        }


        //-------------------------------------
        // Method: CUSerialDataReceived
        //  Event handler for the dut cu serial port
        //
        void SerialDataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            SerialPort sp = (SerialPort)sender;
            string smsg = string.Empty;

            string msg = sp.ReadExisting();
            //if (displayRcvChar)         // if display window is enabled
            //{
            //    byte[] array = Encoding.ASCII.GetBytes(msg);
            //    smsg = DateTime.Now.ToString("HH:mm:ss.fff") + "<- CU command rcv: " + BitConverter.ToString(array) + "\n";
            //    UpdateStatusWindow(smsg, StatusType.cuRcv);
            //}
            serialBuffer += msg;
        }

        //----------------------------------------------
        // routines that suport the BaseStream method
        //---------------------------------------------
        void EndRead(IAsyncResult result)
        {
            string smsg = string.Empty;
            int count = 0;
            try
            {
                if (serialPort.IsOpen)
                    count = serialPort.BaseStream.EndRead(result);

                if (!stopping)
                {
                    byte[] data = new byte[count];
                    Buffer.BlockCopy(buffer, 0, data, 0, count);

                    RecieveData?.Invoke(data);
                    //            serialBuffer += smsg;
                    if (displayRcvChar)         // if display window is enabled
                    {
                        smsg = DateTime.Now.ToString("HH:mm:ss.fff") + "<- CU command rcv: " + BitConverter.ToString(data) + "\n";
                        UpdateStatusWindow(smsg, StatusType.cuRcv);
                    }
                    //if (dumpFileName != string.Empty)
                    //{
                    //    lock (fileLock)
                    //    {
                    //        using (System.IO.StreamWriter file = new System.IO.StreamWriter(dumpFileName, true))
                    //        {
                    //            smsg = DateTime.Now.ToString("HH:mm:ss.fff") + "<- CU command rcv: " + BitConverter.ToString(data) + "\n";
                    //            file.Write(smsg);
                    //        }
                    //    }
                    //}
                    serialBuffer += data;

                }
            }
            catch (IOException exception)
            {
                SerialPortError?.Invoke(exception);
                Debug.WriteLine(exception.ToString());
            }

            // Init another async read or close port as appropriate
            if (!stopping)
            {
                serialPort.BaseStream.BeginRead(buffer, 0, buffer.Length, EndRead, null);
            }
            else
            {
                serialPort.Close();
                Closed?.Invoke();
            }
        }


    }
}
