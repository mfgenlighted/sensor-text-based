﻿namespace SensorManufSY2
{
    partial class ConfigFixtures
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConfigFixtures));
            this.bEdit = new System.Windows.Forms.Button();
            this.bCopy = new System.Windows.Forms.Button();
            this.bDelete = new System.Windows.Forms.Button();
            this.bDone = new System.Windows.Forms.Button();
            this.ttEdit = new System.Windows.Forms.ToolTip(this.components);
            this.ttCopy = new System.Windows.Forms.ToolTip(this.components);
            this.ttDelete = new System.Windows.Forms.ToolTip(this.components);
            this.ttDone = new System.Windows.Forms.ToolTip(this.components);
            this.bNew = new System.Windows.Forms.Button();
            this.ttNew = new System.Windows.Forms.ToolTip(this.components);
            this.SuspendLayout();
            // 
            // bEdit
            // 
            resources.ApplyResources(this.bEdit, "bEdit");
            this.bEdit.Name = "bEdit";
            this.ttEdit.SetToolTip(this.bEdit, resources.GetString("bEdit.ToolTip"));
            this.bEdit.UseVisualStyleBackColor = true;
            this.bEdit.Click += new System.EventHandler(this.bEdit_Click);
            // 
            // bCopy
            // 
            resources.ApplyResources(this.bCopy, "bCopy");
            this.bCopy.Name = "bCopy";
            this.ttCopy.SetToolTip(this.bCopy, resources.GetString("bCopy.ToolTip"));
            this.bCopy.UseVisualStyleBackColor = true;
            this.bCopy.Click += new System.EventHandler(this.bCopy_Click);
            // 
            // bDelete
            // 
            resources.ApplyResources(this.bDelete, "bDelete");
            this.bDelete.Name = "bDelete";
            this.ttDelete.SetToolTip(this.bDelete, resources.GetString("bDelete.ToolTip"));
            this.bDelete.UseVisualStyleBackColor = true;
            this.bDelete.Click += new System.EventHandler(this.bDelete_Click);
            // 
            // bDone
            // 
            this.bDone.DialogResult = System.Windows.Forms.DialogResult.OK;
            resources.ApplyResources(this.bDone, "bDone");
            this.bDone.Name = "bDone";
            this.ttDone.SetToolTip(this.bDone, resources.GetString("bDone.ToolTip"));
            this.bDone.UseVisualStyleBackColor = true;
            // 
            // bNew
            // 
            resources.ApplyResources(this.bNew, "bNew");
            this.bNew.Name = "bNew";
            this.ttNew.SetToolTip(this.bNew, resources.GetString("bNew.ToolTip"));
            this.bNew.UseVisualStyleBackColor = true;
            this.bNew.Click += new System.EventHandler(this.bNew_Click);
            // 
            // ConfigFixtures
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ControlBox = false;
            this.Controls.Add(this.bNew);
            this.Controls.Add(this.bDone);
            this.Controls.Add(this.bDelete);
            this.Controls.Add(this.bCopy);
            this.Controls.Add(this.bEdit);
            this.Name = "ConfigFixtures";
            this.Load += new System.EventHandler(this.SetupForm);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button bEdit;
        private System.Windows.Forms.Button bCopy;
        private System.Windows.Forms.Button bDelete;
        private System.Windows.Forms.Button bDone;
        private System.Windows.Forms.ToolTip ttEdit;
        private System.Windows.Forms.ToolTip ttCopy;
        private System.Windows.Forms.ToolTip ttDelete;
        private System.Windows.Forms.ToolTip ttDone;
        private System.Windows.Forms.Button bNew;
        private System.Windows.Forms.ToolTip ttNew;
    }
}