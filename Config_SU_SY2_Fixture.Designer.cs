﻿namespace SensorManufSY2
{
    partial class Config_SU_SY2_Fixture
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Config_SU_SY2_Fixture));
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.bAutoFindTagComPort = new System.Windows.Forms.Button();
            this.cbTagComPort = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.tbLabelDirectory = new System.Windows.Forms.TextBox();
            this.vAutoFindDUTComPort = new System.Windows.Forms.Button();
            this.bAutoFindSnifferComPort = new System.Windows.Forms.Button();
            this.bAutoFindPMComPort = new System.Windows.Forms.Button();
            this.bAutoFindCUComPort = new System.Windows.Forms.Button();
            this.bAutofindLEDComPort = new System.Windows.Forms.Button();
            this.bAutoFindSMComPort = new System.Windows.Forms.Button();
            this.bAutoFindBLEComPort = new System.Windows.Forms.Button();
            this.bAutoFindTestRadioComPort = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.cbSwitchMateCom = new System.Windows.Forms.ComboBox();
            this.cbSnifferComPort = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.tLimitDirectory = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.nudLoad = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.cbPowerMeterComPort = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.nudControlVoltageGain = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.cbBluetoothComPort = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxTSBlueRange = new System.Windows.Forms.TextBox();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.numericUpDownTSPIRDLSOffDelay = new System.Windows.Forms.NumericUpDown();
            this.label24 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.numericUpDownTSPIRDLSOnDelay = new System.Windows.Forms.NumericUpDown();
            this.label17 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.numericUpDownTSAmbDLSOffDelay = new System.Windows.Forms.NumericUpDown();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.numericUpDownTSAmbDLSOnDelay = new System.Windows.Forms.NumericUpDown();
            this.label40 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.numericUpDownTSAmbDLSLEDV = new System.Windows.Forms.NumericUpDown();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.comboBoxTSAmbDLSType = new System.Windows.Forms.ComboBox();
            this.label38 = new System.Windows.Forms.Label();
            this.textBoxTSGreenRange = new System.Windows.Forms.TextBox();
            this.textBoxTSRedRange = new System.Windows.Forms.TextBox();
            this.comboBoxTSCUComPort = new System.Windows.Forms.ComboBox();
            this.comboBoxTSTestRadioComPort = new System.Windows.Forms.ComboBox();
            this.comboBoxTSDUTComPort = new System.Windows.Forms.ComboBox();
            this.comboBoxTSLEDDetectorComPort = new System.Windows.Forms.ComboBox();
            this.comboBoxTSLEDDetectorType = new System.Windows.Forms.ComboBox();
            this.label33 = new System.Windows.Forms.Label();
            this.comboBoxTSProductPrinter = new System.Windows.Forms.ComboBox();
            this.comboBoxTSPCBAPrinter = new System.Windows.Forms.ComboBox();
            this.textBoxTSCUVersionTag = new System.Windows.Forms.TextBox();
            this.comboBoxTSCUType = new System.Windows.Forms.ComboBox();
            this.numericUpDownTSTestChannel = new System.Windows.Forms.NumericUpDown();
            this.comboBoxTSTempProbe = new System.Windows.Forms.ComboBox();
            this.textBoxTSStationOperation = new System.Windows.Forms.TextBox();
            this.textBoxTSStationTitle = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lbStationSection = new System.Windows.Forms.Label();
            this.lbStationDB = new System.Windows.Forms.Label();
            this.lbFixtureType = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudLoad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudControlVoltageGain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTSPIRDLSOffDelay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTSPIRDLSOnDelay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTSAmbDLSOffDelay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTSAmbDLSOnDelay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTSAmbDLSLEDV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTSTestChannel)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.groupBox4.Controls.Add(this.bAutoFindTagComPort);
            this.groupBox4.Controls.Add(this.cbTagComPort);
            this.groupBox4.Controls.Add(this.label12);
            this.groupBox4.Controls.Add(this.label11);
            this.groupBox4.Controls.Add(this.tbLabelDirectory);
            this.groupBox4.Controls.Add(this.vAutoFindDUTComPort);
            this.groupBox4.Controls.Add(this.bAutoFindSnifferComPort);
            this.groupBox4.Controls.Add(this.bAutoFindPMComPort);
            this.groupBox4.Controls.Add(this.bAutoFindCUComPort);
            this.groupBox4.Controls.Add(this.bAutofindLEDComPort);
            this.groupBox4.Controls.Add(this.bAutoFindSMComPort);
            this.groupBox4.Controls.Add(this.bAutoFindBLEComPort);
            this.groupBox4.Controls.Add(this.bAutoFindTestRadioComPort);
            this.groupBox4.Controls.Add(this.label10);
            this.groupBox4.Controls.Add(this.cbSwitchMateCom);
            this.groupBox4.Controls.Add(this.cbSnifferComPort);
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Controls.Add(this.tLimitDirectory);
            this.groupBox4.Controls.Add(this.label8);
            this.groupBox4.Controls.Add(this.nudLoad);
            this.groupBox4.Controls.Add(this.label7);
            this.groupBox4.Controls.Add(this.cbPowerMeterComPort);
            this.groupBox4.Controls.Add(this.label6);
            this.groupBox4.Controls.Add(this.nudControlVoltageGain);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this.cbBluetoothComPort);
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Controls.Add(this.label2);
            this.groupBox4.Controls.Add(this.textBoxTSBlueRange);
            this.groupBox4.Controls.Add(this.buttonCancel);
            this.groupBox4.Controls.Add(this.button1);
            this.groupBox4.Controls.Add(this.numericUpDownTSPIRDLSOffDelay);
            this.groupBox4.Controls.Add(this.label24);
            this.groupBox4.Controls.Add(this.label45);
            this.groupBox4.Controls.Add(this.numericUpDownTSPIRDLSOnDelay);
            this.groupBox4.Controls.Add(this.label17);
            this.groupBox4.Controls.Add(this.label46);
            this.groupBox4.Controls.Add(this.label43);
            this.groupBox4.Controls.Add(this.label21);
            this.groupBox4.Controls.Add(this.label22);
            this.groupBox4.Controls.Add(this.label39);
            this.groupBox4.Controls.Add(this.label23);
            this.groupBox4.Controls.Add(this.label34);
            this.groupBox4.Controls.Add(this.label25);
            this.groupBox4.Controls.Add(this.label35);
            this.groupBox4.Controls.Add(this.numericUpDownTSAmbDLSOffDelay);
            this.groupBox4.Controls.Add(this.label36);
            this.groupBox4.Controls.Add(this.label37);
            this.groupBox4.Controls.Add(this.label26);
            this.groupBox4.Controls.Add(this.label42);
            this.groupBox4.Controls.Add(this.numericUpDownTSAmbDLSOnDelay);
            this.groupBox4.Controls.Add(this.label40);
            this.groupBox4.Controls.Add(this.label27);
            this.groupBox4.Controls.Add(this.label41);
            this.groupBox4.Controls.Add(this.label28);
            this.groupBox4.Controls.Add(this.numericUpDownTSAmbDLSLEDV);
            this.groupBox4.Controls.Add(this.label29);
            this.groupBox4.Controls.Add(this.label30);
            this.groupBox4.Controls.Add(this.comboBoxTSAmbDLSType);
            this.groupBox4.Controls.Add(this.label38);
            this.groupBox4.Controls.Add(this.textBoxTSGreenRange);
            this.groupBox4.Controls.Add(this.textBoxTSRedRange);
            this.groupBox4.Controls.Add(this.comboBoxTSCUComPort);
            this.groupBox4.Controls.Add(this.comboBoxTSTestRadioComPort);
            this.groupBox4.Controls.Add(this.comboBoxTSDUTComPort);
            this.groupBox4.Controls.Add(this.comboBoxTSLEDDetectorComPort);
            this.groupBox4.Controls.Add(this.comboBoxTSLEDDetectorType);
            this.groupBox4.Controls.Add(this.label33);
            this.groupBox4.Controls.Add(this.comboBoxTSProductPrinter);
            this.groupBox4.Controls.Add(this.comboBoxTSPCBAPrinter);
            this.groupBox4.Controls.Add(this.textBoxTSCUVersionTag);
            this.groupBox4.Controls.Add(this.comboBoxTSCUType);
            this.groupBox4.Controls.Add(this.numericUpDownTSTestChannel);
            this.groupBox4.Controls.Add(this.comboBoxTSTempProbe);
            this.groupBox4.Controls.Add(this.textBoxTSStationOperation);
            this.groupBox4.Controls.Add(this.textBoxTSStationTitle);
            resources.ApplyResources(this.groupBox4, "groupBox4");
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.TabStop = false;
            // 
            // bAutoFindTagComPort
            // 
            resources.ApplyResources(this.bAutoFindTagComPort, "bAutoFindTagComPort");
            this.bAutoFindTagComPort.Name = "bAutoFindTagComPort";
            this.toolTip1.SetToolTip(this.bAutoFindTagComPort, resources.GetString("bAutoFindTagComPort.ToolTip"));
            this.bAutoFindTagComPort.UseVisualStyleBackColor = true;
            this.bAutoFindTagComPort.Click += new System.EventHandler(this.bAutoFindTagComPort_Click);
            // 
            // cbTagComPort
            // 
            this.cbTagComPort.DropDownWidth = 300;
            this.cbTagComPort.FormattingEnabled = true;
            resources.ApplyResources(this.cbTagComPort, "cbTagComPort");
            this.cbTagComPort.Name = "cbTagComPort";
            this.toolTip1.SetToolTip(this.cbTagComPort, resources.GetString("cbTagComPort.ToolTip"));
            this.cbTagComPort.DropDown += new System.EventHandler(this.DutComPort_DropDown);
            this.cbTagComPort.SelectionChangeCommitted += new System.EventHandler(this.cbBLETagComPort_SelectionChangeCommitted);
            // 
            // label12
            // 
            resources.ApplyResources(this.label12, "label12");
            this.label12.Name = "label12";
            // 
            // label11
            // 
            resources.ApplyResources(this.label11, "label11");
            this.label11.Name = "label11";
            // 
            // tbLabelDirectory
            // 
            resources.ApplyResources(this.tbLabelDirectory, "tbLabelDirectory");
            this.tbLabelDirectory.Name = "tbLabelDirectory";
            this.tbLabelDirectory.Click += new System.EventHandler(this.tbLabelDirectory_Clicked);
            // 
            // vAutoFindDUTComPort
            // 
            resources.ApplyResources(this.vAutoFindDUTComPort, "vAutoFindDUTComPort");
            this.vAutoFindDUTComPort.Name = "vAutoFindDUTComPort";
            this.toolTip1.SetToolTip(this.vAutoFindDUTComPort, resources.GetString("vAutoFindDUTComPort.ToolTip"));
            this.vAutoFindDUTComPort.UseVisualStyleBackColor = true;
            this.vAutoFindDUTComPort.Click += new System.EventHandler(this.bAutoFindDUTComPort_Click);
            // 
            // bAutoFindSnifferComPort
            // 
            resources.ApplyResources(this.bAutoFindSnifferComPort, "bAutoFindSnifferComPort");
            this.bAutoFindSnifferComPort.Name = "bAutoFindSnifferComPort";
            this.toolTip1.SetToolTip(this.bAutoFindSnifferComPort, resources.GetString("bAutoFindSnifferComPort.ToolTip"));
            this.bAutoFindSnifferComPort.UseVisualStyleBackColor = true;
            // 
            // bAutoFindPMComPort
            // 
            resources.ApplyResources(this.bAutoFindPMComPort, "bAutoFindPMComPort");
            this.bAutoFindPMComPort.Name = "bAutoFindPMComPort";
            this.bAutoFindPMComPort.UseVisualStyleBackColor = true;
            this.bAutoFindPMComPort.Click += new System.EventHandler(this.autofindPM_Click);
            // 
            // bAutoFindCUComPort
            // 
            resources.ApplyResources(this.bAutoFindCUComPort, "bAutoFindCUComPort");
            this.bAutoFindCUComPort.Name = "bAutoFindCUComPort";
            this.toolTip1.SetToolTip(this.bAutoFindCUComPort, resources.GetString("bAutoFindCUComPort.ToolTip"));
            this.bAutoFindCUComPort.UseVisualStyleBackColor = true;
            this.bAutoFindCUComPort.Click += new System.EventHandler(this.autofindCU_Click);
            // 
            // bAutofindLEDComPort
            // 
            resources.ApplyResources(this.bAutofindLEDComPort, "bAutofindLEDComPort");
            this.bAutofindLEDComPort.Name = "bAutofindLEDComPort";
            this.toolTip1.SetToolTip(this.bAutofindLEDComPort, resources.GetString("bAutofindLEDComPort.ToolTip"));
            this.bAutofindLEDComPort.UseVisualStyleBackColor = true;
            this.bAutofindLEDComPort.Click += new System.EventHandler(this.autofindLED_Click);
            // 
            // bAutoFindSMComPort
            // 
            resources.ApplyResources(this.bAutoFindSMComPort, "bAutoFindSMComPort");
            this.bAutoFindSMComPort.Name = "bAutoFindSMComPort";
            this.toolTip1.SetToolTip(this.bAutoFindSMComPort, resources.GetString("bAutoFindSMComPort.ToolTip"));
            this.bAutoFindSMComPort.UseVisualStyleBackColor = true;
            this.bAutoFindSMComPort.Click += new System.EventHandler(this.autofindSM_Click);
            // 
            // bAutoFindBLEComPort
            // 
            resources.ApplyResources(this.bAutoFindBLEComPort, "bAutoFindBLEComPort");
            this.bAutoFindBLEComPort.Name = "bAutoFindBLEComPort";
            this.toolTip1.SetToolTip(this.bAutoFindBLEComPort, resources.GetString("bAutoFindBLEComPort.ToolTip"));
            this.bAutoFindBLEComPort.UseVisualStyleBackColor = true;
            this.bAutoFindBLEComPort.Click += new System.EventHandler(this.autofindBLE_Click);
            // 
            // bAutoFindTestRadioComPort
            // 
            resources.ApplyResources(this.bAutoFindTestRadioComPort, "bAutoFindTestRadioComPort");
            this.bAutoFindTestRadioComPort.Name = "bAutoFindTestRadioComPort";
            this.toolTip1.SetToolTip(this.bAutoFindTestRadioComPort, resources.GetString("bAutoFindTestRadioComPort.ToolTip"));
            this.bAutoFindTestRadioComPort.UseVisualStyleBackColor = true;
            this.bAutoFindTestRadioComPort.Click += new System.EventHandler(this.bAutofindRefRadio_Click);
            // 
            // label10
            // 
            resources.ApplyResources(this.label10, "label10");
            this.label10.Name = "label10";
            // 
            // cbSwitchMateCom
            // 
            this.cbSwitchMateCom.DropDownWidth = 300;
            this.cbSwitchMateCom.FormattingEnabled = true;
            resources.ApplyResources(this.cbSwitchMateCom, "cbSwitchMateCom");
            this.cbSwitchMateCom.Name = "cbSwitchMateCom";
            this.toolTip1.SetToolTip(this.cbSwitchMateCom, resources.GetString("cbSwitchMateCom.ToolTip"));
            this.cbSwitchMateCom.DropDown += new System.EventHandler(this.DutComPort_DropDown);
            this.cbSwitchMateCom.SelectionChangeCommitted += new System.EventHandler(this.comboBoxTSSwitchMateComPort_SelectionChangeCommitted);
            // 
            // cbSnifferComPort
            // 
            this.cbSnifferComPort.DropDownWidth = 300;
            this.cbSnifferComPort.FormattingEnabled = true;
            resources.ApplyResources(this.cbSnifferComPort, "cbSnifferComPort");
            this.cbSnifferComPort.Name = "cbSnifferComPort";
            this.toolTip1.SetToolTip(this.cbSnifferComPort, resources.GetString("cbSnifferComPort.ToolTip"));
            this.cbSnifferComPort.DropDown += new System.EventHandler(this.DutComPort_DropDown);
            this.cbSnifferComPort.SelectionChangeCommitted += new System.EventHandler(this.comboBoxTSDUTComPort_SelectionChangeCommitted);
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.Name = "label9";
            // 
            // tLimitDirectory
            // 
            resources.ApplyResources(this.tLimitDirectory, "tLimitDirectory");
            this.tLimitDirectory.Name = "tLimitDirectory";
            this.tLimitDirectory.Click += new System.EventHandler(this.tLimitDirectory_Click);
            // 
            // label8
            // 
            resources.ApplyResources(this.label8, "label8");
            this.label8.Name = "label8";
            // 
            // nudLoad
            // 
            this.nudLoad.DecimalPlaces = 2;
            resources.ApplyResources(this.nudLoad, "nudLoad");
            this.nudLoad.Name = "nudLoad";
            this.toolTip1.SetToolTip(this.nudLoad, resources.GetString("nudLoad.ToolTip"));
            this.nudLoad.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label7
            // 
            resources.ApplyResources(this.label7, "label7");
            this.label7.Name = "label7";
            // 
            // cbPowerMeterComPort
            // 
            this.cbPowerMeterComPort.DropDownWidth = 300;
            this.cbPowerMeterComPort.FormattingEnabled = true;
            resources.ApplyResources(this.cbPowerMeterComPort, "cbPowerMeterComPort");
            this.cbPowerMeterComPort.Name = "cbPowerMeterComPort";
            this.toolTip1.SetToolTip(this.cbPowerMeterComPort, resources.GetString("cbPowerMeterComPort.ToolTip"));
            this.cbPowerMeterComPort.DropDown += new System.EventHandler(this.DutComPort_DropDown);
            this.cbPowerMeterComPort.SelectionChangeCommitted += new System.EventHandler(this.comboBoxTSCUComPort_SelectionChangeCommitted);
            // 
            // label6
            // 
            resources.ApplyResources(this.label6, "label6");
            this.label6.Name = "label6";
            // 
            // nudControlVoltageGain
            // 
            this.nudControlVoltageGain.DecimalPlaces = 1;
            this.nudControlVoltageGain.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            resources.ApplyResources(this.nudControlVoltageGain, "nudControlVoltageGain");
            this.nudControlVoltageGain.Name = "nudControlVoltageGain";
            this.toolTip1.SetToolTip(this.nudControlVoltageGain, resources.GetString("nudControlVoltageGain.ToolTip"));
            this.nudControlVoltageGain.Value = new decimal(new int[] {
            30,
            0,
            0,
            65536});
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.Name = "label5";
            // 
            // cbBluetoothComPort
            // 
            this.cbBluetoothComPort.DropDownWidth = 300;
            this.cbBluetoothComPort.FormattingEnabled = true;
            resources.ApplyResources(this.cbBluetoothComPort, "cbBluetoothComPort");
            this.cbBluetoothComPort.Name = "cbBluetoothComPort";
            this.toolTip1.SetToolTip(this.cbBluetoothComPort, resources.GetString("cbBluetoothComPort.ToolTip"));
            this.cbBluetoothComPort.DropDown += new System.EventHandler(this.DutComPort_DropDown);
            this.cbBluetoothComPort.SelectionChangeCommitted += new System.EventHandler(this.cbBlueToothComPort_SelectionChangeCommitted);
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // textBoxTSBlueRange
            // 
            resources.ApplyResources(this.textBoxTSBlueRange, "textBoxTSBlueRange");
            this.textBoxTSBlueRange.Name = "textBoxTSBlueRange";
            this.toolTip1.SetToolTip(this.textBoxTSBlueRange, resources.GetString("textBoxTSBlueRange.ToolTip"));
            // 
            // buttonCancel
            // 
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            resources.ApplyResources(this.buttonCancel, "buttonCancel");
            this.buttonCancel.Name = "buttonCancel";
            this.toolTip1.SetToolTip(this.buttonCancel, resources.GetString("buttonCancel.ToolTip"));
            this.buttonCancel.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            resources.ApplyResources(this.button1, "button1");
            this.button1.Name = "button1";
            this.toolTip1.SetToolTip(this.button1, resources.GetString("button1.ToolTip"));
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // numericUpDownTSPIRDLSOffDelay
            // 
            resources.ApplyResources(this.numericUpDownTSPIRDLSOffDelay, "numericUpDownTSPIRDLSOffDelay");
            this.numericUpDownTSPIRDLSOffDelay.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.numericUpDownTSPIRDLSOffDelay.Name = "numericUpDownTSPIRDLSOffDelay";
            this.toolTip1.SetToolTip(this.numericUpDownTSPIRDLSOffDelay, resources.GetString("numericUpDownTSPIRDLSOffDelay.ToolTip"));
            this.numericUpDownTSPIRDLSOffDelay.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            // 
            // label24
            // 
            resources.ApplyResources(this.label24, "label24");
            this.label24.Name = "label24";
            // 
            // label45
            // 
            resources.ApplyResources(this.label45, "label45");
            this.label45.Name = "label45";
            // 
            // numericUpDownTSPIRDLSOnDelay
            // 
            resources.ApplyResources(this.numericUpDownTSPIRDLSOnDelay, "numericUpDownTSPIRDLSOnDelay");
            this.numericUpDownTSPIRDLSOnDelay.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.numericUpDownTSPIRDLSOnDelay.Name = "numericUpDownTSPIRDLSOnDelay";
            this.toolTip1.SetToolTip(this.numericUpDownTSPIRDLSOnDelay, resources.GetString("numericUpDownTSPIRDLSOnDelay.ToolTip"));
            this.numericUpDownTSPIRDLSOnDelay.Value = new decimal(new int[] {
            2500,
            0,
            0,
            0});
            // 
            // label17
            // 
            resources.ApplyResources(this.label17, "label17");
            this.label17.Name = "label17";
            // 
            // label46
            // 
            resources.ApplyResources(this.label46, "label46");
            this.label46.Name = "label46";
            // 
            // label43
            // 
            resources.ApplyResources(this.label43, "label43");
            this.label43.Name = "label43";
            // 
            // label21
            // 
            resources.ApplyResources(this.label21, "label21");
            this.label21.Name = "label21";
            // 
            // label22
            // 
            resources.ApplyResources(this.label22, "label22");
            this.label22.Name = "label22";
            // 
            // label39
            // 
            resources.ApplyResources(this.label39, "label39");
            this.label39.Name = "label39";
            // 
            // label23
            // 
            resources.ApplyResources(this.label23, "label23");
            this.label23.Name = "label23";
            // 
            // label34
            // 
            resources.ApplyResources(this.label34, "label34");
            this.label34.Name = "label34";
            // 
            // label25
            // 
            resources.ApplyResources(this.label25, "label25");
            this.label25.Name = "label25";
            // 
            // label35
            // 
            resources.ApplyResources(this.label35, "label35");
            this.label35.Name = "label35";
            // 
            // numericUpDownTSAmbDLSOffDelay
            // 
            resources.ApplyResources(this.numericUpDownTSAmbDLSOffDelay, "numericUpDownTSAmbDLSOffDelay");
            this.numericUpDownTSAmbDLSOffDelay.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.numericUpDownTSAmbDLSOffDelay.Name = "numericUpDownTSAmbDLSOffDelay";
            this.toolTip1.SetToolTip(this.numericUpDownTSAmbDLSOffDelay, resources.GetString("numericUpDownTSAmbDLSOffDelay.ToolTip"));
            this.numericUpDownTSAmbDLSOffDelay.Value = new decimal(new int[] {
            500,
            0,
            0,
            0});
            // 
            // label36
            // 
            resources.ApplyResources(this.label36, "label36");
            this.label36.Name = "label36";
            // 
            // label37
            // 
            resources.ApplyResources(this.label37, "label37");
            this.label37.Name = "label37";
            // 
            // label26
            // 
            resources.ApplyResources(this.label26, "label26");
            this.label26.Name = "label26";
            // 
            // label42
            // 
            resources.ApplyResources(this.label42, "label42");
            this.label42.Name = "label42";
            // 
            // numericUpDownTSAmbDLSOnDelay
            // 
            resources.ApplyResources(this.numericUpDownTSAmbDLSOnDelay, "numericUpDownTSAmbDLSOnDelay");
            this.numericUpDownTSAmbDLSOnDelay.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.numericUpDownTSAmbDLSOnDelay.Name = "numericUpDownTSAmbDLSOnDelay";
            this.toolTip1.SetToolTip(this.numericUpDownTSAmbDLSOnDelay, resources.GetString("numericUpDownTSAmbDLSOnDelay.ToolTip"));
            this.numericUpDownTSAmbDLSOnDelay.Value = new decimal(new int[] {
            500,
            0,
            0,
            0});
            // 
            // label40
            // 
            resources.ApplyResources(this.label40, "label40");
            this.label40.Name = "label40";
            // 
            // label27
            // 
            resources.ApplyResources(this.label27, "label27");
            this.label27.Name = "label27";
            // 
            // label41
            // 
            resources.ApplyResources(this.label41, "label41");
            this.label41.Name = "label41";
            // 
            // label28
            // 
            resources.ApplyResources(this.label28, "label28");
            this.label28.Name = "label28";
            // 
            // numericUpDownTSAmbDLSLEDV
            // 
            this.numericUpDownTSAmbDLSLEDV.DecimalPlaces = 1;
            resources.ApplyResources(this.numericUpDownTSAmbDLSLEDV, "numericUpDownTSAmbDLSLEDV");
            this.numericUpDownTSAmbDLSLEDV.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numericUpDownTSAmbDLSLEDV.Name = "numericUpDownTSAmbDLSLEDV";
            this.toolTip1.SetToolTip(this.numericUpDownTSAmbDLSLEDV, resources.GetString("numericUpDownTSAmbDLSLEDV.ToolTip"));
            this.numericUpDownTSAmbDLSLEDV.Value = new decimal(new int[] {
            20,
            0,
            0,
            65536});
            // 
            // label29
            // 
            resources.ApplyResources(this.label29, "label29");
            this.label29.Name = "label29";
            // 
            // label30
            // 
            resources.ApplyResources(this.label30, "label30");
            this.label30.Name = "label30";
            // 
            // comboBoxTSAmbDLSType
            // 
            this.comboBoxTSAmbDLSType.FormattingEnabled = true;
            this.comboBoxTSAmbDLSType.Items.AddRange(new object[] {
            resources.GetString("comboBoxTSAmbDLSType.Items"),
            resources.GetString("comboBoxTSAmbDLSType.Items1")});
            resources.ApplyResources(this.comboBoxTSAmbDLSType, "comboBoxTSAmbDLSType");
            this.comboBoxTSAmbDLSType.Name = "comboBoxTSAmbDLSType";
            this.toolTip1.SetToolTip(this.comboBoxTSAmbDLSType, resources.GetString("comboBoxTSAmbDLSType.ToolTip"));
            this.comboBoxTSAmbDLSType.SelectedIndexChanged += new System.EventHandler(this.comboBoxTSAmbDLSType_SelectedIndexChanged);
            // 
            // label38
            // 
            resources.ApplyResources(this.label38, "label38");
            this.label38.Name = "label38";
            // 
            // textBoxTSGreenRange
            // 
            resources.ApplyResources(this.textBoxTSGreenRange, "textBoxTSGreenRange");
            this.textBoxTSGreenRange.Name = "textBoxTSGreenRange";
            this.toolTip1.SetToolTip(this.textBoxTSGreenRange, resources.GetString("textBoxTSGreenRange.ToolTip"));
            // 
            // textBoxTSRedRange
            // 
            resources.ApplyResources(this.textBoxTSRedRange, "textBoxTSRedRange");
            this.textBoxTSRedRange.Name = "textBoxTSRedRange";
            this.toolTip1.SetToolTip(this.textBoxTSRedRange, resources.GetString("textBoxTSRedRange.ToolTip"));
            // 
            // comboBoxTSCUComPort
            // 
            this.comboBoxTSCUComPort.DropDownWidth = 300;
            this.comboBoxTSCUComPort.FormattingEnabled = true;
            resources.ApplyResources(this.comboBoxTSCUComPort, "comboBoxTSCUComPort");
            this.comboBoxTSCUComPort.Name = "comboBoxTSCUComPort";
            this.toolTip1.SetToolTip(this.comboBoxTSCUComPort, resources.GetString("comboBoxTSCUComPort.ToolTip"));
            this.comboBoxTSCUComPort.DropDown += new System.EventHandler(this.DutComPort_DropDown);
            this.comboBoxTSCUComPort.SelectionChangeCommitted += new System.EventHandler(this.comboBoxTSDUTComPort_SelectionChangeCommitted);
            // 
            // comboBoxTSTestRadioComPort
            // 
            this.comboBoxTSTestRadioComPort.DropDownWidth = 300;
            this.comboBoxTSTestRadioComPort.FormattingEnabled = true;
            resources.ApplyResources(this.comboBoxTSTestRadioComPort, "comboBoxTSTestRadioComPort");
            this.comboBoxTSTestRadioComPort.Name = "comboBoxTSTestRadioComPort";
            this.toolTip1.SetToolTip(this.comboBoxTSTestRadioComPort, resources.GetString("comboBoxTSTestRadioComPort.ToolTip"));
            this.comboBoxTSTestRadioComPort.DropDown += new System.EventHandler(this.DutComPort_DropDown);
            this.comboBoxTSTestRadioComPort.SelectionChangeCommitted += new System.EventHandler(this.cbRefRadioComPort_SelectionChangeCommitted);
            // 
            // comboBoxTSDUTComPort
            // 
            this.comboBoxTSDUTComPort.DropDownWidth = 300;
            this.comboBoxTSDUTComPort.FormattingEnabled = true;
            resources.ApplyResources(this.comboBoxTSDUTComPort, "comboBoxTSDUTComPort");
            this.comboBoxTSDUTComPort.Name = "comboBoxTSDUTComPort";
            this.toolTip1.SetToolTip(this.comboBoxTSDUTComPort, resources.GetString("comboBoxTSDUTComPort.ToolTip"));
            this.comboBoxTSDUTComPort.DropDown += new System.EventHandler(this.DutComPort_DropDown);
            this.comboBoxTSDUTComPort.SelectionChangeCommitted += new System.EventHandler(this.comboBoxTSDUTComPort_SelectionChangeCommitted);
            // 
            // comboBoxTSLEDDetectorComPort
            // 
            this.comboBoxTSLEDDetectorComPort.DropDownWidth = 300;
            this.comboBoxTSLEDDetectorComPort.FormattingEnabled = true;
            resources.ApplyResources(this.comboBoxTSLEDDetectorComPort, "comboBoxTSLEDDetectorComPort");
            this.comboBoxTSLEDDetectorComPort.Name = "comboBoxTSLEDDetectorComPort";
            this.toolTip1.SetToolTip(this.comboBoxTSLEDDetectorComPort, resources.GetString("comboBoxTSLEDDetectorComPort.ToolTip"));
            this.comboBoxTSLEDDetectorComPort.DropDown += new System.EventHandler(this.DutComPort_DropDown);
            this.comboBoxTSLEDDetectorComPort.SelectionChangeCommitted += new System.EventHandler(this.cbLEDSensorComPort_SelectionChangeCommitted);
            // 
            // comboBoxTSLEDDetectorType
            // 
            this.comboBoxTSLEDDetectorType.FormattingEnabled = true;
            this.comboBoxTSLEDDetectorType.Items.AddRange(new object[] {
            resources.GetString("comboBoxTSLEDDetectorType.Items"),
            resources.GetString("comboBoxTSLEDDetectorType.Items1")});
            resources.ApplyResources(this.comboBoxTSLEDDetectorType, "comboBoxTSLEDDetectorType");
            this.comboBoxTSLEDDetectorType.Name = "comboBoxTSLEDDetectorType";
            this.toolTip1.SetToolTip(this.comboBoxTSLEDDetectorType, resources.GetString("comboBoxTSLEDDetectorType.ToolTip"));
            this.comboBoxTSLEDDetectorType.SelectedIndexChanged += new System.EventHandler(this.comboBoxTSLEDDetectorType_SelectedIndexChanged);
            // 
            // label33
            // 
            resources.ApplyResources(this.label33, "label33");
            this.label33.Name = "label33";
            // 
            // comboBoxTSProductPrinter
            // 
            this.comboBoxTSProductPrinter.FormattingEnabled = true;
            resources.ApplyResources(this.comboBoxTSProductPrinter, "comboBoxTSProductPrinter");
            this.comboBoxTSProductPrinter.Name = "comboBoxTSProductPrinter";
            this.toolTip1.SetToolTip(this.comboBoxTSProductPrinter, resources.GetString("comboBoxTSProductPrinter.ToolTip"));
            this.comboBoxTSProductPrinter.DropDown += new System.EventHandler(this.buttonFindBarcodeFile_Click);
            // 
            // comboBoxTSPCBAPrinter
            // 
            this.comboBoxTSPCBAPrinter.FormattingEnabled = true;
            resources.ApplyResources(this.comboBoxTSPCBAPrinter, "comboBoxTSPCBAPrinter");
            this.comboBoxTSPCBAPrinter.Name = "comboBoxTSPCBAPrinter";
            this.toolTip1.SetToolTip(this.comboBoxTSPCBAPrinter, resources.GetString("comboBoxTSPCBAPrinter.ToolTip"));
            this.comboBoxTSPCBAPrinter.DropDown += new System.EventHandler(this.buttonFindBarcodeFile_Click);
            // 
            // textBoxTSCUVersionTag
            // 
            resources.ApplyResources(this.textBoxTSCUVersionTag, "textBoxTSCUVersionTag");
            this.textBoxTSCUVersionTag.Name = "textBoxTSCUVersionTag";
            this.toolTip1.SetToolTip(this.textBoxTSCUVersionTag, resources.GetString("textBoxTSCUVersionTag.ToolTip"));
            // 
            // comboBoxTSCUType
            // 
            this.comboBoxTSCUType.FormattingEnabled = true;
            this.comboBoxTSCUType.Items.AddRange(new object[] {
            resources.GetString("comboBoxTSCUType.Items"),
            resources.GetString("comboBoxTSCUType.Items1"),
            resources.GetString("comboBoxTSCUType.Items2")});
            resources.ApplyResources(this.comboBoxTSCUType, "comboBoxTSCUType");
            this.comboBoxTSCUType.Name = "comboBoxTSCUType";
            this.toolTip1.SetToolTip(this.comboBoxTSCUType, resources.GetString("comboBoxTSCUType.ToolTip"));
            this.comboBoxTSCUType.SelectedValueChanged += new System.EventHandler(this.comboBoxTSCUType_SelectedValueChanged);
            // 
            // numericUpDownTSTestChannel
            // 
            resources.ApplyResources(this.numericUpDownTSTestChannel, "numericUpDownTSTestChannel");
            this.numericUpDownTSTestChannel.Maximum = new decimal(new int[] {
            26,
            0,
            0,
            0});
            this.numericUpDownTSTestChannel.Minimum = new decimal(new int[] {
            11,
            0,
            0,
            0});
            this.numericUpDownTSTestChannel.Name = "numericUpDownTSTestChannel";
            this.toolTip1.SetToolTip(this.numericUpDownTSTestChannel, resources.GetString("numericUpDownTSTestChannel.ToolTip"));
            this.numericUpDownTSTestChannel.Value = new decimal(new int[] {
            11,
            0,
            0,
            0});
            // 
            // comboBoxTSTempProbe
            // 
            this.comboBoxTSTempProbe.FormattingEnabled = true;
            this.comboBoxTSTempProbe.Items.AddRange(new object[] {
            resources.GetString("comboBoxTSTempProbe.Items"),
            resources.GetString("comboBoxTSTempProbe.Items1")});
            resources.ApplyResources(this.comboBoxTSTempProbe, "comboBoxTSTempProbe");
            this.comboBoxTSTempProbe.Name = "comboBoxTSTempProbe";
            this.toolTip1.SetToolTip(this.comboBoxTSTempProbe, resources.GetString("comboBoxTSTempProbe.ToolTip"));
            // 
            // textBoxTSStationOperation
            // 
            resources.ApplyResources(this.textBoxTSStationOperation, "textBoxTSStationOperation");
            this.textBoxTSStationOperation.Name = "textBoxTSStationOperation";
            this.toolTip1.SetToolTip(this.textBoxTSStationOperation, resources.GetString("textBoxTSStationOperation.ToolTip"));
            // 
            // textBoxTSStationTitle
            // 
            resources.ApplyResources(this.textBoxTSStationTitle, "textBoxTSStationTitle");
            this.textBoxTSStationTitle.Name = "textBoxTSStationTitle";
            this.toolTip1.SetToolTip(this.textBoxTSStationTitle, resources.GetString("textBoxTSStationTitle.ToolTip"));
            // 
            // label18
            // 
            resources.ApplyResources(this.label18, "label18");
            this.label18.Name = "label18";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // lbStationSection
            // 
            resources.ApplyResources(this.lbStationSection, "lbStationSection");
            this.lbStationSection.Name = "lbStationSection";
            // 
            // lbStationDB
            // 
            resources.ApplyResources(this.lbStationDB, "lbStationDB");
            this.lbStationDB.Name = "lbStationDB";
            // 
            // lbFixtureType
            // 
            resources.ApplyResources(this.lbFixtureType, "lbFixtureType");
            this.lbFixtureType.Name = "lbFixtureType";
            // 
            // Config_SU_SY2_Fixture
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ControlBox = false;
            this.Controls.Add(this.lbFixtureType);
            this.Controls.Add(this.lbStationDB);
            this.Controls.Add(this.lbStationSection);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.label18);
            this.Name = "Config_SU_SY2_Fixture";
            this.Load += new System.EventHandler(this.SetupForm);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudLoad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudControlVoltageGain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTSPIRDLSOffDelay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTSPIRDLSOnDelay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTSAmbDLSOffDelay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTSAmbDLSOnDelay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTSAmbDLSLEDV)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTSTestChannel)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.NumericUpDown numericUpDownTSPIRDLSOffDelay;
        private System.Windows.Forms.NumericUpDown numericUpDownTSPIRDLSOnDelay;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.NumericUpDown numericUpDownTSAmbDLSOffDelay;
        private System.Windows.Forms.NumericUpDown numericUpDownTSAmbDLSOnDelay;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.NumericUpDown numericUpDownTSAmbDLSLEDV;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.ComboBox comboBoxTSAmbDLSType;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox textBoxTSGreenRange;
        private System.Windows.Forms.TextBox textBoxTSRedRange;
        private System.Windows.Forms.ComboBox comboBoxTSCUComPort;
        private System.Windows.Forms.ComboBox comboBoxTSTestRadioComPort;
        private System.Windows.Forms.ComboBox comboBoxTSDUTComPort;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.ComboBox comboBoxTSLEDDetectorComPort;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.ComboBox comboBoxTSLEDDetectorType;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.ComboBox comboBoxTSProductPrinter;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.ComboBox comboBoxTSPCBAPrinter;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox textBoxTSCUVersionTag;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.ComboBox comboBoxTSCUType;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.NumericUpDown numericUpDownTSTestChannel;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.ComboBox comboBoxTSTempProbe;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox textBoxTSStationOperation;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox textBoxTSStationTitle;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lbStationSection;
        private System.Windows.Forms.Label lbStationDB;
        private System.Windows.Forms.Label lbFixtureType;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxTSBlueRange;
        private System.Windows.Forms.ComboBox cbBluetoothComPort;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown nudControlVoltageGain;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cbPowerMeterComPort;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown nudLoad;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.TextBox tLimitDirectory;
        private System.Windows.Forms.ComboBox cbSnifferComPort;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cbSwitchMateCom;
        private System.Windows.Forms.Button bAutoFindTestRadioComPort;
        private System.Windows.Forms.Button bAutoFindPMComPort;
        private System.Windows.Forms.Button bAutoFindCUComPort;
        private System.Windows.Forms.Button bAutofindLEDComPort;
        private System.Windows.Forms.Button bAutoFindSMComPort;
        private System.Windows.Forms.Button bAutoFindBLEComPort;
        private System.Windows.Forms.Button bAutoFindSnifferComPort;
        private System.Windows.Forms.Button vAutoFindDUTComPort;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox tbLabelDirectory;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button bAutoFindTagComPort;
        private System.Windows.Forms.ComboBox cbTagComPort;
    }
}