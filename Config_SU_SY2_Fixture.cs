﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.IO.Ports;
using System.Drawing.Printing;
using System.Management;
using System.Diagnostics;

using LimitsDatabase;

namespace SensorManufSY2
{
    public partial class Config_SU_SY2_Fixture : Form
    {
        StationConfigure lclStationConfig;
        string fixtureInUse;
        FixtureFunctions lclFixtureFunctions;

        public Config_SU_SY2_Fixture(string fixture)
        {
            InitializeComponent();
            fixtureInUse = fixture;
        }

        private void SetupForm(object sender, EventArgs e)
        {
            string outmsg = string.Empty;

            lclStationConfig = new StationConfigure();      // make emtpy def to open FixtureFunctions

            lclFixtureFunctions = new FixtureFunctions();
            outmsg = LoadFormData(outmsg);
        }

        private string LoadFormData(string outmsg)
        {
            int index;
            FixtureFunctions.fixtureTypes thisFixtureList = new FixtureFunctions.fixtureTypes();

            if (lclStationConfig.ReadStationConfigFile(this.fixtureInUse, ref outmsg) != 0)
            {
                MessageBox.Show(outmsg);
                return outmsg;
            }

            index = lclFixtureFunctions.fixtureTypeList.FindIndex(x => x.fixtureType == lclStationConfig.ConfigFixtureType);
            if (index > lclFixtureFunctions.fixtureTypeList.Count)       // if did not find it
            {
                outmsg = "Fixture code " + lclStationConfig.ConfigFixtureType;
                MessageBox.Show(outmsg);
                return outmsg;
            }
            thisFixtureList = lclFixtureFunctions.fixtureTypeList[index];

            // these are not changable by the user
            lbFixtureType.Text = lclStationConfig.ConfigFixtureType;    // hardware fixture type
            lbStationDB.Text = lclStationConfig.ConfigStationToUse;     // database station to use
            lbStationSection.Text = lclStationConfig.ConfigFixtureName; // Fixture name

            // used by all stations
            textBoxTSStationTitle.Text = lclStationConfig.StationTitle;
            textBoxTSStationOperation.Text = lclStationConfig.ConfigStationOperation;
            tLimitDirectory.Text = lclStationConfig.ConfigFixtureLimitFileDirectory;
            tbLabelDirectory.Text = lclStationConfig.ConfigFixtureLabelFileDirectory;

            // dut console port
            if (thisFixtureList.hasConsolePort)
            {
                comboBoxTSDUTComPort.Text = lclStationConfig.ConfigDUTComPort;
                comboBoxTSDUTComPort.Visible = true;
            }
            else
            {
                comboBoxTSDUTComPort.Visible = false;
            }

            // test radio
            if (thisFixtureList.hasTestRadio)
            {
                numericUpDownTSTestChannel.Value = Convert.ToInt32(lclStationConfig.ConfigTestRadioChannel);
                comboBoxTSTestRadioComPort.Text = lclStationConfig.ConfigTestRadioComPort;
                numericUpDownTSTestChannel.Visible = true;
                comboBoxTSTestRadioComPort.Visible = true;
                bAutoFindTestRadioComPort.Visible = true;
            }
            else
            {
                numericUpDownTSTestChannel.Visible = false;
                comboBoxTSTestRadioComPort.Visible = false;
                bAutoFindTestRadioComPort.Visible = false;
            }

            // sniffer port
            if (thisFixtureList.hasSniffer)
            {
                cbSnifferComPort.Text = lclStationConfig.ConfigSnifferComPort;
                cbSnifferComPort.Visible = true;
                bAutoFindSnifferComPort.Visible = true;
            }
            else
            {
                cbSnifferComPort.Visible = false;
                bAutoFindSnifferComPort.Visible = false;
            }

            // Switch-Mate
            if (thisFixtureList.hasSwitchMate)
            {
                cbSwitchMateCom.Text = lclStationConfig.ConfigSwitchMateComPort;
                cbSwitchMateCom.Visible = true;
                bAutoFindSMComPort.Visible = true;
            }
            else
            {
                cbSwitchMateCom.Visible = false;
                bAutoFindSMComPort.Visible = false;
            }

            // printers
            if (thisFixtureList.hasPCBAPrinter)
            {
                comboBoxTSPCBAPrinter.Text = lclStationConfig.ConfigPCBALabelPrinter;
                comboBoxTSPCBAPrinter.Visible = true;
            }
            else
            {
                comboBoxTSPCBAPrinter.Visible = false;
            }
            if (thisFixtureList.hasHLAPrinter)
            {
                comboBoxTSProductPrinter.Text = lclStationConfig.ConfigProductLabelPrinter;
                comboBoxTSProductPrinter.Visible = true;
            }
            else
            {
                comboBoxTSProductPrinter.Visible = false;
            }

            // LED detector
            if (thisFixtureList.hasLEDDetector)
            {
                comboBoxTSLEDDetectorType.Text = lclStationConfig.ConfigLedDetector.Type;
                if (comboBoxTSLEDDetectorType.Text == "USB")
                {
                    comboBoxTSLEDDetectorComPort.Text = lclStationConfig.ConfigLedDetector.ComPort;
                    comboBoxTSLEDDetectorComPort.Visible = true;
                    bAutofindLEDComPort.Visible = true;
                }
                else
                {
                    comboBoxTSLEDDetectorComPort.Visible = false;
                    bAutofindLEDComPort.Visible = false;
                }
                textBoxTSRedRange.Text = lclStationConfig.ConfigLedDetector.ColorRED;
                textBoxTSGreenRange.Text = lclStationConfig.ConfigLedDetector.ColorGREEN;
                textBoxTSBlueRange.Text = lclStationConfig.ConfigLedDetector.ColorBLUE;
            }
            else
            {
                comboBoxTSLEDDetectorComPort.Visible = false;
                comboBoxTSLEDDetectorType.Visible = false;
                bAutofindLEDComPort.Visible = false;
                textBoxTSRedRange.Visible = false;
                textBoxTSGreenRange.Visible = false;
                textBoxTSBlueRange.Visible = false;
            }

            // used by Sensor type stations
            if (thisFixtureList.hasTempProbe)
            {
                comboBoxTSTempProbe.Text = lclStationConfig.ConfigTempSensorOutput;
                comboBoxTSTempProbe.Visible = true;
            }
            else
                comboBoxTSTempProbe.Visible = false;

            // Sensors that have bluetooth
            if (thisFixtureList.hasBLE)
            {
                cbBluetoothComPort.Text = lclStationConfig.ConfigBLEComPort;
                cbTagComPort.Text = lclStationConfig.ConfigTagComPort;
                cbBluetoothComPort.Visible = true;
                bAutoFindBLEComPort.Visible = true;
                cbTagComPort.Visible = true;
            }
            else
            {
                cbBluetoothComPort.Visible = false;
                bAutoFindBLEComPort.Visible = false;
                cbTagComPort.Visible = false;
            }

            // Sensors with Enlighted/Dali interfaces
            if ((thisFixtureList.hasDaliInterface) | (thisFixtureList.hasEnlightedInterface))
            {
                comboBoxTSCUType.Text = lclStationConfig.ConfigCUType;
                textBoxTSCUVersionTag.Text = lclStationConfig.ConfigCUVersionTag;
                comboBoxTSCUType.Visible = true;
                comboBoxTSCUType.Enabled = false;           // this is not a field that can be edited. based on fixture type
                textBoxTSCUVersionTag.Visible = true;
                // if enlighted interface, it has a cu emulator and control voltage
                if (thisFixtureList.hasEnlightedInterface)
                {
                    comboBoxTSCUComPort.Text = lclStationConfig.ConfigCUComPort;
                    nudControlVoltageGain.Value = Convert.ToDecimal(lclStationConfig.ConfigLjChannelControlVGain);
                    comboBoxTSCUComPort.Visible = true;
                    nudControlVoltageGain.Visible = true;
                }
                else
                {
                    comboBoxTSCUComPort.Visible = false;
                    nudControlVoltageGain.Visible = false;
                    bAutoFindCUComPort.Visible = false;
                }
            }
            else
            {
                comboBoxTSCUType.Visible = false;
                comboBoxTSCUComPort.Visible = false;
                textBoxTSCUVersionTag.Visible = false;
                nudControlVoltageGain.Visible = false;
                bAutoFindCUComPort.Visible = false;
            }

            // Sensors with ambient sensor
            if (thisFixtureList.hasAmbientSensor)
            {
                comboBoxTSAmbDLSType.Text = lclStationConfig.ConfigAmbientDetectorLightSource.Type;
                if (lclStationConfig.ConfigAmbientDetectorLightSource.Type.ToUpper() == "REDLED")
                {
                    numericUpDownTSAmbDLSLEDV.Text = lclStationConfig.ConfigAmbientDetectorLightSource.Voltage.ToString("N3");
                    numericUpDownTSAmbDLSLEDV.Visible = true;
                }
                else
                    numericUpDownTSAmbDLSLEDV.Visible = false;
                numericUpDownTSAmbDLSOnDelay.Value = lclStationConfig.ConfigAmbientDetectorLightSource.OnDelayMs;
                numericUpDownTSAmbDLSOffDelay.Value = lclStationConfig.ConfigAmbientDetectorLightSource.OffDelayMs;
                comboBoxTSAmbDLSType.Visible = true;
                numericUpDownTSAmbDLSOnDelay.Visible = true;
                numericUpDownTSAmbDLSOffDelay.Visible = true;
            }
            else
            {
                comboBoxTSAmbDLSType.Visible = false;
                numericUpDownTSAmbDLSLEDV.Visible = false;
                numericUpDownTSAmbDLSOnDelay.Visible = false;
                numericUpDownTSAmbDLSOffDelay.Visible = false;

            }

            // sensors with PIR sensor
            if (thisFixtureList.hasPirSensor)
            {
                numericUpDownTSPIRDLSOnDelay.Value = lclStationConfig.ConfigPIRDetectorLightSource.OnDelayMs;
                numericUpDownTSPIRDLSOffDelay.Value = lclStationConfig.ConfigPIRDetectorLightSource.OffDelayMs;
                numericUpDownTSPIRDLSOnDelay.Visible = true;
                numericUpDownTSPIRDLSOffDelay.Visible = true;
            }
            else
            {
                numericUpDownTSPIRDLSOnDelay.Visible = false;
                numericUpDownTSPIRDLSOffDelay.Visible = false;
            }

            // fixtures with CU power meter
            if (thisFixtureList.hasPowerMeter)
            {
                cbPowerMeterComPort.Text = lclStationConfig.ConfigPowerMeterComPort;
                cbPowerMeterComPort.Visible = true;
            }
            else
            {
                cbPowerMeterComPort.Visible = false;
                bAutoFindPMComPort.Visible = false;
            }

            // fixtures has a load
            if (thisFixtureList.hasPowerLoad)
            {
                nudLoad.Value = Convert.ToDecimal(lclStationConfig.ConfigFixtureLoadA);
                nudLoad.Visible = true;
            }
            else
            {
                nudLoad.Visible = false;
            }

            return outmsg;
        }

        private void FixtureToConfig_SelectedIndexChanged(object sender, EventArgs e)
        {
            string outmsg = string.Empty;

            LoadFormData(outmsg);
        }

        // save button
        private void button1_Click(object sender, EventArgs e)
        {

            lclStationConfig.StationTitle = textBoxTSStationTitle.Text;
            lclStationConfig.ConfigStationOperation = textBoxTSStationOperation.Text;
            lclStationConfig.ConfigTempSensorOutput = comboBoxTSTempProbe.Text;
            lclStationConfig.ConfigFixtureLimitFileDirectory = tLimitDirectory.Text;
            lclStationConfig.ConfigFixtureLabelFileDirectory = tbLabelDirectory.Text;

            lclStationConfig.ConfigDUTComPort = comboBoxTSDUTComPort.Text;

            lclStationConfig.ConfigTestRadioComPort = comboBoxTSTestRadioComPort.Text;
            lclStationConfig.ConfigTestRadioChannel = numericUpDownTSTestChannel.Value.ToString();

            lclStationConfig.ConfigSwitchMateComPort = cbSwitchMateCom.Text;

            lclStationConfig.ConfigBLEComPort = cbBluetoothComPort.Text;
            lclStationConfig.ConfigTagComPort = cbTagComPort.Text;

            lclStationConfig.ConfigCUType = comboBoxTSCUType.Text;
            lclStationConfig.ConfigCUComPort = comboBoxTSCUComPort.Text;
            lclStationConfig.ConfigCUVersionTag = textBoxTSCUVersionTag.Text;

            lclStationConfig.ConfigPowerMeterComPort = cbPowerMeterComPort.Text;
            lclStationConfig.ConfigFixtureLoadA = Convert.ToDouble(nudLoad.Value);

            lclStationConfig.ConfigSnifferComPort = cbSnifferComPort.Text;

            lclStationConfig.ConfigLjChannelControlVGain = Convert.ToDouble(nudControlVoltageGain.Value);

            lclStationConfig.ConfigPCBALabelPrinter = comboBoxTSPCBAPrinter.Text;
            lclStationConfig.ConfigProductLabelPrinter = comboBoxTSProductPrinter.Text;

            lclStationConfig.ConfigLedDetector.Type = comboBoxTSLEDDetectorType.Text;
            lclStationConfig.ConfigLedDetector.ComPort = comboBoxTSLEDDetectorComPort.Text;
            lclStationConfig.ConfigLedDetector.ColorRED = textBoxTSRedRange.Text;
            lclStationConfig.ConfigLedDetector.ColorGREEN = textBoxTSGreenRange.Text;
            lclStationConfig.ConfigLedDetector.ColorBLUE = textBoxTSBlueRange.Text;

            lclStationConfig.ConfigAmbientDetectorLightSource.Type = comboBoxTSAmbDLSType.Text;
            lclStationConfig.ConfigAmbientDetectorLightSource.Voltage = Convert.ToDouble(numericUpDownTSAmbDLSLEDV.Value);
            lclStationConfig.ConfigAmbientDetectorLightSource.OnDelayMs = Convert.ToInt32(numericUpDownTSAmbDLSOnDelay.Value);
            lclStationConfig.ConfigAmbientDetectorLightSource.OffDelayMs = Convert.ToInt32(numericUpDownTSAmbDLSOffDelay.Value);

            lclStationConfig.ConfigPIRDetectorLightSource.OnDelayMs = Convert.ToInt32(numericUpDownTSPIRDLSOnDelay.Value);
            lclStationConfig.ConfigPIRDetectorLightSource.OffDelayMs = Convert.ToInt32(numericUpDownTSPIRDLSOffDelay.Value);

            lclStationConfig.UpdateFixtureData(fixtureInUse);
        }


        private void DutComPort_DropDown(object sender, EventArgs e)
        {
            string[] ports = SerialPort.GetPortNames();         // get list of serial ports
            ComboBox mybutton = sender as ComboBox;

            mybutton.Items.Clear();
            //foreach (var item in ports)
            //{
            //    mybutton.Items.Add(item.ToString());
            //}

            try
            {
                ManagementObjectSearcher searcher =
                    new ManagementObjectSearcher("root\\CIMV2",
                    "SELECT * FROM Win32_PnPEntity");

                foreach (ManagementObject queryObj in searcher.Get())
                {
                    if (queryObj["Caption"] != null)
                    {
                        if (queryObj["Caption"].ToString().Contains("(COM"))
                        {
                            mybutton.Items.Add(queryObj["Caption"]);
                        }
                    }
                }
            }
            catch (ManagementException exx)
            {
                MessageBox.Show(exx.Message);
            }
        }

        private void buttonFindBarcodeFile_Click(object sender, EventArgs e)
        {

            PrintDocument prtdoc = new PrintDocument();
            ComboBox myprinter = sender as ComboBox;

            //prt.PrinterSettings.PrinterName returns the name of the Default Printer
            string strDefaultPrinter = prtdoc.PrinterSettings.PrinterName;

            //this will loop through all the Installed printers and add the Printer Names to a ComboBox.
            foreach (String strPrinter in PrinterSettings.InstalledPrinters)
            {
                myprinter.Items.Add(strPrinter);

                //This will set the ComboBox Index where the Default Printer Name matches with the current Printer Name returned by for loop
                if (strPrinter.CompareTo(strDefaultPrinter) == 0)
                {
                    myprinter.SelectedIndex = myprinter.Items.IndexOf(strPrinter);
                }
            }

        }

        private void comboBoxTSDUTComPort_SelectionChangeCommitted(object sender, EventArgs e)
        {
            string temp = string.Empty;
            int start = 0;
            StationConfigure myStationConfig;
             ComboBox mybutton = sender as ComboBox;


            using (FixtureFunctions fixture = new FixtureFunctions())
            {
                myStationConfig = lclStationConfig;                         // make a scratch copy for the init
                myStationConfig.FixtureEquipment.hasConsolePort = false;    // do not init console
                myStationConfig.FixtureEquipment.hasTestRadio = false;      // do not init the test radio
                myStationConfig.FixtureEquipment.hasBLE = false;            // do not init the tag radio
                if (cbSwitchMateCom.Text == "COM0")      // if the switch-mate has not been config
                {
                    MessageBox.Show("Switch-Mate must be configured first");
                    comboBoxTSDUTComPort.BackColor = Color.White;
                    mybutton.Text = "Auto Find";
                    return;
                }
                myStationConfig.ConfigSwitchMateComPort = cbSwitchMateCom.Text;     // load current switch mate port
                fixture.StationConfigure = myStationConfig;
                if (!fixture.InitFixture())
                {
                    MessageBox.Show("Error initializing the fixture. " + fixture.LastFunctionErrorMessage);
                }
                else
                {
                    fixture.ControlDUTPower(1);         // turn on DUT
                    start = mybutton.SelectedItem.ToString().IndexOf("(COM") + 1;
                    temp = mybutton.SelectedItem.ToString().Substring(start, mybutton.SelectedItem.ToString().Length - start - 1);
                    mybutton.Items.Clear();
                    mybutton.Items.Add(temp);
                    mybutton.SelectedIndex = 0;
                    displayComWindow(temp, "115200");
                    fixture.ControlDUTPower(0);         // turn off DUT
                }
            }
        }

        private void comboBoxTSSwitchMateComPort_SelectionChangeCommitted(object sender, EventArgs e)
        {
            string temp = string.Empty;
            int start = 0;
            SerialPort smport = new SerialPort();

            ComboBox mybutton = sender as ComboBox;
            start = mybutton.SelectedItem.ToString().IndexOf("(COM") + 1;
            temp = mybutton.SelectedItem.ToString().Substring(start, mybutton.SelectedItem.ToString().Length - start - 1);
            mybutton.Items.Clear();
            mybutton.Items.Add(temp);
            mybutton.SelectedIndex = 0;

            smport.BaudRate = 19200;
            smport.PortName = temp;
            smport.NewLine = "\r";
            smport.Open();
            smport.WriteLine("SM_ID?");
            smport.ReadTimeout = 1000;
            try
            {
                if (!(temp = smport.ReadLine()).Contains("SWITCH-MATE"))
                {
                    MessageBox.Show("Not the Switch-Mate port. Found: " + temp + "\n");
                    mybutton.BackColor = Color.Red;
                }
                else
                { 
                    MessageBox.Show("Correct port");
                    mybutton.BackColor = Color.Green;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Not the Switch-Mate port. Error: " + ex.Message);
                mybutton.BackColor = Color.Red;
            }
            smport.Close();
        }

        private void cbRefRadioComPort_SelectionChangeCommitted(object sender, EventArgs e)
        {
            string temp = string.Empty;
            int start = 0;
            SerialPort smport = new SerialPort();

            ComboBox mybutton = sender as ComboBox;
            start = mybutton.SelectedItem.ToString().IndexOf("(COM") + 1;
            temp = mybutton.SelectedItem.ToString().Substring(start, mybutton.SelectedItem.ToString().Length - start - 1);
            mybutton.Items.Clear();
            mybutton.Items.Add(temp);
            mybutton.SelectedIndex = 0;

            smport.BaudRate = 115200;
            smport.PortName = temp;
            smport.ReadTimeout = 1000;
            smport.Open();
            try
            {
                smport.WriteLine("d v");
                smport.ReadLine();          // get the echo
            }
            catch (Exception ex)
            {
                MessageBox.Show("Not the Ref Radio port. Error: " + ex.Message);
                mybutton.BackColor = Color.Red;
                return;
            }
            try
            {
                if (!(temp = smport.ReadLine()).Contains("Running"))
                {
                    MessageBox.Show("Not the Ref Radio port. Found: " + temp + "\n");
                    mybutton.BackColor = Color.Red;
                }
                else
                {
                    MessageBox.Show("Correct port");
                    mybutton.BackColor = Color.Green;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Not the Ref Radio port. Error: " + ex.Message);
                mybutton.BackColor = Color.Red;
            }
            smport.Close();
        }

        private void cbBlueToothComPort_SelectionChangeCommitted(object sender, EventArgs e)
        {
            string temp = string.Empty;
            int start = 0;

            ComboBox mybutton = sender as ComboBox;
            start = mybutton.SelectedItem.ToString().IndexOf("(COM") + 1;
            temp = mybutton.SelectedItem.ToString().Substring(start, mybutton.SelectedItem.ToString().Length - start - 1);
            mybutton.Items.Clear();
            mybutton.Items.Add(temp);
            mybutton.SelectedIndex = 0;

            BluetoothFunctions bt = new BluetoothFunctions(temp, null, null);
            if (bt.OpenBLEMonitor(ref temp) != (int)ErrorCodes.Program.Success)
            {
                MessageBox.Show("Not the Blue Tooth port.");
                mybutton.BackColor = Color.Red;
            }
            else
            {
                if (bt.dongleInit(ref temp))
                {
                    MessageBox.Show("Is the Blue Tooth dongle. " + temp);
                    mybutton.BackColor = Color.Green;
                }
                else
                {
                    MessageBox.Show("Not the Blue Tooth dongle. " + temp);
                    mybutton.BackColor = Color.Red;
                }
                bt.CloseBLEMonitor();
            }
        }

        private void cbBLETagComPort_SelectionChangeCommitted(object sender, EventArgs e)
        {
            string temp = string.Empty;
            int start = 0;
            SerialPort tagport = new SerialPort();

            ComboBox mybutton = sender as ComboBox;
            start = mybutton.SelectedItem.ToString().IndexOf("(COM") + 1;
            temp = mybutton.SelectedItem.ToString().Substring(start, mybutton.SelectedItem.ToString().Length - start - 1);
            mybutton.Items.Clear();
            mybutton.Items.Add(temp);
            mybutton.SelectedIndex = 0;

            tagport.BaudRate = 115200;
            tagport.PortName = temp;
            tagport.Open();
            tagport.WriteLine("");
            tagport.ReadTimeout = 1000;
            try
            {
                if (!(temp = tagport.ReadLine()).Contains("SU"))
                {
                    MessageBox.Show("Not the BLE Tag port. Found: " + temp + "\n");
                    mybutton.BackColor = Color.Red;
                }
                else
                {
                    MessageBox.Show("Correct port");
                    mybutton.BackColor = Color.Green;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Not the BLE Tag port. Error: " + ex.Message);
                mybutton.BackColor = Color.Red;
            }
            tagport.Close();
        }


        private void cbLEDSensorComPort_SelectionChangeCommitted(object sender, EventArgs e)
        {
            string temp = string.Empty;
            int start = 0;
            SerialPort ledport = new SerialPort();

            ComboBox mybutton = sender as ComboBox;
            start = mybutton.SelectedItem.ToString().IndexOf("(COM") + 1;
            temp = mybutton.SelectedItem.ToString().Substring(start, mybutton.SelectedItem.ToString().Length - start - 1);
            mybutton.Items.Clear();
            mybutton.Items.Add(temp);
            mybutton.SelectedIndex = 0;

            ledport.BaudRate = 19200;
            ledport.PortName = temp;
            ledport.ReadTimeout = 1000;
            ledport.Open();
            try
            {
                ledport.ReadLine();     // get a couple of lines
                ledport.ReadLine();     // get a couple of lines
            }
            catch (Exception)
            {
                MessageBox.Show("Not the LED Detector port.");
                ledport.Close();
                mybutton.BackColor = Color.Red;
                return;
            }

            try
            {
                if (!(temp = ledport.ReadLine()).Contains("w="))
                {
                    MessageBox.Show("Not the LED Detector port. Found: " + temp + "\n");
                    mybutton.BackColor = Color.Red;
                }
                else
                { 
                    MessageBox.Show("Correct port");
                    mybutton.BackColor = Color.Green;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Not the LED Detector port. Error: " + ex.Message);
                mybutton.BackColor = Color.Red;
            }
            ledport.Close();
        }


        private void comboBoxTSCUComPort_SelectionChangeCommitted(object sender, EventArgs e)
        {
            string temp = string.Empty;
            int start = 0;
            string outmsg = string.Empty;
            string ver = string.Empty;

            ComboBox mybutton = sender as ComboBox;
            start = mybutton.SelectedItem.ToString().IndexOf("(COM") + 1;
            temp = mybutton.SelectedItem.ToString().Substring(start, mybutton.SelectedItem.ToString().Length - start - 1);
            mybutton.Items.Clear();
            mybutton.Items.Add(temp);
            mybutton.SelectedIndex = 0;
            ACMeterCommications pm = new ACMeterCommications();
            pm.Open(temp);
            if (pm.ReadVersion(ref ver, ref outmsg) != (int)ErrorCodes.Program.Success)
                MessageBox.Show("Power meter not responding.");
            else
                MessageBox.Show("Power meter version is " + ver);
            pm.Close();
            pm.Dispose();
        }


        private void comboBoxTSLEDDetectorType_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox mybutton = sender as ComboBox;
            if (mybutton.Text == "USB")
            {
                comboBoxTSLEDDetectorComPort.Visible = true;
                textBoxTSRedRange.Text = "6200,6600";
                textBoxTSGreenRange.Text = "5400,5900";
                textBoxTSBlueRange.Text = "4200,4900";
            }
            else
            {
                comboBoxTSLEDDetectorComPort.Visible = false;
                textBoxTSRedRange.Text = "11000,13000";
                textBoxTSGreenRange.Text = "7000,9000";
                textBoxTSBlueRange.Text = "4200,4900";
            }
        }

        private void comboBoxTSAmbDLSType_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox mybutton = sender as ComboBox;
            if (mybutton.Text == "RedLED")
                numericUpDownTSAmbDLSLEDV.Visible = true;
            else
                numericUpDownTSAmbDLSLEDV.Visible = false;

        }

        /// <summary>
        /// process the CU type change
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void comboBoxTSCUType_SelectedValueChanged(object sender, EventArgs e)
        {
            ComboBox mybutton = sender as ComboBox;
            if (mybutton.Text == "Enlighted")
            {
                comboBoxTSCUComPort.Visible = true;
                nudControlVoltageGain.Visible = true;
            }
            else
            {
                comboBoxTSCUComPort.Visible = false;
                nudControlVoltageGain.Visible = false;
            }
        }

        private void tLimitDirectory_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderDlg = new FolderBrowserDialog();
            folderDlg.ShowNewFolderButton = true;
            folderDlg.Description = "Select the folder that contains the limits files.";
            folderDlg.SelectedPath = tLimitDirectory.Text;
            // Show the FolderBrowserDialog.
            DialogResult result = folderDlg.ShowDialog();
            if (result == DialogResult.OK)
            {
                tLimitDirectory.Text = folderDlg.SelectedPath;
                //Environment.SpecialFolder root = folderDlg.RootFolder;
            }
        }


        private void displayComWindow(string comPort, string baud)
        {
            // Prepare the process to run
            ProcessStartInfo start = new ProcessStartInfo();
            // Enter in the command line arguments, everything you would enter after the executable name itself
            start.Arguments = "-serial " + comPort + " -sercfg " + baud +",8,n,1,N" ;
            // Enter the executable to run, including the complete path
            start.FileName = "c:\\Program Files\\ExtraPuTTY\\Bin\\putty.exe";
            // Do you want to show a console window?
            start.WindowStyle = ProcessWindowStyle.Normal;
            start.CreateNoWindow = false;
            int exitCode;


            // Run the external process & wait for it to finish
            //using (Process proc = Process.Start(start))
            //{
            //    proc.WaitForExit();

            //    // Retrieve the app's exit code
            //    exitCode = proc.ExitCode;
            //}
        }

        private void openComPort_Click(object sender, EventArgs e)
        {
            string comPort;
            ComboBox mybutton = sender as ComboBox;
            comPort = mybutton.SelectedItem.ToString();
            displayComWindow(comPort, "115200");
        }

        private void bAutofindRefRadio_Click(object sender, EventArgs e)
        {
            string[] ports = SerialPort.GetPortNames();         // get list of serial ports
            string temp = string.Empty;
            bool willOpen = true;
            bool tookCmd = false;
            bool foundPort = false;

            SerialPort smport = new SerialPort();

            comboBoxTSTestRadioComPort.BackColor = Color.Red;
            Button mybutton = sender as Button;
            mybutton.Text = "Looking";
            foreach (var item in ports)
            {
                try // see if it will open
                {
                    willOpen = true;
                    smport.BaudRate = 115200;
                    smport.ReadTimeout = 1000;
                    smport.PortName = item;
                    smport.Open();
                }
                catch (Exception)
                {
                    willOpen = false;
                }
                if (willOpen)  // if it will open, see if it will take the command
                {
                    try
                    {
                        tookCmd = true;
                        smport.WriteLine("x");
                        temp = smport.ReadTo(">");          // get the echo
                    }
                    catch (Exception)
                    {
                        smport.Close();
                        tookCmd = false;
                    }

                }
                if (willOpen & tookCmd) // if it opened and it took the command, check result
                {
                    try
                    {
                        if (temp.Contains("REF"))
                        {
                            foundPort = true;
                        }
                    }
                    catch (Exception)
                    {
                        foundPort = false;
                    }
                    smport.Close();
                }
                if (foundPort)  // if the port was found
                {
                    comboBoxTSTestRadioComPort.Items.Clear();
                    comboBoxTSTestRadioComPort.Items.Add(item);
                    comboBoxTSTestRadioComPort.SelectedIndex = 0;
                    comboBoxTSTestRadioComPort.BackColor = Color.Green;

                    break;
                }
            }
            mybutton.Text = "Auto Find";

        }

        private void autofindBLE_Click(object sender, EventArgs e)
        {
            SerialPort bleport = new SerialPort();
            string[] ports = SerialPort.GetPortNames();         // get list of serial ports
            string temp = string.Empty;

            cbBluetoothComPort.BackColor = Color.Red;
            Button mybutton = sender as Button;
            mybutton.Text = "Looking";
            Application.DoEvents();
            foreach (var item in ports)
            {
                BluetoothFunctions bt = new BluetoothFunctions(item, null, null);
                if (bt.OpenBLEMonitor(ref temp) == (int)ErrorCodes.Program.Success)
                {
                    if (bt.dongleInit(ref temp))
                    {
                        cbBluetoothComPort.BackColor = Color.Green;
                        cbBluetoothComPort.Items.Clear();
                        cbBluetoothComPort.Items.Add(item);
                        cbBluetoothComPort.SelectedIndex = 0;
                        bt.CloseBLEMonitor();
                        break;
                    }
                }
                bt.CloseBLEMonitor();
            }
            mybutton.Text = "Auto Find";
        }

        private void autofindCU_Click(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Will try to auto find the CU port.
        /// </summary>
        /// <remarks>
        /// Assumes that there is a working DUT running DVTMAN in the fixture and console
        /// has been configured.
        /// Will turn on the unit
        ///     for each com port other than the console port
        ///         open the port as a CU port
        ///         do a p cu-test
        ///         if it passes, assume it is the correct port
        /// </remarks>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void autofindPM_Click(object sender, EventArgs e)
        {
            string[] ports = SerialPort.GetPortNames();         // get list of serial ports
            SensorConsole dconsole = new SensorConsole();
            CUConsole cconsole = new CUConsole();
            SensorDVTMANTests lclmanTests = new SensorDVTMANTests();
            MainWindow.uutdata uutdata = new MainWindow.uutdata();
            string msg = string.Empty;
            LimitsData.stepsItem testToRun = new LimitsData.stepsItem();
            string port = "COM0";
            int status;

            comboBoxTSCUComPort.BackColor = Color.Red;
            Button mybutton = sender as Button;
            mybutton.Text = "Looking";
            Application.DoEvents();
            if (lclStationConfig.ConfigDUTComPort == "COM0")        // if dut com not configured
            {
                MessageBox.Show("DUT Com Port must be configured first.");
                return;
            }
            lclFixtureFunctions.ControlDUTPower(1);             // power up DUT
            dconsole.Open(lclStationConfig.ConfigDUTComPort);
            dconsole.ClearBuffers();
            foreach (var item in ports)
            {
                if (item != lclStationConfig.ConfigDUTComPort)
                {
                    cconsole.Open(item);
                    status = lclmanTests.CUPortTest(testToRun, null, uutdata,"",  out msg);
                    cconsole.Close();
                    if (status == (int)ErrorCodes.Program.Success)
                    {
                        port = item;
                        break;
                    }
                }
            }
            
        }

        private void autofindSM_Click(object sender, EventArgs e)
        {
            string[] ports = SerialPort.GetPortNames();         // get list of serial ports
            string temp = string.Empty;
            bool foundPort = false;
            SerialPort smport = new SerialPort();

            cbSwitchMateCom.BackColor = Color.Red;
            Button mybutton = sender as Button;
            mybutton.Text = "Looking";
            Application.DoEvents();
            foreach (var item in ports)
            {
                smport.BaudRate = 19200;
                smport.PortName = item;
                smport.NewLine = "\r";
                smport.ReadTimeout = 1000;
                smport.Open();
                smport.WriteLine("SM_ID?");
                try
                {
                    if ((temp = smport.ReadLine()).Contains("SWITCH-MATE"))
                    {
                        cbSwitchMateCom.Items.Clear();
                        cbSwitchMateCom.Items.Add(item);
                        cbSwitchMateCom.SelectedIndex = 0;
                        cbSwitchMateCom.BackColor = Color.Green;
                        smport.Close();
                        break;
                    }
                }
                catch (Exception)
                {
                    foundPort = false;
                }
                smport.Close();
            }
            mybutton.Text = "Auto Find";
        }

        private void autofindLED_Click(object sender, EventArgs e)
        {
            string[] ports = SerialPort.GetPortNames();         // get list of serial ports
            string temp = string.Empty;
            bool willOpen = true;
            bool tookCmd = false;

            SerialPort ledport = new SerialPort();

            comboBoxTSLEDDetectorComPort.BackColor = Color.Red;
            Button mybutton = sender as Button;
            mybutton.Text = "Looking";
            Application.DoEvents();
            foreach (var item in ports)
            {
                temp = string.Empty;
                try // see if it will open
                {
                    willOpen = true;
                    ledport.BaudRate = 19200;
                    ledport.PortName = item;
                    ledport.ReadTimeout = 1000;
                    ledport.Open();
                }
                catch (Exception ex)
                {
                    temp = ex.Message;
                    willOpen = false;
                }
                if (willOpen)  // if it will open, see if it will read values
                {
                    try
                    {
                        tookCmd = true;
                        ledport.ReadLine();     // get a couple of lines
                        ledport.ReadLine();
                        temp = ledport.ReadLine();     // get a couple of lines
                    }
                    catch (Exception ex)
                    {
                        temp = ex.Message;
                        ledport.Close();
                        tookCmd = false;
                    }

                }

                if (willOpen & tookCmd & temp.Contains("w="))  // if the port was found
                {
                    comboBoxTSLEDDetectorComPort.Items.Clear();
                    comboBoxTSLEDDetectorComPort.Items.Add(item);
                    comboBoxTSLEDDetectorComPort.SelectedIndex = 0;
                    comboBoxTSLEDDetectorComPort.BackColor = Color.Green;
                    ledport.Close();
                    break;
                }
                if (ledport.IsOpen)
                    ledport.Close();
            }
            mybutton.Text = "Auto Find";

        }

        private void bAutoFindDUTComPort_Click(object sender, EventArgs e)
        {
            string[] ports = SerialPort.GetPortNames();         // get list of serial ports
            string temp = string.Empty;
            bool foundPort = false;
            SerialPort smport = new SerialPort();
            StationConfigure myStationConfig;

            comboBoxTSDUTComPort.BackColor = Color.Red;
            Button mybutton = sender as Button;
            mybutton.Text = "Looking";
            MessageBox.Show("DUT running DVTMAN has to be loaded into the fixture");
            Application.DoEvents();
            using (FixtureFunctions fixture = new FixtureFunctions())
            {
                myStationConfig = lclStationConfig;                         // make a scratch copy for the init
                myStationConfig.FixtureEquipment.hasConsolePort = false;    // do not init console
                myStationConfig.FixtureEquipment.hasTestRadio = false;      // do not init the test radio
                myStationConfig.FixtureEquipment.hasBLE = false;            // do not init the ble radios
                if (cbSwitchMateCom.Text  == "COM0")      // if the switch-mate has not been config
                {
                    MessageBox.Show("Switch-Mate must be configured first");
                    comboBoxTSDUTComPort.BackColor = Color.White;
                    mybutton.Text = "Auto Find";
                    return;
                }
                myStationConfig.ConfigSwitchMateComPort = cbSwitchMateCom.Text;     // load current switch mate port
                fixture.StationConfigure = myStationConfig;
                if (!fixture.InitFixture())
                {
                    MessageBox.Show("Error initializing fixture. " + fixture.LastFunctionErrorMessage);
                }
                else
                {
                    fixture.ControlDUTPower(1);
                    foreach (var item in ports)
                    {
                        smport.BaudRate = 115200;
                        smport.PortName = item;
                        smport.NewLine = "\n";
                        smport.ReadTimeout = 1000;
                        try
                        {
                            smport.Open();
                            foundPort = true;
                        }
                        catch (Exception)       // may be open for fixture function
                        {
                            foundPort = false;
                        }
                        if (foundPort)
                        {
                            smport.DiscardInBuffer();
                            smport.WriteLine("x");
                            try
                            {
                                temp = smport.ReadTo(">");       // read prompt
                                if (temp.Contains("DVTMAN"))
                                {
                                    comboBoxTSDUTComPort.Items.Clear();
                                    comboBoxTSDUTComPort.Items.Add(item);
                                    comboBoxTSDUTComPort.SelectedIndex = 0;
                                    comboBoxTSDUTComPort.BackColor = Color.Green;
                                    smport.Close();
                                    break;
                                }
                            }
                            catch (Exception)
                            {
                                foundPort = false;
                            }
                            smport.Close();
                        }
                    }
                    mybutton.Text = "Auto Find";
                    fixture.ControlDUTPower(0);
                }
            }

        }

        private void tbLabelDirectory_Clicked(object sender, EventArgs e)
        {
            FolderBrowserDialog folderDlg = new FolderBrowserDialog();
            folderDlg.ShowNewFolderButton = true;
            folderDlg.Description = "Select the folder that contains the labels files.";
            folderDlg.SelectedPath = tbLabelDirectory.Text;
            // Show the FolderBrowserDialog.
            DialogResult result = folderDlg.ShowDialog();
            if (result == DialogResult.OK)
            {
                tbLabelDirectory.Text = folderDlg.SelectedPath;
                //Environment.SpecialFolder root = folderDlg.RootFolder;
            }

        }

        private void bAutoFindTagComPort_Click(object sender, EventArgs e)
        {
            string[] ports = SerialPort.GetPortNames();         // get list of serial ports
            string temp = string.Empty;
            bool foundPort = false;
            SerialPort smport = new SerialPort();
            StationConfigure myStationConfig;

            cbTagComPort.BackColor = Color.Red;
            Button mybutton = sender as Button;
            mybutton.Text = "Looking";
            Application.DoEvents();
            using (FixtureFunctions fixture = new FixtureFunctions())
            {
                myStationConfig = lclStationConfig;                                 // make a scratch copy for the init
                myStationConfig.FixtureEquipment.hasConsolePort = false;            // do not init console
                myStationConfig.FixtureEquipment.hasTestRadio = false;              // do not init the test radio
                myStationConfig.FixtureEquipment.hasBLE = false;                    // do not init the ble radios
                myStationConfig.ConfigSwitchMateComPort = cbSwitchMateCom.Text;     // load current switch mate port
                fixture.StationConfigure = myStationConfig;
                if (!fixture.InitFixture())
                {
                    MessageBox.Show("Error initializing fixture. " + fixture.LastFunctionErrorMessage);
                }
                else
                {
                    fixture.ControlDUTPower(0);         // make sure the DUT is off
                    foreach (var item in ports)
                    {
                        smport.BaudRate = 115200;
                        smport.PortName = item;
                        smport.NewLine = "\n";
                        smport.ReadTimeout = 1000;
                        try
                        {
                            smport.Open();
                            foundPort = true;
                        }
                        catch (Exception)       // may be open for fixture function
                        {
                            foundPort = false;
                        }
                        if (foundPort)
                        {
                            smport.DiscardInBuffer();
                            smport.WriteLine("x");
                            try
                            {
                                temp = smport.ReadTo(">");       // read prompt
                                if (temp.Contains("SU"))
                                {
                                    cbTagComPort.Items.Clear();
                                    cbTagComPort.Items.Add(item);
                                    cbTagComPort.SelectedIndex = 0;
                                    cbTagComPort.BackColor = Color.Green;
                                    smport.Close();
                                    break;
                                }
                            }
                            catch (Exception)
                            {
                                foundPort = false;
                            }
                            smport.Close();
                        }
                    }
                    mybutton.Text = "Auto Find";
                }
            }

        }
    }
}
