﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

//using LimitFileFunctions;

namespace SensorManufSY2
{
    partial class ERC_Tests
    {
        /// <summary>
        /// Will put the unit into Run mode or Power Save mode.
        /// </summary>
        /// <remarks>
        /// Mode - Run = put into run mode, issue s p r
        ///                     anything else - put into conserve mode, issue s p c
        /// </remarks>
        /// <param name="mode"></param>
        /// <param name="prompt"></param>
        /// <param name="outmsg"></param>
        /// <returns></returns>
        public int SetPowerMode(string mode, string prompt, ref string outmsg)
        {
            int status = (int)ErrorCodes.Program.Success;
            bool putIntoRunMode = false;            // default to conserver mode

            if (mode.ToUpper() == "RUN")
                putIntoRunMode = true;

            //wsFixture.ClearSerialBuffers();

            sensorConsole.WaitForPrompt(10000,  prompt, out outmsg);
            if (sensorConsole.WriteLine(putIntoRunMode ? "s p r" : "s p c", 2000))
            {
                if (!sensorConsole.WaitForPrompt(2000, prompt, out outmsg))
                    status = (int)ErrorCodes.Program.DutUnexpectedCmdReturn;
            }
            else
                status = (int)ErrorCodes.Program.DutUnexpectedCmdReturn;

            return status;
        }

        //----------------------------------------
        // CheckManData
        /// <summary>
        /// Will verfiy the manufacturing data with what is passed.
        /// </summary>
        /// <param name="mac">string - mac(no leading 0, no :)</param>
        /// <param name="pn">string - pcba pn</param>
        /// <param name="sn">string - pcba sn</param>
        /// <param name="reporttext">string - error string if there was a failure</param>
        /// <returns>bool - true = all is OK</returns>
        private bool checkManData(MainWindow.uutdata uutData, out string reporttext)
        {
            bool status = true;
            string outmsg = string.Empty;
            string uoutmsg = string.Empty;
            string currentMAC = string.Empty;
            string currentSN = string.Empty;
            string currentPN = string.Empty;
            MainWindow.uutdata readuutData = new MainWindow.uutdata();

            reporttext = string.Empty;
            if (!readManufData(uutData, ref readuutData, ref reporttext))
                status = false;

            // now parse the output
            if (status)
            {
                if (readuutData.mac.ToUpper() != uutData.mac.ToUpper())
                {
                    status = false;
                }
                if (readuutData.pcbaPN != uutData.pcbaPN)
                {
                    status = false;
                }
                if (readuutData.topSN != uutData.topSN)
                {
                    status = false;
                }
                if (readuutData.topPN != uutData.topPN)
                {
                    status = false;
                }
                if (readuutData.model != uutData.model)
                {
                    status = false;
                }
            }

            reporttext = string.Format("{0}\t  PN Expected: {1} \tRead: {2}\n\t  SN Expected: {3} \tRead: {4}\n\t  MAC Expected: {5} \tRead: {6}\n",
                                            reporttext, uutData.pcbaPN, readuutData.pcbaPN, uutData.pcbaSN, readuutData.pcbaSN, uutData.mac, readuutData.mac);

            return status;
        }

        /// <summary>
        /// Reads the manufacturing data from the unit
        /// </summary>
        /// <remarks>
        /// Assumes a output of:
        /// uutPrompt d man
        ///     MAC           = 00006854f5db0008
        ///     pcba_partno   = 02-01030-04
        ///     pcba_serilano = CRSP01131100007
        ///     hla_partno    = 01-01034-06
        ///     hla_serialno  = BWS206172400835
        ///     modeL_NO      = WS-2-00
        ///     LFUSE = ff
        ///     HFUSE = 9a
        ///     EFUSE = fe
        ///     LOCK = fc
        /// </remarks>
        private bool readManufData(MainWindow.uutdata uutData, ref MainWindow.uutdata readuutData, ref string outmsg)
        {
            bool status = true;
            string uoutmsg = string.Empty;
            int foundindex = 0;
            char[] delimiterChars = { '\n', '\r' };

            outmsg = string.Empty;
            if (!sensorConsole.WriteLine("d man", 1000))
            {
                outmsg = "Dut Com error: Error while sending 'd man' command.";
                status = false;       // just returns because assuming there is no dut response at all
            }

            if (!sensorConsole.WaitForPrompt(3000, uutData.appPrompt, out outmsg) && status)
            {
                outmsg = "Dut Com error: Error waiting for " + uutData.appPrompt + " after the 'd man' command.";
                status = false;
            }

            if (status)
            {
                string[] lines = outmsg.Split(delimiterChars);

                readuutData.mac = "";
                readuutData.model = "";
                readuutData.pcbaPN = "";
                readuutData.pcbaSN = "";
                readuutData.topPN = "";
                readuutData.topSN = "";

                foreach (var item in lines)
                {
                    // parse out the data
                    if (item.Contains("MAC"))
                        readuutData.mac = item.Substring(item.IndexOf('=') + 6).ToUpper();  // strip off leading 0000

                    if (item.Contains("pcba_partno"))
                        readuutData.pcbaPN = item.Substring(item.IndexOf('=') + 2);

                    if (item.Contains("pcba_serialno"))
                        readuutData.pcbaSN = item.Substring(item.IndexOf('=') + 2).ToUpper();

                    if (item.Contains("hla_partno"))
                    {
                        readuutData.topPN = item.Substring(item.IndexOf('=') + 2);
                    }

                    if (item.Contains("hla_serialno"))
                    {
                        readuutData.topSN = item.Substring(item.IndexOf('=') + 2).ToUpper();
                    }

                    if (item.Contains("model_no"))
                    {
                        readuutData.model = item.Substring(item.IndexOf('=', foundindex) + 2);
                    }
                }
            }

            return status;
        }


        ///// <summary>
        ///// Will get the data for a given constant in the CList.
        ///// </summary>
        ///// <param name="testData">structure that has the CList</param>
        ///// <param name="constantName">name of the constant to get data for</param>
        ///// <param name="constData">returned data. string.empty if none found</param>
        ///// <returns>true = name is found</returns>
        //private bool GetConstantValue(TestToRun testData, string constantName, ref string constValue)
        //{
        //    bool tagfound = false;

        //    constValue = string.Empty;
        //    foreach (var item in testData.CList)   // go through the parameters in the test
        //    {
        //        if (item.Name.ToUpper().Equals(constantName.ToUpper()))
        //        {
        //            constValue = item.Value;
        //            tagfound = true;
        //        }
        //    }
        //    return tagfound;
        //}


        ////--------------------------------------//
        ////--------------------------------------//
        ////                                      //
        ////      Value checking routines         //
        ////--------------------------------------//
        ////--------------------------------------//

        ////-------------------------------------------------
        ///// <summary>
        ///// This function will check a value as defined in a List type parameters.
        ///// </summary>
        ///// <param name="testvalue"></param>
        ///// <param name="item"></param>
        ///// <returns>bool - true = meets the conditions</returns>
        //private static bool CheckResultValue(double testvalue, ListEntry item, ref string resultmessage)
        //{
        //    bool status = true;

        //    resultmessage = string.Empty;
        //    switch (item.Type)
        //    {
        //        case "Range":
        //            switch (item.Operation)
        //            {
        //                case "GTLT":
        //                    if ((testvalue < Convert.ToDouble(item.Value1)) || (testvalue > Convert.ToDouble(item.Value2)))
        //                        status = false;
        //                    resultmessage = string.Format("{0}   Expected: {1} < x < {2}  Found: {3:N2}\n", item.Parameter, item.Value1, item.Value2, testvalue);
        //                    break;
        //                case "GELE":
        //                    if ((testvalue <= Convert.ToDouble(item.Value1)) || (testvalue >= Convert.ToDouble(item.Value2)))
        //                        status = false;
        //                    resultmessage = string.Format("{0}   Expected: {1} <= x <= {2}  Found: {3:N2}\n", item.Parameter, item.Value1, item.Value2, testvalue);
        //                    break;
        //                default:
        //                    throw new Exception("Invalid operation: " + item.Parameter.ToString() + ":" + item.Operation.ToString());
        //            }
        //            break;

        //        case "Value":
        //            switch (item.Operation)
        //            {
        //                case "GE":      // greater than or equal
        //                    if (!(testvalue >= Convert.ToDouble(item.Value1)))
        //                        status = false;
        //                    resultmessage = string.Format("{0}   Expected: x >= {1}  Found: {2:N2}\n", item.Parameter, item.Value1, testvalue);
        //                    break;

        //                case "EQ":      // equal
        //                    if (!(testvalue == Convert.ToDouble(item.Value1)))
        //                        status = false;
        //                    resultmessage = string.Format("{0}   Expected: x = {1}  Found: {2:N2}\n", item.Parameter, item.Value1, testvalue);
        //                    break;

        //                case "LE":      // less than or equal
        //                    if (!(testvalue <= Convert.ToDouble(item.Value1)))
        //                        status = false;
        //                    resultmessage = string.Format("{0}   Expected: x <= {1}  Found: {2:N2}\n", item.Parameter, item.Value1, testvalue);
        //                    break;

        //                case "LT":      // less than
        //                    if (!(testvalue < Convert.ToDouble(item.Value1)))
        //                        status = false;
        //                    resultmessage = string.Format("{0}   Expected: x < {1}  Found: {2:N2}\n", item.Parameter, item.Value1, testvalue);
        //                    break;

        //                default:
        //                    throw new Exception("Invalid operation: " + item.Parameter.ToString() + ":" + item.Operation.ToString());
        //            }
        //            break;

        //        default:
        //            throw new Exception("Invalid type: " + item.Parameter.ToString() + ":" + item.Operation.ToString());
        //    }

        //    return status;
        //}


    }
}
