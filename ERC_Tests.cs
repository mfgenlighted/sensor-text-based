﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

using Seagull.BarTender.Print;

using LimitsDatabase;
using FixtureDatabase;
using PFS_System;

namespace SensorManufSY2
{
    partial class ERC_Tests
    {
        FixtureFunctions fixtureFunctions;
        SensorConsole sensorConsole;
        ResultData testDB;
        StationConfigure stationConfig;

        public ERC_Tests(MainWindow.codeClasses testcodeClasses)
        {
            fixtureFunctions = testcodeClasses.fixtureFunctions;
            sensorConsole = testcodeClasses.sensorConsole;
            testDB = testcodeClasses.testDB;
            stationConfig = testcodeClasses.stationConfigure;
        }


        //------------------------
        // events
        //  Use UpdateStatusWindow(msg) to display a message on the
        //  MainWindow status window.
        //------------------------
        public event EventHandler<StatusUpdatedEventArgs>
            StatusUpdated;

        protected virtual void
            OnStatusUpdated(StatusUpdatedEventArgs e)
        {
            if (StatusUpdated != null)
                StatusUpdated(this, e);
        }

        // call these functions to update the main status window
        private void UpdateStatusWindow(string msg)
        {
            string newStatus = msg;
            StatusUpdatedEventArgs nuea = new StatusUpdatedEventArgs();
            nuea.statusText = msg;
            OnStatusUpdated(nuea);
        }
        private void UpdateStatusWindow(string msg, StatusType status)
        {
            string newStatus = msg;
            StatusUpdatedEventArgs nuea = new StatusUpdatedEventArgs();
            nuea.statusText = msg;
            nuea.statusType = status;
            OnStatusUpdated(nuea);
        }

        private void UpdateTestHeader(int step, int test, string name)
        {
            int newStep = step;
            int newTest = test;
            string newName = name;
            StatusUpdatedEventArgs nuea = new StatusUpdatedEventArgs();
            nuea.statusText = name;
            nuea.stepNumber = step;
            nuea.testNumber = test;
            OnStatusUpdated(nuea);
        }

        private void UpdateTestFooter(int testResultCode, string errormsg)
        {
            StatusUpdatedEventArgs nuea = new StatusUpdatedEventArgs();
            nuea.statusText = errormsg;
            nuea.resultCode = testResultCode;
            OnStatusUpdated(nuea);
        }

        /// <summary>
        /// Program the manufacturing data
        /// </summary>
        /// <remarks>
        ///     s man [16 digit mac] [pcba pn] [pcba sn] [hla pn] [hla sn] [model]
        /// </remarks>
        /// <param name="parameters"></param>
        /// <param name="uutData"></param>
        /// <param name="reporttext"></param>
        /// <returns></returns>
        public int setManData(LimitsDatabase.LimitsData.stepsItem parameters, MainWindow.uutdata uutData, ref string reporttext)
        {
            string cmdline;
            int retests = 2;
            int retestCount = 0;
            bool badEcho = false;

            cmdline = string.Format("s man 0000{0} {1} {2} {3} {4} {5}", uutData.mac, uutData.pcbaPN, uutData.pcbaSN, uutData.topPN, uutData.topSN, uutData.model);

            int stepStatus = (int)ErrorCodes.Program.Success;
            string outmsg = string.Empty;
            string echoReporttext = string.Empty;
            int echoStepStatus = (int)ErrorCodes.Program.Success;


            if (parameters.GetParameterValue("Retrys") != string.Empty)
                retests = Convert.ToInt32(parameters.GetParameterValue("Retrys"));

            reporttext = string.Empty;

            do
            {
                sensorConsole.ClearBuffers();
                stepStatus = (int)ErrorCodes.Program.Success;
                badEcho = false;
                if (!sensorConsole.WriteLine(cmdline, 3000))
                {
                    echoReporttext = sensorConsole.LastErrorMessage;
                    echoStepStatus = (int)sensorConsole.LastErrorCode + (parameters.step_number * 100);
                    badEcho = true;
                    // might be a dropped character. so let loop retry
                }
                if (!sensorConsole.WaitForPrompt(5000, uutData.appPrompt, out outmsg) && (stepStatus == (int)ErrorCodes.Program.Success))
                {
                    // did not see the prompt come back.
                    reporttext = "Error waiting for the prompt for the 's man' command. " + outmsg;
                    stepStatus = (int)ErrorCodes.Program.PromptNotFound + (parameters.step_number * 100);
                    break;     // don't try again. something wrong with the commuications
                }
                else            // found dvtman prompt
                {
                    if (!badEcho)       // if there was no bad echo previously, verfiy the programming
                    {
                        // verify that it programed correctly
                        if (!checkManData(uutData, out reporttext))     // if what is programmed does not match what should be there
                        {
                            reporttext = "\t  Read back data did not match what was programed. " + reporttext;
                            stepStatus = (int)ErrorCodes.Program.PCBADataMismatch + (parameters.step_number * 100);
                        }
                        else
                        {
                            reporttext = string.Format("Programmed  pcba SN: {0} \tPN: {1} \tMAC: {2}\n           hla SN: {3}\tPN: {4}\t Model: {5}\n",
                                                    uutData.pcbaSN, uutData.pcbaPN, uutData.mac, uutData.topPN, uutData.topSN, uutData.model);
                            stepStatus = (int)ErrorCodes.Program.Success;
                        }
                    }
                    else                // else there was a bad echo, so set errors
                    {
                        reporttext = "\t  Echo bad. " + echoReporttext;
                        stepStatus = echoStepStatus + (parameters.step_number * 100);
                    }
                }

            } while ((stepStatus != (int)ErrorCodes.Program.Success) & (retestCount++ < retests));

            testDB.CreateStepRecord(parameters.name, parameters.step_number);
            testDB.AddStepResult((stepStatus == (int)ErrorCodes.Program.Success) ? "PASS" : "FAIL", outmsg, stepStatus);

            return stepStatus;
        }




        /// <summary>
        /// It will apply dc power to the unit.
        /// </summary>
        /// <remarks>
        /// ERC power is controled by the Power constant.
        ///     Battery - no fixture power is applied
        ///     Fixture, x.x - fixture supply is set to x.x vdc
        ///     FixtureAndBattery - the battery voltage is read and the fixture is set to that voltage
        /// </remarks>
        /// <param name="testToRun"></param>
        /// <param name="testDB"></param>
        /// <param name="uutData"></param>
        /// <param name="outmsg"></param>
        /// <returns></returns>
        public int TurnOnPower(LimitsData.stepsItem testToRun,  MainWindow.uutdata uutData, ref string outmsg)
        {
            int status = (int)ErrorCodes.Program.Success;
            string power = "FIXTURE";
            double setVoltage = 3.0;      // if set to 3V
            double batteryVoltage = 0;

            // figure out the power source
            foreach (var item in testToRun.parameters)
            {
                if (item.name.ToUpper() == "POWER")
                {
                    power = item.value;
                    if (power.Contains(","))        // if it has a ',' then get the voltage
                        setVoltage = Convert.ToDouble(power.Substring(power.IndexOf(",") + 2));
                }
            }

            sensorConsole.ClearBuffers();
            fixtureFunctions.SetERCButton(2, 1);
            fixtureFunctions.SetERCButton(4, 1);

            try 
	        {	        
                if (setVoltage != 0)    // if there was a voltage defined
                {
                    Thread.Sleep(1000);
                    status = fixtureFunctions.ControlDUTPower(setVoltage);
                    UpdateStatusWindow("Battery voltage set to " + setVoltage.ToString() + "\n");
                }

                if (power.ToUpper().Contains("AND"))    // if defined to match the battery voltage
                {
                    batteryVoltage = fixtureFunctions.Read3p3Voltage();
                    status = fixtureFunctions.ControlDUTPower(batteryVoltage);
                    UpdateStatusWindow("Battery voltage set to match at " + batteryVoltage.ToString() + "\n");
                }
	        }
	        catch (Exception ex)
	        {
		        status = (int)ErrorCodes.System.LabJackError;
                outmsg = "System error: Labjack reading or setting DUT voltage. " + ex.Message;
	        }


            if (status == (int)ErrorCodes.Program.Success)
                status = SetPowerMode("RUN", uutData.appPrompt, ref outmsg);

            fixtureFunctions.SetERCButton(2, 0);
            fixtureFunctions.SetERCButton(4, 0);
            Thread.Sleep(500);              // give time for the plunger to retract

            if ((status != (int)ErrorCodes.Program.Success) & (status < (int)ErrorCodes.System.SystemErrorCodes))
                status = status + (100 * testToRun.step_number);

            testDB.CreateStepRecord(testToRun.name, testToRun.step_number);
            testDB.AddStepResult((status == (int)ErrorCodes.Program.Success) ? "PASS":"FAIL", outmsg, status);
            return status;
        }


                enum bstatus { found, notFound, stuck};
        //--------------------------------------
        // TestButtons
        //
        //  Will instruct the operator to push one button at a time and verify
        //  that the unit respons.
        //      status 0 = found button
        //              1 = did not find
        //              2 = found, but looks to be stuck
        public int TestButtons(LimitsData.stepsItem testParams, ref string outmsg)
        {
            short[] buttonStatus = {0,0,0,0};

            DateTime endTime;
            buttonStatus[0] = (short)bstatus.notFound;       // set intially as 'not found'
            buttonStatus[1] = (short)bstatus.notFound;
            buttonStatus[2] = (short)bstatus.notFound;
            buttonStatus[3] = (short)bstatus.notFound;
            string rcvtext;
//            bool displayStatus, displaySend;
            bool downFound = false;
            bool upFound = false;
            bool useFixtureButtons = true;

            //displayStatus = wsfixture.DisplaySerialRcv;
            //displaySend = wsfixture.DisplaySerialSend;
            //wsfixture.DisplaySerialRcv = false;
            //wsfixture.DisplaySerialSend = false;        // display command sent displaying

            outmsg = string.Empty;
            testDB.CreateStepRecord(testParams.name, testParams.step_number);

            if (testParams.parameters != null)
            {
                foreach (var item in testParams.parameters)
                {
                    if (item.name.ToUpper() == "PUSHTYPE")
                        if (item.value.ToUpper() == "MANUAL")
                            useFixtureButtons = false;
                }
            }

            Retest:

                    //---- button 1
            if (useFixtureButtons)
            {
                UpdateStatusWindow("Testing Button 1 -- Down");
                fixtureFunctions.SetERCButton(1, 1);
                Thread.Sleep(500);
                endTime = DateTime.Now.AddMilliseconds(500);
            }
            else
            {
                UpdateStatusWindow("--->>> Push button 1 <<<---\n", StatusType.prompt);
                endTime = DateTime.Now.AddMilliseconds(10000);
            }
            downFound = false;
            while (!downFound && (endTime > DateTime.Now))  // wait for the down button
            {
                sensorConsole.WriteLine("d b", 1000);
                sensorConsole.WaitForPrompt(2000, "Switch> ", out rcvtext);
                if (rcvtext.Contains("Button 1 DOWN"))      // if found pushed button
                {
                    buttonStatus[0] = (short)bstatus.found;
                    downFound = true;
                }
            }

            if (useFixtureButtons)
            {
                UpdateStatusWindow(" -- UP");
                fixtureFunctions.SetERCButton(1, 0);
                Thread.Sleep(500);
                endTime = DateTime.Now.AddMilliseconds(500);
            }
            else
            {
                UpdateStatusWindow("--->>> Release button 1 <<<---\n");
                endTime = DateTime.Now.AddMilliseconds(5000);
            }
            upFound = false;
            while (!upFound && (endTime > DateTime.Now))
            {
                sensorConsole.WriteLine("d b", 1000);
                sensorConsole.WaitForPrompt(2000, "Switch> ", out rcvtext);
                if (!rcvtext.Contains("Button 1 DOWN"))      // if did not found pushed button
                    upFound = true;
            }

            if (!downFound & !useFixtureButtons)     // if did not find the button, give the operator a chance to try again
            {
                var result  = MessageBox.Show("Button not found", "Do you want to try again?", MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes)
                    goto Retest;
            }

            if (!upFound)         // if stuck
            {
                buttonStatus[0] = (short)bstatus.stuck;
            }

            if (buttonStatus[0] == 0)
                UpdateStatusWindow("Pass\n", StatusType.passed);
            else
                UpdateStatusWindow("Failed\n", StatusType.failed);

                    //---- button 2
            if (useFixtureButtons)
            {
                UpdateStatusWindow("Testing Button 2 -- Down");
                fixtureFunctions.SetERCButton(2, 1);
                Thread.Sleep(500);
                endTime = DateTime.Now.AddMilliseconds(500);
            }
            else
            {
                UpdateStatusWindow("--->>> Push button 2 <<<---\n");
                endTime = DateTime.Now.AddMilliseconds(2000);
            }
            downFound = false;
            while (!downFound && (endTime > DateTime.Now))
            {
                sensorConsole.WriteLine("d b", 1000);
                sensorConsole.WaitForPrompt(2000, "Switch> ", out rcvtext);
                if (rcvtext.Contains("Button 2 DOWN"))      // if found pushed button
                {
                    buttonStatus[1] = (short)bstatus.found;
                    downFound = true;
                }
            }
            if (useFixtureButtons)
            {
                UpdateStatusWindow(" -- UP");
                fixtureFunctions.SetERCButton(2, 0);
                Thread.Sleep(500);
                endTime = DateTime.Now.AddMilliseconds(500);
            }
            else
            {
                UpdateStatusWindow("--->>> Release button 2 <<<---\n");
                endTime = DateTime.Now.AddMilliseconds(2000);
            }
            upFound = false;
            while (!upFound && (endTime > DateTime.Now))
            {
                sensorConsole.WriteLine("d b", 1000);
                sensorConsole.WaitForPrompt(2000, "Switch> ", out rcvtext);
                if (!rcvtext.Contains("Button 2 DOWN"))      // if did not found pushed button
                    upFound = true;
            }

            if (!upFound)         // if stuck
            {
                buttonStatus[1] = (short)bstatus.stuck;
            }
            if (buttonStatus[0] == 0)
                UpdateStatusWindow("Pass\n", StatusType.passed);
            else
                UpdateStatusWindow("Failed\n", StatusType.failed);

                    //----- button 3
            if (useFixtureButtons)
            {
                UpdateStatusWindow("Testing Button 3 -- Down");
                fixtureFunctions.SetERCButton(3, 1);
                Thread.Sleep(500);
                endTime = DateTime.Now.AddMilliseconds(500);
            }
            else
            {
                UpdateStatusWindow("--->>> Push button 3 <<<---\n");
                endTime = DateTime.Now.AddMilliseconds(2000);
            }
            downFound = false;
            while (!downFound && (endTime > DateTime.Now))
            {
                sensorConsole.WriteLine("d b", 1000);
                sensorConsole.WaitForPrompt(2000, "Switch> ", out rcvtext);
                if (rcvtext.Contains("Button 3 DOWN"))      // if found pushed button
                {
                    buttonStatus[2] = (short)bstatus.found;
                    downFound = true;
                }
            }
            if (useFixtureButtons)
            {
                UpdateStatusWindow(" -- UP");
                fixtureFunctions.SetERCButton(3, 0);
                Thread.Sleep(500);
                endTime = DateTime.Now.AddMilliseconds(500);
            }
            else
            {
                UpdateStatusWindow("--->>> Release button 3 <<<---\n");
                endTime = DateTime.Now.AddMilliseconds(2000);
            }
            upFound = false;
            while (!upFound && (endTime > DateTime.Now))
            {
                sensorConsole.WriteLine("d b", 1000);
                sensorConsole.WaitForPrompt(2000, "Switch> ", out rcvtext);
                if (!rcvtext.Contains("Button 3 DOWN"))      // if did not found pushed button
                    upFound = true;
            }

            if (!upFound)         // if stuck
            {
                buttonStatus[2] = (short)bstatus.stuck;
            }
            if (buttonStatus[0] == 0)
                UpdateStatusWindow("Pass\n", StatusType.passed);
            else
                UpdateStatusWindow("Failed\n", StatusType.failed);

                    //---- button 4
            if (useFixtureButtons)
            {
                UpdateStatusWindow("Testing Button 4 -- Down");
                fixtureFunctions.SetERCButton(4, 1);
                Thread.Sleep(500);
                endTime = DateTime.Now.AddMilliseconds(500);
            }
            else
            {
                UpdateStatusWindow("--->>> Push button 4 <<<---\n");
                endTime = DateTime.Now.AddMilliseconds(2000);
            }
            downFound = false;
            while (!downFound && (endTime > DateTime.Now))
            {
                sensorConsole.WriteLine("d b", 1000);
                sensorConsole.WaitForPrompt(2000, "Switch> ", out rcvtext);
                if (rcvtext.Contains("Button 4 DOWN"))      // if found pushed button
                {
                    buttonStatus[3] = (short)bstatus.found;
                    downFound = true;
                }
            }
            if (useFixtureButtons)
            {
                UpdateStatusWindow(" -- UP");
                fixtureFunctions.SetERCButton(4, 0);
                Thread.Sleep(500);
                endTime = DateTime.Now.AddMilliseconds(500);
            }
            else
            {
                UpdateStatusWindow("--->>> Release button 4 <<<---\n");
                endTime = DateTime.Now.AddMilliseconds(2000);
            }
            upFound = false;
            while (!upFound && (endTime > DateTime.Now))
            {
                sensorConsole.WriteLine("d b", 1000);
                sensorConsole.WaitForPrompt(2000, "Switch> ", out rcvtext);
                if (!rcvtext.Contains("Button 4 DOWN"))      // if did not found pushed button
                    upFound = true;
            }

            if (!upFound)         // if stuck
            {
                buttonStatus[3] = (short)bstatus.stuck;
            }
            if (buttonStatus[0] == 0)
                UpdateStatusWindow("Pass\n", StatusType.passed);
            else
                UpdateStatusWindow("Failed\n", StatusType.failed);

            //wsfixture.DisplaySerialRcv = displayStatus;
            //wsfixture.DisplaySerialSend = displaySend;

            if ((buttonStatus[0] + buttonStatus[1] + buttonStatus[2] + buttonStatus[3]) == 0)
            {
                testDB.AddStepResult("PASS", "", (int)ErrorCodes.Program.Success);
                return (int)ErrorCodes.Program.Success;
            }
            else
            {
                string[] bstat = new string[3] { "Found", "Not Found", "Stuck" };
                outmsg = string.Format("Button 1:{0} 2:{1} 3:{2} 4:{3}",
                    bstat[buttonStatus[0]], bstat[buttonStatus[1]], bstat[buttonStatus[2]], bstat[buttonStatus[3]]);
                testDB.AddStepResult("FAIL", outmsg,
                    (int)ErrorCodes.Program.ValueOutsideOfLimit + (testParams.step_number * 100));
            }


            if ((buttonStatus[0] + buttonStatus[1] + buttonStatus[2] + buttonStatus[3]) != 0)
                return (int)ErrorCodes.Program.ValueOutsideOfLimit + (testParams.step_number * 100);
            else
                return (int)ErrorCodes.Program.Success;
        }


        /// <summary>
        /// Check the battery voltage.
        /// </summary>
        /// <remarks>
        /// this routine will push a button and read the message that is returned.
        /// The battery voltage will be extracted from the message and checked against
        /// the test parameters.
        /// If the TestConstant PushType = Manual, it will ask the operator to push the button.
        /// Otherwise the fixture will push the button(default). The duration of the push will be either
        /// the default of 2 seconds, or the value field in seconds in the TestConstant Fixture.
        /// </remarks>
        /// <param name="testParams"></param>
        /// <param name="testDB"></param>
        /// <param name="uutData"></param>
        /// <param name="outmsg"></param>
        /// <returns></returns>
        public int CheckBatteryTest(LimitsData.stepsItem testParams, MainWindow.uutdata uutData, ref string outmsg)
        {
            bool retrytest = true;
            bool stepPass = true;
            int stepResult = (int)ErrorCodes.Program.Success;
            string testname = string.Empty;
            string lowlimit = string.Empty;
            double dlowlimit = 0;
            string highlimit = string.Empty;
            double dhighlimit = 0;
            string operation = string.Empty;
            string limittype = string.Empty;
            double batvolt = 0;
            bool fixturePush = true;
            int pushTimeSeconds = 2;

            testDB.CreateStepRecord(testParams.name, testParams.step_number);

            // get the limits
            foreach (var item in testParams.limits)
            {
                if (item.name.ToUpper() == "BATVOLTAGE")
                {
                    dlowlimit = double.Parse(item.value1);
                    dhighlimit = double.Parse(item.value2);
                }
                else
                {
                    outmsg = "Missing the Limit parameter 'BatVoltage'";
                    stepResult = (int)ErrorCodes.System.MissingParameter;
                    testDB.AddStepResult("FAIL", outmsg, stepResult);
                    return stepResult;
                }
            }

            // get the button push type
            foreach (var item in testParams.parameters)
            {
                if (item.name.ToUpper() == "MANUAL")
                    fixturePush = false;
                if (item.name.ToUpper() == "FIXTURE")
                {
                    fixturePush = true;
                    pushTimeSeconds = Convert.ToInt32(item.value);
                }
            }

            while (retrytest)
            {
                if (fixturePush)
                    fixtureFunctions.PushERCButtonForXSeconds(1, pushTimeSeconds);
                else
                {
                    UpdateStatusWindow("--->>> Push any button <<<---\n", StatusType.prompt);
                }
                if (sensorConsole.WaitForPrompt(5000, "mV", out outmsg))
                    retrytest = false;
                else
                {
                    var response = MessageBox.Show("Voltage check", "Did not find response. Do you want to try again?", MessageBoxButtons.YesNo);
                    if (response == System.Windows.Forms.DialogResult.No)
                        retrytest = false;
                }
            }

            if (outmsg.Contains("Voltage"))
            {
                int vindex = outmsg.IndexOf("Voltage");
                int mindex = outmsg.IndexOf("mV");
                try
                {
                    batvolt = double.Parse(outmsg.Substring(vindex + 7, mindex - vindex - 7)) / 1000;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            if ((batvolt >= dlowlimit) && (batvolt <= dhighlimit))
            {
                stepPass = true;
            }
            else
            {
                stepPass = false;
                stepResult = (int)ErrorCodes.Program.ValueOutsideOfLimit + (testParams.step_number * 100);
            }
            testDB.AddStepResult(testParams.limits[0].name, stepPass ? "PASS" : "FAIL", "GELE", dlowlimit, dhighlimit, batvolt, "mV", "N3", "", stepResult);
            return stepResult;
        }





    }
}
