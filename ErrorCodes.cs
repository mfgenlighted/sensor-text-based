﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SensorManufSY2
{
    class ErrorCodes
    {
        public enum System
        {
            SystemErrorCodes = 100000,      // start of the System fails
            MissingParameter,               // 1 - one or more test parameters are missing
            ComponetInitFail,               // 2 - one or more of the station componets failed to initialize
            MacDBFailure,                   // 3 - MAC database failed
            SNDBFailure,                    // 4 - SN database failed
            ResultDBFailure,                // 5 - Result database failed
            InvalidParameter,               // 6 - Specified parameter is not a valid parameter
            OperatorCancel,                 // 7 - Operator canceled the opertaion
            FileOpenError,                  // 8 - error opening a file
            LabJackNotSetup,                // 9    - Labjack has not been setup
            LabJackError,                   // 10   - Labjack error executing a function
            LabelPrintError,                // 11   - label printing error
            LEDDetectorNotInitialize,       // 12   - serial LED detector has not be initialized
            InstrumentNotSetUp,             // 13 - instrument not set up 
            ACMeterReadError,               // 14 - error reading the power meter
            FixtureError,                   // 15 - fixture had a error
            CommandTimeout,                 // 16 - command timeout
            DatabaseSetupError,             // 17 - error in the database setup
            NoStationsDefined,              // 18 - there is no TestStation section in the config file
        }

        static public string[] SystemStrings =
        {
            "Start System Error Number",
            "Missing one or more test parameters",          // 1
            "System componet failed",                       // 2
            "MAC Database failure",                         // 3
            "SN Database failure",                          // 4
            "Result Database failure",                      // 5
            "Invalid Parametere",                           // 6
            "Operator Cancel",                              // 7
            "Error opening a file",                         // 8
            "Labjack has not been set up",                  // 9
            "Labjack error executing a function",           // 10
            "Error trying to print a label",                // 11
            "Serial LED detector has not be initialized",   // 12
            "Fixture instrument not setup",                 // 13
            "AC Meter read error",                          // 14
            "Fixture error",                                // 15
            "Command timeout",                              // 16
            "Database has a setup error",                   // 17
            "No TestStation defined in the StationConfig file", // 18
        };

        public enum Program
        {
            Success,                        // 0
            ValueOutsideOfLimit,            // 1 
            DUTComError,                    // 2
            DutUnexpectedCmdReturn,         // 3
            CUComError,                     // 4
            DupicateSN,                     // 5
            PCBADataMismatch,               // 6
            NotConfForChangeover,           // 7
            SeqFail,                        // 8
            NotInDVTMAN,                    // 9
            FailedToPerformCUFunction,      // 10
            OperatorAbort,                  // 11
            FixtureHardwareError,           // 12
            ChangedOver,                    // 13
            RefusedByPFS,                   // 14
            AdvertNotFound,                 // 15
            CommandTimeout,                 // 16
            DUTHardwareError,               // 17
            ExceptionOnConsoleCmdSend,      // 18
            DUTComErrorNoEchoFound,         // 19
            PromptNotFound,                 // 20
        }

        static public string[] ProgramStrings =
        {
            "Success",                              // 0
            "Value outside of limits",              // 1
            "DUT com error",                        // 2
            "DUT unexpected command return",        // 3
            "CU com error",                         // 4
            "Dupicate Serial Number found",         // 5
            "PCBA Data Mismatch",                   // 6
            "Not configured for Change-over",       // 7
            "Sequence failure",                     // 8
            "DUT not in DVTMAN",                    // 9
            "Failed to perform a CU function call", // 10
            "Operator abort",                       // 11
            "Fixture hardware error",               // 12
            "DUT has changed over",                 // 13
            "Refused by PFS",                       // 14
            "Advert not found",                     // 15
            "Command timeout",                      // 16
            "DUT Hardware error found",             // 17
            "Exception thrown when a commmand string was sent to the console",  //18
            "DUT command echo not found",           // 19
            "Prompt not found",                     // 20
        };

        /// <summary>
        /// Return the Error String for a error code.
        /// </summary>
        /// <param name="ErrorCode"></param>
        /// <param name="ErrorString"></param>
        static public string GetErrorString(int ErrorCode)
        {
            string ErrorString = string.Empty;

            if (ErrorCode >= (int)System.SystemErrorCodes)      // if a system error code
            {
                ErrorString = SystemStrings[ErrorCode - (int)System.SystemErrorCodes];
            }
            else
            {
                if (ErrorCode >= 100)       // if it has the test number added to it
                    ErrorString = ProgramStrings[ErrorCode - ((ErrorCode / 100) * 100)];
                else
                    ErrorString = ProgramStrings[ErrorCode];

            }
            return ErrorString;
        }
    }
}
