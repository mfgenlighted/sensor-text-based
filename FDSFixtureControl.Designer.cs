﻿namespace SensorManufSY2
{
    partial class FDSFixtureControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.textLidStatus = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textPushButtonStatus = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.TempProbeReading = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.DUT3p3vReading = new System.Windows.Forms.Label();
            this.checkBoxTestLamp = new System.Windows.Forms.CheckBox();
            this.numericUpDownLEDVoltage = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.buttonExit = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.checkBoxTestIndicator = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.textLEDFreq = new System.Windows.Forms.Label();
            this.textLEDIntest = new System.Windows.Forms.Label();
            this.LEDColor = new System.Windows.Forms.TextBox();
            this.richTextBoxCUWindow = new System.Windows.Forms.RichTextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.textSendToDUT = new System.Windows.Forms.RichTextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.appPrompt = new System.Windows.Forms.RadioButton();
            this.dvtmanPrompt = new System.Windows.Forms.RadioButton();
            this.bScanBluetooth = new System.Windows.Forms.Button();
            this.rtScanStatus = new System.Windows.Forms.RichTextBox();
            this.nBTtimeout = new System.Windows.Forms.NumericUpDown();
            this.label10 = new System.Windows.Forms.Label();
            this.bStartAdvertise = new System.Windows.Forms.Button();
            this.bClearSerailBuffers = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.tbTestRadioMAC = new System.Windows.Forms.Label();
            this.rtbTestRadioData = new System.Windows.Forms.RichTextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.bTestRaio = new System.Windows.Forms.Button();
            this.nudLQIM = new System.Windows.Forms.NumericUpDown();
            this.nudLQIT = new System.Windows.Forms.NumericUpDown();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.lbTestChannel = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.tControlVoltage = new System.Windows.Forms.Label();
            this.RadioTest = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.bStartTagAdvert = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.lbOscStatus = new System.Windows.Forms.Label();
            this.lbPIRMin = new System.Windows.Forms.Label();
            this.lbPIRMax = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.bPIROscTest = new System.Windows.Forms.Button();
            this.bPCBAConfig = new System.Windows.Forms.Button();
            this.bHLAConfig = new System.Windows.Forms.Button();
            this.tControlVoltage2 = new System.Windows.Forms.Label();
            this.lbDUTCommands = new System.Windows.Forms.ListBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.label19 = new System.Windows.Forms.Label();
            this.lbTagRadioMac = new System.Windows.Forms.Label();
            this.cbResetBLEatScan = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLEDVoltage)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nBTtimeout)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudLQIM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudLQIT)).BeginInit();
            this.RadioTest.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // textLidStatus
            // 
            this.textLidStatus.AutoSize = true;
            this.textLidStatus.Location = new System.Drawing.Point(123, 17);
            this.textLidStatus.Name = "textLidStatus";
            this.textLidStatus.Size = new System.Drawing.Size(33, 13);
            this.textLidStatus.TabIndex = 0;
            this.textLidStatus.Text = "Open";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 17);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Lid Status";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Push Button Status";
            // 
            // textPushButtonStatus
            // 
            this.textPushButtonStatus.AutoSize = true;
            this.textPushButtonStatus.Location = new System.Drawing.Point(123, 32);
            this.textPushButtonStatus.Name = "textPushButtonStatus";
            this.textPushButtonStatus.Size = new System.Drawing.Size(63, 13);
            this.textPushButtonStatus.TabIndex = 3;
            this.textPushButtonStatus.Text = "Not Pushed";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 47);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Temp Probe";
            // 
            // TempProbeReading
            // 
            this.TempProbeReading.AutoSize = true;
            this.TempProbeReading.Location = new System.Drawing.Point(123, 47);
            this.TempProbeReading.Name = "TempProbeReading";
            this.TempProbeReading.Size = new System.Drawing.Size(13, 13);
            this.TempProbeReading.TabIndex = 5;
            this.TempProbeReading.Text = "--";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 82);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(67, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "3.3V reading";
            // 
            // DUT3p3vReading
            // 
            this.DUT3p3vReading.AutoSize = true;
            this.DUT3p3vReading.Location = new System.Drawing.Point(123, 82);
            this.DUT3p3vReading.Name = "DUT3p3vReading";
            this.DUT3p3vReading.Size = new System.Drawing.Size(16, 13);
            this.DUT3p3vReading.TabIndex = 7;
            this.DUT3p3vReading.Text = "---";
            // 
            // checkBoxTestLamp
            // 
            this.checkBoxTestLamp.AutoSize = true;
            this.checkBoxTestLamp.Location = new System.Drawing.Point(38, 149);
            this.checkBoxTestLamp.Name = "checkBoxTestLamp";
            this.checkBoxTestLamp.Size = new System.Drawing.Size(76, 17);
            this.checkBoxTestLamp.TabIndex = 8;
            this.checkBoxTestLamp.Text = "Test Lamp";
            this.checkBoxTestLamp.UseVisualStyleBackColor = true;
            this.checkBoxTestLamp.CheckedChanged += new System.EventHandler(this.checkBoxTestLamp_CheckedChanged);
            // 
            // numericUpDownLEDVoltage
            // 
            this.numericUpDownLEDVoltage.DecimalPlaces = 1;
            this.numericUpDownLEDVoltage.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDownLEDVoltage.Location = new System.Drawing.Point(9, 173);
            this.numericUpDownLEDVoltage.Maximum = new decimal(new int[] {
            48,
            0,
            0,
            65536});
            this.numericUpDownLEDVoltage.Name = "numericUpDownLEDVoltage";
            this.numericUpDownLEDVoltage.Size = new System.Drawing.Size(41, 20);
            this.numericUpDownLEDVoltage.TabIndex = 9;
            this.numericUpDownLEDVoltage.ValueChanged += new System.EventHandler(this.numericUpDownLEDVoltage_ValueChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(57, 179);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(67, 13);
            this.label8.TabIndex = 10;
            this.label8.Text = "LED Voltage";
            // 
            // buttonExit
            // 
            this.buttonExit.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonExit.Location = new System.Drawing.Point(28, 603);
            this.buttonExit.Name = "buttonExit";
            this.buttonExit.Size = new System.Drawing.Size(75, 23);
            this.buttonExit.TabIndex = 17;
            this.buttonExit.Text = "Exit";
            this.buttonExit.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 347);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(104, 13);
            this.label3.TabIndex = 19;
            this.label3.Text = "Text to send to DUT";
            // 
            // checkBoxTestIndicator
            // 
            this.checkBoxTestIndicator.AutoSize = true;
            this.checkBoxTestIndicator.Location = new System.Drawing.Point(38, 199);
            this.checkBoxTestIndicator.Name = "checkBoxTestIndicator";
            this.checkBoxTestIndicator.Size = new System.Drawing.Size(82, 17);
            this.checkBoxTestIndicator.TabIndex = 21;
            this.checkBoxTestIndicator.Text = "DUT Power";
            this.checkBoxTestIndicator.UseVisualStyleBackColor = true;
            this.checkBoxTestIndicator.CheckedChanged += new System.EventHandler(this.checkBoxTestIndicator_CheckedChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 219);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(70, 13);
            this.label5.TabIndex = 22;
            this.label5.Text = "LED detector";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(33, 238);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(28, 13);
            this.label7.TabIndex = 23;
            this.label7.Text = "Freq";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(33, 251);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(33, 13);
            this.label9.TabIndex = 24;
            this.label9.Text = "Intest";
            // 
            // textLEDFreq
            // 
            this.textLEDFreq.AutoSize = true;
            this.textLEDFreq.Location = new System.Drawing.Point(84, 238);
            this.textLEDFreq.Name = "textLEDFreq";
            this.textLEDFreq.Size = new System.Drawing.Size(16, 13);
            this.textLEDFreq.TabIndex = 25;
            this.textLEDFreq.Text = "---";
            // 
            // textLEDIntest
            // 
            this.textLEDIntest.AutoSize = true;
            this.textLEDIntest.Location = new System.Drawing.Point(84, 251);
            this.textLEDIntest.Name = "textLEDIntest";
            this.textLEDIntest.Size = new System.Drawing.Size(16, 13);
            this.textLEDIntest.TabIndex = 26;
            this.textLEDIntest.Text = "---";
            // 
            // LEDColor
            // 
            this.LEDColor.Location = new System.Drawing.Point(154, 238);
            this.LEDColor.Name = "LEDColor";
            this.LEDColor.ReadOnly = true;
            this.LEDColor.Size = new System.Drawing.Size(22, 20);
            this.LEDColor.TabIndex = 27;
            this.LEDColor.TabStop = false;
            // 
            // richTextBoxCUWindow
            // 
            this.richTextBoxCUWindow.Location = new System.Drawing.Point(134, 522);
            this.richTextBoxCUWindow.Name = "richTextBoxCUWindow";
            this.richTextBoxCUWindow.Size = new System.Drawing.Size(395, 76);
            this.richTextBoxCUWindow.TabIndex = 28;
            this.richTextBoxCUWindow.Text = "";
            this.richTextBoxCUWindow.TextChanged += new System.EventHandler(this.richTextBoxCUWindow_TextChanged);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(9, 522);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(119, 23);
            this.button2.TabIndex = 29;
            this.button2.Text = "Test CU Interface";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.bCUTest_Click);
            // 
            // textSendToDUT
            // 
            this.textSendToDUT.Location = new System.Drawing.Point(17, 366);
            this.textSendToDUT.Name = "textSendToDUT";
            this.textSendToDUT.Size = new System.Drawing.Size(169, 21);
            this.textSendToDUT.TabIndex = 30;
            this.textSendToDUT.Text = "";
            this.textSendToDUT.TextChanged += new System.EventHandler(this.textSendToDUT_TextChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.appPrompt);
            this.groupBox1.Controls.Add(this.dvtmanPrompt);
            this.groupBox1.Location = new System.Drawing.Point(12, 271);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(133, 73);
            this.groupBox1.TabIndex = 31;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "DUT Prompt";
            // 
            // appPrompt
            // 
            this.appPrompt.AutoSize = true;
            this.appPrompt.Location = new System.Drawing.Point(14, 44);
            this.appPrompt.Name = "appPrompt";
            this.appPrompt.Size = new System.Drawing.Size(46, 17);
            this.appPrompt.TabIndex = 1;
            this.appPrompt.Text = "SU>";
            this.appPrompt.UseVisualStyleBackColor = true;
            // 
            // dvtmanPrompt
            // 
            this.dvtmanPrompt.AutoSize = true;
            this.dvtmanPrompt.Checked = true;
            this.dvtmanPrompt.Location = new System.Drawing.Point(14, 20);
            this.dvtmanPrompt.Name = "dvtmanPrompt";
            this.dvtmanPrompt.Size = new System.Drawing.Size(77, 17);
            this.dvtmanPrompt.TabIndex = 0;
            this.dvtmanPrompt.TabStop = true;
            this.dvtmanPrompt.Text = "DVTMAN>";
            this.dvtmanPrompt.UseVisualStyleBackColor = true;
            this.dvtmanPrompt.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // bScanBluetooth
            // 
            this.bScanBluetooth.Location = new System.Drawing.Point(26, 246);
            this.bScanBluetooth.Name = "bScanBluetooth";
            this.bScanBluetooth.Size = new System.Drawing.Size(148, 23);
            this.bScanBluetooth.TabIndex = 33;
            this.bScanBluetooth.Text = "Dongle Scan Bluetooth";
            this.bScanBluetooth.UseVisualStyleBackColor = true;
            this.bScanBluetooth.Click += new System.EventHandler(this.bScanBluetooth_Click);
            // 
            // rtScanStatus
            // 
            this.rtScanStatus.Location = new System.Drawing.Point(26, 17);
            this.rtScanStatus.Name = "rtScanStatus";
            this.rtScanStatus.Size = new System.Drawing.Size(266, 218);
            this.rtScanStatus.TabIndex = 36;
            this.rtScanStatus.Text = "";
            // 
            // nBTtimeout
            // 
            this.nBTtimeout.Location = new System.Drawing.Point(348, 214);
            this.nBTtimeout.Name = "nBTtimeout";
            this.nBTtimeout.Size = new System.Drawing.Size(52, 20);
            this.nBTtimeout.TabIndex = 37;
            this.nBTtimeout.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(318, 194);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(121, 13);
            this.label10.TabIndex = 38;
            this.label10.Text = "Scan Timeout(Seconds)";
            // 
            // bStartAdvertise
            // 
            this.bStartAdvertise.Location = new System.Drawing.Point(26, 275);
            this.bStartAdvertise.Name = "bStartAdvertise";
            this.bStartAdvertise.Size = new System.Drawing.Size(172, 23);
            this.bStartAdvertise.TabIndex = 39;
            this.bStartAdvertise.Text = "Start Bluetooth Advertise";
            this.bStartAdvertise.UseVisualStyleBackColor = true;
            this.bStartAdvertise.Click += new System.EventHandler(this.bStartAdvertise_Click);
            // 
            // bClearSerailBuffers
            // 
            this.bClearSerailBuffers.Location = new System.Drawing.Point(126, 342);
            this.bClearSerailBuffers.Name = "bClearSerailBuffers";
            this.bClearSerailBuffers.Size = new System.Drawing.Size(105, 23);
            this.bClearSerailBuffers.TabIndex = 40;
            this.bClearSerailBuffers.Text = "Clear serial buffers";
            this.bClearSerailBuffers.UseVisualStyleBackColor = true;
            this.bClearSerailBuffers.Click += new System.EventHandler(this.bClearInputBuffers_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(12, 102);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(80, 13);
            this.label11.TabIndex = 41;
            this.label11.Text = "Test radio MAC";
            // 
            // tbTestRadioMAC
            // 
            this.tbTestRadioMAC.AutoSize = true;
            this.tbTestRadioMAC.Location = new System.Drawing.Point(123, 102);
            this.tbTestRadioMAC.Name = "tbTestRadioMAC";
            this.tbTestRadioMAC.Size = new System.Drawing.Size(16, 13);
            this.tbTestRadioMAC.TabIndex = 42;
            this.tbTestRadioMAC.Text = "---";
            // 
            // rtbTestRadioData
            // 
            this.rtbTestRadioData.Location = new System.Drawing.Point(15, 450);
            this.rtbTestRadioData.Name = "rtbTestRadioData";
            this.rtbTestRadioData.Size = new System.Drawing.Size(169, 21);
            this.rtbTestRadioData.TabIndex = 44;
            this.rtbTestRadioData.Text = "";
            this.rtbTestRadioData.TextChanged += new System.EventHandler(this.textSendToRefRadio_TextChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(12, 434);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(133, 13);
            this.label12.TabIndex = 43;
            this.label12.Text = "Text to send to Test Radio";
            // 
            // bTestRaio
            // 
            this.bTestRaio.Location = new System.Drawing.Point(143, 47);
            this.bTestRaio.Name = "bTestRaio";
            this.bTestRaio.Size = new System.Drawing.Size(75, 23);
            this.bTestRaio.TabIndex = 45;
            this.bTestRaio.Text = "Test Radio";
            this.bTestRaio.UseVisualStyleBackColor = true;
            this.bTestRaio.Click += new System.EventHandler(this.bTestRaio_Click);
            // 
            // nudLQIM
            // 
            this.nudLQIM.Location = new System.Drawing.Point(78, 34);
            this.nudLQIM.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudLQIM.Name = "nudLQIM";
            this.nudLQIM.Size = new System.Drawing.Size(59, 20);
            this.nudLQIM.TabIndex = 46;
            this.nudLQIM.Value = new decimal(new int[] {
            200,
            0,
            0,
            0});
            // 
            // nudLQIT
            // 
            this.nudLQIT.Location = new System.Drawing.Point(78, 59);
            this.nudLQIT.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudLQIT.Name = "nudLQIT";
            this.nudLQIT.Size = new System.Drawing.Size(59, 20);
            this.nudLQIT.TabIndex = 47;
            this.nudLQIT.Value = new decimal(new int[] {
            200,
            0,
            0,
            0});
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(39, 36);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(33, 13);
            this.label13.TabIndex = 48;
            this.label13.Text = "LQIM";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(41, 62);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(31, 13);
            this.label14.TabIndex = 49;
            this.label14.Text = "LQIT";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(27, 13);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(45, 13);
            this.label15.TabIndex = 51;
            this.label15.Text = "channel";
            // 
            // lbTestChannel
            // 
            this.lbTestChannel.AutoSize = true;
            this.lbTestChannel.Location = new System.Drawing.Point(78, 12);
            this.lbTestChannel.Name = "lbTestChannel";
            this.lbTestChannel.Size = new System.Drawing.Size(16, 13);
            this.lbTestChannel.TabIndex = 52;
            this.lbTestChannel.Text = "---";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(12, 67);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(79, 13);
            this.label16.TabIndex = 53;
            this.label16.Text = "Control Voltage";
            // 
            // tControlVoltage
            // 
            this.tControlVoltage.AutoSize = true;
            this.tControlVoltage.Location = new System.Drawing.Point(123, 67);
            this.tControlVoltage.Name = "tControlVoltage";
            this.tControlVoltage.Size = new System.Drawing.Size(16, 13);
            this.tControlVoltage.TabIndex = 54;
            this.tControlVoltage.Text = "---";
            // 
            // RadioTest
            // 
            this.RadioTest.Controls.Add(this.lbTestChannel);
            this.RadioTest.Controls.Add(this.label15);
            this.RadioTest.Controls.Add(this.label14);
            this.RadioTest.Controls.Add(this.label13);
            this.RadioTest.Controls.Add(this.nudLQIT);
            this.RadioTest.Controls.Add(this.nudLQIM);
            this.RadioTest.Controls.Add(this.bTestRaio);
            this.RadioTest.Location = new System.Drawing.Point(260, 333);
            this.RadioTest.Name = "RadioTest";
            this.RadioTest.Size = new System.Drawing.Size(269, 98);
            this.RadioTest.TabIndex = 55;
            this.RadioTest.TabStop = false;
            this.RadioTest.Text = "Radio Test";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cbResetBLEatScan);
            this.groupBox2.Controls.Add(this.bStartTagAdvert);
            this.groupBox2.Controls.Add(this.button1);
            this.groupBox2.Controls.Add(this.bStartAdvertise);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.nBTtimeout);
            this.groupBox2.Controls.Add(this.rtScanStatus);
            this.groupBox2.Controls.Add(this.bScanBluetooth);
            this.groupBox2.Location = new System.Drawing.Point(254, 5);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(445, 326);
            this.groupBox2.TabIndex = 56;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Bluetooth Test";
            // 
            // bStartTagAdvert
            // 
            this.bStartTagAdvert.Location = new System.Drawing.Point(217, 275);
            this.bStartTagAdvert.Name = "bStartTagAdvert";
            this.bStartTagAdvert.Size = new System.Drawing.Size(172, 23);
            this.bStartTagAdvert.TabIndex = 41;
            this.bStartTagAdvert.Text = "Start Tag Advertise";
            this.bStartTagAdvert.UseVisualStyleBackColor = true;
            this.bStartTagAdvert.Click += new System.EventHandler(this.bStartTagAdvert_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(217, 246);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(133, 23);
            this.button1.TabIndex = 40;
            this.button1.Text = "DUT Scan Bluetooth";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.bDUTScanBluetooth_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.lbOscStatus);
            this.groupBox3.Controls.Add(this.lbPIRMin);
            this.groupBox3.Controls.Add(this.lbPIRMax);
            this.groupBox3.Controls.Add(this.label18);
            this.groupBox3.Controls.Add(this.label17);
            this.groupBox3.Controls.Add(this.bPIROscTest);
            this.groupBox3.Location = new System.Drawing.Point(257, 439);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(312, 68);
            this.groupBox3.TabIndex = 57;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "PIR Test";
            // 
            // lbOscStatus
            // 
            this.lbOscStatus.AutoSize = true;
            this.lbOscStatus.Location = new System.Drawing.Point(223, 19);
            this.lbOscStatus.Name = "lbOscStatus";
            this.lbOscStatus.Size = new System.Drawing.Size(16, 13);
            this.lbOscStatus.TabIndex = 5;
            this.lbOscStatus.Text = "---";
            // 
            // lbPIRMin
            // 
            this.lbPIRMin.AutoSize = true;
            this.lbPIRMin.Location = new System.Drawing.Point(171, 40);
            this.lbPIRMin.Name = "lbPIRMin";
            this.lbPIRMin.Size = new System.Drawing.Size(16, 13);
            this.lbPIRMin.TabIndex = 4;
            this.lbPIRMin.Text = "---";
            // 
            // lbPIRMax
            // 
            this.lbPIRMax.AutoSize = true;
            this.lbPIRMax.Location = new System.Drawing.Point(170, 20);
            this.lbPIRMax.Name = "lbPIRMax";
            this.lbPIRMax.Size = new System.Drawing.Size(16, 13);
            this.lbPIRMax.TabIndex = 3;
            this.lbPIRMax.Text = "---";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(140, 40);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(24, 13);
            this.label18.TabIndex = 2;
            this.label18.Text = "Min";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(137, 20);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(27, 13);
            this.label17.TabIndex = 1;
            this.label17.Text = "Max";
            // 
            // bPIROscTest
            // 
            this.bPIROscTest.Location = new System.Drawing.Point(23, 31);
            this.bPIROscTest.Name = "bPIROscTest";
            this.bPIROscTest.Size = new System.Drawing.Size(90, 23);
            this.bPIROscTest.TabIndex = 0;
            this.bPIROscTest.Text = "Oscillation Test";
            this.bPIROscTest.UseVisualStyleBackColor = true;
            this.bPIROscTest.Click += new System.EventHandler(this.bPIROscTest_Click);
            // 
            // bPCBAConfig
            // 
            this.bPCBAConfig.Location = new System.Drawing.Point(15, 476);
            this.bPCBAConfig.Name = "bPCBAConfig";
            this.bPCBAConfig.Size = new System.Drawing.Size(90, 23);
            this.bPCBAConfig.TabIndex = 58;
            this.bPCBAConfig.Text = "PCBA Config";
            this.toolTip1.SetToolTip(this.bPCBAConfig, "Program PCBA SN. Does not display\r\nin status window. Result in Bluetooth\r\nTest Wi" +
        "ndow.");
            this.bPCBAConfig.UseVisualStyleBackColor = true;
            this.bPCBAConfig.Click += new System.EventHandler(this.bPCBAConfig_Click);
            // 
            // bHLAConfig
            // 
            this.bHLAConfig.Location = new System.Drawing.Point(15, 497);
            this.bHLAConfig.Name = "bHLAConfig";
            this.bHLAConfig.Size = new System.Drawing.Size(90, 23);
            this.bHLAConfig.TabIndex = 59;
            this.bHLAConfig.Text = "HLA Config";
            this.toolTip1.SetToolTip(this.bHLAConfig, "Program HLA data based on\r\nconfiguration.");
            this.bHLAConfig.UseVisualStyleBackColor = true;
            this.bHLAConfig.Click += new System.EventHandler(this.bHLAConfig_Click);
            // 
            // tControlVoltage2
            // 
            this.tControlVoltage2.AutoSize = true;
            this.tControlVoltage2.Location = new System.Drawing.Point(151, 67);
            this.tControlVoltage2.Name = "tControlVoltage2";
            this.tControlVoltage2.Size = new System.Drawing.Size(16, 13);
            this.tControlVoltage2.TabIndex = 60;
            this.tControlVoltage2.Text = "---";
            // 
            // lbDUTCommands
            // 
            this.lbDUTCommands.FormattingEnabled = true;
            this.lbDUTCommands.Location = new System.Drawing.Point(17, 392);
            this.lbDUTCommands.Name = "lbDUTCommands";
            this.lbDUTCommands.Size = new System.Drawing.Size(169, 30);
            this.lbDUTCommands.TabIndex = 61;
            this.lbDUTCommands.Click += new System.EventHandler(this.lbDUTCommands_Click);
            this.lbDUTCommands.MouseHover += new System.EventHandler(this.lbDUTCommands_MouseHover);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(15, 119);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(81, 13);
            this.label19.TabIndex = 62;
            this.label19.Text = "Tag Radio Mac";
            // 
            // lbTagRadioMac
            // 
            this.lbTagRadioMac.AutoSize = true;
            this.lbTagRadioMac.Location = new System.Drawing.Point(126, 118);
            this.lbTagRadioMac.Name = "lbTagRadioMac";
            this.lbTagRadioMac.Size = new System.Drawing.Size(16, 13);
            this.lbTagRadioMac.TabIndex = 63;
            this.lbTagRadioMac.Text = "---";
            // 
            // cbResetBLEatScan
            // 
            this.cbResetBLEatScan.AutoSize = true;
            this.cbResetBLEatScan.Location = new System.Drawing.Point(321, 158);
            this.cbResetBLEatScan.Name = "cbResetBLEatScan";
            this.cbResetBLEatScan.Size = new System.Drawing.Size(115, 17);
            this.cbResetBLEatScan.TabIndex = 42;
            this.cbResetBLEatScan.Text = "Reset BLE at scan";
            this.cbResetBLEatScan.UseVisualStyleBackColor = true;
            // 
            // FDSFixtureControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(773, 638);
            this.ControlBox = false;
            this.Controls.Add(this.lbTagRadioMac);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.lbDUTCommands);
            this.Controls.Add(this.tControlVoltage2);
            this.Controls.Add(this.bHLAConfig);
            this.Controls.Add(this.bPCBAConfig);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.RadioTest);
            this.Controls.Add(this.tControlVoltage);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.rtbTestRadioData);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.tbTestRadioMAC);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.bClearSerailBuffers);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.textSendToDUT);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.richTextBoxCUWindow);
            this.Controls.Add(this.LEDColor);
            this.Controls.Add(this.textLEDIntest);
            this.Controls.Add(this.textLEDFreq);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.checkBoxTestIndicator);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.buttonExit);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.numericUpDownLEDVoltage);
            this.Controls.Add(this.checkBoxTestLamp);
            this.Controls.Add(this.DUT3p3vReading);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.TempProbeReading);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textPushButtonStatus);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textLidStatus);
            this.Name = "FDSFixtureControl";
            this.Text = "FDSFixtureControl";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FDSFixtureControl_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLEDVoltage)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nBTtimeout)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudLQIM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudLQIT)).EndInit();
            this.RadioTest.ResumeLayout(false);
            this.RadioTest.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label textLidStatus;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label textPushButtonStatus;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label TempProbeReading;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label DUT3p3vReading;
        private System.Windows.Forms.CheckBox checkBoxTestLamp;
        private System.Windows.Forms.NumericUpDown numericUpDownLEDVoltage;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button buttonExit;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox checkBoxTestIndicator;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label textLEDFreq;
        private System.Windows.Forms.Label textLEDIntest;
        private System.Windows.Forms.TextBox LEDColor;
        private System.Windows.Forms.RichTextBox richTextBoxCUWindow;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.RichTextBox textSendToDUT;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton appPrompt;
        private System.Windows.Forms.RadioButton dvtmanPrompt;
        private System.Windows.Forms.Button bScanBluetooth;
        private System.Windows.Forms.RichTextBox rtScanStatus;
        private System.Windows.Forms.NumericUpDown nBTtimeout;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button bStartAdvertise;
        private System.Windows.Forms.Button bClearSerailBuffers;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label tbTestRadioMAC;
        private System.Windows.Forms.RichTextBox rtbTestRadioData;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button bTestRaio;
        private System.Windows.Forms.NumericUpDown nudLQIM;
        private System.Windows.Forms.NumericUpDown nudLQIT;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label lbTestChannel;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label tControlVoltage;
        private System.Windows.Forms.GroupBox RadioTest;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label lbPIRMin;
        private System.Windows.Forms.Label lbPIRMax;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button bPIROscTest;
        private System.Windows.Forms.Label lbOscStatus;
        private System.Windows.Forms.Button bPCBAConfig;
        private System.Windows.Forms.Button bHLAConfig;
        private System.Windows.Forms.Label tControlVoltage2;
        private System.Windows.Forms.ListBox lbDUTCommands;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button bStartTagAdvert;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label lbTagRadioMac;
        private System.Windows.Forms.CheckBox cbResetBLEatScan;
    }
}