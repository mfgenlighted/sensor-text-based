﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

using LimitsDatabase;

namespace SensorManufSY2
{
    public partial class FDSFixtureControl : Form
    {
        FixtureFunctions fixture;
        SensorConsole senConsole;
        StationConfigure stationConfig;
        TestRadioConsole testRadio;
        CUConsole cuConsole;
        ACMeterCommications powerMeterConsole;
        SensorDVTMANTests sensorDVTMANTests = null;
        //LEDConfigSection section;

        public delegate void SetTextCallback(string text);
        //int temp1 = 0;
        //int temp2 = 0;
        //double dtemp = 0;
        string msg = string.Empty;
        string prompt = "DVTMAN> ";

        public delegate void updateFixtureReadings(string lid, string pushButton, string temp);
        public delegate void updateDUTReadings(string svoltage, string cvoltage, string cvoltage2);
        public delegate void updateLEDReadings(int intensity, int freq, Color backColor);
        public delegate void updateText(string msg);

        Thread tBLEScan;
        Thread tBLEStart;
        Thread tBLEAdvert;
        Thread tBLETagScan;
        Thread tBLETagStart;
        Thread tBLETagAdvert;
        Thread tBLEDUTScan;
        Thread tBLEDUTStart;
        Thread tBLEDUTAdvert;
        Thread tSendCommand;
        Thread tRadioTest;
        Thread tCUTest;
        Thread tPIROscTest;
        Thread tPCBAConfig;
        Thread tHLAConfig;

        ThreadStart updateScreenThreadStart;
        Thread updateScreenThread;
        ThreadStart updateLEDScreenThreadStart;
        Thread updateLEDScreenThread;
        bool runThreads = true;

        MainWindow mainWindow;


        public void sc_StatusUpdated(object sender, StatusUpdatedEventArgs e)
        {
            this.addTextToStatusWindow(e.statusText);
        }

        private void addTextToStatusWindow(string msg)
        {
            if (this.rtScanStatus.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(addTextToStatusWindow);
                this.Invoke(d, new object[] { msg});
            }
            else
            {
                this.rtScanStatus.AppendText(msg);
                this.rtScanStatus.ScrollToCaret();
                this.rtScanStatus.Update();
            }
        }        

        public FDSFixtureControl(MainWindow mainWindowobj, SensorConsole psenConsole)
        {
            string outmsg = string.Empty;


            InitializeComponent();

            stationConfig = mainWindowobj.stationConfig;
            mainWindow = mainWindowobj;


            fixture = new FixtureFunctions();
            fixture.StationConfigure = mainWindowobj.stationConfig;
            if (!fixture.InitFixture())
            {
                MessageBox.Show("Error setting up the fixture: " + fixture.LastFunctionErrorMessage);
                return;
            }

            senConsole = psenConsole;
            senConsole.DVTMANPrompt = prompt;

            //section = new LEDConfigSection();
            //section = LEDConfigSection.GetLEDSection();


            cuConsole = new CUConsole();                // CU serial
            powerMeterConsole = new ACMeterCommications();  // power meter

            cuConsole.DisplayRcvChar = true;
            cuConsole.DisplaySendChar = true;
            cuConsole.StatusUpdated += new EventHandler<StatusUpdatedEventArgs>(sc_StatusUpdated);
            if (stationConfig.ConfigDUTQCs.Count !=0)
            {
                foreach (var item in stationConfig.ConfigDUTQCs.Keys)
                {
                    lbDUTCommands.Items.Add(item);
                }
            }

            try
            {
                if (stationConfig.FixtureEquipment.hasTestRadio)
                {
                    //testRadio = new TestRadioConsole();          // serial console for test radio
                    //testRadio.Open(stationConfig.ConfigTestRadioComPort);
                    //testRadio.Setup(Convert.ToInt32(stationConfig.ConfigTestRadioChannel), ref outmsg);
                    tbTestRadioMAC.Text = fixture.refRadio.MAC;
                    lbTestChannel.Text = stationConfig.ConfigTestRadioChannel;
                }

                if (stationConfig.FixtureEquipment.hasEnlightedInterface)
                {
                    cuConsole.Open(stationConfig.ConfigCUComPort);
                    //cuConsole.StatusUpdated += new EventHandler<StatusUpdatedEventArgs>(sc_StatusUpdated);
                }

                if (stationConfig.FixtureEquipment.hasPowerMeter)
                {
                    powerMeterConsole.Open(stationConfig.ConfigPowerMeterComPort);
                }

                updateScreenThreadStart = new ThreadStart(StartUpdateScreen);
                updateScreenThread = new Thread(updateScreenThreadStart);
                updateScreenThread.Start();

                if (stationConfig.FixtureEquipment.hasLEDDetector)
                {
                    updateLEDScreenThreadStart = new ThreadStart(StartLEDUpdateScreen);
                    updateLEDScreenThread = new Thread(updateLEDScreenThreadStart);
                    updateLEDScreenThread.Start();
                }
                if (stationConfig.FixtureEquipment.hasBLE)
                {
                    lbTagRadioMac.Text = fixture.TagRadioMac;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error initializing fixture. " + ex.Message);
            }
        }


        private void checkBoxTestLamp_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxTestLamp.Checked)
                fixture.ControlPIRSource(1);
            else
                fixture.ControlPIRSource(0);
        }
        /// <summary>
        /// DUT Power control check box
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void checkBoxTestIndicator_CheckedChanged(object sender, EventArgs e)
        {
            string outmsg = string.Empty;

            if (checkBoxTestIndicator.Checked)
                fixture.ControlDUTPower(1);
            else
                fixture.ControlDUTPower(0);
        }

        private void numericUpDownLEDVoltage_ValueChanged(object sender, EventArgs e)
        {
            fixture.SetAmbientSensorLED(Convert.ToDouble(numericUpDownLEDVoltage.Value));
        }


        private void FDSFixtureControl_FormClosing(object sender, FormClosingEventArgs e)
        {
            runThreads = false;
            fixture.ControlPIRSource(0);
            if (powerMeterConsole != null)
                powerMeterConsole.Close();
            fixture.ControlDUTPower(0);
            if (cuConsole != null)
                cuConsole.Close();
            if (updateScreenThread != null)
            {
                while (updateScreenThread.IsAlive) 
                {
                    Thread.Sleep(0);
                }
                updateScreenThread.Join();
            }
            if (updateLEDScreenThread != null)
            {
                while (updateLEDScreenThread.IsAlive)
                {
                    Thread.Sleep(0);
                }
                updateLEDScreenThread.Join();
            }
            fixture.Dispose();
        }

        //---------------
        //  CU test
        private void bCUTest_Click(object sender, EventArgs e)
        {
            if (tCUTest != null)
            {
                if (tCUTest.IsAlive)
                {
                    rtScanStatus.AppendText("Already doing CU test\n");
                    return;
                }
            }
            tCUTest = new Thread(() => lclCUTest());
            tCUTest.Start();
        }

        private void lclCUTest()
        {
            string resultmessage = string.Empty;
            string cuMessage = string.Empty;
            string cuResponse = "CUver=" + stationConfig.ConfigCUVersionTag;
            int stepStatus = 0;
            updateText UpdateTextnDel = new updateText(lclUpdateTestResult);

            MainWindow.uutdata uutData = new MainWindow.uutdata();            // structure to save the data to configure the DUT with
            LimitsData.stepsItem testparams = new LimitsData.stepsItem();
            if (sensorDVTMANTests == null)
            {
                sensorDVTMANTests = new SensorDVTMANTests();
                sensorDVTMANTests.sensorConsole = senConsole;
                sensorDVTMANTests.cuConsole = cuConsole;
                sensorDVTMANTests.fixture = fixture;
                sensorDVTMANTests.stationConfig = stationConfig;
            }

            uutData.dvtmanPrompt = prompt;
            senConsole.ClearBuffers();
            testparams.step_number = 0;
            if (stationConfig.ConfigCUType.ToUpper().Equals("DALI"))
                sensorDVTMANTests.doDaliPortTest(testparams, ref uutData, ref resultmessage, ref stepStatus, cuResponse);
            else
                sensorDVTMANTests.CUPortTest(testparams, null, uutData, "", out resultmessage);

            richTextBoxCUWindow.BeginInvoke(UpdateTextnDel, resultmessage);
        }

        private void textSendToDUT_TextChanged(object sender, EventArgs e)
        {
            string cmd;

            if (textSendToDUT.Text.Contains('\n'))
            {
                senConsole.ClearBuffers();
                if (tSendCommand != null)
                {
                    if (tSendCommand.IsAlive)
                    {
                        rtScanStatus.AppendText("Already sending\n");
                        return;
                    }
                }
                cmd = textSendToDUT.Text.Substring(0, textSendToDUT.Text.Length - 1);
                tSendCommand = new Thread(() => docmdwrite(cmd, 1000));
                tSendCommand.Start();
                //senConsole.WriteLine(textSendToDUT.Text.Substring(0, textSendToDUT.Text.Length - 1), 1000);
                textSendToDUT.Text = "";
            }
        }

        private void docmdwrite(string cmd, int timeout)
        {
            senConsole.WriteLine(cmd, timeout);
        }

        private void textSendToRefRadio_TextChanged(object sender, EventArgs e)
        {
            string cmd;

            if (rtbTestRadioData.Text.Contains('\n'))
            {
                fixture.refRadio.ClearBuffers();
                if (tSendCommand != null)
                {
                    if (tSendCommand.IsAlive)
                    {
                        rtScanStatus.AppendText("Already sending\n");
                        return;
                    }
                }
                cmd = rtbTestRadioData.Text.Substring(0, rtbTestRadioData.Text.Length - 1);
                tSendCommand = new Thread(() => dorrwrite(cmd, 1000));
                tSendCommand.Start();
                //senConsole.WriteLine(textSendToDUT.Text.Substring(0, textSendToDUT.Text.Length - 1), 1000);
                textSendToDUT.Text = "";
            }

        }

        private void dorrwrite(string cmd, int timeout)
        {
            fixture.refRadio.WriteLine(cmd, timeout);
        }


        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            if (sender == dvtmanPrompt)
                prompt = "DVTMAN> ";
            else
                prompt = "SU> ";
        }

        private void bScanBluetooth_Click(object sender, EventArgs e)
        {
            List<string> devices;
            rtScanStatus.Clear();
            devices = new List<string>();
            string outmsg = string.Empty;

            if (tBLEScan != null)
            {
                if (tBLEScan.IsAlive)
                {
                    rtScanStatus.AppendText("Already scanning\n");
                    return;
                }
            }

            rtScanStatus.AppendText("Scan started");

            BluetoothFunctions blescan = new BluetoothFunctions(stationConfig.ConfigBLEComPort, stationConfig.ConfigTagComPort, senConsole);
            blescan.StatusUpdated += new EventHandler<StatusUpdatedEventArgs>(sc_StatusUpdated);

            tBLEScan = new Thread(() => blescan.FindBluetoothDevices(Convert.ToInt32(nBTtimeout.Value)));
            tBLEScan.Start();
            rtScanStatus.AppendText("..\n");

        }

        private void bDUTScanBluetooth_Click(object sender, EventArgs e)
        {
            List<string> devices;
            rtScanStatus.Clear();
            devices = new List<string>();
            string outmsg = string.Empty;

            if (tBLEDUTScan != null)
            {
                if (tBLEDUTScan.IsAlive)
                {
                    rtScanStatus.AppendText("Already scanning\n");
                    return;
                }
            }

            rtScanStatus.AppendText("Scan started");

            BluetoothFunctions blescan = new BluetoothFunctions(stationConfig.ConfigBLEComPort, stationConfig.ConfigTagComPort, senConsole);
            blescan.StatusUpdated += new EventHandler<StatusUpdatedEventArgs>(sc_StatusUpdated);

            tBLEScan = new Thread(() => blescan.DutFindBluetoothDevices(Convert.ToInt32(nBTtimeout.Value), cbResetBLEatScan.Checked ?  5:0));
            tBLEScan.Start();
            rtScanStatus.AppendText("..\n");
        }



        private void bStartAdvertise_Click(object sender, EventArgs e)
        {
            if (tBLEAdvert != null)
            {
                if (tBLEAdvert.IsAlive)
                {
                    rtScanStatus.AppendText("Already changing over\n");
                    return;
                }
            }
            BluetoothFunctions btadvert = new BluetoothFunctions(stationConfig.ConfigBLEComPort, stationConfig.ConfigTagComPort, senConsole);
            btadvert.StatusUpdated += new EventHandler<StatusUpdatedEventArgs>(sc_StatusUpdated);

            senConsole.ClearBuffers();
            tBLEAdvert = new Thread(() => btadvert.DoBLEAdvertise());
            tBLEAdvert.Start();

        }


        private void StartUpdateScreen()
        {
            double dtemp;

            updateFixtureReadings UpdateFixtureScreenDel = new updateFixtureReadings(UpdateFixtureReadings);
            updateDUTReadings UpdateDUTReadingsDel = new updateDUTReadings(UpdateDUTReadings);

            while (runThreads)
	        {
                if (stationConfig.FixtureEquipment.hasLabJack)
                {
                    try
                    {
                        fixture.ReadTemperatureProbe(out dtemp, out msg);
                        this.textLidStatus.BeginInvoke(UpdateFixtureScreenDel, fixture.IsCoverClosed() ? "CLOSED" : "OPEN",
                                                                        fixture.IsBoxButtonPushed() ? "Pushed" : "Not Pushed",
                                                                        dtemp.ToString("N1"));
                        this.textLidStatus.BeginInvoke(UpdateDUTReadingsDel, fixture.Read3p3Voltage().ToString("N1"), fixture.ReadControlVoltage().ToString("N1"),
                                                            fixture.ReadControlVoltage2().ToString("N1"));
                        Thread.Sleep(100);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Error in labjack reading. " + ex.Message);
                    }
                }
                else
                {
                    this.textLidStatus.BeginInvoke(UpdateFixtureScreenDel, "---",
                                                                    "---",
                                                                    "0");
                    this.textLidStatus.BeginInvoke(UpdateDUTReadingsDel, "0", "0");
                    Thread.Sleep(250);
                }
            }
        }

        private void StartLEDUpdateScreen()
        {
            int intensity = 0;
            int freq = 0;
            string msg = string.Empty;
            Color backColor;

            updateLEDReadings UpdateLEDScreenDel = new updateLEDReadings(UpdateLEDReadings);
            while (runThreads)
            {
                try
                {
                    if (fixture.CheckLedColor("Red", stationConfig.ConfigLedDetector.ColorRED, ref freq, ref intensity, ref msg) == (int)ErrorCodes.Program.Success)
                    {
                        backColor = Color.Red;
                    }
                    else if (fixture.CheckLedColor("Green", stationConfig.ConfigLedDetector.ColorGREEN, ref freq, ref intensity, ref msg) == (int)ErrorCodes.Program.Success)
                    {
                        backColor = Color.Green;
                    }
                    else if (fixture.CheckLedColor("Blue", stationConfig.ConfigLedDetector.ColorBLUE, ref freq, ref intensity, ref msg) == (int)ErrorCodes.Program.Success)
                    {
                        backColor = Color.Blue;
                    }
                    else
                    {
                        backColor = Color.Gray;
                    }
                    this.textLEDFreq.BeginInvoke(UpdateLEDScreenDel, intensity, freq, backColor);
                    Thread.Sleep(500);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error reading led detector. " + ex.Message);
                }
            }

        }

        private void UpdateFixtureReadings(string lid, string pushButton, string temp)
        {
            textLidStatus.Text = lid;
            textPushButtonStatus.Text = pushButton;
            TempProbeReading.Text = temp;
        }

        private void UpdateDUTReadings(string svoltage, string cvoltage, string cvoltage2)
        {
            DUT3p3vReading.Text = svoltage;
            tControlVoltage.Text = cvoltage;
            if (stationConfig.FixtureEquipment.has2ControlVoltages)
                tControlVoltage2.Text = cvoltage2;
            else
                tControlVoltage2.Text = "---";
        }

        private void UpdateLEDReadings(int intensity, int freq, Color backColor)
        {
            LEDColor.BackColor = backColor;
            textLEDFreq.Text = freq.ToString("N0");
            textLEDIntest.Text = intensity.ToString("N0");
        }


        /// <summary>
        /// Will clear the DUT console serial buffers.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bClearInputBuffers_Click(object sender, EventArgs e)
        {
            senConsole.ClearBuffers();
        }

        private void richTextBoxCUWindow_TextChanged(object sender, EventArgs e)
        {

        }

        private void bTestRaio_Click(object sender, EventArgs e)
        {


            if (tRadioTest != null)
            {
                if (tRadioTest.IsAlive)
                {
                    rtScanStatus.AppendText("Already doing radio test\n");
                    return;
                }
            }
            tRadioTest = new Thread(() => lclTestNetworks());
            tRadioTest.Start();
        }

        private void lclTestNetworks ()
        {
            string resultmessage = string.Empty;
            string cuMessage = string.Empty;
            string cuResponse = "CUver=" + stationConfig.ConfigCUVersionTag;
            int stepStatus = 0;
            updateText UpdateTextnDel = new updateText(lclUpdateTestResult);
            LimitsData.limitsItem limitItem;

            MainWindow.uutdata uutData = new MainWindow.uutdata();            // structure to save the data to configure the DUT with

            senConsole.ClearBuffers();

            // estToRun(string name, int testnumber, string function, string dllFile, string dllClass, string testType, string limittype)
            LimitsData.stepsItem testparams = new LimitsData.stepsItem();
            limitItem = new LimitsData.limitsItem();
            limitItem.name = "RADIOLQIM";
            limitItem.type = "VALUE";
            limitItem.operation = "GE";
            limitItem.value1 = nudLQIM.Value.ToString();
            limitItem.value2 = "";
            testparams.limits.Add(limitItem);
            limitItem.name = "RADIOLQIT";
            limitItem.type = "VALUE";
            limitItem.operation = "GE";
            limitItem.value1 = nudLQIT.Value.ToString();
            limitItem.value2 = "";
            testparams.limits.Add(limitItem);
            if (sensorDVTMANTests == null)
            {
                sensorDVTMANTests = new SensorDVTMANTests();
                sensorDVTMANTests.sensorConsole = senConsole;
                sensorDVTMANTests.testRadioConsole = testRadio;
                sensorDVTMANTests.cuConsole = cuConsole;
                sensorDVTMANTests.fixture = fixture;
                sensorDVTMANTests.stationConfig = stationConfig;
            }


            uutData.dvtmanPrompt = prompt;
            uutData.testChannel = Convert.ToInt32(stationConfig.ConfigTestRadioChannel);
            stepStatus = sensorDVTMANTests.TestNetworks(testparams, null, uutData, ref resultmessage);
            if (stepStatus == 0)
                resultmessage = "Test Passed\n";
            else
                resultmessage = "Test Failed: " + resultmessage + "\n";
            richTextBoxCUWindow.BeginInvoke(UpdateTextnDel, resultmessage);
        }

        private void lclUpdateTestResult(string msg)
        {
            this.richTextBoxCUWindow.AppendText(msg);
        }

        private void bPIROscTest_Click(object sender, EventArgs e)
        {
            if (tPIROscTest  != null)
            {
                if (tPIROscTest.IsAlive)
                {
                    rtScanStatus.AppendText("Already doing PIR Osc Test test\n");
                    return;
                }
            }
            tPIROscTest = new Thread(() => lclPIROscTest());
            tPIROscTest.Start();

        }

        delegate void delUpdateMinMax(int min, int max);
        private void lclPIROscTest()
        {
            MainWindow.codeClasses codeClasses = new MainWindow.codeClasses();
            updateDUTReadings delUpdateMinMax = new updateDUTReadings(lclUpdatePIRMinMax);
            updateText delUpdateText = new updateText(lclUpdateOscStatus);
            int status;

            codeClasses.sensorConsole = senConsole;
            codeClasses.fixtureFunctions = fixture;

            PIRTests pirTests = new PIRTests(codeClasses);

            if ((status = pirTests.IsPIROscillating(5, prompt)) == 1)
                lbOscStatus.BeginInvoke(delUpdateText, "Oscillating");
            else if (status == -1)
                lbOscStatus.BeginInvoke(delUpdateText, pirTests.ErrorMsg);
            else
                lbOscStatus.BeginInvoke(delUpdateText, "Not Oscillating");

            if (pirTests.pirData.Count != 0)
            {
                lbPIRMin.BeginInvoke(delUpdateMinMax, pirTests.pirData.Min().ToString(), pirTests.pirData.Max().ToString(), "");
            }
            else
            {
                lbPIRMin.BeginInvoke(delUpdateMinMax, "no data", "no data", "");
            }

        }
        private void lclUpdateOscStatus(string msg)
        {
            lbOscStatus.Text = msg;
        }

        private void lclUpdatePIRMinMax(string min, string max, string nothing)
        {
            lbPIRMin.Text = min;
            lbPIRMax.Text = max;
        }

        private void bPCBAConfig_Click(object sender, EventArgs e)
        {
            if (tPCBAConfig != null)
            {
                if (tPCBAConfig.IsAlive)
                {
                    rtScanStatus.AppendText("Already doing function\n");
                    return;
                }
            }
            tPCBAConfig = new Thread(() => lclPCBConfig_Click());
            tPCBAConfig.Start();
        }
        /// <summary>
        /// Will program PCBA info. The S1/S2 values will not allow Changeover
        /// </summary>
        private void lclPCBConfig_Click()
        { 
            string cmd;
            string sn;
            string pn;
            string mac;
            bool oldrcv, oldtrx;
            GetPCBAInfo iForm = new GetPCBAInfo();

            if (iForm.ShowDialog() == DialogResult.OK)
            {
                sn = iForm.SN.ToUpper();
                pn = iForm.pcbaPN;
                mac = iForm.MAC;
                iForm.Dispose();
            }
            else   // operator canceled
            {
                sn = string.Empty;
                iForm.Dispose();
                return;
            }

            cmd = string.Format("s pcba 0000{0} {1} {2} 7CA9997D 656E6C6967", mac, pn, sn);
            senConsole.ClearBuffers();
            oldrcv = senConsole.DisplayRcvChar;
            oldtrx = senConsole.DisplaySendChar;
            senConsole.DisplayRcvChar = false;
            senConsole.DisplaySendChar = false;
            senConsole.WriteLine(cmd, 1000);
            addTextToStatusWindow("PCBA program done\n");
            senConsole.DisplayRcvChar = oldrcv;
            senConsole.DisplaySendChar = oldtrx;
        }

        private void bHLAConfig_Click(object sender, EventArgs e)
        {
            if (tHLAConfig != null)
            {
                if (tHLAConfig.IsAlive)
                {
                    rtScanStatus.AppendText("Already doing function\n");
                    return;
                }
            }
            tHLAConfig = new Thread(() => lclbHLAConfig_Click());
            tHLAConfig.Start();
        }

        private void lclbHLAConfig_Click()
        {
            string cmd;
            string sn;
            string pn;
            string model;

            GetHLAInfo iForm = new GetHLAInfo();

            if (iForm.ShowDialog() == DialogResult.OK)
            {
                sn = iForm.SN.ToUpper();
                pn = iForm.PN;
                model = iForm.Model;
                iForm.Dispose();
            }
            else   // operator canceled
            {
                sn = string.Empty;
                iForm.Dispose();
                return;
            }

            cmd = string.Format("s hla {0} {1} {2}", pn, sn, model);
            senConsole.ClearBuffers();
            senConsole.WriteLine(cmd, 10000);
        }

        private void lbDUTCommands_Click(object sender, EventArgs e)
        {
            ListBox mybutton = sender as ListBox;
            int selectedCmdIndex;

            selectedCmdIndex = mybutton.SelectedIndex;
            textSendToDUT.Text = stationConfig.ConfigDUTQCs[mybutton.SelectedItem.ToString()] + "\n";
            mybutton.Height = 30;
        }

        private void lbDUTCommands_MouseHover(object sender, EventArgs e)
        {
            lbDUTCommands.Height = 200;
        }

        private void bStartTagAdvert_Click(object sender, EventArgs e)
        {
            if (tBLETagAdvert != null)
            {
                if (tBLETagAdvert.IsAlive)
                {
                    rtScanStatus.AppendText("Already running over\n");
                    return;
                }
            }
            BluetoothFunctions btadvert = new BluetoothFunctions(stationConfig.ConfigBLEComPort, stationConfig.ConfigTagComPort, senConsole);
            btadvert.StatusUpdated += new EventHandler<StatusUpdatedEventArgs>(sc_StatusUpdated);

            senConsole.ClearBuffers();
            tBLETagAdvert = new Thread(() => btadvert.DoInitTag());
            tBLETagAdvert.Start();

        }
    }

}
