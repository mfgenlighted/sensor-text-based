﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Diagnostics;
using System.IO;
using System.IO.Ports;
using System.Xml.Linq;
using System.Management;

using LabJack.LabJackUD;
//using LimitFileFunctions;

namespace SensorManufSY2
{

    public class FixtureFunctions : IDisposable
    {
        //----------------------
        // class properties
        //----------------------
        bool disposed = false;
        private StationConfigure stationConfigure = null;   // all the config data
        public StationConfigure StationConfigure { set { stationConfigure = value; } }
        public int DUTTurnOnDelayMs = 100;                 // time to let relays settle after commands
        public string LastFunctionErrorMessage = string.Empty;              // if a function had a error, contains error message
        public int LastFunctionStatus = (int)ErrorCodes.Program.Success;    // status of the last function called

        //------------------------------------------
        // values for the LabJack channels
        //          all | has2ControlVoltages | !has2ControlVoltages | hasSwitchMate | !hasSwitchMate
        //  AIN0        | SO2                 |   LS                 |               |
        //  AIN1    3.3 |                     |                      |               |
        //  AIN2    SO1 |                     |                      |               |
        //  AIN3    TP  |                     |                      |               |
        //  FIO4    OK  |                     |                      |               |
        //  FIO5        | LS                  | SLED                 |               |
        //  FIO6    PIR |                     |                      |               |
        //  FIO7        |                     |                      |               | PC
        //  DAC0    AS
        //  EIO1    B1
        //  EI02    B2
        //  EI03    B3
        //  EIO4    B4
        //  EIO5    WTI
        //  EIO6    WBD
        //  SM-1        |                     |                      | PC            |
        //  SM-2                                                       PIR
        //
        //  TP = temperture probe
        //  3.3 = SU 3.3v
        //  OK = OK operator button
        //  LS = Lid switch
        //  PC = DUT power control
        //  SO1 = orignal sensor output voltage
        //  SO2 = second sensor output voltage
        //  SLED = serial LED detector.
        //          Note: all fixtures that has2ControlVoltages will have the USB LED detector
        //  PIR = PIR heat source power
        //  AS = ambient LED source/Wireless switch power
        //  Bx = wireless switch button x pusher
        //  WTI = wireless switch fixture test indicator
        //  WBD = wireless switch fixture, connect BAT to DAC0
        //-------------------------------------------
        private U3 u3 = null;                               // instance for the LabJack
        SerialLEDDectector serialLedDetector = null;        // serial LED detector

        // for all fixtures
        private const int ljChannelCloseSwitch1 = 0;        // AIN0 - Fixture Closed Switch
        private const int ljChannelCloseSwitch2 = 5;        // FIO5 - Fixture Closed Switch
        private const int ljChannel3p3V = 1;                // AIN1 - SU 3.3v
        private const int ljChannelOKButton = 4;            // FIO4 - OK Button

        // Sensor, Plug load fixtures
        private const int ljChannelDUTPwrControl = 7;       // FI07 - digital output - DUT power/tEST LIGHT
        private const int smChannelDUTPwrControl = 1;       // channel 1 of the Switch-Mate for power control


        // Sensor fixture
        private const int ljChannelAMBSource = 0;       // DAC0 - Ambient sensor light source
        private const int ljChannelPIRSource = 6;       // FI06 - digital output - PIR heat source
        private const int smChannelPIRSource = 2;       // switch mate relay 2 - PIR heat source
        private const int ljChannelTempProbe = 3;       // AIN3 - temperture probe

        // Sensor Fixture - enlighted interface
        private const int ljChannelControlVoltage = 2;  // AIN2 - control output voltage of sensor
        private const int ljChannelControlVoltage2 = 0;  // AIN0 - control output voltage of sensor

        // Plug Load Fixtures
        private const int ljChannelACOuputMonitor = 6;   // FI06 - digital output - AC monitor 0=switched output 1=unswitched output

        // wireless switch fixtures
        private const int ljChannelDACBattery = 0;      // DAC0 - supply voltage
        private const int ljChannelERCButton1 = 8;      // EIO0
        private const int ljChannelERCButton2 = 9;      // EIO1
        private const int ljChannelERCButton3 = 10;     // EIO2
        private const int ljChannelERCButton4 = 11;     // EIO3
        private const int ljChannelERCTestIndicator = 12;  // EIO4
        private const int ljChannelConnectBatToDAC0 = 13; // EIO5

        public bool IsLabJackInitialized { get { return (u3 != null); } }


        //----------------------------------------------
        //  Relays
        //----------------------------------------------
        private RelayDriver relayDriver;

        //---------------------------------------
        //  Ref Radio data
        //---------------------------------------
        private string refRadioSerialPort = string.Empty;       // will be set from station config passed to init
        public TestRadioConsole refRadio;                      // handle for ref radio    

        //-------------------------------------
        //  Tag Radio data
        //-------------------------------------
        public string TagRadioMac = string.Empty;               // this is filled with the InitFixture is run

        //------------------------------------
        //  fixture defines
        //  Defines the functions that the fixture has for each type
        //---------------------------------------
        public class fixtureTypes
        {
            public string desc   = string.Empty;
            public string fixtureType = string.Empty;       // defines the config of a fixture
            public bool hasEnlightedInterface = false;      // enlighted interface, CU and 0-10V output
            public bool hasConsolePort = false;             // has a console port to the DUT
            public bool usesCUPortForConsole = false;       // The console port is on the CU port.
            public bool usesPacketBasedConsole = false;     // uses the packet based commuications instead of plain text
            public bool hasDaliInterface = false;           // dali interface
            public bool has2ControlVoltages = false;        // has 2 sensor controls out instead of 1
            public bool hasAmbientSensor = false;           // has ambient light sensor
            public bool hasPirSensor = false;               // has PIR sensor
            public bool hasBLE = false;                     // has bluetooth. If hasEnlightedBLETestRadio = false then
                                                            //  there is a TI dongle and a Tag radio for the BLE functions.

            public bool hasEnlightedBLETestRadio = false;   // has a single radio which does Enlighted and BLE functions
            public bool hasTestRadio = false;               // has a test radio(Enlighted only)
            public bool hasSniffer = false;                 // has a sniffer
            public bool hasDutPowerControl = false;         // able to control the dut power supply. 
                                                            //  hasSwitchMate = false, controlled by FI07
                                                            //  hasSwitchMate = true, controlled by SwitchMate relay 1
            public bool hasWsButtonPushers = false;         // button push controled by system
            public bool hasWsPowerControl = false;          // has power control for WS fixture
            public bool hasFixturePushButton = false;       // OK button on the fixture
            public bool hasTempProbe = false;               // has a temperature probe
            public bool hasPowerLoad = false;               // has a power load
            public bool hasPowerMeter = false;              // has a CU as a power meter
            public bool hasPCBAPrinter = false;             // has a PCBA printer
            public bool hasHLAPrinter = false;              // has a HLA printer
            public bool hasLEDDetector = false;             // has LED detector of some sort
            public bool hasLidSwitch = false;               // has a lid down switch.
                                                            //  if has2ControlVoltage=false, status is voltage on AIN0, high=closed
                                                            //  if has2ControlVoltage=true, status is digital on FIO5, low=closed
            public bool hasLabJack = false;                 // has a LabJack
            public bool hasSwitchMate = false;              // has a Switch Mate.
        };

        public List<fixtureTypes> fixtureTypeList = new List<fixtureTypes>
        {
                new fixtureTypes
                {
                    desc = "Plug Load Fixture",
                    fixtureType = "JB_SY2",
                    hasDutPowerControl = true,
                    hasFixturePushButton = true,
                    hasPowerLoad = true,
                    hasPCBAPrinter = true,             // has a PCBA printer
                    hasHLAPrinter = true,              // has a HLA printer
                    hasConsolePort = true,             // has a console port to the DUT
                    hasLEDDetector = true,
                    hasLidSwitch = true,
                    hasTestRadio = true,                // has a test radio
                    hasPowerMeter = true,
                    hasLabJack = true,                  // has Labjack data aqc
                },
                new fixtureTypes
                {
                    desc = "SU-2 Fixture, no power control",
                    fixtureType = "SU_SY2",
                    hasEnlightedInterface = true,
                    hasAmbientSensor = true,
                    hasPirSensor = true,
                    hasHLAPrinter = true,              // has a HLA printer
                    hasPCBAPrinter = true,              // has have a PCBA printer
                    hasConsolePort = true,             // has a console port to the DUT
                    hasLEDDetector = true,
                     hasLidSwitch = true,
                    hasTestRadio = true,                // has a test radio
                    hasLabJack = true,                  // has Labjack data aqc
                   hasTempProbe = true
                },

                // updated SU-2 fixtures. Just BLE added
                new fixtureTypes
                {
                    desc = "SU-4S Fixture, BLE, no power control",
                    fixtureType = "SU_BLE_SY2",
                    hasEnlightedInterface = true,
                    hasAmbientSensor = true,
                    hasPirSensor = true,
                    hasHLAPrinter = true,              // has a HLA printer
                    hasPCBAPrinter = true,              // has have a PCBA printer
                    hasConsolePort = true,             // has a console port to the DUT
                    hasLEDDetector = true,
                     hasLidSwitch = true,
                    hasTestRadio = true,                // has a test radio
                    hasLabJack = true,                  // has Labjack data aqc
                   hasTempProbe = true,
                   hasBLE = true
                },
                // Mirage fixture
                new fixtureTypes
                {
                    desc = "CS-D2/FS-D22 2-wire Fixture",
                    fixtureType = "SU4_SY2",
                    hasDaliInterface = true,
                    hasAmbientSensor = true,
                    hasPirSensor = true,
                    hasBLE = true,
                    hasDutPowerControl = true,
                    hasFixturePushButton = true,
                    hasHLAPrinter = true,              // has a HLA printer
                    hasConsolePort = true,             // has a console port to the DUT
                    hasLEDDetector = true,
                    hasLidSwitch = true,
                    hasTestRadio = true,                // has a test radio
                    hasLabJack = true,                  // has Labjack data aqc
                    hasTempProbe = true
                },
                // Cheetah fixture
                new fixtureTypes
                {
                    desc = "6 wire Sensors with BLE, SwitchMate Power Control",
                    fixtureType = "SU_6W_BLE_SMPC",        // 6 wire SU tester, with BLE, and switch-mate dut power control
                    hasEnlightedInterface = true,
                    hasDaliInterface = false,
                    hasAmbientSensor = true,
                    hasPirSensor = true,
                    hasBLE = true,
                    hasDutPowerControl = true,
                    hasSwitchMate = true,               // DUT power controlled with the Switch-Mate
                    hasFixturePushButton = true,
                    hasHLAPrinter = true,              // has a HLA printer
                    hasConsolePort = true,             // has a console port to the DUT
                    hasLEDDetector = true,
                    hasLidSwitch = true,
                    hasTestRadio = true,                // has a test radio
                    hasLabJack = true,                  // has Labjack data aqc
                    hasTempProbe = true,
                    has2ControlVoltages = true          // two control outs, AIN0/AIN2, Lid switch FIO5
                },

                // Kona fixture
                new fixtureTypes
                {
                    desc = "Functional SU-5 fixture",
                    fixtureType = "SU_8W_2DIM_BLE_SMPC_1RADIO_1COM",
                    hasEnlightedInterface = true,       // enlighted interface, CU and 0-10V output
                    hasConsolePort = true,              // has a console port to the DUT
                    usesCUPortForConsole = true,        // The console port is on the CU port.
                    usesPacketBasedConsole = true,      // use packet base commuications
                    hasDaliInterface = false,           // use dali interface
                    has2ControlVoltages = true,         // has 2 sensor controls out instead of 1
                    hasAmbientSensor = true,            // has ambient light sensor
                    hasPirSensor = true,                // has PIR sensor
                    hasBLE = true,                      // has bluetooth. If hasEnlightedBLETestRadio = false then
                                                                    //  there is a TI dongle and a Tag radio for the BLE functions.

                    hasEnlightedBLETestRadio = true,    // has a single radio which does Enlighted and BLE functions
                    hasTestRadio = false,               // has a test radio(Enlighted only)
                    hasSniffer = false,                 // has a sniffer
                    hasDutPowerControl = true,          // able to control the dut power supply. 
                                                                    //  hasSwitchMate = false, controlled by FI07
                                                                    //  hasSwitchMate = true, controlled by SwitchMate relay 1
                    hasWsButtonPushers = false,         // button push controled by system
                    hasWsPowerControl = false,          // has power control for WS fixture
                    hasFixturePushButton = true,        // OK button on the fixture
                    hasTempProbe = true,                // has a temperature probe
                    hasPowerLoad = false,               // has a power load
                    hasPowerMeter = false,              // has a CU as a power meter
                    hasPCBAPrinter = false,             // has a PCBA printer
                    hasHLAPrinter = false,              // has a HLA printer
                    hasLEDDetector = true,              // has LED detector of some sort
                    hasLidSwitch = true,                // has a lid down switch.
                                                                    //  if has2ControlVoltage=false, status is voltage on AIN0, high=closed
                                                                    //  if has2ControlVoltage=true, status is digital on FIO5, low=closed
                    hasLabJack = true,                  // has a LabJack
                    hasSwitchMate = true,               // has a Switch Mate.
                 },

                new fixtureTypes
                {
                    desc = "ERC",
                    fixtureType = "WS_SY2",
                    hasDutPowerControl = true,
                    hasWsButtonPushers = true,
                    hasLEDDetector = true,
                    hasLidSwitch = true,
                    hasHLAPrinter = true,              // has a HLA printer
                    hasConsolePort = true,             // has a console port to the DUT
                    hasTestRadio = true,                // has a test radio
                    hasLabJack = true,                  // has Labjack data aqc
                    hasFixturePushButton = true,
                    hasWsPowerControl = true,           // has WS fixture power control
                },
                new fixtureTypes
                {
                    desc = "HLA SN Check",
                    fixtureType = "SNCK",
                    hasSniffer = true,                  // has a sniffer
                }
        };

        /// <summary>
        /// Will open the class, but will not initialize the fixture. StationConfig
        /// must be setup before calling InitFixture.
        /// </summary>
        public FixtureFunctions()
        {
            // get any extentions of the fixtures
            GetFixtureDefFile(ref fixtureTypeList);
        }


        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    if (relayDriver != null)
                    {
                        relayDriver.Close();
                        relayDriver = null;
                    }
                    if (u3 != null)
                        u3 = null;
                    if (refRadio != null)
                    {
                        refRadio.Close();
                        refRadio = null;
                    }
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Will intialize the fixture hardware. will return false if error.
        /// </summary>
        public bool InitFixture()
        {
            LJUD.IO ioType = 0;
            LJUD.CHANNEL channel = 0;
            double dblValue = 0;
            string msgs = string.Empty;
            int status = (int)ErrorCodes.Program.Success;

            // Variables to satisfy certain method signatures
            int dummyInt = 0;
            double dummyDouble = 0;
            double[] dummyDoubleArray = { 0 };
            int state = 0;
            bool finished = false;

            LastFunctionErrorMessage = string.Empty;
            LastFunctionStatus = (int)ErrorCodes.Program.Success;

            if (stationConfigure == null)   // if it has not been set up yet
            {
                LastFunctionErrorMessage = "StationConfigure has not been set up before call InitFixture";
                LastFunctionStatus = (int)ErrorCodes.System.FixtureError;
                return false;
            }

            if (stationConfigure.FixtureEquipment == null)   // if it has not been set up yet
            {
                LastFunctionErrorMessage = "StationConfigure has not been set up before call InitFixture";
                LastFunctionStatus = (int)ErrorCodes.System.FixtureError;
                return false;
            }

            UpdateStatusWindow("Start Fixture init\n", StatusType.passed);
            // if Labjack part of the equipment list, init it
            if (stationConfigure.FixtureEquipment.hasLabJack)
            {
                UpdateStatusWindow("   Setting up LabJack\n", StatusType.passed);
                try
                {
                    u3 = new U3(LJUD.CONNECTION.USB, "0", true); // Connection through USB

                }
                catch (Exception)
                {
                    LastFunctionErrorMessage = "Error connecting to the LabJack";
                    LastFunctionStatus = (int)ErrorCodes.System.LabJackError;
                    return false;
                }
                try     // open the LabJack
                {

                    //Start by using the pin_configuration_reset IOType so that all
                    //pin assignments are in the factory default condition.
                    LJUD.ePut(u3.ljhandle, LJUD.IO.PIN_CONFIGURATION_RESET, 0, 0, 0);

                    //First some configuration commands.  These will be done with the ePut
                    //function which combines the add/go/get into a single call.

                    //Configure
                    //      FIO0-FIO3 as analog
                    //      FI04 - digital in. lid switch if has2ControlVoltages=true
                    //      FI05 - timer0 in, LED Detector input/digital in. Operator button
                    //      FI06 - digial out - light control relay
                    //      FI07 - digital out - Fixture AC Control
                    //That means we
                    //will start from channel 0 and update all 16 flexible bits.  We will
                    //pass a value of b0000000000001111(000F, 15).
                    LJUD.ePut(u3.ljhandle, LJUD.IO.PUT_ANALOG_ENABLE_PORT, 0, 15, 16);
                    LJUD.eDI(u3.ljhandle, ljChannelCloseSwitch2, ref state);
                    LJUD.eDI(u3.ljhandle, ljChannelOKButton, ref state);
                    LJUD.eDO(u3.ljhandle, ljChannelPIRSource, 0);
                    LJUD.eDO(u3.ljhandle, ljChannelDUTPwrControl, 0);
                    LJUD.eDO(u3.ljhandle, ljChannelERCButton1, 0);
                    LJUD.eDO(u3.ljhandle, ljChannelERCButton2, 0);
                    LJUD.eDO(u3.ljhandle, ljChannelERCButton3, 0);
                    LJUD.eDO(u3.ljhandle, ljChannelERCButton4, 0);
                    LJUD.eDO(u3.ljhandle, ljChannelERCTestIndicator, 0);
                    LJUD.eDO(u3.ljhandle, ljChannelConnectBatToDAC0, 0);

                    LJUD.eDAC(u3.ljhandle, ljChannelDACBattery, 0, 0, 0, 0);

                    if (stationConfigure.FixtureEquipment.hasLEDDetector & !stationConfigure.FixtureEquipment.has2ControlVoltages)
                    {
                        // it has a LED detector connected to the LabJack
                        // set timer0 to F105, 48MHz
                        LJUD.AddRequest(u3.ljhandle, LJUD.IO.PUT_CONFIG, LJUD.CHANNEL.TIMER_COUNTER_PIN_OFFSET, 5, 0, 0);
                        LJUD.AddRequest(u3.ljhandle, LJUD.IO.PUT_CONFIG, LJUD.CHANNEL.TIMER_CLOCK_BASE, (double)LJUD.TIMERCLOCKS.MHZ48_DIV, 0, 0);
                        LJUD.AddRequest(u3.ljhandle, LJUD.IO.PUT_CONFIG, LJUD.CHANNEL.TIMER_CLOCK_DIVISOR, 1, 0, 0);
                        LJUD.AddRequest(u3.ljhandle, LJUD.IO.PUT_CONFIG, LJUD.CHANNEL.NUMBER_TIMERS_ENABLED, 1, 0, 0);
        //              LJUD.AddRequest(u3.ljhandle, LJUD.IO.PUT_TIMER_MODE, 0, (double)LJUD.TIMERMODE.FALLINGEDGES32, 0, 0);
                        LJUD.AddRequest(u3.ljhandle, LJUD.IO.PUT_TIMER_MODE, 0, (double)LJUD.TIMERMODE.DUTYCYCLE, 0, 0);
                        LJUD.GoOne(u3.ljhandle);
                    }

                    // clear out the results from all the adds
                    try { LJUD.GetFirstResult(u3.ljhandle, ref ioType, ref channel, ref dblValue, ref dummyInt, ref dummyDouble); }
                    catch (Exception e)
                    {
                        LastFunctionErrorMessage = "Error setting up LabJack. " + e.Message;
                        LastFunctionStatus = (int)ErrorCodes.System.LabJackError;
                        return false;
                    }

                    while (!finished)
                    {
                        try { LJUD.GetNextResult(u3.ljhandle, ref ioType, ref channel, ref dblValue, ref dummyInt, ref dummyDouble); }
                        catch (LabJackUDException e)
                        {
                            // If we get an error, report it.  If the error is NO_MORE_DATA_AVAILABLE we are done
                            if (e.LJUDError == UE9.LJUDERROR.NO_MORE_DATA_AVAILABLE)
                                finished = true;
                            else
                            {
                                LastFunctionErrorMessage = "Error setting up LabJack. " + e.Message;
                                LastFunctionStatus = (int)ErrorCodes.System.LabJackError;
                                return false;
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    LastFunctionErrorMessage = "Error setting up LabJack. " + e.Message;
                    LastFunctionStatus = (int)ErrorCodes.System.LabJackError;
                    return false;
                }
            }   // end of labjack setup

            // if the Switch-Mate is installed
            if (stationConfigure.FixtureEquipment.hasSwitchMate)
            {
                UpdateStatusWindow("   Setting up Switch-Mate\n", StatusType.passed);

                relayDriver = new RelayDriver();
                if (!relayDriver.Open(RelayDriver.RelayTypes.SwitchMate, stationConfigure.ConfigSwitchMateComPort))
                {
                    LastFunctionErrorMessage = relayDriver.ErrorMessage;
                    LastFunctionStatus = relayDriver.ErrorCode;
                    return false;
                }
            }

            // if ref radio is installed
            if (stationConfigure.FixtureEquipment.hasTestRadio)
            {
                UpdateStatusWindow("   Setting up Ref Radio\n", StatusType.passed);
                try
                {
                    refRadio = new TestRadioConsole();
                    refRadio.Open(stationConfigure.ConfigTestRadioComPort);
                    status = refRadio.Setup(Convert.ToInt32(stationConfigure.ConfigTestRadioChannel), ref msgs);
                    if (status != (int)ErrorCodes.Program.Success)
                    {
                        LastFunctionErrorMessage = "Error setting up the ref radio. " + msgs;
                        LastFunctionStatus = (int)ErrorCodes.System.FixtureError;
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    LastFunctionErrorMessage = "Error opening Ref Radio. " + ex.Message;
                    LastFunctionStatus = (int)ErrorCodes.System.FixtureError;
                    return false;
                }
            }

            // if BLE is installed
            if (stationConfigure.FixtureEquipment.hasBLE)
            {
                string outmsg = string.Empty;
                BluetoothFunctions ble = new BluetoothFunctions();

                // initialize the TI dongle used to do transmit test. do not leave the port open.
                UpdateStatusWindow("   Init TI Dongle\n", StatusType.passed);
                ble.BluetoothPort = stationConfigure.ConfigBLEComPort;
                if ((LastFunctionStatus = ble.OpenBLEMonitor(ref outmsg)) != (int)ErrorCodes.Program.Success)
                {
                    LastFunctionErrorMessage = "Error opening the BLE Monitor port: " + outmsg + "\n";
                    return false;
                }
                ble.dongleInit(ref outmsg);
                ble.CloseBLEMonitor();

                // initilize the tag radio use for recive. This will start it advertising.
                UpdateStatusWindow("   Init Tag Radio\n", StatusType.passed);
                ble.BLETagPort = stationConfigure.ConfigTagComPort;
                if (ble.BLETagPort != "COM0")
                {
                    if  (ble.DoInitTag() != (int)ErrorCodes.Program.Success)
                    {
                        LastFunctionErrorMessage = ble.FailMessage;
                        LastFunctionStatus = ble.FailErrorCode;
                        return false;
                    }
                    TagRadioMac = ble.tagMac;           // save the Tag Radio Mac for use in tests
                }

            }
            return true;
        }

        /// <summary>
        /// Returns the lid status
        /// </summary>
        /// <returns></returns>
        public bool IsCoverClosed()
        {
            double readvalue = 0;

            if (u3 == null)              // if the LabJack has not been set up yet
                return false;                  // return with error
            try
            {
                if (stationConfigure.FixtureEquipment.hasLabJack)
                {
                    if (stationConfigure.FixtureEquipment.has2ControlVoltages)
                        LJUD.eGet(u3.ljhandle, LJUD.IO.GET_DIGITAL_BIT, (LJUD.CHANNEL)ljChannelCloseSwitch2, ref readvalue, 0);
                    else
                        LJUD.eGet(u3.ljhandle, LJUD.IO.GET_AIN, (LJUD.CHANNEL)ljChannelCloseSwitch1, ref readvalue, 0);
                }
            }
            catch (LabJackUDException ex)
            {
                throw new Exception(ex.Message + ex.LJUDError.ToString());
            }
            if (stationConfigure.FixtureEquipment.has2ControlVoltages)
            {
                if (readvalue == 0)
                    return true;
                else
                    return false;
            }
            if (readvalue > 2)
                return true;
            return false;
        }

        /// <summary>
        /// Returns the box button status
        /// </summary>
        /// <returns></returns>
        public bool IsBoxButtonPushed()
        {
            double readvalue = 0;

            if (u3 == null)              // if the LabJack has not been set up yet
                return false;                  // return with error
            try { LJUD.eGet(u3.ljhandle, LJUD.IO.GET_DIGITAL_BIT, (LJUD.CHANNEL)ljChannelOKButton, ref readvalue, 0); }
            catch (LabJackUDException)
            {
                throw;
            }
            if (readvalue == 0)
                return true;
            return false;
        }

        //--------------------------------------------
        // Read3p3Voltage
        /// <summary>
        /// Will read the 3.3V input. Will apply the gain from the config file.
        /// </summary>
        /// <returns></returns>
        public double Read3p3Voltage()
        {
            double readvalue = 0;

            if (u3 == null)          // if the labjack is not set up
                throw new Exception("LabJack is not setup.");

            try
            {
                LJUD.eGet(u3.ljhandle, LJUD.IO.GET_AIN, (LJUD.CHANNEL)ljChannel3p3V, ref readvalue, 0);
            }
            catch (Exception)
            {
                throw;
            }
            return readvalue;
        }

        //--------------------------------------------
        // ReadControlVoltage
        /// <summary>
        /// Will read the control voltage from the sensor. Will apply the gain from the config file.
        /// </summary>
        /// <returns></returns>
        public double ReadControlVoltage()
        {
            double readvalue = 0;

            if (u3 == null)          // if the labjack is not set up
                throw new Exception("LabJack is not setup.");

            try
            {
                LJUD.eGet(u3.ljhandle, LJUD.IO.GET_AIN, (LJUD.CHANNEL)ljChannelControlVoltage, ref readvalue, 0);
                readvalue = readvalue * stationConfigure.ConfigLjChannelControlVGain;
            }
            catch (Exception)
            {
                throw;
            }
            return readvalue;
        }

        //--------------------------------------------
        // ReadControlVoltage2
        /// <summary>
        /// Will read the control voltage 2 from the sensor. Will apply the gain from the config file.
        /// </summary>
        /// <returns></returns>
        public double ReadControlVoltage2()
        {
            double readvalue = 0;

            if (u3 == null)          // if the labjack is not set up
                throw new Exception("LabJack is not setup.");

            try
            {
                LJUD.eGet(u3.ljhandle, LJUD.IO.GET_AIN, (LJUD.CHANNEL)ljChannelControlVoltage2, ref readvalue, 0);
                readvalue = readvalue * stationConfigure.ConfigLjChannelControlVGain;
            }
            catch (Exception)
            {
                throw;
            }
            return readvalue;
        }


        //--------------------------------------
        // ControlPIRSource
        /// <summary>
        /// Control the Test source for the PIR. Will delay ConfigPIRDetectorLightSource.onDelayMs
        /// when turned on and ConfigPIRDetectorLightSource.OffDelayMs when turned off.
        /// </summary>
        /// <param name="control"></param>
        /// <returns></returns>
        public bool ControlPIRSource(int control)
        {
            if (u3 == null)          // if the labjack is not set up
                throw new Exception("LabJack is not setup.");

            //// if there is a Swtich-Mate, relay 2 controls the power
            if (stationConfigure.FixtureEquipment.hasSwitchMate)
            {
                if (!relayDriver.ControlRelay(smChannelPIRSource, (control == 0) ? RelayDriver.RelayStates.Open : RelayDriver.RelayStates.Close))
                {
                    LastFunctionErrorMessage = relayDriver.ErrorMessage;
                    LastFunctionStatus = relayDriver.ErrorCode;
                    return false;
                }
            }

            //Labjack might be controlling
            try
            {
                LJUD.eDO(u3.ljhandle, ljChannelPIRSource, control);
                if (control != 0)
                    Thread.Sleep(stationConfigure.ConfigPIRDetectorLightSource.OnDelayMs);
                else
                    Thread.Sleep(stationConfigure.ConfigPIRDetectorLightSource.OffDelayMs);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public enum ACMonitorSelection { Switched, Unswitchted };
        //--------------------------------------
        // ControlACMonitor
        /// <summary>
        /// Controls the AC Monitor relay in the Jedi fixture. 0=switched, 1=unswitched
        /// </summary>
        /// <param name="control"></param>
        /// <returns></returns>
        public bool ControlACMonitor(ACMonitorSelection control)
        {

            if (u3 == null)          // if the labjack is not set up
                throw new Exception("LabJack is not setup.");

            try
            {
               Trace.WriteLine("AC Monitor Relay set " + ((control == ACMonitorSelection.Switched) ? "Switched":"unSwitched"));
                LJUD.eDO(u3.ljhandle, ljChannelACOuputMonitor, (int)control);
                Thread.Sleep(1000);         // let relay change
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }


        /// <summary>
        /// Will turn power on and off to the dut. If it is the
        /// wireless fixture, it will set the voltage. If not,
        /// it will set the relay on for anything that is not 0.
        /// Will set the test indicator for the fixture.
        /// </summary>
        /// <remarks>
        /// Properties:
        ///     DUTTurnOnDelayMs - default 100ms. Delay from relay/DAC command to 
        ///                         return.
        /// </remarks>
        /// <param name="voltage"></param>
        /// <returns></returns>
        public int ControlDUTPower(double voltage)
        {
            string temp = string.Empty;
            try
            {
                if (stationConfigure.FixtureEquipment.hasWsPowerControl)
                {
                    if (u3 == null)          // if the labjack is not set up
                        throw new Exception("LabJack is not setup.");

                    LJUD.eDAC(u3.ljhandle, ljChannelDACBattery, voltage, 0, 0, 0);
                    if (voltage == 0) // if the voltage is zero, disconnect from battery
                    {
                        LJUD.eDO(u3.ljhandle, ljChannelConnectBatToDAC0, 0);
                        LJUD.eDO(u3.ljhandle, ljChannelERCTestIndicator, 0);
                    }
                    else        // else connect DAC to DUT
                    {
                        LJUD.eDO(u3.ljhandle, ljChannelConnectBatToDAC0, 1);
                        LJUD.eDO(u3.ljhandle, ljChannelERCTestIndicator, 1);
                    }
                    Thread.Sleep(DUTTurnOnDelayMs);
                }
                if (stationConfigure.FixtureEquipment.hasDutPowerControl)
                {
                    if (stationConfigure.FixtureEquipment.hasDutPowerControl)
                    {
                        // if there is a Swtich-Mate, relay 1 controls the power
                        if (stationConfigure.FixtureEquipment.hasSwitchMate)
                        {
                            if (!relayDriver.ControlRelay(smChannelDUTPwrControl, (voltage == 0) ? RelayDriver.RelayStates.Open : RelayDriver.RelayStates.Close))
                            {
                                return relayDriver.ErrorCode;
                            }
                        }
                        else  // otherwise, no Switch-Mate so LabJack controls DUT power
                        {
                            if (voltage == 0)
                                LJUD.eDO(u3.ljhandle, ljChannelDUTPwrControl, 0);
                            else
                                LJUD.eDO(u3.ljhandle, ljChannelDUTPwrControl, 1);
                        }
                    }
                    Thread.Sleep(DUTTurnOnDelayMs);
                }
                return (int)ErrorCodes.Program.Success;
            }
            catch (Exception)
            {
                return (int)ErrorCodes.System.LabJackError;
            }
        }

        //---------------------------------------
        // ControlAmbientSensorLightSource
        /// <summary>
        /// Set the Ambient Light source. 0=off non 0=on. Will delay ConfigAmbientDetectorLightSource.onDelayMs
        /// when turned on.
        /// </summary>
        /// <returns></returns>
        public bool ControlAmbientSensorLightSource(int state)
        {
            if (u3 == null)          // if the labjack is not set up
                throw new Exception("LabJack is not setup.");

            try
            {
                if (stationConfigure.ConfigAmbientDetectorLightSource.Type.ToUpper() == "REDLED")
                {
                    if (state != 0)
                    {
                        LJUD.eDAC(u3.ljhandle, ljChannelAMBSource, stationConfigure.ConfigAmbientDetectorLightSource.Voltage, 0, 0, 0);
                        Thread.Sleep(stationConfigure.ConfigAmbientDetectorLightSource.OnDelayMs);
                    }
                    else
                    {
                        LJUD.eDAC(u3.ljhandle, ljChannelAMBSource, 0, 0, 0, 0);
                        Thread.Sleep(stationConfigure.ConfigAmbientDetectorLightSource.OffDelayMs);
                    }
                }
                else
                {
                    ControlPIRSource(state);    
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        //---------------------------------------
        // SetAmbientSensorLED
        /// <summary>
        /// Set the Ambient to the voltage.
        /// </summary>
        /// <returns></returns>
        public bool SetAmbientSensorLED(double voltage)
        {
            if (u3 == null)          // if the labjack is not set up
                throw new Exception("LabJack is not setup.");

            try
            {
                LJUD.eDAC(u3.ljhandle, ljChannelAMBSource, voltage, 0, 0, 0);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }


        //----------------------------------------
        // ReadTemperature
        //
        //  Read the temperature probe in C
        public int ReadTemperatureProbe(out double temp, out string errormsg)
        {
            double readvalue = 0;
            double accreadvalue = 0;

            temp = 0;
            errormsg = string.Empty;

            if (u3 == null)          // if the labjack is not set up
            {
                errormsg = "LabJack is not setup.";
                return (int)ErrorCodes.System.LabJackNotSetup;
            }

            try
            {
                for (int i = 0; i < 10; i++)
                {
                    LJUD.eGet(u3.ljhandle, LJUD.IO.GET_AIN, (LJUD.CHANNEL)ljChannelTempProbe, ref readvalue, 0);
                    accreadvalue = accreadvalue + readvalue;
                }
            }
            catch (Exception)
            {
                errormsg = "Error reading Temperture input.";
                return (int)ErrorCodes.System.LabJackError;
            }

            if (stationConfigure.ConfigTempSensorOutput.Equals("K"))
                temp = (accreadvalue * 10) - 273.15;  // only mult by 10(instead of 100) beacuse there are 10 readings.
            if (stationConfigure.ConfigTempSensorOutput.Equals("F"))
                temp = (accreadvalue / 10) * 55.56 + 255.37 - 273.15;
            return (int)ErrorCodes.Program.Success;
        }


        //----------------------------------
        // Reads the LED Detector.
        //  Exceptions
        //      LabJack exceptions
        //
        // The following properties are set:
        //      double value - frequency, average of 10 period readings
        public int ReadLEDDetector(ref double freq, ref double dutyCycle)
        {
            double dblValue = 0;

            // Variables to satisfy certain method signatures
            double[] dummyDoubleArray = { 0 };
            double[] valuearray = new double[10];
            double[] valuearraydc = new double[10];
            double low = 0;
            double high = 0;
            double[] valuearraylow = new double[10];
            double[] valuearrayhigh = new double[10];
            double[] valuearrayraw = new double[10];
            int wavelength = 0;
            int intisity = 0;

            if (stationConfigure.ConfigLedDetector.Type.ToUpper().Equals("NONE"))
                return (int)ErrorCodes.Program.Success;

            if (stationConfigure.ConfigLedDetector.Type.ToUpper().Equals("USB"))
            {
                try
                {
                    serialLedDetector = new SerialLEDDectector();
                    serialLedDetector.Open(stationConfigure.ConfigLedDetector.ComPort);
                    Thread.Sleep(500);                      // let everything get started
                }
                catch (Exception)
                {
                    throw;
                }
            }

            //            Thread.Sleep(100);
            freq = 0;
            try
            {
                for (int i = 0; i < 10; i++)
                {
                    if (serialLedDetector == null)
                    {
                        //Request a read from the timer0.
                        LJUD.eGet(u3.ljhandle, LJUD.IO.GET_TIMER, (LJUD.CHANNEL)0, ref dblValue, dummyDoubleArray);
                        valuearrayraw[i] = dblValue;
                        low = (double)(((ulong)dblValue) / (65536));
                        valuearraylow[i] = low;
                        high = (double)(((ulong)dblValue) % (65536));
                        valuearrayhigh[i] = high;
                        valuearraydc[i] = 100 * (high / (high + low));
                        valuearray[i] = 48000000 / (low + high);          // using a 48Mhz clock
                        Thread.Sleep(10);
                        if ((low == 0) | (high == 0))
                            i--;
                    }
                    else
                    {
                        int retrycount = 0;
                        do      // this loop is to try to catch when a 0 comes back from the read.
                                // assuming that for some reason no reads are being done, so give a some 
                                // time.
                        {
                            serialLedDetector.ReadLEDValues(ref wavelength, ref intisity);
                            if (wavelength == 0)
                                Thread.Sleep(100);
                            valuearray[i] = wavelength;
                            valuearraydc[i] = intisity;
                        } while ((freq == 0) & (retrycount++ < 5));
                    }
                }
            }
            catch (LabJackUDException)
            {
                if (serialLedDetector != null)
                    serialLedDetector.Dispose();
                return (int)ErrorCodes.System.LabJackError;
            }

            if (serialLedDetector != null)
            {
                serialLedDetector.Close();
                serialLedDetector.Dispose();
            }
            freq = valuearray.Average();
            dutyCycle = valuearraydc.Average();
            return (int)ErrorCodes.Program.Success;
        }

        /// <summary>
        /// Will read the LED Detector and see if it is in the color range.
        /// </summary>
        /// <param name="ledColor"></param>
        /// <param name="readFreq"></param>
        /// <param name="outmsg"></param>
        /// <returns></returns>
        public int CheckLedColor(string checkColor, string freqRange, ref int readFreq, ref int readIntensity, ref string outmsg)
        {
            int status = (int)ErrorCodes.Program.Success;
            double freq = 0;
            int checkFreqLow = 0;
            int checkFreqHigh = 0;
            double intensity = 0;

            outmsg = string.Empty;
            readFreq = 0;
            checkFreqLow = Convert.ToInt32(freqRange.Substring(0, freqRange.IndexOf(',')));
            checkFreqHigh = Convert.ToInt32(freqRange.Substring(freqRange.IndexOf(',') + 1));

            if (status == (int)ErrorCodes.Program.Success)
                status = ReadLEDDetector(ref freq, ref intensity);

            if (status == (int)ErrorCodes.Program.Success)
            {
                readFreq = (int)freq;
                readIntensity = (int)intensity;
                if (((int)freq < checkFreqLow) || ((int)freq > checkFreqHigh))
                {
                    status = (int)ErrorCodes.Program.ValueOutsideOfLimit;
                    outmsg = string.Format("Reading({0:N0}) for color {1} is outside the limits({2}-{3})",
                                            freq, checkColor, checkFreqHigh, checkFreqLow);
                }
            }
            return status;
        }

        /// <summary>
        /// Will read the LED Detector and see if it is in the color/int range.
        /// </summary>
        /// <param name="ledColor"></param>
        /// <param name="readFreq"></param>
        /// <param name="outmsg"></param>
        /// <returns></returns>
        public int CheckLedValues(int checkFreqLow, int checkFreqHigh, int checkIntLow, int checkIntHigh,
                    ref int freqStatus, ref int intensityStatus, ref int readFreq, ref int readIntensity, ref string outmsg)
        {
            int status = (int)ErrorCodes.Program.Success;
            double freq = 0;
            double intensity = 0;

            outmsg = string.Empty;
            readFreq = 0;
            freqStatus = (int)ErrorCodes.Program.Success;
            intensityStatus = (int)ErrorCodes.Program.Success;

            if (status == (int)ErrorCodes.Program.Success)
                status = ReadLEDDetector(ref freq, ref intensity);

            if (status == (int)ErrorCodes.Program.Success)
            {
                readFreq = (int)freq;
                readIntensity = (int)intensity;
                if (checkFreqLow != -1)         // -1 indicates that it is not to be tested
                {
                    if (((int)freq < checkFreqLow) || ((int)freq > checkFreqHigh))
                    {
                        status = (int)ErrorCodes.Program.ValueOutsideOfLimit;
                        freqStatus = (int)ErrorCodes.Program.ValueOutsideOfLimit;
                        outmsg = string.Format("Reading({0:N0}) is outside the limits({1}-{2})",
                                                freq, checkFreqHigh, checkFreqLow);
                    }
                }
                if (checkIntLow != -1)         // -1 indicates that it is not to be tested
                {
                    if (((int)intensity < checkIntLow) || ((int)intensity > checkIntHigh))
                    {
                        status = (int)ErrorCodes.Program.ValueOutsideOfLimit;
                        intensityStatus = (int)ErrorCodes.Program.ValueOutsideOfLimit;
                        outmsg = string.Format("Reading({0:N0}) is outside the limits({1}-{2})",
                                                intensity, checkIntHigh, checkIntLow);
                    }
                }
            }
            return status;
        }

        //-------------------------------------
        //-------------------------------------
        //  ERC Fixture button push routines
        //-------------------------------------
        //-------------------------------------

        /// <summary>
        /// Will active the fixture air button push for pushSeconds seconds.
        /// </summary>
        /// <remarks>
        /// Maps:
        ///     button x -> ljChannelERCButtonx
        /// </remarks>
        /// <param name="buttonNumber"></param>
        /// <param name="pushSeconds"></param>
        /// <returns></returns>
        public int PushERCButtonForXSeconds(int buttonNumber, int pushSeconds)
        {
            int status = (int)ErrorCodes.Program.Success;

            if ((buttonNumber < 1) | (buttonNumber > 4))
                return (int)ErrorCodes.System.InvalidParameter;

            if (u3 == null)          // if the labjack is not set up
                throw new Exception("LabJack is not setup.");

            try
            {
                LJUD.eDO(u3.ljhandle, ljChannelERCButton1 + buttonNumber - 1, 1);
            }
            catch (Exception)
            {
                return (int)ErrorCodes.System.LabJackError;
            }

            Thread.Sleep(pushSeconds * 1000);
            try
            {
                LJUD.eDO(u3.ljhandle, ljChannelERCButton1 + buttonNumber - 1, 0);
            }
            catch (Exception)
            {
                return (int)ErrorCodes.System.LabJackError;
            }

            return status;
        }

        /// <summary>
        /// Will set the ERC button according to the state passed. 1=pushed.
        /// </summary>
        /// <param name="buttonNumber"></param>
        /// <param name="state"></param>
        /// <returns></returns>
        public int SetERCButton(int buttonNumber, int state)
        {
            int status = (int)ErrorCodes.Program.Success;
            if ((buttonNumber < 1) | (buttonNumber > 4))
                return (int)ErrorCodes.System.InvalidParameter;

            if (u3 == null)          // if the labjack is not set up
                throw new Exception("LabJack is not setup.");

            try
            {
                LJUD.eDO(u3.ljhandle, ljChannelERCButton1 + buttonNumber - 1, state);
            }
            catch (Exception)
            {
                return (int)ErrorCodes.System.LabJackError;
            }

            return status;
        }

        //-------------------------------------------------------
        // Class utilities
        //-------------------------------------------------------
        
        /// <summary>
        /// Will look for fixtureTypeDefs.xml file. If found, will add the defs
        /// to the fixDefs list. Will replace predefined if type matches predefined.
        /// </summary>
        /// <param name="fixDefs">current list of fixture definitions</param>
        public void GetFixtureDefFile (ref List<fixtureTypes> fixDefs)
        {
            if (File.Exists("FixtureTypeDefs.xml"))
            {
                fixtureTypes readTypes = new fixtureTypes();
                XDocument xdoc;
                xdoc = XDocument.Load("FixtureTypeDefs.xml");

                var xx = xdoc.Elements();

                foreach (var typeDefs in xx.Descendants("Fixture"))
                {
                    readTypes.desc = string.Empty;
                    readTypes.hasEnlightedInterface = false;     // enlighted interface, CU and 0-10V output
                    readTypes.hasDaliInterface = false;          // dali interface
                    readTypes.hasBLE = false;                    // has bluetooth
                    readTypes.hasAmbientSensor = false;          // has ambient light sensor
                    readTypes.hasPirSensor = false;              // has PIR sensor
                    readTypes.hasDutPowerControl = false;        // able to control the dut power supply
                    readTypes.hasWsButtonPushers = false;        // button push controled by system
                    readTypes.hasWsPowerControl = false;          // has power control for WS fixture
                    readTypes.hasFixturePushButton = false;       // OK button on the fixture
                    readTypes.hasTempProbe = false;               // has a temperature probe
                    readTypes.hasPowerLoad = false;               // has a power load
                    readTypes.hasPowerMeter = false;              // has a CU as a power meter
                    readTypes.hasPCBAPrinter = false;             // has a PCBA printer
                    readTypes.hasHLAPrinter = false;              // has a HLA printer
                    readTypes.hasConsolePort = false;             // has a console port to the DUT
                    readTypes.hasLEDDetector = false;             // has LED detector of some sort
                    readTypes.hasLidSwitch = false;               // has a lid down switch
                    readTypes.hasTestRadio = false;               // has a test radio
                    readTypes.hasSniffer = false;                 // has a sniffer
                    readTypes.hasLabJack = false;                 // has a LabJack
                    readTypes.hasSwitchMate = false;              // has switch-mate
                    readTypes.has2ControlVoltages = false;        // has only one control voltage

                    foreach (var item in typeDefs.Elements())
                    {
                        if (item.Name.ToString().ToUpper() == "DESC")
                            readTypes.desc = item.Value.ToString();
                        if (item.Name.ToString().ToUpper() == "TYPE")
                            readTypes.fixtureType = item.Value.ToString();
                        if (item.Name.ToString().ToUpper() == "HASENLIGHTEDINTERFACE")
                            if (item.Value.ToUpper() == "YES")
                                readTypes.hasEnlightedInterface = true;
                        if (item.Name.ToString().ToUpper() == "HASDALIINTERFACE")
                            if (item.Value.ToUpper() == "YES")
                                readTypes.hasDaliInterface = true;
                        if (item.Name.ToString().ToUpper() == "HASBLE")
                            if (item.Value.ToUpper() == "YES")
                                readTypes.hasBLE = true;
                        if (item.Name.ToString().ToUpper() == "HASAMBIENTSENSOR")
                            if (item.Value.ToUpper() == "YES")
                                readTypes.hasAmbientSensor = true;
                        if (item.Name.ToString().ToUpper() == "HASPIRSENSOR")
                            if (item.Value.ToUpper() == "YES")
                                readTypes.hasPirSensor = true;
                        if (item.Name.ToString().ToUpper() == "HASDUTPOWERCONTROL")
                            if (item.Value.ToUpper() == "YES")
                                readTypes.hasDutPowerControl = true;
                        if (item.Name.ToString().ToUpper() == "HASWSBUTTONPUSHERS")
                            if (item.Value.ToUpper() == "YES")
                                readTypes.hasWsButtonPushers = true;
                        if (item.Name.ToString().ToUpper() == "HASWSPOWERCONTROL")
                            if (item.Value.ToUpper() == "YES")
                                readTypes.hasWsPowerControl = true;
                        if (item.Name.ToString().ToUpper() == "HASFIXTUREPUSHBUTTON")
                            if (item.Value.ToUpper() == "YES")
                                readTypes.hasFixturePushButton = true;
                        if (item.Name.ToString().ToUpper() == "HASTEMPPROBE")
                            if (item.Value.ToUpper() == "YES")
                                readTypes.hasTempProbe = true;
                        if (item.Name.ToString().ToUpper() == "HASPOWERLOAD")
                            if (item.Value.ToUpper() == "YES")
                                readTypes.hasPowerLoad = true;
                        if (item.Name.ToString().ToUpper() == "HASPOWERMETER")
                            if (item.Value.ToUpper() == "YES")
                                readTypes.hasPowerMeter = true;
                        if (item.Name.ToString().ToUpper() == "HASPCBAPRINTER")
                            if (item.Value.ToUpper() == "YES")
                                readTypes.hasPCBAPrinter = true;
                        if (item.Name.ToString().ToUpper() == "HASHLAPRINTER")
                            if (item.Value.ToUpper() == "YES")
                                readTypes.hasHLAPrinter = true;
                        if (item.Name.ToString().ToUpper() == "HASCONSOLEPORT")
                            if (item.Value.ToUpper() == "YES")
                                readTypes.hasConsolePort = true;
                        if (item.Name.ToString().ToUpper() == "HASLEDDETECTOR")
                            if (item.Value.ToUpper() == "YES")
                                readTypes.hasLEDDetector = true;
                        if (item.Name.ToString().ToUpper() == "HASLIDSWITCH")
                            if (item.Value.ToUpper() == "YES")
                                readTypes.hasLidSwitch = true;
                        if (item.Name.ToString().ToUpper() == "HASTESTRADIO")
                            if (item.Value.ToUpper() == "YES")
                                readTypes.hasTestRadio = true;
                        if (item.Name.ToString().ToUpper() == "HASSNIFFER")
                            if (item.Value.ToUpper() == "YES")
                                readTypes.hasSniffer = true;
                        if (item.Name.ToString().ToUpper() == "HASLABJACK")
                            if (item.Value.ToUpper() == "YES")
                                readTypes.hasLabJack = true;
                        if (item.Name.ToString().ToUpper() == "HASSWITCHMATE")
                            if (item.Value.ToUpper() == "YES")
                                readTypes.hasSwitchMate = true;
                        if (item.Name.ToString().ToUpper() == "HAS2CONTROLVOLTAGES")
                            if (item.Value.ToUpper() == "YES")
                                readTypes.has2ControlVoltages = true;
                    }

                    // see if this def is in the list alread
                    bool found = false;
                    for (int i = 0; i < fixDefs.Count; i++)
                    {
                        if (fixDefs[i].fixtureType.ToUpper() == readTypes.fixtureType.ToUpper())
                            fixDefs[i] = readTypes;         // replace with what is in the file
                    }
                    if (!found)     // if not found in the list, it is new
                        fixDefs.Add(readTypes);
                }
            }
        }

        //------------------------
        // events
        //  Use UpdateStatusWindow(msg) to display a message on the
        //  MainWindow status window.
        //------------------------
        public event EventHandler<StatusUpdatedEventArgs>
            StatusUpdated;

        protected virtual void
            OnStatusUpdated(StatusUpdatedEventArgs e)
        {
            if (StatusUpdated != null)
                StatusUpdated(this, e);
        }

        // call these functions to update the main status window
        private void UpdateStatusWindow(string msg)
        {
            string newStatus = msg;
            StatusUpdatedEventArgs nuea = new StatusUpdatedEventArgs();
            nuea.statusText = msg;
            OnStatusUpdated(nuea);
        }
        private void UpdateStatusWindow(string msg, StatusType status)
        {
            string newStatus = msg;
            StatusUpdatedEventArgs nuea = new StatusUpdatedEventArgs();
            nuea.statusText = msg;
            nuea.statusType = status;
            OnStatusUpdated(nuea);
        }


    }
}

