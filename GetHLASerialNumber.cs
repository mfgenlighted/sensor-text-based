﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace SensorManufSY2
{
    public partial class GetHLASerialNumber : Form
    {
        public GetHLASerialNumber()
        {
            InitializeComponent();
        }
        FixtureFunctions lclfixture;

        public GetHLASerialNumber(FixtureFunctions fixture)
        {
            InitializeComponent();
            lclfixture = fixture;
            while (fixture.IsBoxButtonPushed()) ;     // wait for box button up, if it is pushed
            timer1.Enabled = true;
        }

        public string HlaSN
        {
            get { return hlaSN.Text; }
        }



        private void okbutton_Click(object sender, EventArgs e)
        {
            Regex fmt = new Regex(@"^....\d\d\d\d\d\d\d\d\d\d\d$");          // new SN format
            hlaSN.Text = hlaSN.Text.ToUpper();
            if (!fmt.Match(hlaSN.Text).Success)       // if failed
            {
                MessageBox.Show("Invalid Serial Number format");
                hlaSN.Focus();
                this.DialogResult = DialogResult.None;
                while (lclfixture.IsBoxButtonPushed()) ;     // wait for box button up, if it is pushed
                timer1.Enabled = true;
                return;
            }
            DialogResult = System.Windows.Forms.DialogResult.OK;

        }

        private void checkBoxButton(object sender, EventArgs e)
        {
            if (lclfixture.IsBoxButtonPushed())
            {
                timer1.Enabled = false;
                okbutton_Click(sender, e);
            }
        }
    }
}
