﻿namespace SensorManufSY2
{
    partial class GetMAC
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.tbMAC = new System.Windows.Forms.TextBox();
            this.abortButton = new System.Windows.Forms.Button();
            this.okButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label1.Location = new System.Drawing.Point(43, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 13);
            this.label1.TabIndex = 13;
            this.label1.Text = "MAC";
            // 
            // tbMAC
            // 
            this.tbMAC.Location = new System.Drawing.Point(43, 45);
            this.tbMAC.Name = "tbMAC";
            this.tbMAC.Size = new System.Drawing.Size(130, 20);
            this.tbMAC.TabIndex = 10;
            // 
            // abortButton
            // 
            this.abortButton.BackColor = System.Drawing.Color.Red;
            this.abortButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.abortButton.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.abortButton.Location = new System.Drawing.Point(142, 90);
            this.abortButton.Name = "abortButton";
            this.abortButton.Size = new System.Drawing.Size(87, 41);
            this.abortButton.TabIndex = 12;
            this.abortButton.Text = "Exit Test";
            this.abortButton.UseVisualStyleBackColor = false;
            // 
            // okButton
            // 
            this.okButton.BackColor = System.Drawing.Color.Lime;
            this.okButton.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.okButton.Location = new System.Drawing.Point(17, 90);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(100, 41);
            this.okButton.TabIndex = 11;
            this.okButton.Text = "Start Test";
            this.okButton.UseVisualStyleBackColor = false;
            this.okButton.Click += new System.EventHandler(this.okButton_Click);
            // 
            // GetMAC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(247, 161);
            this.ControlBox = false;
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbMAC);
            this.Controls.Add(this.abortButton);
            this.Controls.Add(this.okButton);
            this.Name = "GetMAC";
            this.Text = "Get MAC";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbMAC;
        private System.Windows.Forms.Button abortButton;
        private System.Windows.Forms.Button okButton;
    }
}