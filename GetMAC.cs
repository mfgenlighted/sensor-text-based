﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SensorManufSY2
{
    public partial class GetMAC : Form
    {

        public string MAC { get { return tbMAC.Text; } }

        public GetMAC()
        {
            InitializeComponent();
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            if (tbMAC.Text.Length != 15)
            {
                MessageBox.Show("Invalid format. The SN must be 15 characters long.");
                tbMAC.Clear();
                tbMAC.Focus();
                return;
            }

            DialogResult = System.Windows.Forms.DialogResult.OK;
            return;

        }
    }
}
