﻿namespace SensorManufSY2
{
    partial class GetPCBASerialNumber
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.cancelbutton = new System.Windows.Forms.Button();
            this.okbutton = new System.Windows.Forms.Button();
            this.pcasn = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // cancelbutton
            // 
            this.cancelbutton.BackColor = System.Drawing.Color.Red;
            this.cancelbutton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelbutton.Location = new System.Drawing.Point(196, 86);
            this.cancelbutton.Name = "cancelbutton";
            this.cancelbutton.Size = new System.Drawing.Size(100, 49);
            this.cancelbutton.TabIndex = 10;
            this.cancelbutton.Text = "Exit Test";
            this.cancelbutton.UseVisualStyleBackColor = false;
            // 
            // okbutton
            // 
            this.okbutton.BackColor = System.Drawing.Color.Lime;
            this.okbutton.Location = new System.Drawing.Point(38, 86);
            this.okbutton.Name = "okbutton";
            this.okbutton.Size = new System.Drawing.Size(105, 49);
            this.okbutton.TabIndex = 9;
            this.okbutton.Text = "Start Test";
            this.okbutton.UseVisualStyleBackColor = false;
            this.okbutton.Click += new System.EventHandler(this.okbutton_Click);
            // 
            // pcasn
            // 
            this.pcasn.Location = new System.Drawing.Point(73, 47);
            this.pcasn.Name = "pcasn";
            this.pcasn.Size = new System.Drawing.Size(144, 20);
            this.pcasn.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(143, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "Scan the PCA Serial Number";
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.checkBoxButton);
            // 
            // GetPCBASerialNumber
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(329, 180);
            this.ControlBox = false;
            this.Controls.Add(this.cancelbutton);
            this.Controls.Add(this.okbutton);
            this.Controls.Add(this.pcasn);
            this.Controls.Add(this.label1);
            this.Name = "GetPCBASerialNumber";
            this.Text = "GetPCBASerialNumber";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button cancelbutton;
        private System.Windows.Forms.Button okbutton;
        private System.Windows.Forms.TextBox pcasn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Timer timer1;
    }
}