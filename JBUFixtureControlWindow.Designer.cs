﻿namespace SensorManufSY2
{
    partial class JBUFixtureControlWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBoxDUTACPower = new System.Windows.Forms.GroupBox();
            this.radioButtonDUTACPowerOff = new System.Windows.Forms.RadioButton();
            this.radioButtonDUTACPowerOn = new System.Windows.Forms.RadioButton();
            this.groupBoxMonitorRelay = new System.Windows.Forms.GroupBox();
            this.radioButtonOutputMonitorUnswitched = new System.Windows.Forms.RadioButton();
            this.radioButtonOutputMonitorSwitched = new System.Windows.Forms.RadioButton();
            this.buttonReadPowerMeter = new System.Windows.Forms.Button();
            this.buttonReadDUTPower = new System.Windows.Forms.Button();
            this.textBoxPMVolts = new System.Windows.Forms.TextBox();
            this.textBoxPMWatts = new System.Windows.Forms.TextBox();
            this.textBoxDUTVoltsSwitched = new System.Windows.Forms.TextBox();
            this.textBoxDUTWattsSwitched = new System.Windows.Forms.TextBox();
            this.buttonExit = new System.Windows.Forms.Button();
            this.textWindowStatus = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxDUTVoltsUnswitched = new System.Windows.Forms.TextBox();
            this.textBoxDUTWattsUnswitched = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textSendToDUT = new System.Windows.Forms.RichTextBox();
            this.LEDColor = new System.Windows.Forms.TextBox();
            this.textLEDIntest = new System.Windows.Forms.Label();
            this.textLEDFreq = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.DUT3p3vReading = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.textPushButtonStatus = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.textLidStatus = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radioButtonSwitchedOuputOff = new System.Windows.Forms.RadioButton();
            this.radioButtonSwitchedOutputOn = new System.Windows.Forms.RadioButton();
            this.rtbStatus = new System.Windows.Forms.RichTextBox();
            this.groupBoxDUTACPower.SuspendLayout();
            this.groupBoxMonitorRelay.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxDUTACPower
            // 
            this.groupBoxDUTACPower.Controls.Add(this.radioButtonDUTACPowerOff);
            this.groupBoxDUTACPower.Controls.Add(this.radioButtonDUTACPowerOn);
            this.groupBoxDUTACPower.Location = new System.Drawing.Point(49, 38);
            this.groupBoxDUTACPower.Name = "groupBoxDUTACPower";
            this.groupBoxDUTACPower.Size = new System.Drawing.Size(135, 66);
            this.groupBoxDUTACPower.TabIndex = 0;
            this.groupBoxDUTACPower.TabStop = false;
            this.groupBoxDUTACPower.Text = "DUT AC Power";
            // 
            // radioButtonDUTACPowerOff
            // 
            this.radioButtonDUTACPowerOff.AutoSize = true;
            this.radioButtonDUTACPowerOff.Location = new System.Drawing.Point(85, 33);
            this.radioButtonDUTACPowerOff.Name = "radioButtonDUTACPowerOff";
            this.radioButtonDUTACPowerOff.Size = new System.Drawing.Size(39, 17);
            this.radioButtonDUTACPowerOff.TabIndex = 1;
            this.radioButtonDUTACPowerOff.TabStop = true;
            this.radioButtonDUTACPowerOff.Text = "Off";
            this.radioButtonDUTACPowerOff.UseVisualStyleBackColor = true;
            this.radioButtonDUTACPowerOff.Click += new System.EventHandler(this.radioButtonradioButtonDUTACPowerOff_Click);
            // 
            // radioButtonDUTACPowerOn
            // 
            this.radioButtonDUTACPowerOn.AutoSize = true;
            this.radioButtonDUTACPowerOn.Location = new System.Drawing.Point(22, 33);
            this.radioButtonDUTACPowerOn.Name = "radioButtonDUTACPowerOn";
            this.radioButtonDUTACPowerOn.Size = new System.Drawing.Size(39, 17);
            this.radioButtonDUTACPowerOn.TabIndex = 0;
            this.radioButtonDUTACPowerOn.TabStop = true;
            this.radioButtonDUTACPowerOn.Text = "On";
            this.radioButtonDUTACPowerOn.UseVisualStyleBackColor = true;
            this.radioButtonDUTACPowerOn.Click += new System.EventHandler(this.radioButtonradioButtonDUTACPowerOn_Click);
            // 
            // groupBoxMonitorRelay
            // 
            this.groupBoxMonitorRelay.Controls.Add(this.radioButtonOutputMonitorUnswitched);
            this.groupBoxMonitorRelay.Controls.Add(this.radioButtonOutputMonitorSwitched);
            this.groupBoxMonitorRelay.Location = new System.Drawing.Point(49, 115);
            this.groupBoxMonitorRelay.Name = "groupBoxMonitorRelay";
            this.groupBoxMonitorRelay.Size = new System.Drawing.Size(185, 65);
            this.groupBoxMonitorRelay.TabIndex = 1;
            this.groupBoxMonitorRelay.TabStop = false;
            this.groupBoxMonitorRelay.Text = "Output Monitor";
            // 
            // radioButtonOutputMonitorUnswitched
            // 
            this.radioButtonOutputMonitorUnswitched.AutoSize = true;
            this.radioButtonOutputMonitorUnswitched.Location = new System.Drawing.Point(97, 33);
            this.radioButtonOutputMonitorUnswitched.Name = "radioButtonOutputMonitorUnswitched";
            this.radioButtonOutputMonitorUnswitched.Size = new System.Drawing.Size(81, 17);
            this.radioButtonOutputMonitorUnswitched.TabIndex = 1;
            this.radioButtonOutputMonitorUnswitched.TabStop = true;
            this.radioButtonOutputMonitorUnswitched.Text = "Unswitched";
            this.radioButtonOutputMonitorUnswitched.UseVisualStyleBackColor = true;
            this.radioButtonOutputMonitorUnswitched.Click += new System.EventHandler(this.radioButtonOutputMonitorUnswitched_Click);
            // 
            // radioButtonOutputMonitorSwitched
            // 
            this.radioButtonOutputMonitorSwitched.AutoSize = true;
            this.radioButtonOutputMonitorSwitched.Location = new System.Drawing.Point(22, 33);
            this.radioButtonOutputMonitorSwitched.Name = "radioButtonOutputMonitorSwitched";
            this.radioButtonOutputMonitorSwitched.Size = new System.Drawing.Size(69, 17);
            this.radioButtonOutputMonitorSwitched.TabIndex = 0;
            this.radioButtonOutputMonitorSwitched.TabStop = true;
            this.radioButtonOutputMonitorSwitched.Text = "Switched";
            this.radioButtonOutputMonitorSwitched.UseVisualStyleBackColor = true;
            this.radioButtonOutputMonitorSwitched.Click += new System.EventHandler(this.radioButtonOutputMonitorSwitched_Click);
            // 
            // buttonReadPowerMeter
            // 
            this.buttonReadPowerMeter.Location = new System.Drawing.Point(250, 38);
            this.buttonReadPowerMeter.Name = "buttonReadPowerMeter";
            this.buttonReadPowerMeter.Size = new System.Drawing.Size(75, 23);
            this.buttonReadPowerMeter.TabIndex = 2;
            this.buttonReadPowerMeter.Text = "Read Power Meter";
            this.buttonReadPowerMeter.UseVisualStyleBackColor = true;
            this.buttonReadPowerMeter.Click += new System.EventHandler(this.buttonReadPowerMeter_Click);
            // 
            // buttonReadDUTPower
            // 
            this.buttonReadDUTPower.Location = new System.Drawing.Point(250, 115);
            this.buttonReadDUTPower.Name = "buttonReadDUTPower";
            this.buttonReadDUTPower.Size = new System.Drawing.Size(110, 23);
            this.buttonReadDUTPower.TabIndex = 3;
            this.buttonReadDUTPower.Text = "Read DUT Power";
            this.buttonReadDUTPower.UseVisualStyleBackColor = true;
            this.buttonReadDUTPower.Click += new System.EventHandler(this.buttonReadDUTPower_Click);
            // 
            // textBoxPMVolts
            // 
            this.textBoxPMVolts.Location = new System.Drawing.Point(395, 41);
            this.textBoxPMVolts.Name = "textBoxPMVolts";
            this.textBoxPMVolts.Size = new System.Drawing.Size(100, 20);
            this.textBoxPMVolts.TabIndex = 4;
            // 
            // textBoxPMWatts
            // 
            this.textBoxPMWatts.Location = new System.Drawing.Point(395, 68);
            this.textBoxPMWatts.Name = "textBoxPMWatts";
            this.textBoxPMWatts.Size = new System.Drawing.Size(100, 20);
            this.textBoxPMWatts.TabIndex = 5;
            // 
            // textBoxDUTVoltsSwitched
            // 
            this.textBoxDUTVoltsSwitched.Location = new System.Drawing.Point(395, 117);
            this.textBoxDUTVoltsSwitched.Name = "textBoxDUTVoltsSwitched";
            this.textBoxDUTVoltsSwitched.Size = new System.Drawing.Size(100, 20);
            this.textBoxDUTVoltsSwitched.TabIndex = 6;
            // 
            // textBoxDUTWattsSwitched
            // 
            this.textBoxDUTWattsSwitched.Location = new System.Drawing.Point(395, 144);
            this.textBoxDUTWattsSwitched.Name = "textBoxDUTWattsSwitched";
            this.textBoxDUTWattsSwitched.Size = new System.Drawing.Size(100, 20);
            this.textBoxDUTWattsSwitched.TabIndex = 7;
            // 
            // buttonExit
            // 
            this.buttonExit.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonExit.Location = new System.Drawing.Point(431, 367);
            this.buttonExit.Name = "buttonExit";
            this.buttonExit.Size = new System.Drawing.Size(75, 23);
            this.buttonExit.TabIndex = 8;
            this.buttonExit.Text = "Exit";
            this.buttonExit.UseVisualStyleBackColor = true;
            // 
            // textWindowStatus
            // 
            this.textWindowStatus.AutoSize = true;
            this.textWindowStatus.Location = new System.Drawing.Point(49, 300);
            this.textWindowStatus.Name = "textWindowStatus";
            this.textWindowStatus.Size = new System.Drawing.Size(16, 13);
            this.textWindowStatus.TabIndex = 9;
            this.textWindowStatus.Text = "...";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(382, 101);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "Switched";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(382, 167);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "unSwitched";
            // 
            // textBoxDUTVoltsUnswitched
            // 
            this.textBoxDUTVoltsUnswitched.Location = new System.Drawing.Point(395, 188);
            this.textBoxDUTVoltsUnswitched.Name = "textBoxDUTVoltsUnswitched";
            this.textBoxDUTVoltsUnswitched.Size = new System.Drawing.Size(100, 20);
            this.textBoxDUTVoltsUnswitched.TabIndex = 12;
            // 
            // textBoxDUTWattsUnswitched
            // 
            this.textBoxDUTWattsUnswitched.Location = new System.Drawing.Point(395, 215);
            this.textBoxDUTWattsUnswitched.Name = "textBoxDUTWattsUnswitched";
            this.textBoxDUTWattsUnswitched.Size = new System.Drawing.Size(100, 20);
            this.textBoxDUTWattsUnswitched.TabIndex = 13;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(52, 252);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(104, 13);
            this.label3.TabIndex = 15;
            this.label3.Text = "Text to send to DUT";
            // 
            // textSendToDUT
            // 
            this.textSendToDUT.Location = new System.Drawing.Point(49, 269);
            this.textSendToDUT.Name = "textSendToDUT";
            this.textSendToDUT.Size = new System.Drawing.Size(276, 22);
            this.textSendToDUT.TabIndex = 16;
            this.textSendToDUT.Text = "";
            this.textSendToDUT.TextChanged += new System.EventHandler(this.textSendToDUT_TextChanged);
            // 
            // LEDColor
            // 
            this.LEDColor.Location = new System.Drawing.Point(177, 358);
            this.LEDColor.Name = "LEDColor";
            this.LEDColor.ReadOnly = true;
            this.LEDColor.Size = new System.Drawing.Size(22, 20);
            this.LEDColor.TabIndex = 33;
            this.LEDColor.TabStop = false;
            // 
            // textLEDIntest
            // 
            this.textLEDIntest.AutoSize = true;
            this.textLEDIntest.Location = new System.Drawing.Point(107, 371);
            this.textLEDIntest.Name = "textLEDIntest";
            this.textLEDIntest.Size = new System.Drawing.Size(16, 13);
            this.textLEDIntest.TabIndex = 32;
            this.textLEDIntest.Text = "---";
            // 
            // textLEDFreq
            // 
            this.textLEDFreq.AutoSize = true;
            this.textLEDFreq.Location = new System.Drawing.Point(107, 358);
            this.textLEDFreq.Name = "textLEDFreq";
            this.textLEDFreq.Size = new System.Drawing.Size(16, 13);
            this.textLEDFreq.TabIndex = 31;
            this.textLEDFreq.Text = "---";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(56, 371);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(33, 13);
            this.label9.TabIndex = 30;
            this.label9.Text = "Intest";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(56, 358);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(28, 13);
            this.label7.TabIndex = 29;
            this.label7.Text = "Freq";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(40, 337);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(70, 13);
            this.label5.TabIndex = 28;
            this.label5.Text = "LED detector";
            // 
            // DUT3p3vReading
            // 
            this.DUT3p3vReading.AutoSize = true;
            this.DUT3p3vReading.Location = new System.Drawing.Point(370, 361);
            this.DUT3p3vReading.Name = "DUT3p3vReading";
            this.DUT3p3vReading.Size = new System.Drawing.Size(16, 13);
            this.DUT3p3vReading.TabIndex = 41;
            this.DUT3p3vReading.Text = "---";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(261, 361);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(67, 13);
            this.label6.TabIndex = 40;
            this.label6.Text = "3.3V reading";
            // 
            // textPushButtonStatus
            // 
            this.textPushButtonStatus.AutoSize = true;
            this.textPushButtonStatus.Location = new System.Drawing.Point(370, 344);
            this.textPushButtonStatus.Name = "textPushButtonStatus";
            this.textPushButtonStatus.Size = new System.Drawing.Size(63, 13);
            this.textPushButtonStatus.TabIndex = 37;
            this.textPushButtonStatus.Text = "Not Pushed";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(261, 344);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(98, 13);
            this.label8.TabIndex = 36;
            this.label8.Text = "Push Button Status";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(261, 323);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(54, 13);
            this.label10.TabIndex = 35;
            this.label10.Text = "Lid Status";
            // 
            // textLidStatus
            // 
            this.textLidStatus.AutoSize = true;
            this.textLidStatus.Location = new System.Drawing.Point(370, 323);
            this.textLidStatus.Name = "textLidStatus";
            this.textLidStatus.Size = new System.Drawing.Size(33, 13);
            this.textLidStatus.TabIndex = 34;
            this.textLidStatus.Text = "Open";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioButtonSwitchedOuputOff);
            this.groupBox1.Controls.Add(this.radioButtonSwitchedOutputOn);
            this.groupBox1.Location = new System.Drawing.Point(49, 187);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 62);
            this.groupBox1.TabIndex = 42;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Switched output ";
            // 
            // radioButtonSwitchedOuputOff
            // 
            this.radioButtonSwitchedOuputOff.AutoSize = true;
            this.radioButtonSwitchedOuputOff.Location = new System.Drawing.Point(85, 28);
            this.radioButtonSwitchedOuputOff.Name = "radioButtonSwitchedOuputOff";
            this.radioButtonSwitchedOuputOff.Size = new System.Drawing.Size(39, 17);
            this.radioButtonSwitchedOuputOff.TabIndex = 3;
            this.radioButtonSwitchedOuputOff.TabStop = true;
            this.radioButtonSwitchedOuputOff.Text = "Off";
            this.radioButtonSwitchedOuputOff.UseVisualStyleBackColor = true;
            this.radioButtonSwitchedOuputOff.Click += new System.EventHandler(this.radioButtonSwitchedOuputOff_CheckedChanged);
            // 
            // radioButtonSwitchedOutputOn
            // 
            this.radioButtonSwitchedOutputOn.AutoSize = true;
            this.radioButtonSwitchedOutputOn.Location = new System.Drawing.Point(22, 28);
            this.radioButtonSwitchedOutputOn.Name = "radioButtonSwitchedOutputOn";
            this.radioButtonSwitchedOutputOn.Size = new System.Drawing.Size(39, 17);
            this.radioButtonSwitchedOutputOn.TabIndex = 2;
            this.radioButtonSwitchedOutputOn.TabStop = true;
            this.radioButtonSwitchedOutputOn.Text = "On";
            this.radioButtonSwitchedOutputOn.UseVisualStyleBackColor = true;
            this.radioButtonSwitchedOutputOn.Click += new System.EventHandler(this.radioButtonSwitchedOutputOn_CheckedChanged);
            // 
            // rtbStatus
            // 
            this.rtbStatus.Location = new System.Drawing.Point(43, 403);
            this.rtbStatus.Name = "rtbStatus";
            this.rtbStatus.Size = new System.Drawing.Size(360, 96);
            this.rtbStatus.TabIndex = 43;
            this.rtbStatus.Text = "";
            // 
            // JBUFixtureControlWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(557, 509);
            this.ControlBox = false;
            this.Controls.Add(this.rtbStatus);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.DUT3p3vReading);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.textPushButtonStatus);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.textLidStatus);
            this.Controls.Add(this.LEDColor);
            this.Controls.Add(this.textLEDIntest);
            this.Controls.Add(this.textLEDFreq);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.textSendToDUT);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBoxDUTWattsUnswitched);
            this.Controls.Add(this.textBoxDUTVoltsUnswitched);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textWindowStatus);
            this.Controls.Add(this.buttonExit);
            this.Controls.Add(this.textBoxDUTWattsSwitched);
            this.Controls.Add(this.textBoxDUTVoltsSwitched);
            this.Controls.Add(this.textBoxPMWatts);
            this.Controls.Add(this.textBoxPMVolts);
            this.Controls.Add(this.buttonReadDUTPower);
            this.Controls.Add(this.buttonReadPowerMeter);
            this.Controls.Add(this.groupBoxMonitorRelay);
            this.Controls.Add(this.groupBoxDUTACPower);
            this.Name = "JBUFixtureControlWindow";
            this.Text = "JB Fixture Control Window";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.JBFixtureControlWindow_FormClosing);
            this.Load += new System.EventHandler(this.JBUFixtureControlWindow_Load);
            this.groupBoxDUTACPower.ResumeLayout(false);
            this.groupBoxDUTACPower.PerformLayout();
            this.groupBoxMonitorRelay.ResumeLayout(false);
            this.groupBoxMonitorRelay.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxDUTACPower;
        private System.Windows.Forms.RadioButton radioButtonDUTACPowerOff;
        private System.Windows.Forms.RadioButton radioButtonDUTACPowerOn;
        private System.Windows.Forms.GroupBox groupBoxMonitorRelay;
        private System.Windows.Forms.RadioButton radioButtonOutputMonitorUnswitched;
        private System.Windows.Forms.RadioButton radioButtonOutputMonitorSwitched;
        private System.Windows.Forms.Button buttonReadPowerMeter;
        private System.Windows.Forms.Button buttonReadDUTPower;
        private System.Windows.Forms.TextBox textBoxPMVolts;
        private System.Windows.Forms.TextBox textBoxPMWatts;
        private System.Windows.Forms.TextBox textBoxDUTVoltsSwitched;
        private System.Windows.Forms.TextBox textBoxDUTWattsSwitched;
        private System.Windows.Forms.Button buttonExit;
        private System.Windows.Forms.Label textWindowStatus;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxDUTVoltsUnswitched;
        private System.Windows.Forms.TextBox textBoxDUTWattsUnswitched;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RichTextBox textSendToDUT;
        private System.Windows.Forms.TextBox LEDColor;
        private System.Windows.Forms.Label textLEDIntest;
        private System.Windows.Forms.Label textLEDFreq;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label DUT3p3vReading;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label textPushButtonStatus;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label textLidStatus;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton radioButtonSwitchedOuputOff;
        private System.Windows.Forms.RadioButton radioButtonSwitchedOutputOn;
        private System.Windows.Forms.RichTextBox rtbStatus;
    }
}