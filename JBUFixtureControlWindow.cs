﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace SensorManufSY2
{
    public partial class JBUFixtureControlWindow : Form
    {
        FixtureFunctions fixture;
        SensorConsole senConsole;
        StationConfigure stationConfig;
        private ACMeterCommications acMeterConsole;
        private JunctionBox plugLoadTests;
        MainWindow.uutdata uutData = new MainWindow.uutdata();
//        LEDConfigSection section;

        string msg = string.Empty;
        string prompt = "DVTMAN> ";

        Thread tSendCommand;
        ThreadStart updateScreenThreadStart;
        Thread updateScreenThread;
        ThreadStart updateLEDScreenThreadStart;
        Thread updateLEDScreenThread;
        bool runThreads = true;

        public delegate void updateFixtureReadings(string lid, string pushButton, string voltage);
        public delegate void updateLEDReadings(int intensity, int freq, Color backColor);
        public delegate void updateText(string msg);

        public JBUFixtureControlWindow(MainWindow mainWindowobj, SensorConsole psenConsole)
        {
            string outmsg = string.Empty;

            InitializeComponent();

            senConsole = psenConsole;          // serial console for dut
            senConsole.DVTMANPrompt = prompt;

            stationConfig = mainWindowobj.stationConfig;

            fixture = new FixtureFunctions();
            fixture.StationConfigure = mainWindowobj.stationConfig;
            if (!fixture.InitFixture())
            {
                MessageBox.Show("Error initializing fixture. " + fixture.LastFunctionErrorMessage);
                return;
            }

            acMeterConsole = new ACMeterCommications();
            acMeterConsole.Open(stationConfig.ConfigPowerMeterComPort);

            uutData.dvtmanPrompt = "DVTMAN> ";
            plugLoadTests = new JunctionBox(senConsole, fixture, stationConfig, acMeterConsole, uutData);


            // set every thing to a know state
            fixture.ControlACMonitor(FixtureFunctions.ACMonitorSelection.Switched);
            fixture.ControlDUTPower(0);

            radioButtonDUTACPowerOff.Checked= true;
            radioButtonOutputMonitorSwitched.Checked = true;
            radioButtonSwitchedOutputOn.Checked = true;

            if (stationConfig.ConfigEnableConsoleDump)
            {
                senConsole.SetDumpFileName(stationConfig.ConfigReportDirectory, "DebugSen_" + DateTime.UtcNow.ToString("o").Replace(':', '_') + ".txt");
            }

            updateScreenThreadStart = new ThreadStart(StartUpdateScreen);
            updateScreenThread = new Thread(updateScreenThreadStart);
            updateScreenThread.Start();
            updateLEDScreenThreadStart = new ThreadStart(StartLEDUpdateScreen);
            updateLEDScreenThread = new Thread(updateLEDScreenThreadStart);
            updateLEDScreenThread.Start();

        }

        // screen background stuff
        private void StartUpdateScreen()
        {

            updateFixtureReadings UpdateFixtureScreenDel = new updateFixtureReadings(UpdateFixtureReadings);
            while (runThreads)
            {
                if (stationConfig.FixtureEquipment.hasLabJack)
                {
                this.textLidStatus.BeginInvoke(UpdateFixtureScreenDel, fixture.IsCoverClosed() ? "CLOSED" : "OPEN",
                                                                fixture.IsBoxButtonPushed() ? "Pushed" : "Not Pushed",
                                                                fixture.Read3p3Voltage().ToString("N1"));
                Thread.Sleep(100);
                }
            }
        }

        private void UpdateFixtureReadings(string lid, string pushButton, string voltage)
        {
            textLidStatus.Text = lid;
            textPushButtonStatus.Text = pushButton;
            DUT3p3vReading.Text = voltage;
        }

        private void StartLEDUpdateScreen()
        {
            int intensity = 0;
            int freq = 0;
            string msg = string.Empty;
            Color backColor;

            updateLEDReadings UpdateLEDScreenDel = new updateLEDReadings(UpdateLEDReadings);
            while (runThreads)
            {
                if (fixture.CheckLedColor("Red", stationConfig.ConfigLedDetector.ColorRED, ref freq, ref intensity, ref msg) == (int)ErrorCodes.Program.Success)
                {
                    backColor = Color.Red;
                }
                else if (fixture.CheckLedColor("Green", stationConfig.ConfigLedDetector.ColorGREEN, ref freq, ref intensity, ref msg) == (int)ErrorCodes.Program.Success)
                {
                    backColor = Color.Green;
                }
                else if (fixture.CheckLedColor("Blue", stationConfig.ConfigLedDetector.ColorBLUE, ref freq, ref intensity, ref msg) == (int)ErrorCodes.Program.Success)
                {
                    backColor = Color.Blue;
                }
                else
                {
                    backColor = Color.Gray;
                }
                this.textLEDFreq.BeginInvoke(UpdateLEDScreenDel, intensity, freq, backColor);
                Thread.Sleep(500);
            }

        }

        
        private void UpdateFixtureReadings(string lid, string pushButton, string temp, string voltage)
        {
            textLidStatus.Text = lid;
            textPushButtonStatus.Text = pushButton;
            DUT3p3vReading.Text = voltage;
        }

        private void UpdateLEDReadings(int intensity, int freq, Color backColor)
        {
            LEDColor.BackColor = backColor;
            textLEDFreq.Text = freq.ToString("N0");
            textLEDIntest.Text = intensity.ToString("N0");
        }

        // fixture power control
        private void radioButtonradioButtonDUTACPowerOn_Click(object sender, EventArgs e)
        {
            radioButtonDUTACPowerOn.Checked = true;
            fixture.ControlDUTPower(1);
        }

        private void radioButtonradioButtonDUTACPowerOff_Click(object sender, EventArgs e)
        {
            radioButtonDUTACPowerOff.Checked = true;
            fixture.ControlDUTPower(0);
        }

        // output monitor switch
        private void radioButtonOutputMonitorSwitched_Click(object sender, EventArgs e)
        {
            radioButtonOutputMonitorSwitched.Checked = true;
            fixture.ControlACMonitor(FixtureFunctions.ACMonitorSelection.Switched);
        }

        private void radioButtonOutputMonitorUnswitched_Click(object sender, EventArgs e)
        {
            radioButtonOutputMonitorUnswitched.Checked = true;
            fixture.ControlACMonitor(FixtureFunctions.ACMonitorSelection.Unswitchted);
        }


        // read the power meter
        private void buttonReadPowerMeter_Click(object sender, EventArgs e)
        {
            double volts = 0;
            double watts = 0;
            string msg = string.Empty;

            acMeterConsole.ReadVoltage(ref volts, ref msg);
            acMeterConsole.ReadWatts(ref watts, ref msg);
            textBoxPMVolts.Text = volts.ToString("N0");
            textBoxPMWatts.Text = watts.ToString("N0");
        }

        // read the DUT power readings
        private void buttonReadDUTPower_Click(object sender, EventArgs e)
        {
            PlugLoadData plugLoadData = new PlugLoadData();
            MainWindow.uutdata uutData = new MainWindow.uutdata();
            uutData.dvtmanPrompt = "DVTMAN> ";
            string msg = string.Empty;

            senConsole.ClearBuffers();
            senConsole.DisplayRcvChar = false;
            senConsole.DisplaySendChar = false;
            plugLoadTests.ReadCUStatus(uutData, ref plugLoadData, ref msg);
            senConsole.DisplayRcvChar = true;
            senConsole.DisplaySendChar = true;
            textBoxDUTVoltsSwitched.Text = plugLoadData.RMSvoltage.ToString("N0");
            textBoxDUTWattsSwitched.Text = plugLoadData.Power.ToString("N0");
            textBoxDUTVoltsUnswitched.Text = plugLoadData.RMSvoltage2.ToString("N0");
            textBoxDUTWattsUnswitched.Text = plugLoadData.Power2.ToString("N0");
       }


        private void JBFixtureControlWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            runThreads = false;
            fixture.ControlDUTPower(0);
            acMeterConsole.Close();
            acMeterConsole.Dispose();
            while (updateScreenThread.IsAlive | updateLEDScreenThread.IsAlive)
            {

            }
            updateScreenThread.Join();
        }

        // send text data to sensor
        private void textSendToDUT_TextChanged(object sender, EventArgs e)
        {
             if (textSendToDUT.Text.Contains('\n'))
             {
                 lclsendtext(textSendToDUT.Text.Substring(0, textSendToDUT.Text.Length - 1));
                 textSendToDUT.Text = "";
             }
        }

        private void lclsendtext(string textSendToDUT)
        {
            string cmd = string.Empty;
            if (tSendCommand != null)
            {
                if (tSendCommand.IsAlive)
                {
                    rtbStatus.AppendText("Already sending\n");
                    return;
                }
            }
            senConsole.ClearBuffers();
            cmd = textSendToDUT;
            tSendCommand = new Thread(() => docmdwrite(cmd, 1000));
            tSendCommand.Start();
        }

        private void docmdwrite(string cmd, int timeout)
        {
            senConsole.WriteLine(cmd, timeout);
        }

        private void radioButtonSwitchedOutputOn_CheckedChanged(object sender, EventArgs e)
        {
            radioButtonSwitchedOutputOn.Checked = true;
            lclsendtext("p relay 1");
        }

        private void radioButtonSwitchedOuputOff_CheckedChanged(object sender, EventArgs e)
        {
            radioButtonSwitchedOuputOff.Checked = true;
            lclsendtext("p relay 0");

        }

        private void JBUFixtureControlWindow_Load(object sender, EventArgs e)
        {

        }



    }
}
