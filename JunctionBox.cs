﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;


using LimitsDatabase;
using FixtureDatabase;

namespace SensorManufSY2
{
    public class PlugLoadData
    {
        public enum CURelayStatus {OPENED, CLOSED};



        public string CUver = string.Empty;
        public string CUstatus = string.Empty;
        public int CUrelay = (int)CURelayStatus.CLOSED;
        public double CUtemp = 0;
        public double RMSvoltage = 0;
        public double RMSvoltage2 = 0;
        public double Linefreq = 0;
        public double RMScurrent = 0;
        public double RMScurrent2 = 0;
        public double Energysincestart = 0;
        public double Energy2sincestart = 0;
        public double Energysincelastread = 0;
        public double Energy2sincelastread = 0;
        public double Power = 0;
        public double Power2 = 0;
        public double Apparentpwr = 0;
        public double Apparentpwr2 = 0;
        public double Reactivepwr = 0;
        public double Reactivepwr2 = 0;
        public double Pwrfactor = 0;
        public double Pwrfactor2 = 0;
        public double Phaseangle = 0;
        public double Phaseangle2 = 0;
        public int CUStatsErrCnt = 0;
        public int CUStatsErrCntat = 0;
        public int CUStatsTx = 0;
        public int CUStatsRx = 0;
        public int CUStatsRx0 = 0;
        public int CUStatsRx1 = 0;
        public int CUStatsRx2 = 0;
        public int CUStatsRx3 = 0;
        public int CUStatsRx4 = 0;
        public int CUStatsRx5 = 0;
        public int CUStatsRx6 = 0;
        public int CUStatsRx7 = 0;
        public int CUStatsRxSerialNAKError = 0;
        public int CUStatsRxSerialTimeout = 0;
        public int CUStatsRxBADCRC = 0;
        public int RelayTotalOn = 0;
        public int RelayTotalOff = 0;
        public int RelaySuccessOn = 0;
        public int RelaySuccessOff = 0;
        public int RelayFailOn = 0;
        public int RelayFailOff = 0;

        public PlugLoadData ()
        {
            CUver = string.Empty;
            CUstatus = string.Empty;
            CUrelay = (int)CURelayStatus.CLOSED;
            CUtemp = 0;
            RMSvoltage = 0;
            RMSvoltage2 = 0;
            Linefreq = 0;
            RMScurrent = 0;
            RMScurrent2 = 0;
            Energysincestart = 0;
            Energy2sincestart = 0;
            Energysincelastread = 0;
            Energy2sincelastread = 0;
            Power = 0;
            Power2 = 0;
            Apparentpwr = 0;
            Apparentpwr2 = 0;
            Reactivepwr = 0;
            Reactivepwr2 = 0;
            Pwrfactor = 0;
            Pwrfactor2 = 0;
            Phaseangle = 0;
            Phaseangle2 = 0;
            CUStatsErrCnt = 0;
            CUStatsErrCntat = 0;
            CUStatsTx = 0;
            CUStatsRx = 0;
            CUStatsRx0 = 0;
            CUStatsRx1 = 0;
            CUStatsRx2 = 0;
            CUStatsRx3 = 0;
            CUStatsRx4 = 0;
            CUStatsRx5 = 0;
            CUStatsRx6 = 0;
            CUStatsRx7 = 0;
            CUStatsRxSerialNAKError = 0;
            CUStatsRxSerialTimeout = 0;
            CUStatsRxBADCRC = 0;
            RelayTotalOn = 0;
            RelayTotalOff = 0;
            RelaySuccessOn = 0;
            RelaySuccessOff = 0;
            RelayFailOn = 0;
            RelayFailOff = 0;
        }
    }

    public partial class JunctionBox
    {
        //------------------------
        // events
        //  Use UpdateStatusWindow(msg) to display a message on the
        //  MainWindow status window.
        //------------------------
        public event EventHandler<StatusUpdatedEventArgs>
            StatusUpdated;

        protected virtual void
            OnStatusUpdated(StatusUpdatedEventArgs e)
        {
            if (StatusUpdated != null)
                StatusUpdated(this, e);
        }

        // call these functions to update the main status window
        private void UpdateStatusWindow(string msg)
        {
            string newStatus = msg;
            StatusUpdatedEventArgs nuea = new StatusUpdatedEventArgs();
            nuea.statusText = msg;
            OnStatusUpdated(nuea);
        }
        private void UpdateStatusWindow(string msg, StatusType status)
        {
            string newStatus = msg;
            StatusUpdatedEventArgs nuea = new StatusUpdatedEventArgs();
            nuea.statusText = msg;
            nuea.statusType = status;
            OnStatusUpdated(nuea);
        }

        private void UpdateTestHeader(int step, int test, string name)
        {
            int newStep = step;
            int newTest = test;
            string newName = name;
            StatusUpdatedEventArgs nuea = new StatusUpdatedEventArgs();
            nuea.statusText = name;
            nuea.stepNumber = step;
            nuea.testNumber = test;
            OnStatusUpdated(nuea);
        }

        private void UpdateTestFooter(int testResultCode, string errormsg)
        {
            StatusUpdatedEventArgs nuea = new StatusUpdatedEventArgs();
            nuea.statusText = errormsg;
            nuea.resultCode = testResultCode;
            OnStatusUpdated(nuea);
        }


        SensorConsole sensorConsole;
        TestRadioConsole testRadioConsole;
        FixtureFunctions fixture;
        private PlugLoadData plugloadData;
        StationConfigure stationConfig;
        MainWindow.uutdata uutData;
        ACMeterCommications powerMeterConsole;

        public JunctionBox(SensorConsole sc, FixtureFunctions fx, StationConfigure conf,
            ACMeterCommications pm, MainWindow.uutdata ud)
        {
            sensorConsole = sc;
            fixture = fx;
            stationConfig = conf;
            plugloadData = new PlugLoadData();
            uutData = ud;
            powerMeterConsole = pm;
        }

        /// <summary>
        /// Verify that the firmware on the Maxim is correct.
        /// </summary>
        /// <remarks>
        /// Will read the cu status and get the version from CUver.
        /// 
        /// the Limit parameter is:
        ///     param: CUVersion  type: string
        ///     
        /// </remarks>
        /// <param name="parameters"></param>
        /// <param name="testDB"></param>
        /// <param name="resultmsg"></param>
        /// <returns></returns>
        public int VerifyCUVersion(LimitsData.stepsItem parameters, FixtureDatabase.ResultData testDB, ref string resultmsg)
        {
            string verifyVersion = string.Empty;
            int status = (int)ErrorCodes.Program.Success;

            resultmsg = string.Empty;
            foreach (var item in parameters.limits)
            {
                if (item.name.ToUpper().Equals("CUVERSION"))
                    verifyVersion = item.value1;
            }
            if (verifyVersion == string.Empty)
            {
                status = (int)ErrorCodes.System.MissingParameter;
                resultmsg = "Limit parameter CUVersion is missing from test.";
                return status;
            }
            status = ReadCUStatus(uutData, ref plugloadData, ref resultmsg);
            if (status == (int)ErrorCodes.Program.Success)
            {
                if (!plugloadData.CUver.Equals(verifyVersion))
                {
                    status = (int)ErrorCodes.Program.ValueOutsideOfLimit + (100 * parameters.step_number);
                    resultmsg = "Expected: " + verifyVersion + " Found: " + plugloadData.CUver;
                }
            }
            return status;
        }

        /// <summary>
        /// Verify that the relay on the switched AC line is working.
        /// </summary>
        /// <remarks>
        /// The realy operation will be verified by
        ///     set the fixture load select relay off(monitor switched side)
        ///     set the board switched AC relay to Close
        ///     verify that the RMS current is greater than 50% of the test constant FixtureLoadA
        ///     set the board switched AC relay to Open
        ///     Verify that the RMS current is 0
        /// </remarks>
        /// <param name="parameters"></param>
        /// <param name="testDB"></param>
        /// <param name="resultmsg"></param>
        /// <returns></returns>
        public int VerifySwitchedRelay(LimitsData.stepsItem parameters, FixtureDatabase.ResultData testDB, ref string resultmsg)
        {
            int status = (int)ErrorCodes.Program.Success;
            double fixLoad = 0;
            string msg = string.Empty;

            resultmsg = string.Empty;

            testDB.CreateStepRecord(parameters.name, parameters.step_number);

            // get the fixture load power
            fixLoad = stationConfig.ConfigFixtureLoadA;

            // Get the loaded switched output reading
            status = SetSwitchedOuputRelay(SwitchedOutputRelay.Closed, ref resultmsg);            // turn on switched output
            if (status == (int)ErrorCodes.Program.Success)
            {
                // read the status
                if (ReadCUStatus(uutData, ref plugloadData, ref resultmsg) == (int)ErrorCodes.Program.Success)
                {
                    if (plugloadData.RMScurrent < (fixLoad / 2))                // if don't see power
                    {
                        status = (int)ErrorCodes.Program.ValueOutsideOfLimit;
                        resultmsg = "Did not see switched on power";
                    }
                }
            }

            if (status == (int)ErrorCodes.Program.Success)
                UpdateStatusWindow("Relay closed reading: " + plugloadData.RMScurrent.ToString("N3") + "\n", StatusType.text);
            else
                UpdateStatusWindow("Error: " + resultmsg + "\n");

            // get the unloaded switched output reading
            if (status == (int)ErrorCodes.Program.Success)
            {
                status = SetSwitchedOuputRelay(SwitchedOutputRelay.Open, ref resultmsg);            // turn off switched output
                if (status == (int)ErrorCodes.Program.Success)
                {
                    // read the status
                    if (ReadCUStatus(uutData, ref plugloadData, ref resultmsg) == (int)ErrorCodes.Program.Success)
                    {
                        if (plugloadData.RMScurrent > (fixLoad / 2))                // if don't see power
                        {
                            status = (int)ErrorCodes.Program.ValueOutsideOfLimit;
                            resultmsg = "Reading power with relay off";
                        }
                    }
                }
            }
            if (status == (int)ErrorCodes.Program.Success)
                UpdateStatusWindow("Relay open reading: " + plugloadData.RMScurrent.ToString("N3") + "\n", StatusType.text);
            else
            {
                UpdateStatusWindow("Error: " + resultmsg + "\n");
                if (status < (int)ErrorCodes.System.SystemErrorCodes)
                    status = status + +(100 * parameters.step_number);
            }

            SetSwitchedOuputRelay(SwitchedOutputRelay.Closed, ref msg);                   // turn off the switched output
            //Thread.Sleep(500);
            testDB.AddStepResult((status == (int)ErrorCodes.Program.Success) ? "PASS" : "FAIL", resultmsg, status);
            return status;
        }

        /// <summary>
        /// Verify the Voltage and Current readings of a output.
        /// </summary>
        /// <remarks>
        /// The outputs will be compared with the fixture meter.
        ///     set the board switched AC relay to Close 
        ///     wait for Delay ms
        ///     Read the fixture power monitor
        ///     Read the board Voltage2 and Current2 readings
        ///     Read the board Voltage and Current readings
        ///     Compare with power meter using the difference limits passed.
        /// The parameters will be
        ///     param: VoltageError  type: Range 
        ///     param: CurrentError  type: Range    current is in Amps
        /// Note that both of the outputs are on. So the Power meter will read the load of
        /// both ouputs. So each watts reading should read 1/2 of the power meter.
        /// </remarks>
        /// <param name="parameters"></param>
        /// <param name="testDB"></param>
        /// <param name="resultmsg"></param>
        /// <returns></returns>
        public int VerifyReadings(LimitsData.stepsItem parameters, FixtureDatabase.ResultData testDB, ref string resultmsg)
        {
            int status = (int)ErrorCodes.Program.Success;
            bool limitStatus = true;
            string limitResultmsg = string.Empty;
            double pmVoltage = 0;
            double pmWatts = 0;
            int pmStatus = (int)ErrorCodes.Program.Success;
            double switchedVoltage = 0;
            double switchedWatts = 0;
            double unswitchedVoltage = 0;
            double unswitchedWatts = 0;
            LimitsData.limitsItem voltageLimits = null;
            LimitsData.limitsItem wattsLimits = null;
            int setCloseRelayDelay = 1000;      // milliseconds to wait after the relay close command given

            // get the parameters. Must have Voltage and/or Watts
            foreach (var item in parameters.limits)
            {
                if (item.name.ToUpper().Equals("VOLTAGE"))
                {
                    voltageLimits = item;
                }
                if (item.name.ToUpper().Equals("WATTS"))
                {
                    wattsLimits = item;
                }
            }
            
            if ((voltageLimits == null) | (wattsLimits == null))
            {
                status = (int)ErrorCodes.System.MissingParameter;
                resultmsg = "The Limits Voltage or Watts are missing from the test.";
                return status;
            }

            // look for any constants
            if (parameters.parameters != null)
            {
                foreach (var item in parameters.parameters)
                {
                    if (item.name.ToUpper().Contains("DELAY"))
                        if (!Int32.TryParse(item.value, out setCloseRelayDelay))
                        {
                            status = (int)ErrorCodes.System.InvalidParameter;
                            resultmsg = "The constant 'Delay' is not a number";
                            return status;
                        }
                }
            }

            testDB.CreateStepRecord(parameters.name, parameters.step_number);                // create the test step in the database

            // turn on the switched output
            if ((status = SetSwitchedOuputRelay(SwitchedOutputRelay.Closed, ref resultmsg)) != (int)ErrorCodes.Program.Success)
            {
                UpdateStatusWindow("Error setting relay. " + resultmsg);
                status = (int)ErrorCodes.Program.DutUnexpectedCmdReturn;
            }
            else
                Thread.Sleep(setCloseRelayDelay);       // give time for the relay to close

            // get the fixture data
            if (status == (int)ErrorCodes.Program.Success)
            {
                pmStatus = powerMeterConsole.ReadVoltage(ref pmVoltage, ref resultmsg);
                if (pmStatus != (int)ErrorCodes.Program.Success)
                    status = pmStatus;
                else
                {
                    pmStatus = powerMeterConsole.ReadWatts(ref pmWatts, ref resultmsg);
                    if (pmStatus != (int)ErrorCodes.Program.Success)
                        status = pmStatus;
                    else
                        UpdateStatusWindow(string.Format("Power Meter readings: Voltage: {0:N0} Watts: {1:N0}\n", pmVoltage, pmWatts));
                }
            }

            // get the data from the DUT
            if (status == (int)ErrorCodes.Program.Success)
            {
                if ((status = ReadCUStatus(uutData, ref plugloadData, ref resultmsg)) == (int)ErrorCodes.Program.Success)
                {
                    unswitchedVoltage = plugloadData.RMSvoltage2;
                    unswitchedWatts = plugloadData.Power2;
                    UpdateStatusWindow(string.Format("unSwitched readings: Voltage: {0:N0} Watts: {1:N0}\n", unswitchedVoltage, unswitchedWatts));
                    switchedVoltage = plugloadData.RMSvoltage;
                    switchedWatts = plugloadData.Power;
                    UpdateStatusWindow(string.Format("Switched readings: Voltage: {0:N0} Watts: {1:N0}\n", unswitchedVoltage, unswitchedWatts));
                }
            }

            // test the values with the limits
            if (status == (int)ErrorCodes.Program.Success)
            {
                try         // just in case there is a problem with the limit format
                {
                    limitStatus = CheckResultValue(unswitchedVoltage - pmVoltage, voltageLimits, ref limitResultmsg);
                    testDB.AddStepResult("UnSwitchedVoltage", limitStatus ? "PASS" : "FAIL", voltageLimits.operation, Convert.ToDouble(voltageLimits.value1), Convert.ToDouble(voltageLimits.value2),
                        unswitchedVoltage, "V", "N0", resultmsg, limitStatus ? (int)ErrorCodes.Program.Success : (int)ErrorCodes.Program.ValueOutsideOfLimit);
                    if (!limitStatus)
                    {
                        status = (int)ErrorCodes.Program.ValueOutsideOfLimit;
                        resultmsg = limitResultmsg;
                    }

                    limitStatus = CheckResultValue(unswitchedWatts - (pmWatts/2), wattsLimits, ref limitResultmsg);
                    testDB.AddStepResult("UnSwitchedWatts", limitStatus ? "PASS" : "FAIL", wattsLimits.operation, Convert.ToDouble(wattsLimits.value1), Convert.ToDouble(wattsLimits.value2),
                        unswitchedWatts, "W", "N0", resultmsg, limitStatus ? (int)ErrorCodes.Program.Success : (int)ErrorCodes.Program.ValueOutsideOfLimit);
                    if (!limitStatus)
                    {
                        status = (int)ErrorCodes.Program.ValueOutsideOfLimit;
                        resultmsg = resultmsg + ((resultmsg == string.Empty) ? "" : "\n") + limitResultmsg;
                    }
                    limitStatus = CheckResultValue(switchedVoltage - pmVoltage, voltageLimits, ref limitResultmsg);
                    testDB.AddStepResult("SwitchedVoltage", limitStatus ? "PASS" : "FAIL", voltageLimits.operation, Convert.ToDouble(voltageLimits.value1), Convert.ToDouble(voltageLimits.value2),
                        switchedVoltage, "V", "N0", resultmsg, limitStatus ? (int)ErrorCodes.Program.Success : (int)ErrorCodes.Program.ValueOutsideOfLimit);
                    if (!limitStatus)
                    {
                        status = (int)ErrorCodes.Program.ValueOutsideOfLimit;
                        resultmsg = resultmsg + ((resultmsg == string.Empty) ? "" : "\n") + limitResultmsg;
                    }

                    limitStatus = CheckResultValue(switchedWatts - (pmWatts/2), wattsLimits, ref limitResultmsg);
                    testDB.AddStepResult("SwitchedWatts", limitStatus ? "PASS" : "FAIL", wattsLimits.operation, Convert.ToDouble(wattsLimits.value1), Convert.ToDouble(wattsLimits.value2),
                        switchedWatts, "W", "N0", resultmsg, limitStatus ? (int)ErrorCodes.Program.Success : (int)ErrorCodes.Program.ValueOutsideOfLimit);
                    if (!limitStatus)
                    {
                        status = (int)ErrorCodes.Program.ValueOutsideOfLimit;
                        resultmsg = resultmsg + ((resultmsg == string.Empty) ? "" : "\n") + limitResultmsg;
                    }
                    testDB.AddStepRef("PMVoltage", pmVoltage.ToString("N0"), "V", "N0", "");
                    testDB.AddStepRef("PMWatts", pmWatts.ToString("N0"), "W", "N0", "");
                }
                catch (Exception ex)
                {
                    status = (int)ErrorCodes.System.InvalidParameter;
                    resultmsg = ex.Message;
                }
            }

            if (status != (int)ErrorCodes.Program.Success)
            {
                if (status < (int)ErrorCodes.System.SystemErrorCodes)
                    status = status + (100 * parameters.step_number);
                testDB.AddStepResult(ResultData.TEST_FAILED, resultmsg, status);
            }

            return status;
        }
    }
}
