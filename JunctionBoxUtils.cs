﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Diagnostics;

//using LimitFileFunctions;

namespace SensorManufSY2
{
    public partial class JunctionBox
    {
        const string CUverText = "CUver=";
        const string CUstatusText = "CU status=";
        const string CUrelayText = "CU relay=";
        const string CUtempText = "CU temp=";
        const string RMSvoltageText = "RMS voltage=";
        const string RMSvoltage2Text = "RMS voltage2=";
        const string LinefreqText = "Line freq=";
        const string RMScurrentText = "RMS current=";
        const string RMScurrent2Text = "RMS current2=";
        const string EnergysincestartText = "Energy since start=";
        const string Energy2sincestartText = "Energy2 since start=";
        const string EnergysincelastreadText = "Energy since last read=";
        const string Energy2sincelastreadText = "Energy2 since last read2=";
        const string PowerText = "Power=";
        const string Power2Text = "Power2=";
        const string ApparentpwrText = "Apparent pwr=";
        const string Apparentpwr2Text = "Apparent pwr2=";
        const string ReactivepwrText = "Reactive pwr=";
        const string Reactivepwr2Text = "Reactive pwr2=";
        const string PwrfactorText = "Pwr factor=";
        const string Pwrfactor2Text = "Pwr factor2=";
        const string PhaseangleText = "Phase angle=";
        const string Phaseangle2Text = "Phase angle2=";
        const string ErrcntText = "ErrCnt=";
        const string ErrcntatText = ", at=";
        const string TxText = "Tx=";
        const string RxText = "Rx=";
        const string Rx0Text = "Rx[0]=";
        const string Rx1Text = "Rx[1]=";
        const string Rx2Text = "Rx[2]=";
        const string Rx3Text = "Rx[3]=";
        const string Rx4Text = "Rx[4]=";
        const string Rx5Text = "Rx[5]=";
        const string Rx6Text = "Rx[6]=";
        const string Rx7Text = "Rx[7]=";
        const string RxSerialNAKErrorText = "Rx Serial NAK Error=";
        const string RxSerialTimeoutText = "Rx Serial Timeout=";
        const string RxBADCRCText = "Rx BAD CRC=";
        const string RelayTotalText = "Relay (Total) On:";      // with Off: after
        const string RelaySuccessText = "Relay (Success) On:";  // with Off: after
        const string RelayFailText = "Relay (Fail) On:";        // with Off: after


        /// <summary>
        /// Read the CU status.
        /// </summary>
        /// <remarks>
        /// Will issue a 'd cu' command and parse the return:
        /// CUver=0041
        /// CU status=LREVS
        /// CU relay=1(closed)
        /// CU temp=220
        /// RMS voltage=2206
        /// RMS voltage2=2206
        /// Line freq=5999
        /// RMS current=0
        /// RMS current2=149
        /// Energy since start=0
        /// Energy2 since start=0
        /// Energy since last read=150
        /// Energy2 since last read2=32
        /// Power=0
        /// Power2=328
        /// Apparent pwr=0
        /// Apparent pwr2=329
        /// Reactive pwr=0
        /// Reactive pwr2=52
        /// Pwr factor=1000
        /// Pwr factor2=996
        /// Phase angle=0
        /// Phase angle2=5235
        /// CU Stats:
        /// ---------
        /// ErrCnt=0, at=0
        /// Tx=54
        /// Rx=54
        ///     Rx[0]=54
        ///     Rx[1]=0
        ///     Rx[2]=0
        ///     Rx[3]=0
        ///     Rx[4]=0
        ///     Rx[5]=0
        ///     Rx[6]=0
        ///     Rx[7]=0
        ///     Rx Serial NAK Error=0
        ///     Rx Serial Timeout=0
        ///     Rx BAD CRC=0
        /// Relay Stats:
        /// ------------
        /// Relay (Total) On:0, Off:0
        /// Relay (Success) On:0, Off:0
        /// Relay (Fail) On:0, Off:0
        /// </remarks>
        /// <param name="plData"></param>
        /// <param name="outmsg"></param>
        /// <returns></returns>
        public int ReadCUStatus(MainWindow.uutdata uutData, ref PlugLoadData plugloadData, ref string outmsg)
        {
            int status = (int)ErrorCodes.Program.Success;
            string output = string.Empty;

            if (!sensorConsole.WriteLine("d cu", 2000))
            {
                status = (int)ErrorCodes.Program.DUTComError;
                outmsg = "Error sending 'd cu' command";
            }
            else
            {
                if (!sensorConsole.WaitForPrompt(6000, uutData.dvtmanPrompt, out outmsg))
                {
                    status = (int)ErrorCodes.Program.DUTComError;
                    outmsg = "Did not find prompt of 'd cu' command. " + outmsg;
                }
                else
                {
                    // time do decode
 
                    // do the last item first. If it is missing then there is a problem
                    output = GetCUValue(outmsg, RelayFailText);      // returns '0, Off:0'
                    if (output == string.Empty)
                    {
                        status = (int)ErrorCodes.Program.DUTComError;
                        outmsg = "Did not get complete status returned.\n" + outmsg;
                    }
                    else        // got the last item, so get the rest
                    {
                        plugloadData.RelayFailOn = Convert.ToInt32(output.Substring(0, output.IndexOf(',')));
                        plugloadData.RelayFailOff = Convert.ToInt32(GetCUValue(output + '\r', "Off:"));

                        plugloadData.CUver = GetCUValue(outmsg, CUverText);
                        plugloadData.CUstatus = GetCUValue(outmsg, CUstatusText);
                        plugloadData.CUrelay = GetCUValue(outmsg, CUrelayText).Contains("closed") ? 1:0;
                        plugloadData.CUtemp = Convert.ToDouble(GetCUValue(outmsg, CUtempText)) / 10;
                        plugloadData.RMSvoltage = Convert.ToDouble(GetCUValue(outmsg, RMSvoltageText)) / 10;
                        plugloadData.RMSvoltage2 = Convert.ToDouble(GetCUValue(outmsg, RMSvoltage2Text)) / 10;
                        plugloadData.Linefreq = Convert.ToDouble(GetCUValue(outmsg, LinefreqText)) / 100;
                        plugloadData.RMScurrent = Convert.ToDouble(GetCUValue(outmsg, RMScurrentText)) / 1000;
                        plugloadData.RMScurrent2 = Convert.ToDouble(GetCUValue(outmsg, RMScurrent2Text)) / 1000;
                        plugloadData.Energysincestart = Convert.ToDouble(GetCUValue(outmsg, EnergysincestartText));
                        plugloadData.Energy2sincestart = Convert.ToDouble(GetCUValue(outmsg, Energy2sincestartText));
                        plugloadData.Energysincelastread = Convert.ToDouble(GetCUValue(outmsg, EnergysincelastreadText));
                        plugloadData.Energy2sincelastread = Convert.ToDouble(GetCUValue(outmsg, Energy2sincelastreadText));
                        plugloadData.Power = Convert.ToDouble(GetCUValue(outmsg, PowerText)) / 10;
                        plugloadData.Power2 = Convert.ToDouble(GetCUValue(outmsg, Power2Text)) / 10;
                        plugloadData.Apparentpwr = Convert.ToDouble(GetCUValue(outmsg, ApparentpwrText)) / 10;
                        plugloadData.Apparentpwr2 = Convert.ToDouble(GetCUValue(outmsg, Apparentpwr2Text)) / 10;
                        plugloadData.Reactivepwr = Convert.ToDouble(GetCUValue(outmsg, ReactivepwrText)) / 10;
                        plugloadData.Reactivepwr2 = Convert.ToDouble(GetCUValue(outmsg, Reactivepwr2Text)) / 10;
                        plugloadData.Pwrfactor = Convert.ToDouble(GetCUValue(outmsg, PwrfactorText)) / 1000;
                        plugloadData.Pwrfactor2 = Convert.ToDouble(GetCUValue(outmsg, Pwrfactor2Text)) / 1000;
                        plugloadData.Phaseangle = Convert.ToDouble(GetCUValue(outmsg, PhaseangleText)) / 1000;
                        plugloadData.Phaseangle2 = Convert.ToDouble(GetCUValue(outmsg, Phaseangle2Text)) / 1000;

                        output = GetCUValue(outmsg, ErrcntText);   // this returns '0, at=0'
                        plugloadData.CUStatsErrCnt = Convert.ToInt32(output.Substring(0, output.IndexOf(',')));
                        plugloadData.CUStatsErrCntat = Convert.ToInt32(GetCUValue(output + '\r', "at="));

                        plugloadData.CUStatsTx = Convert.ToInt32(GetCUValue(outmsg, TxText));
                        plugloadData.CUStatsRx = Convert.ToInt32(GetCUValue(outmsg, RxText));
                        plugloadData.CUStatsRx0 = Convert.ToInt32(GetCUValue(outmsg, Rx0Text));
                        plugloadData.CUStatsRx1 = Convert.ToInt32(GetCUValue(outmsg, Rx1Text));
                        plugloadData.CUStatsRx2 = Convert.ToInt32(GetCUValue(outmsg, Rx2Text));
                        plugloadData.CUStatsRx3 = Convert.ToInt32(GetCUValue(outmsg, Rx3Text));
                        plugloadData.CUStatsRx4 = Convert.ToInt32(GetCUValue(outmsg, Rx4Text));
                        plugloadData.CUStatsRx5 = Convert.ToInt32(GetCUValue(outmsg, Rx5Text));
                        plugloadData.CUStatsRx6 = Convert.ToInt32(GetCUValue(outmsg, Rx6Text));
                        plugloadData.CUStatsRx7 = Convert.ToInt32(GetCUValue(outmsg, Rx7Text));
                        plugloadData.CUStatsRxSerialNAKError = Convert.ToInt32(GetCUValue(outmsg, RxSerialNAKErrorText));
                        plugloadData.CUStatsRxSerialTimeout = Convert.ToInt32(GetCUValue(outmsg, RxSerialTimeoutText));
                        plugloadData.CUStatsRxBADCRC = Convert.ToInt32(GetCUValue(outmsg, RxBADCRCText));

                        output = GetCUValue(outmsg, RelayTotalText);      // returns '0, Off:0'
                        plugloadData.RelayTotalOn = Convert.ToInt32(output.Substring(0, output.IndexOf(',')));
                        plugloadData.RelayTotalOff = Convert.ToInt32(GetCUValue(output + '\r', "Off:"));
                        output = GetCUValue(outmsg, RelaySuccessText);      // returns '0, Off:0'
                        plugloadData.RelaySuccessOn = Convert.ToInt32(output.Substring(0, output.IndexOf(',')));
                        plugloadData.RelaySuccessOff = Convert.ToInt32(GetCUValue(output + '\r', "Off:"));

                    }

                }

            }

            return status;
        }

        /// <summary>
        /// will get the value after the tag and before the \r
        /// </summary>
        /// <param name="fullstring"></param>
        /// <param name="tag"></param>
        /// <returns></returns>
        string GetCUValue(string fullstring, string tag)
        {
            string value;
            char[] terms = {'\n', '\r'};

            value = string.Empty;

            if (fullstring.Contains(tag))
            {
                value =  fullstring.Substring(fullstring.IndexOf(tag) + tag.Length,
                    fullstring.IndexOfAny(terms, fullstring.IndexOf(tag)) - fullstring.IndexOf(tag) - tag.Length);
            }
            return value;
        }

        enum SwitchedOutputRelay { Open, Closed };

        /// <summary>
        /// Set the Switched ouput relay. Use the enum SwitchedOutputRelay.
        /// </summary>
        /// <param name="state"></param>
        /// <param name="resultmsg"></param>
        /// <returns></returns>
        int SetSwitchedOuputRelay(SwitchedOutputRelay state, ref string resultmsg)
        {
            int status = (int)ErrorCodes.Program.Success;
            bool bstatus;
            string output;

            if (state == SwitchedOutputRelay.Closed)
            {
                bstatus = sensorConsole.WriteLine("p relay 1", 1000);
                output = "1";
            }
            else
            {
                bstatus = sensorConsole.WriteLine("p relay 0", 1000);
                output = "0";
            }

            if (!bstatus)             // turn on switched output
            {
                status = (int)ErrorCodes.Program.DUTComError;
                resultmsg = "Error sending 'p relay " + output + "' command.";
            }
            else if (sensorConsole.WaitForPrompt(1000, uutData.dvtmanPrompt, out resultmsg))   // wait for prompt
            {
                Thread.Sleep(1000);          // let relay settle
            }
            else                            // did not get prompt
            {
                status = (int)ErrorCodes.Program.DUTComError;
                resultmsg = "Error waiting 'p relay " + output + "' '" + uutData.dvtmanPrompt + "' return.";
                Trace.WriteLine(resultmsg);
            }
            return status;

        }

        //--------------------------------------//
        //--------------------------------------//
        //                                      //
        //      Value checking routines         //
        //--------------------------------------//
        //--------------------------------------//

        //-------------------------------------------------
        /// <summary>
        /// This function will check a value as defined in a List type parameters.
        /// </summary>
        /// <param name="testvalue"></param>
        /// <param name="item"></param>
        /// <returns>bool - true = meets the conditions</returns>
        private static bool CheckResultValue(double testvalue, LimitsDatabase.LimitsData.limitsItem item, ref string resultmessage)
        {
            bool status = true;

            resultmessage = string.Empty;
            switch (item.type.ToUpper())
            {
                case "RANGE":
                    switch (item.operation.ToUpper())
                    {
                        case "GTLT":
                            if ((testvalue <= Convert.ToDouble(item.value1)) || (testvalue >= Convert.ToDouble(item.value2)))
                                status = false;
                            resultmessage = string.Format("{0}   Expected: {1} < x < {2}  Found: {3:N2}\n", item.name, item.value1, item.value2, testvalue);
                            break;
                        case "GELE":
                            if ((testvalue < Convert.ToDouble(item.value1)) || (testvalue > Convert.ToDouble(item.value2)))
                                status = false;
                            resultmessage = string.Format("{0}   Expected: {1} <= x <= {2}  Found: {3:N2}\n", item.name, item.value1, item.value2, testvalue);
                            break;
                        default:
                            throw new Exception("Invalid operation: " + item.name.ToString() + ":" + item.operation.ToString());
                    }
                    break;

                case "VALUE":
                    switch (item.operation.ToUpper())
                    {
                        case "GE":      // greater than or equal
                            if (!(testvalue >= Convert.ToDouble(item.value1)))
                                status = false;
                            resultmessage = string.Format("{0}   Expected: x >= {1}  Found: {2:N2}\n", item.name, item.value1, testvalue);
                            break;

                        case "EQ":      // equal
                            if (!(testvalue == Convert.ToDouble(item.value1)))
                                status = false;
                            resultmessage = string.Format("{0}   Expected: x = {1}  Found: {2:N2}\n", item.name, item.value1, testvalue);
                            break;

                        case "LE":      // less than or equal
                            if (!(testvalue <= Convert.ToDouble(item.value1)))
                                status = false;
                            resultmessage = string.Format("{0}   Expected: x <= {1}  Found: {2:N2}\n", item.name, item.value1, testvalue);
                            break;

                        case "LT":      // less than
                            if (!(testvalue < Convert.ToDouble(item.value1)))
                                status = false;
                            resultmessage = string.Format("{0}   Expected: x < {1}  Found: {2:N2}\n", item.name, item.value1, testvalue);
                            break;

                        default:
                            throw new Exception("Invalid operation: " + item.name.ToString() + ":" + item.operation.ToString());
                    }
                    break;

                default:
                    throw new Exception("Invalid type: " + item.name.ToString() + ":" + item.operation.ToString());
            }

            return status;
        }

    }
}
