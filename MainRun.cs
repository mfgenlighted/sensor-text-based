﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Reflection;
using System.Drawing;
using System.IO;
using System.Diagnostics;
using System.Resources;
using System.Text.RegularExpressions;

using Seagull.BarTender.Print;

using LimitsDatabase;
//using LimitFileFunctions;
using FixtureDatabase;
using fakepfs2;
using PFS_System;
//using ShopFloorSystem;

namespace SensorManufSY2
{
    public partial class MainWindow : Form
    {
        // Test Limits
        public struct uutdata
        {
            public string pcbaSN;
            public string pcbaPN;
            public string mac;
            public bool printMac; 
            public string topSN;
            public string topPN;
            public string model;
            public bool s1Set;
            public bool s2Set;
            public string dvtmanPrompt;
            public string appPrompt;
            public int testChannel;
            public string pfsAssemblyNumber;
        }

        public struct codeClasses
        {
            public CUConsole cuConsole;
            public SensorConsole sensorConsole;
            public ACMeterCommications powermeterConsole;
            public FixtureFunctions fixtureFunctions;
            public StationConfigure stationConfigure;
            public ResultData testDB;
        }

        string lastPCBABarcodeFileName = string.Empty;       // barcode file name of last label printed
        string lastProductBarcodeFileName = string.Empty;   // barcode file name of last label printed
        string lastProductLabelPrinter = string.Empty;      // printer used for last label printed
        string lastPCBALabelPrinter = string.Empty;         // printer used for last label printed
        public codeClasses testcodeClasses;

        public void MainRun()
        {
            bool newPcbaManfData = true;
            bool newHlaManfData = true;
            int debugLoopCount = 0;                     // if in debug mode and DebugOptions has loop=<nonzero>, count of loops done
            int maxDebugLoopCount = 0;                  // value of DebugOptions loop

            uutdata uutData = new uutdata();            // structure to save the data to configure the DUT with
            ProductDatabase.Product_data productCodeData = new ProductDatabase.Product_data();

            LimitsData.product_codesItem testsToRun = new LimitsData.product_codesItem();       // All the test steps and globals
            LimitsData.stepsItem testparams = new LimitsData.stepsItem();                       // single test step
            LimitsData testLimits = new LimitsData(stationConfig.SQLServer, stationConfig.SQLTestLimitDataName,
                                    stationConfig.SQLOperatorName, stationConfig.SQLOperatorPassword, stationConfig.SQLTestLimitDataName);

            string outmsg = string.Empty;
            string scrstring = string.Empty;

            int stepStatus = (int)ErrorCodes.Program.Success;   // status for a step
            int firstFailCode = -1;                    // storage to track the first and last error
            string firstFailMessage = string.Empty;
            string firstFailTestName = string.Empty;
            int lastFailCode = -1;
            string lastFailMessage = string.Empty;
            string lastFailTestName = string.Empty;
            string lastFailedStepNumber = string.Empty;
            int lastFailedTestNumber = 0;
            bool testStatus = false;                    // status of over test. set to false in cause some of the setup fails
            DateTime stepStartTime;
            TimeSpan stepTime;
            DateTime testStartTime;
            TimeSpan testTime;

            string dumpFileBaseName = string.Empty;

            testcodeClasses.cuConsole = null;
            testcodeClasses.fixtureFunctions = null;
            testcodeClasses.powermeterConsole = null;
            testcodeClasses.sensorConsole = null;
            testcodeClasses.stationConfigure = null;
            testcodeClasses.testDB = null;

            //addTextToStatusWindow("Starting Station Initialize\n");
            addTextToStatusWindow(Properties.Resources.StationInitializeError + "\n");

            abortButtonPushed = false;

            if ((productCodeToTest == string.Empty) | (productCodeToTest.Contains("unkno")))
            {
                MessageBox.Show(Properties.Resources.NoProductConfigured);
                //MessageBox.Show(Properties.Resources.NoProductConfigured);
                //addTextToStatusWindow("Starting Station Initialize\n");
                addTextToStatusWindow(Properties.Resources.StationInitializeError + "\n");
                return;
            }

            // figure out if configured product is in the product list
            productCodeToTest = productCodeToTest.Substring(0, 5);      // get product code from the SN
            if (!ProductInfoList.Exists(x => x.product_code == productCodeToTest))
            {
                outmsg = "Product code (" + productCodeToTest + ") does not exist in database.";
                MessageBox.Show(outmsg);
                addTextToStatusWindow(outmsg, false);
                return;
            }
            // data for product configured
            ProductInfo = ProductInfoList.Find(x => x.product_code == productCodeToTest);

            // see if a program version is defined
            if (StationInfo.allowed_testprogram_version != string.Empty)
            {
                if (StationInfo.allowed_testprogram_version.ToUpper()[0] == 'R')
                {
                    if (!Regex.IsMatch(Assembly.GetExecutingAssembly().GetName().Version.ToString(), StationInfo.allowed_testprogram_version.Substring(1)))
                    {
                        if (stationConfig.ConfigDebug)
                        {
                            if (MessageBox.Show("Failed program version check. Do you want to ignore it?", "Version check", MessageBoxButtons.YesNo) == DialogResult.No)
                            {
                                // Running the wrong version of the program. Must be
                                MessageBox.Show(Properties.Resources.WrongPrgVersion + StationInfo.allowed_testprogram_version);
                                addTextToStatusWindow(Properties.Resources.WrongPrgVersion + StationInfo.allowed_testprogram_version + "\n");
                                return;
                            }
                        }
                        else
                        {
                            // Running the wrong version of the program. Must be
                            MessageBox.Show(Properties.Resources.WrongPrgVersion + StationInfo.allowed_testprogram_version);
                            addTextToStatusWindow(Properties.Resources.WrongPrgVersion + StationInfo.allowed_testprogram_version + "\n");
                            return;
                        }
                    }
                }
                else
                {
                    string[] expected = StationInfo.allowed_testprogram_version.Split('.');
                    string[] read = Assembly.GetExecutingAssembly().GetName().Version.ToString().Split('.');
                    if (!expected[0].Equals(read[0]) | !expected[1].Equals(read[1]))
                    {
                        if (stationConfig.ConfigDebug)
                        {
                            if (MessageBox.Show("Failed program version check. Do you want to ignore it?", "Version check", MessageBoxButtons.YesNo) == DialogResult.No)
                            {
                                // Running the wrong version of the program. Must be
                                MessageBox.Show(Properties.Resources.WrongPrgVersion + expected[0] + "." + expected[1] + "." + expected[2]);
                                addTextToStatusWindow(Properties.Resources.WrongPrgVersion + expected[0] + "." + expected[1] + "." + expected[2] + "\n");
                                return;
                            }
                        }
                        else
                        {
                            // Running the wrong version of the program. Must be
                            MessageBox.Show(Properties.Resources.WrongPrgVersion + expected[0] + "." + expected[1] + "." + expected[2]);
                            addTextToStatusWindow(Properties.Resources.WrongPrgVersion + expected[0] + "." + expected[1] + "." + expected[2] + "\n");
                            return;
                        }
                    }
                    if (expected[2].ToUpper() != "X")           // if the last part is defined
                    {
                        if (!expected[2].Equals(read[2]))
                        {
                            if (stationConfig.ConfigDebug)
                            {
                                if (MessageBox.Show("Failed program version check. Do you want to ignore it?", "Version check", MessageBoxButtons.YesNo) == DialogResult.No)
                                {
                                    // Running the wrong version of the program. Must be
                                    MessageBox.Show(Properties.Resources.WrongPrgVersion + expected[0] + "." + expected[1] + "." + expected[2]);
                                    addTextToStatusWindow(Properties.Resources.WrongPrgVersion + expected[0] + "." + expected[1] + "." + expected[2] + "\n");
                                    return;
                                }
                            }
                            else
                            {
                                // Running the wrong version of the program. Must be
                                MessageBox.Show(Properties.Resources.WrongPrgVersion + expected[0] + "." + expected[1] + "." + expected[2]);
                                addTextToStatusWindow(Properties.Resources.WrongPrgVersion + expected[0] + "." + expected[1] + "." + expected[2] + "\n");
                                return;
                            }
                        }
                    }
                }
            }

            //--------------------------------------------------------------------------//
            // read the test limit file                                                 //
            // the fixture limit directory will override the station limit directory.   //
            //--------------------------------------------------------------------------//
            // NOTE: the limitfile name is the limit version to use. productCode is the the productcode + version
            if (!testLimits.GetData(ProductInfo.product_code, ProductInfo.limit_version, StationInfo.function, ref testsToRun, ref outmsg))
            {
                addTextToStatusWindow(Properties.Resources.StationInitializeError + "\n" + outmsg + "\n", false);
                return;
            }

            // see if any tests were found
            if (testsToRun.steps.Count == 0)
            {
                MessageBox.Show("No tests were found for " + ProductInfo.product_code + " version " + ProductInfo.limit_version + ".");
                //addTextToStatusWindow("Starting Station Initialize\n");
                addTextToStatusWindow(Properties.Resources.StationInitializeError + "\n");
                return;
            }

            uutData.dvtmanPrompt = testsToRun.GetGlobalValue("DVTMANPrompt");
            if (uutData.dvtmanPrompt == string.Empty)
                uutData.dvtmanPrompt = "DVTMAN> ";
            uutData.appPrompt = testsToRun.GetGlobalValue("APPPrompt");
            if (uutData.appPrompt == string.Empty)
                uutData.appPrompt = "SU> ";

            if ((stationConfig.ConfigTestRadioChannel == string.Empty) || (stationConfig.ConfigTestRadioChannel == ""))
                uutData.testChannel = 15;
            else
                uutData.testChannel = Convert.ToInt32(stationConfig.ConfigTestRadioChannel);
            addLimitFileNameScreen(ProductInfo.product_code);
            addLimitFileVersionScreen(ProductInfo.limit_version);
            addUnitsRunCounter(unitsRun, unitsPassed, unitsFailed);

            if (stationConfig.ConfigDebugOptions.ToUpper().Contains("LOOP="))   // if in auto loop
            {
                if (!int.TryParse(stationConfig.ConfigDebugOptions.Substring(stationConfig.ConfigDebugOptions.ToUpper().IndexOf("LOOP=") + 5), out maxDebugLoopCount))
                {
                    MessageBox.Show("Error in the debug option Loop. Value not a interger.");
                    return;
                }
            }

            using (ResultData testDB = new ResultData())                                // using for Result database
            //using (SensorConsole sensor = new SensorConsole())          // serial console for dut
            //using (CUConsole cuConsole = new CUConsole())                               // CU serial
            using (ACMeterCommications powerMeterConsole = new ACMeterCommications())   // power meter
            using (FixtureFunctions fixture = new FixtureFunctions())                   // labjack, switch-mate, ref radio
            {
                // Init the fixture
                fixture.StationConfigure = stationConfig;
                fixture.StatusUpdated += new EventHandler<StatusUpdatedEventArgs>(sc_StatusUpdated);
                if (!fixture.InitFixture())
                {
                    MessageBox.Show("Error initializing fixture: " + fixture.LastFunctionErrorMessage);
                    //addTextToStatusWindow("Starting Station Initialize\n");
                    addTextToStatusWindow(Properties.Resources.StationInitializeError + "\n");
                    return;
                }
                fixture.ControlDUTPower(0);                 // make sure dut power is off

                //--------------------------//
                // wait for fixture to open //
                //--------------------------//
                if (fixture.IsCoverClosed() & stationConfig.FixtureEquipment.hasLidSwitch)
                { 
                    //addPromptToStatusWindow("Remove any unit in the test fixture.\n");
                    addPromptToStatusWindow(Properties.Resources.RemoveProduct + "\n");
                    if (stationConfig.ConfigDebug)
                    {
                        if (MessageBox.Show("Bypass lid message?", "Debug mode", MessageBoxButtons.YesNo) == DialogResult.Yes)
                        {
                            MessageBox.Show(Properties.Resources.RemoveProduct);        // "Remove any unit in the test fixture."
                        }
                        else
                        {
                            // wait for fixture to open or the abort button is pushed
                            while (fixture.IsCoverClosed() && !abortButtonPushed) { };
                        }
                    }
                    else
                    {
                        // wait for fixture to open or the abort button is pushed
                        while (fixture.IsCoverClosed() && !abortButtonPushed) { };
                    }
                }


                SensorConsole sensor = new SensorConsole();
                if (stationConfig.FixtureEquipment.hasConsolePort)
                {
                    sensor.DisplayRcvChar = stationConfig.ConfigDisplayRcvChar;
                    sensor.DisplaySendChar = stationConfig.ConfigDisplaySendChar;
                    sensor.DVTMANPrompt = uutData.dvtmanPrompt;
                    sensor.Open(stationConfig.ConfigDUTComPort);
                    sensor.StatusUpdated += new EventHandler<StatusUpdatedEventArgs>(sc_StatusUpdated);
                    addTextToStatusWindow("   Sensor Port Open\n");
                }


                if (stationConfig.FixtureEquipment.hasPowerMeter)
                {
                    powerMeterConsole.Open(stationConfig.ConfigPowerMeterComPort);
                    addTextToStatusWindow("   Power Meter Port Open\n");

                }

                // this is common stuff that is use by: ercTests
                testcodeClasses.testDB = testDB;
                testcodeClasses.sensorConsole = sensor;
                testcodeClasses.powermeterConsole = powerMeterConsole;
                testcodeClasses.fixtureFunctions = fixture;
                testcodeClasses.stationConfigure = stationConfig;


                //----------------------------------------------------------//
                // --- main loop. start at top with waiting for a new unit  //
                //----------------------------------------------------------//
                while (!abortButtonPushed)
                {
                    // reset the first and last errors
                    firstFailCode = (int)ErrorCodes.Program.Success;
                    firstFailMessage = string.Empty;
                    lastFailCode = (int)ErrorCodes.Program.Success;
                    lastFailMessage = string.Empty;
                    lastFailedStepNumber = string.Empty;
                    stepStatus = (int)ErrorCodes.Program.Success;

                    // check to make sure console port is still open
                    if (stationConfig.FixtureEquipment.hasConsolePort)
                    {
                        if (!sensor.IsOpen)
                            sensor.Open(stationConfig.ConfigDUTComPort);
                    }

                    int limitIndex = 0;
                    string result1 = string.Empty;
                    string result2 = string.Empty;
                    string resultmessage = string.Empty;
                    testStatus = true;


                    SensorDVTMANTests sensorDVTMANTests = new SensorDVTMANTests();
                    sensorDVTMANTests.sensorConsole = sensor;
                    //sensorDVTMANTests.cuConsole = cuConsole;
                    sensorDVTMANTests.fixture = fixture;
                    sensorDVTMANTests.stationConfig = stationConfig;

                    sensorDVTMANTests.StatusUpdated += new EventHandler<StatusUpdatedEventArgs>(sc_StatusUpdated);
                    JunctionBox plugLoadTests = new JunctionBox(sensor, fixture, stationConfig, powerMeterConsole, uutData);
                    plugLoadTests.StatusUpdated += new EventHandler<StatusUpdatedEventArgs>(sc_StatusUpdated);
                    ERC_Tests ercTests = new ERC_Tests(testcodeClasses);
                    ercTests.StatusUpdated += new EventHandler<StatusUpdatedEventArgs>(sc_StatusUpdated);
                    BluetoothFunctions bleTests = new BluetoothFunctions(stationConfig.ConfigBLEComPort, stationConfig.ConfigTagComPort, sensor);
                    bleTests.StatusUpdated += new EventHandler<StatusUpdatedEventArgs>(sc_StatusUpdated);
                    bleTests.tagMac = fixture.TagRadioMac;                  // this was read and saved from the tag radio when Fixture.InitFixture was run. 
                    SensorAPPTests apptests = new SensorAPPTests();
                    apptests.stationConfig = stationConfig;
                    apptests.sensorConsole = sensor;
                    testStartTime = DateTime.Now;
                    Debug.WriteLine("Test Start: " + uutData.pcbaSN + " at " + DateTime.Now);
                    for (limitIndex = 0; limitIndex < testsToRun.steps.Count; limitIndex++)
                    {
                        resultmessage = string.Empty;
                        stepStartTime = DateTime.Now;                   // save step start time
                        LimitsData.stepsItem testToRun = new LimitsData.stepsItem();       // holder for a single step
                        testToRun = testsToRun.steps[limitIndex];                           // get the single step
                        addTestHeaderToScreen(limitIndex + 1, testToRun.name);
                        //if (testToRun.DLLFileName != string.Empty)      // if this is calling a dll
                        //{
                        //    try
                        //    {
                        //        Assembly asm = Assembly.Load(testToRun.DLLFileName);
                        //        Type t = asm.GetType(testToRun.DLLFileName + "." + testToRun.DLLClassName);
                        //        MethodInfo m = t.GetMethod(testToRun.Function);
                        //        m.Invoke(null, new object[] { testToRun, testDB, uutData, resultmessage });
                        //    }
                        //    catch (Exception ex)
                        //    {

                        //        MessageBox.Show(ex.Message);
                        //    }
                        //}
                        if (testToRun.step_operation.ToUpper() == "SKIP")
                        {
                            resultmessage = "Test configured to SKIP";
                            addTextToResultsWindow(resultmessage, true);
                            addTextToStatusWindow(resultmessage);
                        }
                        else
                        {
                            switch (testToRun.function_call.ToUpper())
                            {
                                // Get the PCBA SN from the operator
                                //  This must be the first one called
                                case "GETSN":
                                    string scanType = string.Empty;

                                    if (stationConfig.ConfigDebug)
                                    {
                                        if (maxDebugLoopCount == 0)         // if not running in auto loop mode
                                        {
                                            var yn = MessageBox.Show("Warning: This system running in DEBUG mode.", "DEBUG Mode", MessageBoxButtons.OKCancel);
                                            if (yn == DialogResult.Cancel)
                                            {
                                                stepStatus = (int)ErrorCodes.System.OperatorCancel;
                                                resultmessage = "Operator cancel. In debug mode.";
                                                break;
                                            }
                                        }
                                    }

                                    scanType = testToRun.GetParameterValue("SNScanType");
                                    if (debugLoopCount == 0)            // first loop of debug loop has to get serial data. it is only
                                                                        //  incremented if maxDebugLoopCount != 0, which is set if DebugOptions
                                                                        //  has loop=x
                                    {
                                        if (scanType.ToUpper() == "HLASN")
                                            stepStatus = GetHLASN(fixture, productCodeData, ref uutData, ref resultmessage);
                                        else
                                            stepStatus = GetPCBASN(fixture, ProductInfo, ref uutData, ref resultmessage);
                                    }
                                    if (stepStatus == (int)ErrorCodes.Program.Success)
                                    {
                                        // setup console dump if configed
                                        if (stationConfig.ConfigEnableConsoleDump)
                                        {
                                            dumpFileBaseName = uutData.pcbaSN + "_" + DateTime.UtcNow.ToString("o").Replace(':', '_') + ".txt";
                                            //cuConsole.SetDumpFileName(stationConfig.ConfigReportDirectory, dumpFileBaseName);
                                            sensor.SetDumpFileName(stationConfig.ConfigReportDirectory, dumpFileBaseName);
                                            statusWindowFileDump.SetDumpFileName(stationConfig.ConfigReportDirectory, dumpFileBaseName);
                                        }

                                        // set up the result recording
                                        if (stationConfig.SQLResultsDatabaseName == string.Empty)       // not defined in station config
                                        {
                                            if (stationConfig.ConfigDebug)
                                                testDB.DBInfoDatabase = "debug_production_results";
                                            else
                                                testDB.DBInfoDatabase = "production_results";
                                        }
                                        else
                                        {
                                            if (stationConfig.ConfigDebug)
                                                testDB.DBInfoDatabase = "debug_" + stationConfig.SQLResultsDatabaseName;
                                            else
                                                testDB.DBInfoDatabase = stationConfig.SQLResultsDatabaseName;
                                        }
                                        addResultDBNameScreen(testDB.DBInfoDatabase);

                                        testDB.DBInfoServer = stationConfig.SQLServer;
                                        testDB.DBInfoUser = stationConfig.SQLOperatorName;
                                        testDB.DBInfoPassword = stationConfig.SQLOperatorPassword;
                                        testDB.DBInfoRecordOption = (ResultData.DBRecordType)stationConfig.SQLRecordOption;
                                        testDB.StationName = stationConfig.ConfigStationName + ":" + stationConfig.ConfigFixtureName;
                                        testDB.TestOperator = operatorName;
                                        testDB.TextFileInfoDirectory = stationConfig.ConfigReportDirectory;
                                        testDB.RecordTextFile = stationConfig.ConfigRecordTextFile;
                                        testDB.TestOperation = stationConfig.ConfigStationOperation;
                                        try
                                        {
                                            string workorder = string.Empty;
                                            if (stationConfig.ConfigEnablePFS)
                                                workorder = ProductInfo.pfs_assembly_number;
                                            //if (stationConfig.ConfigEnableSFS)
                                            //    workorder = sfSystem.WorkOrderNumber;
                                            testDB.CreateTestRecord(uutData.pcbaSN, stationConfig.ConfigStationName, ProductInfo.limit_version, ProductInfo.product_code,
                                                                        Assembly.GetExecutingAssembly().GetName().Version.ToString(), workorder, ProductInfo.product_code);
                                        }
                                        catch (Exception ex)
                                        {
                                            resultmessage = "Error opeing results database: " + ex.Message;
                                            stepStatus = (int)ErrorCodes.System.ResultDBFailure;
                                            break;
                                        }
                                        testDB.CreateStepRecord("GetSN", testToRun.step_number);
                                        testDB.AddStepRef("ProductConfig", stationConfig.ConfigStationToUse, "", "", "");     // record the product config used
                                        testDB.AddStepRef("ProductCode", productCodeToTest, "", "", "");       // record the product config code used

                                        addHeaderResultsWindow("Result for:\n " + uutData.pcbaSN + "/" + uutData.topSN + "/" + uutData.mac + "\n");
                                        if (stationConfig.FixtureEquipment.hasLidSwitch)
                                        {
                                            //addPromptToStatusWindow("Insert the DUT in the test fixture and close the cover to start the test.\n");
                                            addPromptToStatusWindow(Properties.Resources.InsertProduct + "\n");
                                            while (!fixture.IsCoverClosed()) { };
                                        }
                                        else
                                        {
                                            MessageBox.Show(Properties.Resources.InsertProduct);
                                        }
                                        addTextToStatusWindow("Setting up station....\n");
                                        addTextToStatusWindow("   Result Record created for " + uutData.pcbaSN + "\n");
                                        Thread.Sleep(1000);     // give time for power to stablize
                                    }
                                    else
                                    {
                                        if (stepStatus < (int)ErrorCodes.System.SystemErrorCodes)
                                            stepStatus = (testToRun.step_number * 100) + stepStatus;
                                    }
                                    testStartTime = DateTime.Now;
                                    break;

                                case "TURNONDUT":
                                    testToRun.step_number = (testToRun.step_number == 0) ? 20 : testToRun.step_number;
                                    stepStatus = sensorDVTMANTests.TurnOnDut(testToRun, testDB, uutData, ref resultmessage);
                                    break;

                                //----------- CheckFirmware -------------//
                                case "CHECKFIRMWARE":
                                    testToRun.step_number = (testToRun.step_number == 0) ? 3 : testToRun.step_number;
                                    stepStatus = sensorDVTMANTests.SUCheckFirmware(testToRun, testDB, ref resultmessage);
                                    break;

                                //------------ CheckBLEFirmware ------------//
                                case "CHECKBLEFIRMWARE":
                                    testToRun.step_number = (testToRun.step_number == 0) ? 36 : testToRun.step_number;
                                    stepStatus = sensorDVTMANTests.SUCheckBLEFirmware(testToRun, testDB, ref resultmessage);
                                    break;

                                //------------ SUBatteryTest ------------//
                                case "SUBATTERYTEST":
                                    testToRun.step_number = (testToRun.step_number == 0) ? 1 : testToRun.step_number;
                                    stepStatus = sensorDVTMANTests.SUBatteryTest(testToRun, testDB, ref resultmessage);
                                    break;

                                //------------- CheckControlVoltage -----------//
                                case "CHECKCONTROLOUTPUT":
                                    testToRun.step_number = (testToRun.step_number == 0) ? 6 : testToRun.step_number;
                                    stepStatus = sensorDVTMANTests.CheckControlOutput(testToRun, testDB, uutData, ref resultmessage);
                                    break;

                                //---------- RadioTest ----------//
                                case "RADIOTEST":
                                    testToRun.step_number = (testToRun.step_number == 0) ? 5 : testToRun.step_number;
                                    stepStatus = sensorDVTMANTests.TestNetworks(testToRun, testDB, uutData, ref resultmessage);
                                    break;

                                //-------------- CheckCPUTemperature -----------//
                                //case "CHECKCPUTEMPERATURE":
                                //    stepStatus = fixture.CheckCPUTemperature(testToRun, testDB, ref resultmessage);
                                //    break;

                                //-------------- CalibrateCPUTemperature --------------
                                case "CALIBRATECPUTEMPERATURE":
                                    testToRun.step_number = (testToRun.step_number == 0) ? 10 : testToRun.step_number;
                                    stepStatus = sensorDVTMANTests.CalibrateCPUTemperature(testToRun, testDB, uutData, ref resultmessage);
                                    break;

                                //------------------ Check CU port ----------------
                                case "CUPORTTEST":
                                    testToRun.step_number = (testToRun.step_number == 0) ? 9 : testToRun.step_number;
                                    if (stationConfig.ConfigCUType.ToUpper().Equals("DALI"))
                                        stepStatus = sensorDVTMANTests.DaliPortTest(testToRun, stationConfig, testDB, uutData, out resultmessage);
                                    else
                                        stepStatus = sensorDVTMANTests.CUPortTest(testToRun, testDB, uutData, dumpFileBaseName, out resultmessage);
                                    break;

                                //    //------------------ Chec AMB Sensor ---------------
                                //    case "CHECKAMBSENSOR":
                                //        stepStatus = fixture.CheckAMBSensor(testToRun, testDB, ref resultmessage);
                                //        break;

                                //    //------------------ Chec PIR Sensor ---------------
                                //    case "CHECKPIRSENSOR":
                                //        stepStatus = fixture.CheckPIRSensor(testToRun, testDB, ref resultmessage);
                                //        break;

                                //------------------Check PIR, AMB, and Temp sensors
                                case "CHECKSENSORS":
                                    addTextToStatusWindow("ambient source = " + stationConfig.ConfigAmbientDetectorLightSource.Type);
                                    if (stationConfig.ConfigAmbientDetectorLightSource.ToString().ToUpper() == "REDLED")
                                        addTextToStatusWindow(" voltage" + stationConfig.ConfigAmbientDetectorLightSource.Voltage.ToString());
                                    addTextToStatusWindow("\n");
                                    testToRun.step_number = (testToRun.step_number == 0) ? 18 : testToRun.step_number;
                                    stepStatus = sensorDVTMANTests.AllSensorTest(testToRun, testDB, uutData, ref resultmessage);
                                    break;

                                //------------------- set su to default ---------------
                                case "SETSUTODEFAULT":
                                    testToRun.step_number = (testToRun.step_number == 0) ? 2 : testToRun.step_number;
                                    stepStatus = sensorDVTMANTests.SetSUDefault(testToRun, testDB, uutData.dvtmanPrompt, uutData.dvtmanPrompt, out resultmessage);
                                    break;

                                //------------------- set pcba manufacturing data ---------------
                                case "SETPCBAMANDATA":
                                    testToRun.step_number = (testToRun.step_number == 0) ? 29 : testToRun.step_number;
                                    stepStatus = sensorDVTMANTests.SetPcbaManData(testToRun, testDB, uutData, newPcbaManfData, out resultmessage);
                                    break;

                                //------------------ set HLA manufacturing data ------------------
                                case "SETHLAMANDATA":
                                    testToRun.step_number = (testToRun.step_number == 0) ? 30 : testToRun.step_number;
                                    stepStatus = sensorDVTMANTests.SetHlaManData(testToRun, testDB, uutData, newHlaManfData, out resultmessage);
                                    break;

                                //------------------ set all manufacturing data ------------------
                                case "SETALLMANDATA":
                                    testToRun.step_number = (testToRun.step_number == 0) ? 4 : testToRun.step_number;
                                    stepStatus = sensorDVTMANTests.SetAllManData(testToRun, testDB, uutData, newPcbaManfData, newHlaManfData, out resultmessage);
                                    break;

                                //----------------- Verify Label --------------------------------
                                case "VERIFYLABEL":
                                    testToRun.step_number = (testToRun.step_number == 0) ? 26 : testToRun.step_number;
                                    stepStatus = apptests.VerifyLabel(testToRun, testDB, uutData, out resultmessage);
                                    break;

                                //    //---------------------- check pcba manufacture data -----------
                                //    case "CHECKPCBMANDATA":
                                //        testDB.CreateStepRecord(testToRun.Name, testToRun.step_number);
                                //        bresult = fixture.CheckPcbaManData(uutData.mac, uutData.pcbaPN, uutData.pcbaSN, out resultmessage);
                                //        testDB.AddStepResult(bresult ? "PASS" : "FAIL", reporttext, bresult ? (int)lclerror.Success : (int)lclerror.ValueOutsideOfLimit);
                                //        if (bresult)
                                //            stepStatus = (int)lclerror.Success;
                                //        else
                                //            stepStatus = (int)lclerror.ValueOutsideOfLimit + (testToRun.step_number * 100);
                                //        break;

                                //------------------ POST test ----------------
                                case "CHECKPOST":
                                    testToRun.step_number = (testToRun.step_number == 0) ? 13 : testToRun.step_number;
                                    stepStatus = sensorDVTMANTests.CheckPOST(testToRun, testDB, uutData, out resultmessage);
                                    break;

                                //------------------ POST test ----------------
                                case "CHECKFLASH":
                                    testToRun.step_number = (testToRun.step_number == 0) ? 25 : testToRun.step_number;
                                    stepStatus = sensorDVTMANTests.CheckFlash(testToRun, testDB, uutData, out resultmessage);
                                    break;

                                //------------------ POST test ----------------
                                case "CHECKPROFILE":
                                    testToRun.step_number = (testToRun.step_number == 0) ? 33 : testToRun.step_number;
                                    if (stationConfig.ConfigDebug)        // if in debug mode, give option to not do change over
                                    {
                                        var yn = MessageBox.Show("Running in debug mode. Do you want to do the Profile Check?", "Debug CheckProfile", MessageBoxButtons.YesNo);
                                        if (yn == System.Windows.Forms.DialogResult.Yes)
                                        {
                                            stepStatus = apptests.CheckProfile(testToRun, testDB, uutData.appPrompt, ref resultmessage);
                                        }
                                        else    // in debug mode, but choose to not do CheckProfile. 
                                        {
                                            stepStatus = (int)ErrorCodes.Program.Success;         // make it look like it did the changeover
                                            testDB.CreateStepRecord(testToRun.name, testToRun.step_number);
                                            testDB.AddStepResult("PASS", "In debug mode and did not do the CheckProfile", stepStatus);
                                        }
                                    }
                                    else        // not in debug mode
                                    {
                                        stepStatus = apptests.CheckProfile(testToRun, testDB, uutData.appPrompt, ref resultmessage);
                                    }
                                    break;

                                //------------------ Change Over ----------------
                                case "CHANGEOVER":
                                    testToRun.step_number = (testToRun.step_number == 0) ? 14 : testToRun.step_number;
                                    if (stationConfig.ConfigDebug)        // if in debug mode, give option to not do change over
                                    {
                                        var yn = MessageBox.Show("Running in debug mode. Do you want to do the changeover?", "Debug Changeover", MessageBoxButtons.YesNo);
                                        if (yn == System.Windows.Forms.DialogResult.Yes)
                                        {
                                            stepStatus = sensorDVTMANTests.ChangeOver(testToRun, testDB, uutData, out resultmessage);
                                        }
                                        else    // in debug mode, but choose to not do changeover. 
                                        {
                                            stepStatus = (int)ErrorCodes.Program.Success;         // make it look like it did the changeover
                                            testDB.CreateStepRecord(testToRun.name, testToRun.step_number);
                                            testDB.AddStepResult("PASS", "In debug mode and did not do the changeover", stepStatus);
                                        }
                                    }
                                    else        // not in debug mode
                                    {
                                        stepStatus = sensorDVTMANTests.ChangeOver(testToRun, testDB, uutData, out resultmessage);
                                    }
                                    break;

                                //-------------------Led check ----------------
                                case "CHECKLEDS":
                                    testToRun.step_number = (testToRun.step_number == 0) ? 12 : testToRun.step_number;
                                    stepStatus = sensorDVTMANTests.LEDTest(testToRun, testDB, uutData, stationConfig.ConfigLedDetector.Type, out resultmessage);
                                    break;

                                //-------------------Led check - Manual----------------
                                case "CHECKLEDSMANUAL":
                                    testToRun.step_number = (testToRun.step_number == 0) ? 12 : testToRun.step_number;
                                    stepStatus = sensorDVTMANTests.LEDTestManual(testToRun, testDB, out resultmessage);
                                    break;

                                //-------------------Delay x seconds -----------
                                case "DELAY":
                                    //Thread.Sleep((int)(Convert.ToDouble(testToRun.LowLimit) * 1000));
                                    Thread.Sleep((int)(Convert.ToDouble(testToRun.parameters[0].value)));
                                    stepStatus = (int)ErrorCodes.Program.Success;
                                    break;

                                //------------------ Print a label on the PCBA printer ---------------
                                case "PRINTPCBALABEL":
                                    if (stationConfig.ConfigDebug)
                                    {
                                        var yn = MessageBox.Show("In debug mode. Do you want to print a label?", "Label Print", MessageBoxButtons.YesNo);
                                        if (yn == System.Windows.Forms.DialogResult.No)
                                            break;
                                    }
                                    testToRun.step_number = (testToRun.step_number == 0) ? 16 : testToRun.step_number;
                                    stepStatus = sensorDVTMANTests.PrintPcbaLabel(testToRun, testDB, uutData, stationConfig.ConfigPCBALabelPrinter, out resultmessage, ref lastPCBABarcodeFileName);
                                    lastPCBALabelPrinter = stationConfig.ConfigPCBALabelPrinter;
                                    break;

                                //------------------ Print a label on the Product printer ---------------
                                case "PRINTPRODUCTLABEL":
                                    if (stationConfig.ConfigDebug)
                                    {
                                        var yn = MessageBox.Show("In debug mode. Do you want to print a label?", "Label Print", MessageBoxButtons.YesNo);
                                        if (yn == System.Windows.Forms.DialogResult.No)
                                            break;
                                    }
                                    testToRun.step_number = (testToRun.step_number == 0) ? 17 : testToRun.step_number;
                                    stepStatus = sensorDVTMANTests.PrintProductLabel(testToRun, testDB, uutData, stationConfig.ConfigProductLabelPrinter, out resultmessage, ref lastProductBarcodeFileName);
                                    lastProductLabelPrinter = stationConfig.ConfigProductLabelPrinter;
                                    break;


                                //------------- Junction Box Test functions -----------------
                                //case "VERIFYCUVERSION":
                                //    stepStatus = plugLoadTests.VerifyCUVersion(testToRun, testDB, ref resultmessage);
                                //    break;

                                case "VERIFYSWITCHEDRELAY":
                                    testToRun.step_number = (testToRun.step_number == 0) ? 22 : testToRun.step_number;
                                    stepStatus = plugLoadTests.VerifySwitchedRelay(testToRun, testDB, ref resultmessage);
                                    break;

                                case "VERIFYPOWERREADINGS":
                                    testToRun.step_number = (testToRun.step_number == 0) ? 21 : testToRun.step_number;
                                    stepStatus = plugLoadTests.VerifyReadings(testToRun, testDB, ref resultmessage);
                                    break;


                                //--------------- ERC Test Functions ------------------
                                case "TURNONERCPOWER":
                                    testToRun.step_number = (testToRun.step_number == 0) ? 31 : testToRun.step_number;
                                    stepStatus = ercTests.TurnOnPower(testToRun, uutData, ref resultmessage);
                                    break;

                                case "BUTTONSTEST":
                                    testToRun.step_number = (testToRun.step_number == 0) ? 32 : testToRun.step_number;
                                    stepStatus = ercTests.TestButtons(testToRun, ref resultmessage);
                                    break;

                                case "ERCCHECKBATTERY":
                                    testToRun.step_number = (testToRun.step_number == 0) ? 34 : testToRun.step_number;
                                    stepStatus = ercTests.CheckBatteryTest(testToRun, uutData, ref resultmessage);
                                    break;

                                case "ERCPROGRAMMANDATA":
                                    stepStatus = ercTests.setManData(testToRun, uutData, ref resultmessage);
                                    break;

                                //--------------- Bluetooth tests -------------------
                                case "BLECHANGEOVER":
                                    testToRun.step_number = (testToRun.step_number == 0) ? 23 : testToRun.step_number;
                                    if (stationConfig.ConfigDebug)        // if in debug mode, give option to not do change over
                                    {
                                        var yn = MessageBox.Show("Running in debug mode. Do you want to do the BLE changeover?", "Debug Changeover", MessageBoxButtons.YesNo);
                                        if (yn == System.Windows.Forms.DialogResult.Yes)
                                        {
                                            stepStatus = bleTests.BLETest_DoLoad(testToRun, testDB, ref resultmessage);
                                        }
                                        else    // in debug mode, but choose to not do changeover. 
                                        {
                                            stepStatus = (int)ErrorCodes.Program.Success;         // make it look like it did the changeover
                                            testDB.CreateStepRecord(testToRun.name, testToRun.step_number);
                                            testDB.AddStepResult("PASS", "In debug mode and did not do the changeover", stepStatus);
                                        }
                                    }
                                    else        // not in debug mode
                                    {
                                        stepStatus = bleTests.BLETest_DoLoad(testToRun, testDB, ref resultmessage);
                                    }

                                    break;

                                case "BLERADIOTEST":        // pre 2.9.9 - automatic start
                                    testToRun.step_number = (testToRun.step_number == 0) ? 24 : testToRun.step_number;
                                    stepStatus = bleTests.BLETest_LookForAdvert(1, testToRun, testDB, uutData, ref resultmessage);
                                    break;

                                case "BLERADIOTEST2":       // 2.9.9 and on - start with p ble-mode 2
                                    testToRun.step_number = (testToRun.step_number == 0) ? 24 : testToRun.step_number;
                                    stepStatus = bleTests.BLETest_LookForAdvert(2, testToRun, testDB, uutData, ref resultmessage);
                                    break;

                                case "BLERCVRADIOTEST":
                                    testToRun.step_number = (testToRun.step_number == 0) ? 35 : testToRun.step_number;
                                    stepStatus = bleTests.BLETest_DutLookForAdvert(testToRun, testDB, ref resultmessage);
                                    break;

                                default:
                                    resultmessage = "There is a invalid test defined in file:" + ProductInfo.limit_version + ", Test: " + testToRun.name + " Function: " + testToRun.function_call;
                                    testStatus = false;
                                    stepStatus = (int)ErrorCodes.System.InvalidParameter;
                                    break;
                            }  // end of switch

                        }
                        stepTime = DateTime.Now - stepStartTime;

                        if (stepStatus != (int)ErrorCodes.Program.Success)               // if the step failed
                        {
                            if (stepStatus == (int)ErrorCodes.System.OperatorCancel)     // and canceled
                            {
                                lastFailedTestNumber = testToRun.step_number;
                                limitIndex = testsToRun.steps.Count + 1;      // force to goto end
                                addTextToStatusWindow("Operator canceled.\n");
                                lastFailCode = stepStatus;
                                lastFailMessage = resultmessage;
                                lastFailTestName = testToRun.function_call;
                                lastFailedStepNumber = (limitIndex + 1).ToString();
                            }
                            else {
                                addTestFooterToScreen(stepStatus, testToRun.step_number, resultmessage);
                                testStatus = false;         // then fail the test
                                if (firstFailCode == (int)ErrorCodes.Program.Success)
                                {
                                    firstFailCode = stepStatus;
                                    firstFailMessage = resultmessage;
                                    firstFailTestName = testToRun.name;
                                }
                                lastFailCode = stepStatus;
                                lastFailMessage = resultmessage;
                                lastFailTestName = testToRun.name;
                                lastFailedTestNumber = testToRun.step_number;
                                lastFailedStepNumber = (limitIndex + 1).ToString();
                                // if it is a system error, do not do any more tests
                                if (lastFailCode > (int)ErrorCodes.System.SystemErrorCodes)
                                    limitIndex = testsToRun.steps.Count + 1;
                                // get the FailTestOption option
                                if (!testToRun.do_on_fail.ToUpper().Equals("NEXT"))
                                {
                                    if (testToRun.do_on_fail.ToUpper().Equals("END"))
                                        limitIndex = testsToRun.steps.Count + 1;      // force to goto end
                                    if (testToRun.do_on_fail.ToUpper().Equals("SYSTEMCONFIG"))
                                        if (stationConfig.ConfigNextStepAfterFail.ToUpper().Equals("END"))
                                            limitIndex = testsToRun.steps.Count + 1;
                                }
                            }
                        }
                        else        // passed the step
                        {
                            addTestFooterToScreen(stepStatus, testToRun.step_number, resultmessage);
                            if (testToRun.do_on_pass.ToUpper().Equals("END"))
                                limitIndex = testsToRun.steps.Count + 1;      // force to goto end
                            if (testToRun.do_on_pass.ToUpper().Equals("SYSTEMCONFIG"))
                                if (stationConfig.ConfigNextStepAfterPass.ToUpper().Equals("END"))
                                    limitIndex = testsToRun.steps.Count + 1;
                        }
                        if (stationConfig.ConfigDebug)
                            addTextToStatusWindow("time of last test = " + stepTime.ToString("ss") + "\n");
                    }   // end of foreach loop

                    testTime = DateTime.Now - testStartTime;
                    if (stationConfig.ConfigDebug)
                        addTextToStatusWindow("time of test = " + testTime.ToString(@"mm\:ss") + "\n");

                    fixture.ControlDUTPower(0);     // turn off test light

                    // only log to pfs and shopfloor if it is NOT a system error or Operator cancel(from get SN)
                    if (lastFailCode < (int)ErrorCodes.System.SystemErrorCodes)
                    {
                        unitsRun++;
                        // werite data to shop floor if setup
                        //if (stationConfig.ConfigEnableSFS)
                        //{
                        //    sfSystem.TopSN = uutData.topSN;
                        //    sfSystem.PcbaSN = uutData.pcbaSN;
                        //    sfSystem.MAC = uutData.mac;
                        //    sfSystem.TestStatus = testStatus ? "PASS" : "FAIL";
                        //    sfSystem.ErrorCode = lastFailCode.ToString();
                        //    sfSystem.StartTime = DateTime.Now;
                        //    sfSystem.RecordShopData();
                        //}

                        if (stationConfig.ConfigEnablePFS)      // if PFS is enabled
                        {
                            int sfStatus = (int)ErrorCodes.Program.Success;
                            string sfresultMsg = string.Empty;

                            if (testStatus)     // if the unit passed all tests
                            {
                                pfsInterface.PSFSendResults(stationConfig.PFSWorkCenter, testStatus, ref outmsg);
                                if (!outmsg.Contains("OK"))
                                {
                                    sfStatus = (int)ErrorCodes.System.FixtureError;
                                    sfresultMsg = "PFS Send Result error: " + outmsg;
                                    MessageBox.Show(sfresultMsg + "\n");
                                    addTextToStatusWindow(sfresultMsg, false);
                                }
                            }
                            else
                            {
                                if (stationConfig.ConfigPFSAskIfFail)      // if station configured to allow NOT logging in PFS if fail
                                {
                                    //var opAnswer = MessageBox.Show("Unit FAILED. Do you want to send the Fail result to PFS?", "Unit Failed", MessageBoxButtons.YesNo);
                                    var opAnswer = MessageBox.Show(Properties.Resources.FailedUnitSendFailToPFSQ, Properties.Resources.UnitFailed, MessageBoxButtons.YesNo);
                                    if (opAnswer == DialogResult.Yes)
                                    {
                                        string fmsg = lastFailMessage;
                                        if (lastFailMessage.Length > 512)
                                            fmsg = lastFailMessage.Substring(0, 512);
                                        fmsg = fmsg.Replace('\n', '-');
                                        fmsg = fmsg.Replace('\r', '-');
                                        fmsg = fmsg.Replace('\t', '-');
                                        fmsg = fmsg.Replace('>', 'G');
                                        //pfsInterface.PSFSendFailResults(wsfixture.PFSWorkCenter, lastFailTestName, fmsg, ref outmsg);
                                        pfsInterface.PSFSendFailResults(stationConfig.PFSWorkCenter, lastFailCode.ToString(), lastFailTestName + ":" + fmsg, ref outmsg);
                                        if (!outmsg.Contains("OK"))
                                        {
                                            //MessageBox.Show("PFS Send Fail Result error: " + outmsg);
                                            //addTextToStatusWindow("PFS Send Fail Result error: " + outmsg + "\nComment: " + fmsg + "\n", false);
                                            sfresultMsg = Properties.Resources.PFSSendFail + outmsg;
                                            sfStatus = (int)ErrorCodes.System.FixtureError;
                                            MessageBox.Show(Properties.Resources.PFSSendFail + outmsg);
                                            addTextToStatusWindow(Properties.Resources.PFSSendFail + outmsg + "\nComment: " + fmsg + "\n", false);
                                        }

                                    }
                                    else
                                    {
                                        //addTextToResultsWindow("Fail status not sent to PFS.\n", false);
                                        //addTextToStatusWindow("Fail status not sent to PFS.", false);
                                        sfresultMsg = Properties.Resources.FailNotSentToPFS;
                                        sfStatus = (int)ErrorCodes.System.FixtureError;
                                        addTextToResultsWindow(Properties.Resources.FailNotSentToPFS + "\n", false);
                                        addTextToStatusWindow(Properties.Resources.FailNotSentToPFS + "\n", false);
                                    }
                                }
                                else        // configured to always send fail results to PFS
                                {
                                    string fmsg = lastFailMessage;
                                    if (lastFailMessage.Length > 512)
                                        fmsg = lastFailMessage.Substring(0, 512);
                                    fmsg = fmsg.Replace('\n', '-');
                                    fmsg = fmsg.Replace('\r', '-');
                                    fmsg = fmsg.Replace('\t', '-');
                                    fmsg = fmsg.Replace('>', 'G');
                                    //pfsInterface.PSFSendFailResults(wsfixture.PFSWorkCenter, lastFailTestName, fmsg, ref outmsg);
                                    pfsInterface.PSFSendFailResults(stationConfig.PFSWorkCenter, lastFailCode.ToString(), lastFailTestName + ":" + fmsg, ref outmsg);
                                    if (!outmsg.Contains("OK"))
                                    {
                                        sfStatus = (int)ErrorCodes.System.FixtureError;
                                        sfresultMsg = "PFS Send Fail Result error: " + outmsg;
                                        MessageBox.Show("PFS Send Fail Result error: " + outmsg);
                                        addTextToStatusWindow("PFS Send Fail Result error: " + outmsg + "\nComment: " + fmsg + "\n", false);
                                    }
                                }
                            }
                            testDB.CreateStepRecord("RecordPFS", 27);
                            testDB.AddStepResult((sfStatus == (int)ErrorCodes.Program.Success) ? FixtureDatabase.ResultData.TEST_PASSED : FixtureDatabase.ResultData.TEST_FAILED,
                                        sfresultMsg, sfStatus);
                        }
                    }

                    try
                    {
                        if (lastFailedStepNumber == string.Empty)
                            lastFailedStepNumber = "0";
                        testDB.CloseTestRecord(lastFailCode, lastFailMessage, Convert.ToInt32(lastFailedStepNumber), lastFailTestName, lastFailedTestNumber);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Error writing out database. " + ex.Message);
                    }
                    // just in case console dump is enabled
                    sensor.EndDump();
                    //cuConsole.EndDump();
                    statusWindowFileDump.EndDump();


                    if (stepStatus == (int)ErrorCodes.System.OperatorCancel)
                        break;

                    addTextToStatusWindow("Done\n");
                    Banner pfForm = new Banner(fixture);
                    if (abortButtonPushed)
                    {
                        //addTextToStatusWindow("--  Test was ABORTED\n", testStatus);
                        //addTextToResultsWindow("--  Test was ABORTED\n", testStatus);
                        //pfForm.PFText = ("ABORTED");
                        addTextToStatusWindow("--  " + Properties.Resources.TestAborted + "\n", testStatus);
                        addTextToResultsWindow("--  " + Properties.Resources.TestAborted + "\n", testStatus);
                        pfForm.PFText = (Properties.Resources.Aborted);
                        pfForm.BackColor = Color.Orange;
                    }
                    else if (lastFailCode >= (int)ErrorCodes.System.SystemErrorCodes)       // system failure
                    {
                        //addTextToStatusWindow("--  Test was a System Error\n", testStatus);
                        //addTextToResultsWindow("--  Test was a System Error\n", testStatus);
                        //pfForm.PFText = ("System Error");
                        addTextToStatusWindow("--  " + Properties.Resources.TestWasSytemError + "\n", testStatus);
                        addTextToResultsWindow("--  " + Properties.Resources.TestWasSytemError + "\n", testStatus);
                        pfForm.PFText = (Properties.Resources.SystemError);
                        pfForm.BackColor = Color.Yellow;
                    }
                    else
                    {
                        if (testStatus)
                            unitsPassed++;
                        else
                            unitsFailed++;
                        addUnitsRunCounter(unitsRun, unitsPassed, unitsFailed);
                        //addTextToStatusWindow("--  Test has " + (testStatus ? "PASS" : "FAIL") + "\n", testStatus);
                        //addTextToResultsWindow("--  Test has " + (testStatus ? "PASS" : "FAIL") + "\n", testStatus);
                        //pfForm.PFText = (testStatus ? "PASS" : "FAIL");
                        addTextToStatusWindow("--  " + Properties.Resources.TestHas + (testStatus ? Properties.Resources.Pass : Properties.Resources.Fail) + "\n", testStatus);
                        addTextToResultsWindow("--  " + Properties.Resources.TestHas + (testStatus ? Properties.Resources.Pass : Properties.Resources.Fail) + "\n", testStatus);
                        pfForm.PFText = (testStatus ? Properties.Resources.Pass : Properties.Resources.Fail);
                        pfForm.BackColor = testStatus ? Color.Lime : Color.Red;
                    }
                    if (uutData.pcbaSN != null)
                        pfForm.SN = uutData.pcbaSN;
                    else
                        pfForm.SN = "unknown";
                    if (uutData.mac != null)
                        pfForm.MAC = uutData.mac;
                    else
                        pfForm.MAC = "unknown";
                    pfForm.LastFailCode = lastFailCode.ToString() + " " + ErrorCodes.GetErrorString(lastFailCode);
                    pfForm.FirstFailCode = firstFailCode.ToString() + " " + ErrorCodes.GetErrorString(firstFailCode);
                    if (maxDebugLoopCount == 0)         // do not show if in autoloop mode
                        pfForm.ShowDialog();
                    pfForm.Dispose();

                     if (maxDebugLoopCount != 0)
                    {
                        debugLoopCount++;
                        if (debugLoopCount == maxDebugLoopCount)
                            break;
                        addTextToStatusWindow("Done " + debugLoopCount + " of " + maxDebugLoopCount);
                    }
                   if (stationConfig.FixtureEquipment.hasLidSwitch)
                    {
                        //addPromptToStatusWindow("Remove unit from fixture\n");
                        addPromptToStatusWindow(Properties.Resources.RemoveProduct + "\n");
                        if (stationConfig.ConfigDebug)
                        {
                            if (maxDebugLoopCount == 0)             // if not running in auto loop mode
                            {
                                if (MessageBox.Show("Bypass lid message?", "Debug mode", MessageBoxButtons.YesNo) == DialogResult.Yes)
                                {
                                    MessageBox.Show(Properties.Resources.RemoveProduct);        // "Remove any unit in the test fixture."
                                }
                                else
                                {
                                    // wait for fixture to open or the abort button is pushed
                                    while (fixture.IsCoverClosed() && !abortButtonPushed) { };
                                }
                            }
                        }
                        else
                        {
                            // wait for fixture to open or the abort button is pushed
                            while (fixture.IsCoverClosed() && !abortButtonPushed) { };
                        }
                    }

                } // end of main while loop

                if (stationConfig.FixtureEquipment.hasConsolePort)
                    sensor.Close();
                //if (stationConfig.FixtureEquipment.hasEnlightedInterface)
                //    cuConsole.Close();
                if (stationConfig.FixtureEquipment.hasPowerMeter)
                    powerMeterConsole.Close();
                //if (stationConfig.FixtureEquipment.hasTestRadio)
                //    if (fixture.refRadio != null)
                //        fixture.refRadio.Close();

            }   // end of using
        }

    }
}
