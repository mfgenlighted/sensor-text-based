﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Threading;

using Seagull.BarTender.Print;

//using LimitFileFunctions;
using FixtureDatabase;

namespace SensorManufSY2
{
    public partial class MainWindow
    {

        /// <summary>
        ///// Will ask the operator for the PCBA SN. Returns all manufacturing data.
        ///// If it existed in the database, it is use. Otherwise new data is made.
        ///// </summary>
        ///// <remarks>
        ///// Will get the base PN of the PCBA based on the scanned SN and the products table
        ///// of the Product Database.
        ///// </remarks>
        ///// <param name="uutData"></param>
        ///// <param name="testparams"></param>
        ///// <param name="outmsg"></param>
        ///// <returns></returns>
        //public int GetPCBASN(FixtureFunctions fixtureFunctions, List<ProductDatabase.Product_data> productList,
        //                       ProductDatabase.Product_data productCodeData, ref uutdata uutData, ref string outmsg)
        //{
        //    int status = (int)ErrorCodes.Program.Success;
        //    // get the PCBA SN 
        //    GetPCBASerialNumber iForm = new GetPCBASerialNumber(fixtureFunctions);
        //    if (iForm.ShowDialog() == DialogResult.OK)
        //    {
        //        uutData.pcbaSN = iForm.PcaSN.ToUpper();
        //        iForm.Dispose();
        //    }
        //    else   // operator canceled
        //    {
        //        uutData.pcbaSN = string.Empty;
        //        iForm.Dispose();
        //        return (int)ErrorCodes.System.OperatorCancel;
        //    }

        //    // if in debug mode, change the factory code of the PCBA to X
        //    if (stationConfig.ConfigDebug)
        //        uutData.pcbaSN = "X" + uutData.pcbaSN.Substring(1);

        //    using (MACDatabase macDB = new MACDatabase(stationConfig.SQLServer, stationConfig.SQLMacDatabaseName, stationConfig.SQLOperatorName, stationConfig.SQLOperatorPassword))
        //    using (SerialNumberDatabase snDB = new SerialNumberDatabase(stationConfig.SQLServer, stationConfig.SQLSerialNumberDatabaseName, stationConfig.SQLOperatorName, stationConfig.SQLOperatorPassword, stationConfig.ConfigStationName))
        //    {
        //        string newMac = string.Empty;
        //        string familyCode = string.Empty;
        //        string pcbaPN = string.Empty;
        //        string pcbaSN = string.Empty;
        //        string hlaPN = string.Empty;
        //        string hlaSN = string.Empty;
        //        List<string> MacList = new List<string>();
        //        int numberOfMacs = 0;


        //        // find the base part number for the pcba product code in the product table of the Products database.
        //        // It was read at the program startup into productList
        //        foreach (var item in productList)
        //        {
        //            if (item.pcba_product_code  == uutData.pcbaSN.Substring(1, 3))     // if found the product code
        //                pcbaPN = item.pcbaBasePN + "-" + uutData.pcbaSN.Substring(4, 2);    // add the version from the serial number
        //        }
        //        // if the pcba product code was not found in the product table
        //        if (pcbaPN == string.Empty)
        //        {
        //            outmsg = string.Format("Error: Could not find the pcba product code {0} in the Product table.", uutData.pcbaSN.Substring(1, 3));
        //            MessageBox.Show(outmsg);
        //            return (int)ErrorCodes.System.DatabaseSetupError;
        //        }

        //        // if the pcba PN matches what is configured for the HLA
        //        if (pcbaPN == productCodeData.pcbaUsed)
        //        {
        //            uutData.pcbaPN = pcbaPN;
        //            uutData.mac = string.Empty;

        //            // if the pcba serial number does not exists in the MAC data base
        //            try
        //            {
        //                if (!macDB.DoesPcaSnExist(ref MacList, ref numberOfMacs, productCodeData.familycode, uutData.pcbaSN))
        //                {
        //                    if (stationConfig.ConfigDebug)
        //                    {
        //                        MacList.Clear();
        //                        MacList.Add("6854f50002d9");
        //                    }
        //                    else
        //                        macDB.GetAndRecordNextMac(productCodeData.familycode, uutData.pcbaSN, stationConfig.ConfigStationName, ref MacList);
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                MessageBox.Show(string.Format("Error rying to get mac for sn {0} for family {1}. ", uutData.pcbaSN, productCodeData.familycode) + ex.Message);
        //                return (int)ErrorCodes.System.MacDBFailure;
        //            }
        //            uutData.mac = MacList.ElementAt(0);     // currently all text based sensors only have one mac
        //        }
        //        else    // pcba PN does not match what the HLA needs
        //        {
        //            MessageBox.Show(string.Format("Error: The pcba PN {0} is wrong for {1}. Expects {2}", pcbaPN, productCodeData.productCode, productCodeData.pcbaUsed));
        //            return (int)ErrorCodes.Program.PCBADataMismatch;
        //        }
        //        // at this point we have a valid pcba SN and MAC
        //        // see if this MAC has a HLA SN assigned to it yet

        //        try
        //        {
        //            if (!snDB.LookForMac(productCodeData.productCode.Substring(0, 3), uutData.mac, ref uutData.topSN, ref uutData.topPN, ref pcbaSN))
        //            {
        //                if (snDB.LastErrorCode != 0)           // if there was a error
        //                {
        //                    outmsg = snDB.LastErrorMessage;
        //                    return (int)ErrorCodes.System.MacDBFailure;
        //                }
        //                // if in debug mode, use the Factory code of X. This indicates that this not a shipible unit.
        //                MacList.Clear();
        //                MacList.Add(uutData.mac);
        //                uutData.topSN = snDB.GetAndRecordNextSN(stationConfig.ConfigDebug ? "X":stationConfig.ConfigFactoryCode, productCodeData.productCode.Substring(0, 3), productCodeData.productCode.Substring(3, 2),
        //                                            productCodeData.pn, uutData.pcbaSN, MacList);
        //                uutData.topPN = productCodeData.pn;
        //            }
        //        }
        //        catch (Exception ex)   // problem with getting the mac
        //        {
        //            outmsg = ex.Message;
        //            return (int)ErrorCodes.System.MacDBFailure;
        //        }
        //    }   // end of using block
        //    uutData.model = productCodeData.model;

        //    addTextToStatusWindow(string.Format("Using\n PCBA SN: {0} PN: {1} MAC: {2}\nHLA SN: {3} PN: {4}\nModel: {5} \n", uutData.pcbaSN, uutData.pcbaPN, uutData.mac,
        //                                        uutData.topSN, uutData.topPN, uutData.model));
        //    if (stationConfig.ConfigEnablePFS)
        //    {
        //        uutData.pfsAssemblyNumber = productCodeData.pfsAssemblyNumber;
        //        pfsInterface.PSFQuery(uutData.pfsAssemblyNumber, uutData.pcbaSN, stationConfig.ConfigStationOperation, ref outmsg);
        //        if (!outmsg.Contains("OK"))     // if sn was rejected
        //        {
        //            addTextToStatusWindow("This SN was refused by PFS.\n" + outmsg + "\n", StatusType.failed);
        //            MessageBox.Show("This SN was refused by PFS.\n" + outmsg);
        //            status = (int)ErrorCodes.Program.RefusedByPFS;
        //        }
        //    }


        //    return status;

        //}
        /// <summary>
        /// Will ask the operator for the PCBA SN. Returns all manufacturing data.
        /// If it existed in the database, it is use. Otherwise new data is made.
        /// </summary>
        /// <remarks>
        /// If in debug mode, and it dose not exist in the database, it will not get a real HLA serial number. It will be a generated debug number
        /// which will not be recorded in the database. It will also return debug mac addresses. It will not
        /// get them from the MAC pools.
        /// </remarks>
        /// <param name="uutData"></param>
        /// <param name="testparams"></param>
        /// <param name="outmsg"></param>
        /// <returns>
        /// 6    ErrorCodes.Program.PCBADataMismatch
        /// -7   ErrorCodes.System.OperatorCancel, operator canceled when asked for sn
        /// -17  ErrorCodes.System.DatabaseSetupError, product code was not found database product table
        /// -3   ErrorCodes.System.MacDBFailure, Error reading or generating a MAC
        /// -4   ErrorCodes.System.SNDBFailure, Error reading or generating serial numbers
        /// </returns>
        public int GetPCBASN(FixtureFunctions fixtureFunctions,
                               ProductDatabase.Product_data productCodeData, ref uutdata uutData, ref string outmsg)
        {
            int status = 0;

            // special debug function. If the first character is # then give the operator the 
            //  option of using this or enter a new one.
            if (uutData.pcbaSN != null)
            {
                if (uutData.pcbaSN.Length > 1)
                {
                    if (uutData.pcbaSN.Substring(0, 1) == "#")
                    {
                        if (MessageBox.Show("Do you want to use the last sn(" + uutData.pcbaSN.Substring(1) + ")?", "SN reuse", MessageBoxButtons.YesNo) == DialogResult.Yes)
                            uutData.pcbaSN = uutData.pcbaSN.Substring(1);
                        else
                            uutData.pcbaSN = string.Empty;
                    }
                    else
                        uutData.pcbaSN = string.Empty;
                }
            }

            // get the PCBA SN 
            if ((uutData.pcbaSN == string.Empty) | (uutData.pcbaSN == null))
            {
                GetPCBASerialNumber iForm = new GetPCBASerialNumber(fixtureFunctions);
                if (iForm.ShowDialog() == DialogResult.OK)
                {
                    uutData.pcbaSN = iForm.PcaSN.ToUpper();
                    iForm.Dispose();
                }
                else   // operator canceled
                {
                    uutData.pcbaSN = string.Empty;
                    iForm.Dispose();
                    return (int)ErrorCodes.System.OperatorCancel;
                }
            }

            using (MACDatabase macDB = new MACDatabase(stationConfig.SQLServer, stationConfig.SQLMacDatabaseName, stationConfig.SQLOperatorName, stationConfig.SQLOperatorPassword))
            using (SerialNumberDatabase snDB = new SerialNumberDatabase(stationConfig.SQLServer, stationConfig.SQLSerialNumberDatabaseName, stationConfig.SQLOperatorName,
                                                                            stationConfig.SQLOperatorPassword, ""))
            {
                string newMac = string.Empty;
                string newMac2 = string.Empty;
                List<string> MacList = new List<string>();
                int numberOfMacs = 0;
                string familyCode = string.Empty;
                string pcbaPN = string.Empty;
                string pcbaSN = string.Empty;
                string hlaPN = string.Empty;
                string hlaSN = string.Empty;


                // if the pcba product code matches what is database data
                if (uutData.pcbaSN.Substring(1, 5) == productCodeData.pcba_product_code)
                {
                    uutData.pcbaPN = productCodeData.pcba_used;     // get the pcba pn from database data
                    uutData.mac = string.Empty;

                    // if the pcba serial number does not exists in the MAC data base
                    if (!macDB.DoesPcaSnExist(ref MacList, ref numberOfMacs, productCodeData.family_code, uutData.pcbaSN))
                    {
                        if (macDB.LastErrorCode != 0)       // if there was a database error
                        {
                            outmsg = "MAC DB Error " + macDB.LastErrorCode + " looking for serial number. " + macDB.LastErrorMessage;
                            return (int)ErrorCodes.System.MacDBFailure;
                        }
                        if (stationConfig.ConfigDebug)      // if in debug mode, make fake macs
                        {
                            MacList.Clear();
                            MacList.Add("6854F50002D9");
                            if (numberOfMacs == 2)
                                MacList.Add("6854F50002DA");
                        }
                        else        // get a record real macs
                        {
                            macDB.GetAndRecordNextMac(productCodeData.family_code, uutData.pcbaSN, stationConfig.ConfigStationName, ref MacList);
                            if (macDB.LastErrorCode != 0)       // if there was a error
                            {
                                outmsg = string.Format("Error recording MACs. Error code = {0}. Error message = {1}", macDB.LastErrorCode, macDB.LastErrorMessage);
                                return (int)ErrorCodes.System.MacDBFailure;
                            }
                        }
                    }
                    uutData.mac = MacList.ElementAt(0);         // save data in stucture
                }
                else    // pcba PN does not match what the HLA needs
                {
                    outmsg = string.Format("Error: The pcba PN {0} is wrong for {1}. Expects {2}", pcbaPN, productCodeData.product_code, productCodeData.pcba_used);
                    MessageBox.Show(outmsg);
                    return (int)ErrorCodes.Program.PCBADataMismatch;
                }
                // at this point we have a valid pcba SN and MAC
                // see if this MAC has a HLA SN assigned to it yet

                // look for the mac. only the first mac is looked for
                if (!snDB.LookForPCBASN(uutData.pcbaSN, ref uutData.topSN, ref uutData.topPN))
                {
                    if (snDB.LastErrorCode != 0)        // if some error
                    {
                        outmsg = string.Format("Error looking for macs. Error code = {0}. Error message = {1}", snDB.LastErrorCode, snDB.LastErrorMessage);
                        return (int)ErrorCodes.System.SNDBFailure;
                    }
                    // if in debug mode, use the Factory code of X and a fake SN. This indicates that this not a shipible unit.
                    if (stationConfig.ConfigDebug)
                    {
                        uutData.topSN = "X" + productCodeData.product_code.Substring(0, 5) + "999900001";
                    }
                    else
                    {
                        uutData.topSN = snDB.GetAndRecordNextSN(stationConfig.ConfigFactoryCode, productCodeData.product_code.Substring(0, 3), productCodeData.product_code.Substring(3, 2),
                                                productCodeData.pn, uutData.pcbaSN, MacList);
                        if (snDB.LastErrorCode != 0)
                        {
                            outmsg = string.Format("Error getting and recording next SN. Error code = {0}. Error message = {1}", snDB.LastErrorCode, snDB.LastErrorMessage);
                            return (int)ErrorCodes.System.SNDBFailure;
                        }
                    }
                    uutData.topPN = productCodeData.pn;
                }
            }   // end of using block
            uutData.model = productCodeData.model;

            addTextToStatusWindow(string.Format("Using\n PCBA SN: {0} PN: {1} MAC: {2}\nHLA SN: {3} PN: {4}\nModel: {5} \n", uutData.pcbaSN, uutData.pcbaPN, uutData.mac,
                                                uutData.topSN, uutData.topPN, uutData.model));

            if (stationConfig.ConfigEnablePFS & !stationConfig.ConfigDebug) // do not do if in debug mode
            {
                uutData.pfsAssemblyNumber = productCodeData.pfs_assembly_number;
                pfsInterface.PSFQuery(uutData.pfsAssemblyNumber, uutData.pcbaSN, stationConfig.ConfigStationOperation, ref outmsg);
                if (!outmsg.Contains("OK"))     // if sn was rejected
                {
                    addTextToStatusWindow("This SN was refused by PFS.\n" + outmsg + "\n", StatusType.failed);
                    MessageBox.Show("This SN was refused by PFS.\n" + outmsg);
                    status = (int)ErrorCodes.Program.RefusedByPFS;
                }
            }


            return status;

        }


        /// <summary>
        /// Will ask the operator for the HLA SN. Returns all manufacturing data.
        /// </summary>
        /// <remarks>
        /// Gets from stations config:
        ///     product code
        ///     model
        ///     hla pn
        ///     pcba pn
        ///  Gets from serialnumbersused table based on scaned in HLA SN
        ///     MAC
        ///     pcba sn
        /// </remarks>
        /// <param name="uutData"></param>
        /// <param name="testparams"></param>
        /// <param name="outmsg"></param>
        /// <returns></returns>
        public int GetHLASN(FixtureFunctions fixtureFunctions, ProductDatabase.Product_data productCodeData, ref uutdata uutData, ref string outmsg)
        {
            int status = (int)ErrorCodes.Program.Success;
            // get the HLA SN 
            GetHLASerialNumber iForm = new GetHLASerialNumber(fixtureFunctions);
            if (iForm.ShowDialog() == DialogResult.OK)
            {
                uutData.topSN = iForm.HlaSN.ToUpper();
                iForm.Dispose();
            }
            else   // operator canceled
            {
                uutData.topSN = string.Empty;
                iForm.Dispose();
                return (int)ErrorCodes.System.OperatorCancel;
            }

            // get the stuff from the station configuration
            uutData.model = productCodeData.model;
            uutData.topPN = productCodeData.pn;
            uutData.pcbaPN = productCodeData.pcba_used;

            using (SerialNumberDatabase snDB = new SerialNumberDatabase(stationConfig.SQLServer, stationConfig.SQLSerialNumberDatabaseName, stationConfig.SQLOperatorName,
                                                                            stationConfig.SQLOperatorPassword, ""))
            {
                string newMac = string.Empty;
                string familyCode = string.Empty;
                string pcbaPN = string.Empty;
                string pcbaSN = string.Empty;
                string hlaPN = string.Empty;
                string hlaSN = string.Empty;


                try
                {
                    if (!snDB.LookForHLASN(productCodeData.product_code.Substring(0, 3), uutData.topSN, ref uutData.pcbaSN, ref uutData.mac))
                    {
                        // if it did not find the serial number, big issues
                        outmsg = "HLA Serial Number " + uutData.topSN + " was not found in the serialnumbersused table.";
                        return (int)ErrorCodes.System.SNDBFailure;
                    }
                }
                catch (Exception ex)   // problem with getting the mac
                {
                    outmsg = ex.Message;
                    return (int)ErrorCodes.System.MacDBFailure;
                }
            }   // end of using block

            addTextToStatusWindow(string.Format("Using\n PCBA SN: {0} PN: {1} MAC: {2}\nHLA SN: {3} PN: {4}\nModel: {5} \n", uutData.pcbaSN, uutData.pcbaPN, uutData.mac,
                                                uutData.topSN, uutData.topPN, uutData.model));
            if (stationConfig.ConfigEnablePFS)
            {
                uutData.pfsAssemblyNumber = productCodeData.pfs_assembly_number;
                pfsInterface.PSFQuery(uutData.pfsAssemblyNumber, uutData.pcbaSN, stationConfig.ConfigStationOperation, ref outmsg);
                if (!outmsg.Contains("OK"))     // if sn was rejected
                {
                    addTextToStatusWindow("This SN was refused by PFS.\n" + outmsg + "\n", StatusType.failed);
                    MessageBox.Show("This SN was refused by PFS.\n" + outmsg);
                    status = (int)ErrorCodes.Program.RefusedByPFS;
                }
            }


            return status;

        }

        /// <summary>
        /// Will ask the operator for the MAC. Returns all manufacturing data.
        /// If it existed in the database, it is use. Otherwise test fails.
        /// </summary>
        /// <remarks>
        /// Will verify that it matches the configured product. Retrives all the manufacturing
        /// data from the database.
        /// </remarks>
        /// <param name="uutData"></param>
        /// <param name="testparams"></param>
        /// <param name="outmsg"></param>
        /// <returns></returns>
        public int GetMAC(FixtureFunctions fixtureFunctions, List<ProductDatabase.ProductInfo> productList,
                               ProductDatabase.Product_data productCodeData, ref uutdata uutData, ref string outmsg)
        {
            int status = (int)ErrorCodes.Program.Success;
            // get the MAC 
            GetMAC iForm = new GetMAC();
            if (iForm.ShowDialog() == DialogResult.OK)
            {
                uutData.mac = iForm.MAC.ToUpper();
                iForm.Dispose();
            }
            else   // operator canceled
            {
                uutData.pcbaSN = string.Empty;
                iForm.Dispose();
                return (int)ErrorCodes.System.OperatorCancel;
            }

            using (SerialNumberDatabase snDB = new SerialNumberDatabase(stationConfig.SQLServer, stationConfig.SQLSerialNumberDatabaseName, stationConfig.SQLOperatorName,
                                                                            stationConfig.SQLOperatorPassword, ""))
            {
                string newMac = string.Empty;
                string familyCode = string.Empty;
                string pcbaPN = string.Empty;
                string pcbaSN = string.Empty;
                string hlaPN = string.Empty;
                string hlaSN = string.Empty;


                // at this point we have a valid  MAC
                // get the data for this mac
                try
                {
                    if (!snDB.LookForMac(productCodeData.product_code.Substring(0, 3), uutData.mac, ref uutData.topSN, ref uutData.topPN, ref pcbaSN))
                    {
                        // no mac found
                        outmsg = "No data found for MAC " + uutData.mac;
                        addTextToStatusWindow(outmsg + "\n");
                        status =  (int)ErrorCodes.Program.ValueOutsideOfLimit;
                    }
                }
                catch (Exception ex)   // problem with getting the mac
                {
                    outmsg = ex.Message;
                    status = (int)ErrorCodes.System.MacDBFailure;
                }

            }   // end of using block

            if (status == (int)ErrorCodes.Program.Success)
                addTextToStatusWindow(string.Format("Data Found\n PCBA SN: {0} PN: {1} MAC: {2}\nHLA SN: {3} PN: {4}\n", uutData.pcbaSN, uutData.pcbaPN, uutData.mac,
                                                uutData.topSN, uutData.topPN));

            return status;

        }


        //--------------------------------------//
        //--------------------------------------//
        //                                      //
        //      Value checking routines         //
        //--------------------------------------//
        //--------------------------------------//

        //-------------------------------------------------
        /// <summary>
        /// This function will check a value as defined in a List type parameters.
        /// </summary>
        /// <param name="testvalue"></param>
        /// <param name="item"></param>
        /// <returns>bool - true = meets the conditions</returns>
        public static bool CheckResultValue(double testvalue, LimitsDatabase.LimitsData.limitsItem item, ref string resultmessage)
        {
            bool status = true;

            resultmessage = string.Empty;
            switch (item.type.ToUpper())
            {
                case "RANGE":
                    switch (item.operation)
                    {
                        case "GTLT":
                            if ((testvalue <= Convert.ToDouble(item.value1)) || (testvalue >= Convert.ToDouble(item.value2)))
                                status = false;
                            resultmessage = string.Format("{0}   Expected: {1} < x < {2}  Found: {3:N2}\n", item.name, item.value1, item.value2, testvalue);
                            break;
                        case "GELE":
                            if ((testvalue < Convert.ToDouble(item.value1)) || (testvalue > Convert.ToDouble(item.value2)))
                                status = false;
                            resultmessage = string.Format("{0}   Expected: {1} <= x <= {2}  Found: {3:N2}\n", item.name, item.value1, item.value2, testvalue);
                            break;
                        default:
                            throw new Exception("Invalid operation: " + item.name.ToString() + ":" + item.operation.ToString());
                    }
                    break;

                case "VALUE":
                    switch (item.operation)
                    {
                        case "GE":      // greater than or equal
                            if (!(testvalue >= Convert.ToDouble(item.value1)))
                                status = false;
                            resultmessage = string.Format("{0}   Expected: x >= {1}  Found: {2:N2}\n", item.name, item.value1, testvalue);
                            break;

                        case "EQ":      // equal
                            if (!(testvalue == Convert.ToDouble(item.value1)))
                                status = false;
                            resultmessage = string.Format("{0}   Expected: x = {1}  Found: {2:N2}\n", item.name, item.value1, testvalue);
                            break;

                        case "LE":      // less than or equal
                            if (!(testvalue <= Convert.ToDouble(item.value1)))
                                status = false;
                            resultmessage = string.Format("{0}   Expected: x <= {1}  Found: {2:N2}\n", item.name, item.value1, testvalue);
                            break;

                        case "LT":      // less than
                            if (!(testvalue < Convert.ToDouble(item.value1)))
                                status = false;
                            resultmessage = string.Format("{0}   Expected: x < {1}  Found: {2:N2}\n", item.name, item.value1, testvalue);
                            break;

                        default:
                            throw new Exception("Invalid operation: " + item.name.ToString() + ":" + item.operation.ToString());
                    }
                    break;

                default:
                    throw new Exception("Invalid type: " + item.name.ToString() + ":" + item.operation.ToString());
            }

            return status;
        }

    }
}