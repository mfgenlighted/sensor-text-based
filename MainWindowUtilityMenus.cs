﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Threading;

using Seagull.BarTender.Print;

using LimitsDatabase;
using FixtureDatabase;

namespace SensorManufSY2
{
    public partial class MainWindow
    {
        //private System.ComponentModel.IContainer components;
        private FixtureFunctions fixture = null;

        private void ReprintLabel(object sender, EventArgs e)
        {
            char[] delimiterChars = { '\n', '\r' };
            uutdata readuutData = new uutdata();
            LimitsData testLimits = new LimitsData(stationConfig.SQLServer, stationConfig.SQLTestLimitDataName,
                                    stationConfig.SQLOperatorName, stationConfig.SQLOperatorPassword, stationConfig.SQLTestLimitDataName);
            LimitsData.product_codesItem allLimits = new LimitsData.product_codesItem();
            ProductDatabase.Product_data productCodeData = new ProductDatabase.Product_data();
            string outmsg = string.Empty;
            string appPrompt;
            int limitIndex;
            string pcbaLabelFileName = string.Empty;
            string productLabelFileName = string.Empty;
            System.Windows.Forms.DialogResult answer;


            ReprintWindow rpl = new ReprintWindow();
            if ((answer = rpl.ShowDialog()) == System.Windows.Forms.DialogResult.Yes) // yes = reprint last barcode
            {
                if ((lastPCBABarcodeFileName == string.Empty) & (lastProductBarcodeFileName == string.Empty))
                {
                    MessageBox.Show("There is no barcode to print.");
                    return;
                }
                try
                {
                    if (lastPCBABarcodeFileName != string.Empty)
                    {
                        var btengine = new Engine(true);
                        string appPath = Path.GetDirectoryName(Application.ExecutablePath);
                        appPath = "\"" + appPath + "\\" + lastPCBABarcodeFileName + "\"";
                        var btformat = btengine.Documents.Open(appPath);
                        btformat.PrintSetup.PrinterName = lastPCBALabelPrinter;
                        btformat.Save();
                        Result result = btformat.Print();
                        btformat.Close(SaveOptions.DoNotSaveChanges);
                        btengine.Dispose();
                        MessageBox.Show("Label printed");
                    }
                    if (lastProductBarcodeFileName != string.Empty)
                    {
                        var btengine = new Engine(true);
                        string appPath = Path.GetDirectoryName(Application.ExecutablePath);
                        appPath = "\"" + appPath + "\\" + lastProductBarcodeFileName + "\"";
                        var btformat = btengine.Documents.Open(appPath);
                        btformat.PrintSetup.PrinterName = lastProductLabelPrinter;
                        btformat.Save();
                        Result result = btformat.Print();
                        btformat.Close(SaveOptions.DoNotSaveChanges);
                        btengine.Dispose();
                        MessageBox.Show("Label printed");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("There was a problem printing the label.\n" + ex.Message);
                }
            }
            //else if (answer == System.Windows.Forms.DialogResult.No)       // if no = read a unit
            //{
            //    if (productCodeToTest == string.Empty)
            //    {
            //        MessageBox.Show("No product has been configured.");
            //        return;
            //    }
            //    // read the limit file for the PCBA and get the label file from the PrintProductLabel test
            //    // figure out if configured product is in the product list
            //    productCodeToTest = productCodeToTest.Substring(0, 5);      // get product code from the SN
            //    productCodeData.active_part = "NA";
            //    foreach (var item in ProductCodeDataList)
            //    {
            //        if (item.productCode == productCodeToTest)
            //            productCodeData = item;
            //    }
            //    if (productCodeData.activePart.ToUpper() == "NA")
            //    {
            //        MessageBox.Show("Part was not found in database.");
            //        return;
            //    }
            //    addTextToStatusWindow("Reading limit file " + productCodeData.limitFilename + " to get label file name\n");
            //    //--------------------------------------------------------------------------//
            //    // read the test limit file                                                 //
            //    //--------------------------------------------------------------------------//
            //    if (!testLimits.GetData(productCodeToTest.Substring(0,5), productCodeData.limitFilename, stationConfig.ConfigFixtureType, ref allLimits, ref outmsg))
            //    {
            //        MessageBox.Show(outmsg);    // if there was a problem reading the file
            //        return;
            //    }

            //    // get the PrintProductLabel and PrintPCBALabel tests to get the label file
            //    for (limitIndex = 0; limitIndex < allLimits.steps.Count; limitIndex++)
            //    {
            //        if (allLimits.steps[limitIndex].function_call.ToUpper() == "PRINTPRODUCTLABEL")
            //            productLabelFileName = allLimits.steps[limitIndex].GetParameterValue("BarcodeFile");
            //        if (allLimits.steps[limitIndex].function_call.ToUpper() == "PRINTPCBALABEL")
            //            pcbaLabelFileName = allLimits.steps[limitIndex].GetParameterValue("BarcodeFile");
            //    }
            //    if (allLimits.GetGlobalValue("appPrompt") == string.Empty)
            //        appPrompt = "SU> ";
            //    else
            //        appPrompt = allLimits.GetGlobalValue("appPrompt");

            //    try
            //    {
            //        addTextToStatusWindow("Initializing fixture.\n");
            //        fixture = new FixtureFunctions();
            //        fixture.StationConfigure = stationConfig;
            //        if (!fixture.InitFixture())
            //        {
            //            MessageBox.Show("Error intializing the fixture. " + fixture.LastFunctionErrorMessage);
            //            fixture.Dispose();
            //            return;
            //        }

            //        fixture.ControlDUTPower(1);     // turn dut power on
            //        Thread.Sleep(5000);

            //        SensorConsole rrc = new SensorConsole();
            //        rrc.Open(stationConfig.ConfigDUTComPort);
            //        addTextToStatusWindow("DUT serial open.\n");
                        
            //        if (!rrc.WriteLine("d man", 1000))
            //        {
            //            addTextToStatusWindow("Dut Com error: Error while sending 'd man' command.\n", false);
            //            fixture.ControlDUTPower(0);     // turn dut power off
            //            rrc.Close();
            //            fixture.Dispose();
            //            return;
            //        }

            //        if (!rrc.WaitForPrompt(3000, appPrompt, out outmsg))
            //        {
            //            addTextToStatusWindow("Dut Com error: Error waiting for " + appPrompt + " after the 'd man' command. \n Found: " + outmsg + "\n", false);
            //            fixture.ControlDUTPower(0);     // turn dut power off
            //            rrc.Close();
            //            fixture.Dispose();
            //            return;
            //        }
            //        rrc.Close();
            //        fixture.ControlDUTPower(0);     // turn dut power off
            //        fixture.Dispose();
            //        addTextToStatusWindow("Have data.\n");
            //        // now parse the output
            //        string[] lines = outmsg.Split(delimiterChars);

            //        readuutData.mac = "";
            //        readuutData.topSN = "";

            //        foreach (var item in lines)
            //        {
            //            // parse out the data
            //            if (item.Contains("MAC="))
            //                readuutData.mac = item.Substring(item.IndexOf('=') + 5).ToUpper();      // need to get pass the leading 0000
            //            if (item.Contains("HLA Serial="))
            //            {
            //                readuutData.topSN = item.Substring(item.IndexOf('=') + 1);
            //                if (readuutData.topSN.Contains("--"))   // if it has not been set yet
            //                {
            //                    addTextToStatusWindow("There is no HLA Serial Number programed. Can not reprint the label.\n", false);
            //                    rrc.Close();
            //                    return;
            //                }
            //            }
            //            if (item.Contains("PCBA Serial="))
            //                readuutData.pcbaSN = item.Substring(item.IndexOf("=") + 1).ToUpper();
            //        }
            //    }
            //    catch (Exception ex)
            //    {
            //        MessageBox.Show(ex.Message);
            //        string[] ports = System.IO.Ports.SerialPort.GetPortNames();         // get list of serial ports
            //        string msg;
            //        addTextToStatusWindow("Bad DUT serial\n", false);
            //        msg = ex.Message + "\n";
            //        msg += "Valid ports: \n";
            //        foreach (string i in ports)
            //            msg += i.ToString() + "\n";
            //        addTextToStatusWindow(msg);
            //    }
                    
            //    try
            //    {
            //        if (pcbaLabelFileName != string.Empty)
            //        {
            //            string fullLabelFileName = string.Empty;
            //            addTextToStatusWindow("Printing PCBA label for SN " + readuutData.topSN + " and MAC " + readuutData.mac + "\n");
            //            var btengine = new Engine(true);
            //            if (stationConfig.ConfigLabelFileDirectory == string.Empty)     // if no directory is defined
            //            {
            //                string appPath = Path.GetDirectoryName(Application.ExecutablePath);
            //                fullLabelFileName = Path.Combine(appPath, pcbaLabelFileName);
            //            }
            //            else                                                            // directory is defined
            //            {
            //                fullLabelFileName = Path.Combine(stationConfig.ConfigLabelFileDirectory, pcbaLabelFileName);
            //            }
            //            var btformat = btengine.Documents.Open(fullLabelFileName);
            //            btformat.PrintSetup.PrinterName = stationConfig.ConfigProductLabelPrinter;
            //            var btlist = btformat.SubStrings;
            //            foreach (var item in btformat.SubStrings.ToList())
            //            {
            //                if (item.Name == "MAC")
            //                    btformat.SubStrings["MAC"].Value = readuutData.mac;
            //                if (item.Name == "SN")
            //                    btformat.SubStrings["SN"].Value = readuutData.pcbaSN;
            //            }
            //            btformat.Save();
            //            Result result = btformat.Print();
            //            btformat.Close(SaveOptions.DoNotSaveChanges);
            //            btengine.Dispose();
            //        }
            //        if (productLabelFileName != string.Empty)
            //        {
            //            string fullLabelFileName = string.Empty;
            //            addTextToStatusWindow("Printing Product label for SN " + readuutData.topSN + " and MAC " + readuutData.mac + "\n");
            //            var btengine = new Engine(true);
            //            if (stationConfig.ConfigLabelFileDirectory == string.Empty)     // if no directory is defined
            //            {
            //                string appPath = Path.GetDirectoryName(Application.ExecutablePath);
            //                fullLabelFileName = Path.Combine(appPath, productLabelFileName);
            //            }
            //            else                                                            // directory is defined
            //            {
            //                fullLabelFileName = Path.Combine(stationConfig.ConfigLabelFileDirectory, productLabelFileName);
            //            }
            //            var btformat = btengine.Documents.Open(fullLabelFileName);
            //            btformat.PrintSetup.PrinterName = stationConfig.ConfigProductLabelPrinter;
            //            foreach (var item in btformat.SubStrings.ToList())
            //            {
            //                if (item.Name == "MAC")
            //                    btformat.SubStrings["MAC"].Value = readuutData.mac;
            //                if (item.Name == "SN")
            //                    btformat.SubStrings["SN"].Value = readuutData.topSN;
            //            }
            //            btformat.Save();
            //            Result result = btformat.Print();
            //            btformat.Close(SaveOptions.DoNotSaveChanges);
            //            btengine.Dispose();
            //        }
            //    }
            //    catch (Exception ex)
            //    {
            //        addTextToStatusWindow("There was a problem printing the label.\n" + ex.Message, false);
            //    }

            //}

        }

        /// <summary>
        /// This will clear the Manufacturing data from the unit. Must be in DVTMAN.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ClearManufacutureData(object sender, EventArgs e)
        {
            string outmsg = string.Empty;

            //using (SensorConsole rrc = new SensorConsole())
            {
                addTextToStatusWindow("Starting DUT Manufacture Data clear.....", true);
//                rrc.StatusUpdated += new EventHandler<StatusUpdatedEventArgs>(sc_StatusUpdated);
                // open the console port
                SensorConsole rrc = new SensorConsole();
                try
                {
                    rrc.Open(stationConfig.ConfigDUTComPort);
                    //rrc.DisplayRcvChar = true;
                    //rrc.DisplaySendChar = true;
                    //rrc.StatusUpdated += new EventHandler<StatusUpdatedEventArgs>(sc_StatusUpdated);
                }
                catch (Exception ex)
                {
                    addTextToStatusWindow("Error opening console port. \n" + ex.Message + "\n", false);
                    return;
                }

                fixture = new FixtureFunctions();
                fixture.StationConfigure = stationConfig;
                if (!fixture.InitFixture())
                {
                    MessageBox.Show("Error initializing fixture. " + fixture.LastFunctionErrorMessage);
                    rrc.Close();
                    return;
                }
                fixture.ControlDUTPower(1);     // turn dut power on
                Thread.Sleep(1000);
                
                // Verify it is in DVTMAN.
                if (!rrc.WriteLine("d man", 1000))
                {
                    addTextToStatusWindow("Dut Com error: Error while sending 'd man' command.\n", false);
                    rrc.Close();
                    fixture.ControlDUTPower(0);     // turn dut power off
                    return;
                }

                if (!rrc.WaitForPrompt(3000, "DVTMAN> ", out outmsg))
                {
                    addTextToStatusWindow("Dut Com error: Error waiting for DVTMAN after the 'd man' command. \n Found: " + outmsg + "\n", false);
                    rrc.Close();
                    fixture.ControlDUTPower(0);     // turn dut power off
                    return;
                }

                // issue p clear
                if (!rrc.WriteLine("p clear", 1000))
                {
                    addTextToStatusWindow("Dut Com error: Error while sending 'p clear' command.\n", false);
                    rrc.Close();
                    fixture.ControlDUTPower(0);     // turn dut power off
                    return;
                }
                if (!rrc.WaitForPrompt(3000, "DVTMAN> ", out outmsg))
                {
                    addTextToStatusWindow("Dut Com error: Error waiting for DVTMAN after the 'd man' command. \n Found: " + outmsg + "\n", false);
                    rrc.Close();
                    fixture.ControlDUTPower(0);     // turn dut power off
                    return;
                }
                rrc.Close();
                Thread.Sleep(1000);                 // let everything catch up
                fixture.ControlDUTPower(0);     // turn dut power off
                fixture.Dispose();

                addTextToStatusWindow("DUT Manufacture Data cleared\n", true);
            }
        }

    }
}
