﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Threading;
using System.Diagnostics;

namespace SensorManufSY2
{
    /// <summary>
    /// PIR tests.
    /// </summary>
    class PIRTests
    {
        private FixtureFunctions fixture;       // has the fixture functions plus the the station config
        private SensorConsole senConsole;
        private SensorDVTMANTests senDVTMANTests = null;
        private TestRadioConsole testRadioConsole;
        private CUConsole cuConsole;
        private SUSensorData sensorData;

        //private int changeCounter = 0;
        MainWindow.uutdata uutData = new MainWindow.uutdata();

        //private int testcount;
        private bool dataRead = false;

        public int ErrorCode = (int)ErrorCodes.Program.Success;
        public string ErrorMsg = string.Empty;
        public int MinValueChange = 100;
        public List<int> pirData;

        public PIRTests(MainWindow.codeClasses functions)
        {
            fixture = functions.fixtureFunctions;
            senConsole = functions.sensorConsole;
            testRadioConsole = functions.fixtureFunctions.refRadio;
            cuConsole = functions.cuConsole;
        }

        /// <summary>
        /// Will monitor for at least intervalToCheckOver seconds to see if output oscillates.
        /// Once a oscillation is found, it will return with a true.
        /// </summary>
        /// <remarks>
        /// Oscillation is defined as a input that enters and exits the ranges of max 15000 or min 1000.
        /// </remarks>
        /// <param name="intervalToCheckOver">Max number of seconds to watch</param>
        /// <returns> 0 = not osc
        ///  1 = osc
        ///  -1 = error</returns>
        int highRangeCount = 0;
        int midRangeCount = 0;
        int lowRangeCount = 0;
        enum state { start, mid, high, low };
        System.Timers.Timer dataTimer;

        public int IsPIROscillating(int intervalToCheckOver, string dvtmanPrompt)
        {
            DateTime endTime;
            if (senDVTMANTests == null)
            {
                senDVTMANTests = new SensorDVTMANTests();
                senDVTMANTests.sensorConsole = senConsole;
                senDVTMANTests.testRadioConsole = testRadioConsole;
                senDVTMANTests.cuConsole = cuConsole;
                senDVTMANTests.fixture = fixture;
            }
            int isOsc = 0;
            int currentState = (int)state.start;
            int lastRead = 0;
            string outmsg = string.Empty;

            // make sure PIR source is off
            fixture.ControlPIRSource(0);


            // setup timer to gather data
            dataTimer = new System.Timers.Timer();
            dataTimer.Elapsed += new ElapsedEventHandler(dataGatherTimer);
            dataTimer.Interval = 100;
            dataTimer.AutoReset = false;

            // setup the data list
            pirData = new List<int>();
            pirData.Clear();
            sensorData = new SUSensorData();        // set this up for the timer use
            uutData.dvtmanPrompt = dvtmanPrompt;
            senConsole.ClearBuffers();              // get rid of anything in the buffers

            endTime = DateTime.Now.AddSeconds(intervalToCheckOver);

            //// start timer and wait till time is up or changeCount > 2
            dataTimer.Enabled = true;
            currentState = (int)state.mid;                              // assume mid

            while ((DateTime.Now < endTime) & (highRangeCount < 2) & (lowRangeCount < 2))
            {
                // wait for data
                if (dataRead)
                {
                    lastRead = pirData.Last();
                    dataRead = false;
                    dataTimer.Enabled = true;               // start to get next tic
                    switch (currentState)
                    {
                        case (int)state.start:
                            currentState = (int)state.mid;
                            if (lastRead > 15000)      // if now in the high state
                            {
                                currentState = (int)state.high;                          // state = high state
                            }
                            if (lastRead < 1000)       // if now in the low state
                            {
                                currentState = (int)state.low;                           // state = low state
                            }
                            break;

                        case (int)state.mid:         // mid
                            if (lastRead > 15000)      // if now in the high state
                            {
                                highRangeCount++;
                                currentState = (int)state.high;                          // state = high state
                            }
                            if (lastRead < 1000)       // if now in the low state
                            {
                                lowRangeCount++;
                                currentState = (int)state.low;                           // state = low state
                            }
                            break;

                        case (int)state.high:     // high
                            if (lastRead < 14500)      // if it has come out of the high state
                            {
                                midRangeCount++;
                                currentState = (int)state.mid;
                            }
                            break;

                        case (int)state.low:
                            if (lastRead > 1500)       // if it has come out of the low state
                            {
                                midRangeCount++;
                                currentState = (int)state.mid;
                            }
                            break;

                        default:
                            break;
                    }
                }
            }

            if ((highRangeCount >= 2) | (lowRangeCount >= 2))
                isOsc = 1;

            return isOsc;
        }

        /// <summary>
        /// Will record the value of the output over a time period of timePeriodMS and return
        /// the min/max values. Sets isOsc to true if a oscillation is found.
        /// </summary>
        /// <remarks>
        /// Will turn on the PIR source and monitor the output recording the value before turning
        /// on the source and value at the end of the time period. Then the source will be
        /// turned off. If oscillation is found, isOsc will be set to true.
        /// Oscillation is defined as a input that changes direction 3 times. The change must be
        /// more that MinValueChange.
        /// </remarks>
        /// <param name="timePeriodMS"></param>
        /// <param name="maxValue"></param>
        /// <param name="minValue"></param>
        /// <param name="isOsc"></param>
        /// <returns></returns>
        public bool OnOffTest(int timePeriodMS, ref int maxValue, ref int minValue, ref bool isOsc)
        {
            
            return false;
        }


        private void dataGatherTimer(object source, ElapsedEventArgs e)
        {
            string outmsg;
            int status;

            status = senDVTMANTests.ReadSensors(ref sensorData, uutData, out outmsg);
            if (sensorData.PIRIR != 99)
            {
                pirData.Add(sensorData.PIRIR);
                dataRead = true;
            }
        }
    }

}
