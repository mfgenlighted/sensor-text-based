﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;
using System.Threading;

namespace SensorManufSY2
{
    class RelayDriver
    {
        // properties
        public string ErrorMessage = string.Empty;                  // will hold the error message if error occured
        public int ErrorCode = (int)ErrorCodes.Program.Success;     // will hold the error code if error occured
        public enum RelayTypes { SwitchMate };
        public enum RelayStates { Open, Close};

        // private
        private RelayTypes relayType;         // only one valid value currently: SwitchMate

        //--------------------------------------------
        //  Switch-Mate data
        //--------------------------------------------
        private string switchMateSerialPort = string.Empty;      // set from station config passed to init

        private SerialPort serialPort_SwitchMate;               // place to save the Switch-Mate serial port handle
        private const int switchMateBaud = 19200;               // default baud rate
        private const string sm_prompt = "-> ";                 // prompt
        private const string sm_newline = "\r";                 // new line character
        private const string switchMateID = "SWITCH-MATE";      // leading part of the board ID. Read with SM_ID?
        private const string sm_dutPowerRelay = "01";              // relay number for DUT power
        private const string sm_pirSource = "02";                  // relay number for the PIR source

        private const string sm_select_relay = "SM_SR";         // select specific relay, rrs - rr= relay 01-08, s=0/1
        private const string sm_get_rly_status = "SM_RS?";      // get relay status
        private const string sm_master_reset = "SM_MR";         // master reset
        private const string sm_get_device_id = "SM_ID?";       // get device ID


        // methods
        /// <summary>
        /// Will setup the relay board to be able to commuicate with.
        /// </summary>
        /// <param name="relayType"></param>
        /// <param name="serialPort"></param>
        /// <returns></returns>
        public bool Open(RelayTypes type, string comPort)
        {
            bool status = true;                                // assume everthing is good

            relayType = type;
            if (type == RelayTypes.SwitchMate)
            {
                string readData = string.Empty;
                switchMateSerialPort = comPort;
                // setup the serial port
                serialPort_SwitchMate = new SerialPort();
                if (switchMateSerialPort == string.Empty)
                {
                    ErrorCode = (int)ErrorCodes.System.ComponetInitFail;
                    ErrorMessage = "serial port must be specified";
                    return false;
                }
                // set up the port
                serialPort_SwitchMate.PortName = switchMateSerialPort;
                serialPort_SwitchMate.BaudRate = switchMateBaud;
                serialPort_SwitchMate.ReadTimeout = 1000;
                serialPort_SwitchMate.NewLine = sm_newline;
                serialPort_SwitchMate.Open();
                try
                {
                    serialPort_SwitchMate.WriteLine(sm_get_device_id);
                    readData = serialPort_SwitchMate.ReadTo(sm_prompt);
                    Thread.Sleep(500);
                    serialPort_SwitchMate.WriteLine(sm_get_device_id);  // Do a second time just incase garbage
                    readData = serialPort_SwitchMate.ReadTo(sm_prompt);
                    Thread.Sleep(500);
                    if (!readData.Contains(switchMateID))
                    {
                        ErrorMessage = "Switch-Mate ID is wrong. " + readData;
                        ErrorCode = (int)ErrorCodes.System.ComponetInitFail;
                        return false;
                    }
                }
                catch (Exception ex)        // something went wrong getting the port open or reading the ID
                {
                    ErrorMessage = "Error setting up Switch-Mate: " + ex.Message;
                    ErrorCode = (int)ErrorCodes.System.ComponetInitFail;
                    return false;
                }

            }
            else
            {
                ErrorMessage = "Invalid relay type";
                ErrorCode = (int)ErrorCodes.System.ComponetInitFail;
                status = false;
            }

            return status;
        }

        /// <summary>
        /// Close the relay com ports
        /// </summary>
        /// <returns></returns>
        public void Close()
        {
            if (relayType == RelayTypes.SwitchMate)
            {
                if (serialPort_SwitchMate.IsOpen)
                    serialPort_SwitchMate.Close();
                if (serialPort_SwitchMate != null)
                    serialPort_SwitchMate.Dispose();
            }
        }

        /// <summary>
        /// Will set a relay to to state.
        /// </summary>
        /// <param name="relayNumber"></param>
        /// <param name="state"></param>
        /// <returns></returns>
        public bool ControlRelay(int relayNumber, RelayStates state)
        {
            string responseString = string.Empty;

            try
            {
                serialPort_SwitchMate.DiscardInBuffer();
                serialPort_SwitchMate.DiscardOutBuffer();
                if (state == RelayStates.Open)
                {
                    serialPort_SwitchMate.Write(sm_select_relay + relayNumber.ToString("D2") + "0\r");
                }
                else
                {
                    serialPort_SwitchMate.Write(sm_select_relay + relayNumber.ToString("D2") + "1\r");
                }
                serialPort_SwitchMate.ReadTo(sm_prompt);            // wait for the command response prompt
                // verify that it was set
                serialPort_SwitchMate.Write(sm_get_rly_status + "\r");
                responseString = serialPort_SwitchMate.ReadTo(sm_prompt);
                if (responseString.Substring(responseString.IndexOf('<') + relayNumber, 1) == "1") // if showing closed
                {
                    if (state == RelayStates.Open)      // if showing closed when it should be open
                    {
                        ErrorCode = (int)ErrorCodes.System.FixtureError;
                        ErrorMessage = "Relay not in correct state";
                        return false;
                    }
                }
                Thread.Sleep(500);                              // so next does not come to quick
            }
            catch (Exception EX)
            {
                ErrorMessage = "Error issuing relay command: " + EX.Message;
                ErrorCode = (int)ErrorCodes.System.FixtureError;
                return false;
            }
            return true;
        }
    }
}
