﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SensorManufSY2
{
    public partial class SNCKFixtureControl : Form
    {
        StationConfigure stationConfig;

        public SNCKFixtureControl(StationConfigure config)
        {
            InitializeComponent();
            stationConfig = config;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string outmsg;

            SnifferConsole snifferConsole = new SnifferConsole();
            string modMac = tbMAC.Text.Substring(6, 2) + ":" + tbMAC.Text.Substring(8, 2) + ":" + tbMAC.Text.Substring(10, 2);
            snifferConsole.ComPort = stationConfig.ConfigSnifferComPort;
            snifferConsole.Open();
            snifferConsole.SnifferComWriteLine("p w " + modMac, 1000);
            rtbStatus.AppendText("p w " + modMac + "\n");
            snifferConsole.SnifferWaitForPrompt(5000, "Comm", out outmsg);
            rtbStatus.AppendText(outmsg);
            snifferConsole.Close();
        }
    }
}
