﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

using LimitsDatabase;
using FixtureDatabase;


namespace SensorManufSY2
{
    class SensorAPPTests
    {
        //------------------------
        // events
        //  Use UpdateStatusWindow(msg) to display a message on the
        //  MainWindow status window.
        //------------------------
        public event EventHandler<StatusUpdatedEventArgs>
            StatusUpdated;

        protected virtual void
            OnStatusUpdated(StatusUpdatedEventArgs e)
        {
            if (StatusUpdated != null)
                StatusUpdated(this, e);
        }

        // call these functions to update the main status window
        private void UpdateStatusWindow(string msg)
        {
            string newStatus = msg;
            StatusUpdatedEventArgs nuea = new StatusUpdatedEventArgs();
            nuea.statusText = msg;
            OnStatusUpdated(nuea);
        }
        private void UpdateStatusWindow(string msg, StatusType status)
        {
            string newStatus = msg;
            StatusUpdatedEventArgs nuea = new StatusUpdatedEventArgs();
            nuea.statusText = msg;
            nuea.statusType = status;
            OnStatusUpdated(nuea);
        }


        //--------------------
        // Properties
        //--------------------
        public StationConfigure stationConfig = null;
        public SensorConsole sensorConsole = null;

        //CUConsole cuConsole;
        //TestRadioConsole testRadioConsole;
        //FixtureFunctions fixture;

        public SensorAPPTests()
        {
        }


        public int VersionTest(LimitsData.stepsItem parameters, FixtureDatabase.ResultData testDB, ref string resultmsg)
        {
            int status = (int)ErrorCodes.Program.Success;

            return status;
        }

        /// <summary>
        /// Uses the scanned the Product Label Serial number and verifies that it
        /// can sniff the mac. Note that the Sniffer is used, not the test radio.
        /// </summary>
        /// <remarks>
        /// TestConstant ReadMethod defines how to read the programmed data. RF(default) or Serial
        /// </remarks>
        /// <param name="parameters"></param>
        /// <param name="testDB"></param>
        /// <param name="resultmsg"></param>
        /// <returns></returns>
        public int VerifyLabel(LimitsData.stepsItem parameters, FixtureDatabase.ResultData testDB, MainWindow.uutdata uutData, out string resultmsg)
        {
            int status = (int)ErrorCodes.Program.Success;
            bool readMethodIsRF = true;
            double radioLQILimit = 100;
            double readLQI = 0;
            string[] readLines = new string[7];
            string modifiedMAC = string.Empty;
            string foundMacs = string.Empty;

            // format MAC to xx:xx:xx
            if (uutData.mac.Contains(':'))      // if in the form of aa:bb:cc:dd:ee:ff
            {
                if (uutData.mac.Length != 17)
                {
                    resultmsg = "MAC is not in a accepted format. Should be aa:bb:cc:dd:ee:ff or aabbccddeeff.";
                    return (int)ErrorCodes.System.InvalidParameter;
                }
                modifiedMAC = uutData.mac.Substring(8, 8);      // get last 3 parts
            }
            else                        // else in aabbccddeeff format
            {
                if (uutData.mac.Length != 12)
                {
                    resultmsg = "MAC is not in a accepted format. Should be aa:bb:cc:dd:ee:ff or aabbccddeeff.";
                    return (int)ErrorCodes.System.InvalidParameter;
                }
                modifiedMAC = uutData.mac.Substring(6, 2) + ":" + uutData.mac.Substring(8, 2) + ":" + uutData.mac.Substring(10, 2);      // get last 3 parts
            }

            if (testDB != null)
                testDB.CreateStepRecord(parameters.name, parameters.step_number);      // create the test record header.
            resultmsg = string.Empty;


            foreach (var item in parameters.parameters)
            {
                if (item.name.ToUpper().Contains("READMETHOD"))
                {
                    if (item.value.ToUpper().Contains("SERIAL"))
                        readMethodIsRF = false;
                }
                if (item.name.ToUpper().Contains("MINRFVALUE"))
                    radioLQILimit = Convert.ToInt32(item.value);
            }

            // if read method is RF
            // this mode just verifies that it can see the MAC
            // uses the remote command
            //      Comm > scan start
            //      Scanning at 15 and 6854..
            //      Comm > scan remote on
            //      remote output is on
            //      Comm > remote mac 05:40:0b
            //      Setting remote_mac to 05:40:0b
            //      Comm > r d ver
            //      Transmitting command of size 5 to 05:40:0b
            //      Comm > 0 d ver
            //        1 Running image 2, version 3.4.0.4137
            //        2 Backup is - NA -
            //        3
            //        4 BLE version: 2.0.6.0
            //        5 BLE bootloader: 1.1.0.51000
            //       ---- there is more info past this

            if (readMethodIsRF)
            {
                SnifferConsole snifferConsole = new SnifferConsole();
                try
                {
                    snifferConsole.Open(stationConfig.ConfigSnifferComPort);
                }
                catch (Exception ex)
                {
                    resultmsg = "Error opening Sniffer port: " + ex.Message;
                    status = (int)ErrorCodes.System.ComponetInitFail;
                }

                // setup to talk to unit
                if (status == (int)ErrorCodes.Program.Success)
                {
                    if (snifferConsole.SetupForMAC(uutData.mac, ref resultmsg) != (int)ErrorCodes.Program.Success)
                    {
                        resultmsg = "Error setting up remote: " + resultmsg;
                        status = (int)ErrorCodes.Program.DUTComError;
                    }
                }

                // send the version request
                if (status == (int)ErrorCodes.Program.Success)
                {
                    if (!snifferConsole.SnifferRemoteWriteLine("d ver", 1000))
                    {
                        resultmsg = "Error waiting for d ver echo";
                        status = (int)ErrorCodes.Program.DUTComError;
                    }
                }

                // send the version request
                if (status == (int)ErrorCodes.Program.Success)
                {
                    if (!snifferConsole.SnifferReadLines(ref readLines, 7, 3000))
                    {
                        resultmsg = "Error waiting for d ver response";
                        status = (int)ErrorCodes.Program.ValueOutsideOfLimit;
                    }
                }

                snifferConsole.Close();
            }
            // if read method is serial
            // read the serial data
            else
            {

            }

            if ((status > 0) & (status < (int)ErrorCodes.System.SystemErrorCodes))
                status = status + (parameters.step_number * 100);
            testDB.AddStepResult((status == (int)ErrorCodes.Program.Success) ? ResultData.TEST_PASSED : ResultData.TEST_FAILED, resultmsg, status);
            testDB.AddStepRef("VerifyLQI", readLQI.ToString(), "", "", "");

            return status;
        }


        /// <summary>
        /// Will verify that the output from the d profile command does not contain any
        /// 255 or 65535
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="testDB"></param>
        /// <param name="resultmsg"></param>
        /// <returns></returns>
        public int CheckProfile(LimitsData.stepsItem parameters, FixtureDatabase.ResultData testDB, string prompt, ref string resultmsg)
        {
            int status = (int)ErrorCodes.Program.Success;
            string outmsg = string.Empty;
            int retrysDone = 0;
            int maxRetrys = 0;


            testDB.CreateStepRecord(parameters.name, parameters.step_number);


            do
            {
                resultmsg = string.Empty;
                status = (int)ErrorCodes.Program.Success;

                sensorConsole.ClearBuffers();
                if (!sensorConsole.WriteLine("d profile", 1000))
                {
                    resultmsg = "Error sending 'd profile' command";
                    status = (int)ErrorCodes.Program.DUTComError + (parameters.step_number * 100);
                }
                if (status == (int)ErrorCodes.Program.Success)
                {
                    if (!sensorConsole.WaitForPrompt(5000, prompt, out outmsg))
                    {
                        resultmsg = "Error waiting for prompt after 'd profile' command";
                        status = (int)ErrorCodes.Program.DUTComError + (parameters.step_number * 100);
                    }
                }

                if (status == (int)ErrorCodes.Program.Success)
                {
                    // should now have the response in outmsg
                    // make sure there are no '255' or '65535' in the text
                    if ((outmsg.Contains("255")) | outmsg.Contains("65535"))
                    {
                        resultmsg = "Profile has a 255 or 65535 in the text.\n" + outmsg;
                        status = (int)ErrorCodes.Program.ValueOutsideOfLimit + (parameters.step_number * 100);

                    }
                }

                if (status == (int)ErrorCodes.Program.Success)
                {
                    // check for the required strings
                    foreach (var item in parameters.limits)
                    {
                        if (!outmsg.Contains(item.value1))         // if missing the string
                        {
                            if (resultmsg == string.Empty)
                                resultmsg = "Error: One or more required strings are missing: " + item.value1;
                            else
                                resultmsg += " : " + item;
                            status = (int)ErrorCodes.Program.ValueOutsideOfLimit + (parameters.step_number * 100);
                        }
                    }
                }
            } while ((status != (int)ErrorCodes.Program.Success) & (retrysDone < maxRetrys));


            testDB.AddStepResult((status == (int)ErrorCodes.Program.Success) ? "PASS" : "FAIL", resultmsg, status);
            if (retrysDone != 0)        // if there were retrys, log it
                testDB.AddStepRef("ProfileCheckRetrys", retrysDone.ToString(), "", "", "");
            return status;
        }
    }
}
