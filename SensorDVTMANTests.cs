﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Text.RegularExpressions;
using System.IO;
using System.Diagnostics;

using Seagull.BarTender.Print;

using LimitsDatabase;
using FixtureDatabase;
using System.Reflection;

namespace SensorManufSY2
{
    public class SUSensorData
    {
        public double PIRTemperature;
        public int PIRIR;
        public int Motion;
        public int AmbVisable;
        public int AmbIR;
        public int AmbLux;
        public int AmbRed = 0;
        public int AmbBlue = 0;
        public int AmbGreen = 0;
        public int CPUTemperature;

        public SUSensorData ()
        {
            PIRTemperature = 0;
            PIRIR = 0;
            Motion = 0;
            AmbVisable = 0;
            AmbRed = 0;
            AmbBlue = 0;
            AmbGreen = 0;
            AmbIR = 0;
            AmbLux = 0;
            CPUTemperature = 0;
        }
    }


    public partial class SensorDVTMANTests
    {
        //------------------------------
        // DVTMAN commands
        //------------------------------
        private const string cmd_cutest = "p cu-test";          // CU port test


        //------------------------
        // events
        //  Use UpdateStatusWindow(msg) to display a message on the
        //  MainWindow status window.
        //------------------------
        public event EventHandler<StatusUpdatedEventArgs>
            StatusUpdated;

        protected virtual void
            OnStatusUpdated(StatusUpdatedEventArgs e)
        {
            if (StatusUpdated != null)
                StatusUpdated(this, e);
        }

        // call these functions to update the main status window
        private void UpdateStatusWindow(string msg)
        {
            string newStatus = msg;
            StatusUpdatedEventArgs nuea = new StatusUpdatedEventArgs();
            nuea.statusText = msg;
            OnStatusUpdated(nuea);
        }
        private void UpdateStatusWindow(string msg, StatusType status)
        {
            string newStatus = msg;
            StatusUpdatedEventArgs nuea = new StatusUpdatedEventArgs();
            nuea.statusText = msg;
            nuea.statusType = status;
            OnStatusUpdated(nuea);
        }

        private void UpdateTestHeader(int step, int test, string name)
        {
            int newStep = step;
            int newTest = test;
            string newName = name;
            StatusUpdatedEventArgs nuea = new StatusUpdatedEventArgs();
            nuea.statusText = name;
            nuea.stepNumber = step;
            nuea.testNumber = test;
            OnStatusUpdated(nuea);
        }

        private void UpdateTestFooter(int testResultCode, string errormsg)
        {
            StatusUpdatedEventArgs nuea = new StatusUpdatedEventArgs();
            nuea.statusText = errormsg;
            nuea.resultCode = testResultCode;
            OnStatusUpdated(nuea);
        }

        private const string cmd_bleNoCodeLoad = "p ble-spi 0";             // will make the ble not load code from spi. 3.0.5 on

        public SensorConsole sensorConsole;
        public CUConsole cuConsole;
        public TestRadioConsole testRadioConsole;
        public FixtureFunctions fixture;
        public StationConfigure stationConfig;


        public SensorDVTMANTests()
        {
            sensorConsole = null;
            cuConsole = null;
            testRadioConsole = null;
            fixture = null;
            stationConfig = null;
        }


        //---------------------------------------------------
        /// <summary>
        /// SUBatteryTest
        ///  Will read the 3.3V from the SU. Will record data.
        /// </summary>
        /// <remarks>
        /// Assumed that it is a 1 parameter limit list.
        /// Value1=low, Value2=high
        /// </remarks>
        /// <param name="parameters">test to run structure</param>
        /// <param name="testDB">test database object</param>
        /// <param name="resultmsg">result message</param>
        /// <returns>
        ///      ErrorCodes.System.LabJackReadError    100028
        ///      ErrorCodes.Program.ValueOutsideOfLimit                1
        /// </returns>
        public int SUBatteryTest(LimitsData.stepsItem parameters, FixtureDatabase.ResultData testDB, ref string resultmsg)
        {
            int status = (int)ErrorCodes.Program.Success;
            double voltage = 0;
            int currentRetrys = 0;
            int maxRetrys = 0;
            string smaxRetrys = string.Empty;
            bool reseatOnRetry = false;
            int testNumber = 0;

            resultmsg = string.Empty;
            testDB.CreateStepRecord(parameters.name, testNumber);      // create the test record header.

            if (parameters.limits.Count == 0)        // if the limit list is missing
            {
                testDB.AddStepResult("FAIL", "System Error: Missing the Limit List", (int)ErrorCodes.System.MissingParameter);
                return (int)ErrorCodes.System.MissingParameter;
            }
            if (parameters.GetLimitValues("DUT_3p3V") == null)
            {
                testDB.AddStepResult("FAIL", "System Error: Missing the Limit DUT_3p3V", (int)ErrorCodes.System.MissingParameter);
                return (int)ErrorCodes.System.MissingParameter;
            }
            currentRetrys = 0;
            maxRetrys = 0;              // default to no retrys
            reseatOnRetry = false;     // default to not reseating on retrys
            if (parameters.GetParameterValue("RETRYS") != string.Empty)
            {
                maxRetrys = Convert.ToInt32(parameters.GetParameterValue("RETRYS"));
                if (parameters.GetParameterValue("RESEAT").ToUpper() == "YES")
                    reseatOnRetry = true;
            }

            do
            {
                status = (int)ErrorCodes.Program.Success; 
                try
                {
                    voltage =  fixture.Read3p3Voltage();
                }
                catch (Exception ex)
                {
                    resultmsg =  "System Error reading the 3.3V.\n\t" + ex.Message;
                    testDB.AddStepResult("FAIL", "SystemError reading 3.3v. " + ex.Message, (int)ErrorCodes.System.LabJackError);
                    return (int)ErrorCodes.System.LabJackError;
                }

                try
                {
                    if (!CheckResultValue(voltage, parameters.GetLimitValues("DUT_3p3V"), ref resultmsg)) // if fail
                    {
                        status = (int)ErrorCodes.Program.ValueOutsideOfLimit + (testNumber * 100);
                        if (currentRetrys < maxRetrys)  // if there are retrys to try
                        {
                            UpdateStatusWindow("Retrying test\n");
                            testDB.AddStepRef("SUBatteryTest", string.Format("Reseat= {0}, Try #= {1}, Voltage= {2}", (reseatOnRetry ? "YES" : "NO"), currentRetrys, voltage), "", "", resultmsg);
                            if (reseatOnRetry)
                            {
                                ReloadBanner rlb = new ReloadBanner(fixture);
                                if (rlb.ShowDialog() == DialogResult.Abort)
                                {
                                    currentRetrys = 999;                    // force to exit
                                    status = (int)ErrorCodes.Program.OperatorAbort + (testNumber * 100);
                                }
                                rlb.Dispose();
                            }
                        }
                    }
                }
                catch (Exception ex)    // to catch exceptions from the CheckResultValue method
                {
                    resultmsg = ex.Message;
                    status = (int)ErrorCodes.System.InvalidParameter;
                    currentRetrys = 999;              // system error of some sort, so get out
                }
            } while ((status != (int)ErrorCodes.Program.Success) & (currentRetrys++ < maxRetrys));

            LimitsData.limitsItem limitItem = parameters.GetLimitValues("DUT_3p3V");
            testDB.AddStepResult(limitItem.name, ((status == (int)ErrorCodes.Program.Success) ? "PASS" : "FAIL"), limitItem.operation,
                                    Convert.ToDouble(limitItem.value1), Convert.ToDouble(limitItem.value2), voltage, "V", "N2", resultmsg, status);
            resultmsg = string.Format("\t Expected: {0} - {1}   Measured: {2:N2}", limitItem.value1, limitItem.value2, voltage);
            UpdateStatusWindow(resultmsg + "\n");

            return status;
        }

        ////-----------------------------
        // SetSUDefault
        ///<summary>
        ///Sets the SU to its default values. Command is 's d all'.
        /// </summary>
        /// <remarks>
        ///<param name="dvtmanPrompt">prompt expected</param>
        ///<param name="parameters">current test struct</param>
        ///<param name="testDB">pointer to result database functions</param>
        ///<param name="reporttext">out string - If the function failed, has data on the failure.</param>
        ///<returns>bool - true = command was issued and got 'Radio set to defaults' and 'Profile set to default' responses.</returns>
        // 
        public int SetSUDefault(LimitsData.stepsItem parameters, FixtureDatabase.ResultData testDB, string dvtmanPrompt, string appPrompt, out string reporttext)
        {
            string outmsg = string.Empty;
            int stepStatus = (int)ErrorCodes.Program.Success;
            int currentRetrys = 0;
            int maxRetrys = 0;
            string smaxRetrys = string.Empty;
            bool reseatOnRetry = false;
            int timeOutSecs = 2;
            string stimeOutSecs = string.Empty;
            int testNumber = 0;

            testNumber = parameters.step_number;
            testDB.CreateStepRecord(parameters.name, testNumber);
            sensorConsole.ClearBuffers();       // make sure no leftovers from any other test.

            currentRetrys = 0;
            maxRetrys = 0;              // default to no retrys
            reseatOnRetry = false;     // default to not reseating on retrys

            if (parameters.GetParameterValue("RETRYS") != string.Empty)
            {
                maxRetrys = Convert.ToInt32(parameters.GetParameterValue("RETRYS"));
                if (parameters.GetParameterValue("RESEAT").ToUpper() == "YES")
                    reseatOnRetry = smaxRetrys.ToUpper().Equals("YES");
            }

            if (parameters.GetParameterValue("TimeOutSecs") != string.Empty)
            {
                maxRetrys = Convert.ToInt32(parameters.GetParameterValue("TimeOutSecs"));
            }

            do
            {
                stepStatus = (int)ErrorCodes.Program.Success;
                reporttext = string.Empty;

                if (!sensorConsole.WriteLine("s d all", 1000))
                {
                    reporttext = "Dut Com error: Failed sending 's d all' command.";
                    stepStatus = (int)ErrorCodes.Program.DUTComError + (testNumber * 100);
                }
//                else if (!sensorConsole.WaitForPrompt(timeOutSecs * 1000, "Radio set to defaults", out outmsg))
                else if (!sensorConsole.WaitForPrompt(timeOutSecs * 1000, "Radio", out outmsg))
                {
//                  reporttext = "Dut Com error: Failed to get the 'Radio set to defaults' from the 's d all' command.";
                    reporttext = "Dut Com error: Failed to get the 'Radio' from the 's d all' command.";
                    stepStatus = (int)ErrorCodes.Program.DUTComError + (testNumber * 100);
                }
                else if (!sensorConsole.WaitForPrompt(500, dvtmanPrompt, out outmsg))
                {
                    outmsg = sensorConsole.GetCopyOfSerialBuffer();
                    if (outmsg.Contains(appPrompt))
                    {
                        reporttext = "Appears to be changed over. " + outmsg;
                        stepStatus = (int)ErrorCodes.Program.NotInDVTMAN + (testNumber * 100);
                        break;
                    }
                    else
                    {
                        reporttext = "Dut Com error: Failed to get the prompt from the 's d all' command.";
                        stepStatus = (int)ErrorCodes.Program.DUTComError + (testNumber * 100);
                    }
                }
                if (stepStatus != (int)ErrorCodes.Program.Success)
                {
                    if (currentRetrys < maxRetrys)  // if there are retrys to try
                    {
                        UpdateStatusWindow("Retrying test\n");
                        if (reseatOnRetry)
                        {
                            ReloadBanner rlb = new ReloadBanner(fixture);
                            rlb.ShowDialog();
                            rlb.Dispose();
                        }
                        testDB.AddStepRef("SetSUDefaultRetry", string.Format("Reseat= {0}, Try #= {1}", (reseatOnRetry ? "YES":"NO"), currentRetrys), "", "", reporttext);
                    }
                }
            } while ((stepStatus != (int)ErrorCodes.Program.Success) & (currentRetrys++ < maxRetrys));


            testDB.AddStepResult((stepStatus == (int)ErrorCodes.Program.Success) ? "PASS" : "FAIL", reporttext, stepStatus);

            return stepStatus;
        }

        //---------------------------------------------------
        // SUCheckFirmware
        /// <summary>
        /// Will verify the Atmel and SPI code version. Will record the results
        /// </summary>
        /// <remarks>
        ///  Will look for AtmelVersion and SPIVersion in the parameter list.
        ///  If SPIVersion is not it the list, it will not be checked. AtmelVersion must be
        ///  present in the list.
        ///
        ///  'd ver' will return
        ///      Running image 0, version -atmeVersion-|-image0-, ....
        ///      Image 1 is version -image1-
        ///      Image 2 is version -spiversion-|-image2-
        ///      
        /// for 3.x and newer code, this is added
        ///     BLE version: -bleversion-
        ///     BLE bootloader: -blebootloaderversion-
        /// 
        ///  Will check for 'Read external flash failed' and fail the test if found.
        /// </remarks>
        /// <param name="parameters">test to run structure</param>
        /// <param name="testDB">test result recording object</param>
        /// <param name="dvtmanPrompt">expected dvtman prompt</param>
        /// <param name="resultmessage">error message</param>
        /// <returns>
        /// ErrorCodes.Program.Success
        /// ErrorCodes.Program.DUTComError
        /// ErrorCodes.Program.DutUnexpectedCmdReturn
        /// ErrorCodes.Program.ValueOutsideOfLimit
        /// ErrorCodes.System.MissingParameter
        /// </returns>
        public int SUCheckFirmware(LimitsData.stepsItem parameters, ResultData testDB, ref string resultmessage)
        {
            int status = (int)ErrorCodes.Program.Success;
            string outmsg = string.Empty;
            int startIndex, endIndex;
            string atmelVersion = string.Empty;     // image 0
            string image1Version = string.Empty;    // image 1
            string spiVersion = string.Empty;       // image 2
            string bleVersion = string.Empty;       // ble version
            string bleBootloaderVersion = string.Empty; // ble bootloader version
            string atmelVRead = string.Empty;
            string spiVRead = string.Empty;
            string image1VRead = string.Empty;
            string bleVRead = string.Empty;
            string bleBLVRead = string.Empty;
            int testNumber = 0;
            string prompt = "DVTMAN>";
            DateTime startTime = DateTime.Now;

            resultmessage = string.Empty;
            // if the limit file defines a test number, use it
            testDB.CreateStepRecord(parameters.name, testNumber);

            foreach (var item in parameters.limits)      // get the parameters
            {
                switch (item.name.ToUpper())
                {
                    case "ATMELVERSION":
                    case "IMAGE0VERSION":
                        atmelVersion = item.value1;
                        break;

                    case "SPIVERSION":
                    case "IMAGE2VERSION":
                        spiVersion = item.value1;
                        break;

                    case "IMAGE1VERSION":
                        image1Version = item.value1;
                        break;

                    case "BLEVERSION":
                        bleVersion = item.value1;
                        break;

                    case "BLEBOOTVERSION":
                        bleBootloaderVersion = item.value1;
                        break;

                    default:
                        resultmessage = string.Format("There is a error in the Limit File. " +
                            "{0} is not a valid parameter. Only AtmelVersion, SPIVersion, Image0, Image1, Image2, BLEVersion, and BLEBootVersion are valid.", item.name);
                        testDB.AddStepResult("FAIL", resultmessage, (int)ErrorCodes.System.MissingParameter);         // record PASS/FAIL type
                        return (int)ErrorCodes.System.MissingParameter;
                }
            }

            if (parameters.GetParameterValue("Prompt") != string.Empty)
                prompt = parameters.GetParameterValue("Prompt");

            resultmessage = string.Empty;
            status = (int)ErrorCodes.Program.Success;

            try
            {
                sensorConsole.WriteLine("d ver", 1000);
                sensorConsole.WaitForPrompt(5000, prompt, out outmsg);
            }
            catch (Exception ex)
            {
                resultmessage = "Error with 'd ver' command.\n" + outmsg + "\n" + ex.Message;
                testDB.AddStepResult("FAIL", resultmessage, (int)ErrorCodes.Program.DUTComError + (testNumber * 100));             // record PASS/FAIL type
                return (int)ErrorCodes.Program.DUTComError + (testNumber * 100);
            }

            // check for 'Read external flash failed'
            if (outmsg.Contains("Read external flash failed"))
            {
                resultmessage = string.Format("Error: Found 'Read external flash failed'");
                UpdateStatusWindow(resultmessage, StatusType.failed);
                testDB.AddStepResult("FAIL", "Error: Found 'Read external flash failed'", (int)ErrorCodes.Program.FixtureHardwareError + (testNumber * 100));
                return (int)ErrorCodes.Program.DUTHardwareError + (testNumber * 100);
            }

            // check on chip firmware version
            startIndex = outmsg.IndexOf("Running image");
            if (startIndex == -1)           // if 'Running image' not found
            {
                resultmessage = string.Format("Error: Did not find 'Running image'. Found {0}", outmsg);
                UpdateStatusWindow(resultmessage, StatusType.failed);
                testDB.AddStepResult("AtmelVersion", "FAIL", resultmessage, (int)ErrorCodes.Program.DutUnexpectedCmdReturn + (testNumber * 100));
                return (int)ErrorCodes.Program.DutUnexpectedCmdReturn + (testNumber * 100);
            }
            else        // found the tag for the Atmel Version
            {
                endIndex = outmsg.IndexOf('\n', startIndex);
                startIndex = outmsg.IndexOf("image 0, version ") + "image 0, version ".Length;
                if (startIndex != -1)
                    atmelVRead = outmsg.Substring(startIndex, endIndex - startIndex);

                startIndex = outmsg.IndexOf("Image 1 is ") + "Image 1 is ".Length;     // look for image 1 version
                endIndex = outmsg.IndexOf('\n', startIndex);
                image1VRead = outmsg.Substring(startIndex, endIndex - startIndex);

                startIndex = outmsg.IndexOf("Image 2 is ") + "Image 2 is ".Length;     // look for image 2 version
                endIndex = outmsg.IndexOf('\n', startIndex);
                spiVRead = outmsg.Substring(startIndex, endIndex - startIndex);

                startIndex = outmsg.IndexOf("BLE version: ");     // look for ble version. May not be there
                if (startIndex != -1)
                {
                    endIndex = outmsg.IndexOf('\n', startIndex);
                    bleVRead = outmsg.Substring(startIndex + "BLE version: ".Length, endIndex - startIndex - "BLE version: ".Length);
                }

                startIndex = outmsg.IndexOf("BLE bootloader: ");     // look for ble bootloader version. May not be there
                if (startIndex != -1)
                {
                    endIndex = outmsg.IndexOf('\n', startIndex);
                    bleBLVRead = outmsg.Substring(startIndex + "BLE bootloader: ".Length, endIndex - startIndex - "BLE bootloader: ".Length);
                }

                if (atmelVRead != string.Empty)
                {
                    if (!atmelVRead.Contains(atmelVersion))      // check the atmel version
                    {
                        status = (int)ErrorCodes.Program.ValueOutsideOfLimit + (testNumber * 100);
                        testDB.AddStepResult("AtmelVersion", "FAIL", "EQ", atmelVersion, atmelVRead, "", status);
                    }
                    else
                        testDB.AddStepResult("AtmelVersion", "PASS", "EQ", atmelVersion, atmelVRead, "", (int)ErrorCodes.Program.Success);
                }
                if (spiVersion != string.Empty)             // if spi version/image 2 is to be checked
                {
                    if (!spiVRead.Contains(spiVersion))     // and it does not match
                    {
                        status = (int)ErrorCodes.Program.ValueOutsideOfLimit + (testNumber * 100);
                        testDB.AddStepResult("SPIVersion", "FAIL", "EQ", spiVersion, spiVRead, "", status);
                    }
                    else
                        testDB.AddStepResult("SPIVersion", "PASS", "EQ", spiVersion, spiVRead, "", (int)ErrorCodes.Program.Success);
                }
                if (image1Version != string.Empty)             // if image 1 version is to be checked
                {
                    if (!image1VRead.Contains(image1Version))     // and it does not match
                    {
                        status = (int)ErrorCodes.Program.ValueOutsideOfLimit + (testNumber * 100);
                        testDB.AddStepResult("Image 1 Version", "FAIL", "EQ", image1Version, image1VRead, "", status);
                    }
                    else
                        testDB.AddStepResult("Image1Version", "PASS", "EQ", image1Version, image1VRead, "", (int)ErrorCodes.Program.Success);
                }


                if (bleVersion != string.Empty)             // if ble version is to be checked
                {
                    if (!bleVRead.Contains(bleVersion))     // and it does not match
                    {
                        status = (int)ErrorCodes.Program.ValueOutsideOfLimit + (testNumber * 100);
                        testDB.AddStepResult("BLEVersion", "FAIL", "EQ", bleVersion, bleVRead, "", status);
                    }
                    else
                        testDB.AddStepResult("BLEVersion", "PASS", "EQ", bleVersion, bleVRead, "", (int)ErrorCodes.Program.Success);
                }
                if (bleBootloaderVersion != string.Empty)             // if ble bootloader version is to be checked
                {
                    if (!bleBLVRead.Contains(bleBootloaderVersion))     // and it does not match
                    {
                        status = (int)ErrorCodes.Program.ValueOutsideOfLimit + (testNumber * 100);
                        testDB.AddStepResult("SPIVersion", "FAIL", "EQ", bleBootloaderVersion, bleBLVRead, "", status);
                    }
                    else
                        testDB.AddStepResult("SPIVersion", "PASS", "EQ", bleBootloaderVersion, bleBLVRead, "", (int)ErrorCodes.Program.Success);
                }

            }

            if (atmelVRead != string.Empty)
                resultmessage = string.Format("\t  Atmel Version: Expected: {0}  Found: {1}", atmelVersion, atmelVRead);
            if (spiVersion != string.Empty)
                resultmessage = resultmessage + string.Format("\n\t  SPIVersion: Expected: {0}  Found: {1}", spiVersion, spiVRead);
            if (image1Version != string.Empty)
                resultmessage = resultmessage + string.Format("\n\t  Image1Version: Expected: {0}  Found: {1}", image1Version, image1VRead);
            if (bleVersion != string.Empty)
                resultmessage = resultmessage + string.Format("\n\t  BLEVersion: Expected: {0}  Found: {1}", bleVersion, bleVRead);
            if (bleBootloaderVersion != string.Empty)
                resultmessage = resultmessage + string.Format("\n\t  SPIVersion: Expected: {0}  Found: {1}", bleBootloaderVersion, bleBLVRead);
            UpdateStatusWindow(resultmessage + "\n");


            return status;
        }

        //---------------------------------------------------
        // SUCheckBLEFirmware
        /// <summary>
        /// Will verify the BLE code version. Will record the results. Works with ver 3 and better.
        /// </summary>
        /// <remarks>
        ///  Will look for BLEVersion and BLEBootVersion in the parameter list.
        ///
        ///  'd ver' will return
        ///      Running image 0, version -atmeVersion-|-image0-, ....
        ///      Image 1 is version -image1-
        ///      Image 2 is version -spiversion-|-image2-
        ///     BLE version: -bleversion-
        ///     BLE bootloader: -blebootloaderversion-
        /// 
        ///  Will check for 'Read external flash failed' and fail the test if found.
        ///  The check will keep checking on a fail for the timeout and only log the
        ///  finial results in the database.
        /// </remarks>
        /// <param name="parameters">test to run structure</param>
        /// <param name="testDB">test result recording object</param>
        /// <param name="dvtmanPrompt">expected dvtman prompt</param>
        /// <param name="resultmessage">error message</param>
        /// <returns>
        /// ErrorCodes.Program.Success
        /// ErrorCodes.Program.DUTComError
        /// ErrorCodes.Program.DutUnexpectedCmdReturn
        /// ErrorCodes.Program.ValueOutsideOfLimit
        /// ErrorCodes.System.MissingParameter
        /// </returns>
        public int SUCheckBLEFirmware(LimitsData.stepsItem parameters, ResultData testDB, ref string resultmessage)
        {
            int status = (int)ErrorCodes.Program.Success;
            string outmsg = string.Empty;
            string bleVersion = string.Empty;       // ble version
            string bleVRead = string.Empty;
            int testNumber = 0;
            int timeoutsecs = 0;
            bool powerCycleOnFail = false;
            int powerCycleOffSecs = 1;
            int retryOnWaitSecs = 1;
            bool rebootOnFail = false;
            int maxNumberOfReboots = 1;
            int numberOfRebootsDone = 0;

            string prompt = "DVTMAN>";

            resultmessage = string.Empty;
            // if the limit file defines a test number, use it
            testDB.CreateStepRecord(parameters.name, testNumber);

            foreach (var item in parameters.limits)      // get the parameters
            {
                switch (item.name.ToUpper())
                {
                    case "BLEVERSION":
                        bleVersion = item.value1;
                        break;

                    default:
                        resultmessage = string.Format("There is a error in the Limit File. " +
                            "{0} is not a valid parameter. Only BLEVersion is valid.", item.name);
                        testDB.AddStepResult("FAIL", resultmessage, (int)ErrorCodes.System.MissingParameter);         // record PASS/FAIL type
                        return (int)ErrorCodes.System.MissingParameter;
                }
            }

            foreach (var item in parameters.parameters)
            {
                switch (item.name.ToUpper())
                {
                    case "POWERCYCLEOFFSECS":
                        if (!int.TryParse(item.value, out powerCycleOffSecs))
                        {
                            resultmessage = string.Format("There is a error in the Limit File. " +
                                "TestConstant PowerCycleOffSecs must be a interger");
                            testDB.AddStepResult("FAIL", resultmessage, (int)ErrorCodes.System.MissingParameter);         // record PASS/FAIL type
                            return (int)ErrorCodes.System.InvalidParameter;
                        }
                        break;

                    case "RETRYONWAITSECS":
                        if (!int.TryParse(item.value, out retryOnWaitSecs))
                        {
                            resultmessage = string.Format("There is a error in the Limit File. " +
                                "TestConstant RetryOnWaitSecs must be a interger");
                            testDB.AddStepResult("FAIL", resultmessage, (int)ErrorCodes.System.MissingParameter);         // record PASS/FAIL type
                            return (int)ErrorCodes.System.InvalidParameter;
                        }
                        break;

                    case "MAXNUMBEROFREBOOTS":
                        if (!int.TryParse(item.value, out maxNumberOfReboots))
                        {
                            resultmessage = string.Format("There is a error in the Limit File. " +
                                "TestConstant MaxNumberOfReboots must be a interger");
                            testDB.AddStepResult("FAIL", resultmessage, (int)ErrorCodes.System.MissingParameter);         // record PASS/FAIL type
                            return (int)ErrorCodes.System.InvalidParameter;
                        }
                        break;

                    case "TIMEOUTSECS":
                        if (!int.TryParse(item.value, out timeoutsecs))
                        {
                            resultmessage = string.Format("There is a error in the Limit File. " +
                                "TestConstant TimeOutSecs must be a interger");
                            testDB.AddStepResult("FAIL", resultmessage, (int)ErrorCodes.System.MissingParameter);         // record PASS/FAIL type
                            return (int)ErrorCodes.System.InvalidParameter;
                        }
                        break;

                    case "PROMPT":
                        prompt = item.value;
                        break;

                    case "RESETTYPE":
                        powerCycleOnFail = item.value.ToUpper().Equals("POWERCYCLE");
                        rebootOnFail = item.value.ToUpper().Equals("REBOOT");
                        break;

                    default:
                        resultmessage = string.Format("There is a error in the Limit File. " +
                            "{0} is not a valid parameter.", item.name);
                        testDB.AddStepResult("FAIL", resultmessage, (int)ErrorCodes.System.MissingParameter);         // record PASS/FAIL type
                        return (int)ErrorCodes.System.MissingParameter;
                }
            }

            if (bleVersion == string.Empty)
            {
                resultmessage = "Parameter BLEVersion is missing.";
                testDB.AddStepResult("FAIL", resultmessage, (int)ErrorCodes.System.MissingParameter);         // record PASS/FAIL type
                return (int)ErrorCodes.System.MissingParameter;
            }


            status = CheckBLEVersion(bleVersion, prompt, timeoutsecs, ref bleVRead, ref resultmessage);

            UpdateStatusWindow(resultmessage + "\n");

            if ((status != (int)ErrorCodes.Program.Success) & (powerCycleOnFail | rebootOnFail))
            {
                do
                {
                    sensorConsole.WriteLine("p ble-spi", 2000);         // get what value it is now
                    sensorConsole.WaitForAppPrompt(2000, out resultmessage);
                    testDB.AddStepRef("AppBLEFailSPIValue", resultmessage, "", "", "");
                    testDB.AddStepRef("AppBLEFailBLEVer", bleVRead, "", "", "");
                    UpdateStatusWindow("Configured to " + (powerCycleOnFail ? "power cycle" : "") + (rebootOnFail ? "reboot" : "") + " if BLE version fails\n");
                    testDB.AddStepRef("BLEVersionRetry", (powerCycleOnFail ? "power cycle" : "") + (rebootOnFail ? "reboot" : ""), "", "", "");
                    if (powerCycleOnFail)
                    {
                        UpdateStatusWindow("Power cycling unit\n");
                        fixture.ControlDUTPower(0);     // turn off the power
                        Thread.Sleep(500);
                        fixture.ControlDUTPower(1);
                        Thread.Sleep(1000);
                    }
                    if (rebootOnFail)
                    {
                        UpdateStatusWindow("Rebooting unit\n");
                        sensorConsole.WriteLine("p reboot", 1000);
                        Thread.Sleep(1000);
                    }
                    sensorConsole.ClearBuffers();
                    sensorConsole.WaitForPrompt(10000, prompt, out resultmessage);
                    if (resultmessage == string.Empty)      // if did not find the prompt
                    {
                        status = (int)ErrorCodes.Program.DutUnexpectedCmdReturn;
                        UpdateStatusWindow("Prompt (" + prompt +") not found.\n");
                    }
                    else
                    {
                        status = CheckBLEVersion(bleVersion, prompt, timeoutsecs, ref bleVRead, ref resultmessage);
                        UpdateStatusWindow(resultmessage + "\n");
                    }
                    numberOfRebootsDone++;
                } while ((status != (int)ErrorCodes.Program.Success) & (numberOfRebootsDone < maxNumberOfReboots));
            }

            if (status != (int)ErrorCodes.Program.Success)
            {
                status = status + (100 * testNumber);
            }

            testDB.AddStepResult("BLEVersion", (status == (int)ErrorCodes.Program.Success) ? "PASS":"FAIL", "EQ", bleVersion, bleVRead, resultmessage, status);

            return status;
        }

        //-------------------------------
        // utilty: GetBLEVersion
        // Will issue the 'd ver' command and wait for a valid version.
        //-------------------------------
        private int CheckBLEVersion(string bleVersion, string prompt, int timeoutsecs, ref string bleVRead, ref string resultmessage)
        {
            string outmsg = string.Empty;
            int status = (int)ErrorCodes.Program.Success;
            int startIndex = 0;
            int endIndex = 0;
            bool firsttime = true;
            Regex digitcheck = new Regex(@"\d+");           // use to check for presents of a digit
            Match match;
            DateTime startTime = DateTime.Now;

            bleVRead = string.Empty;

            do
            {
                resultmessage = string.Empty;

                try
                {
                    sensorConsole.WriteLine("d ver", 1000);
                    if (!sensorConsole.WaitForPrompt(5000, prompt, out outmsg))
                    {
                        resultmessage = "Did not find prompt: " + prompt;
                        return (int)ErrorCodes.Program.PromptNotFound;
                    }
                }
                catch (Exception ex)
                {
                    resultmessage = "Error with 'd ver' command.\n" + outmsg + "\n" + ex.Message;
                    return (int)ErrorCodes.Program.DUTComError;
                }

                // check for 'Read external flash failed'
                if (outmsg.Contains("Read external flash failed"))
                {
                    resultmessage = string.Format("Error: Found 'Read external flash failed'");
                    return (int)ErrorCodes.Program.DUTHardwareError;
                }

                // check on chip firmware version
                startIndex = outmsg.IndexOf("BLE version: ");     // look for ble version. May not be there
                if (startIndex != -1)
                {
                    endIndex = outmsg.IndexOf('\n', startIndex);
                    bleVRead = outmsg.Substring(startIndex + "BLE version: ".Length, endIndex - startIndex - "BLE version: ".Length);
                }

                // while the ble is booting up or changing over, no version is present
                match = digitcheck.Match(bleVRead);
                if (match.Success)      // if found version of some sort
                {
                    status =  bleVRead.Contains(bleVersion) ? (int)ErrorCodes.Program.Success : (int)ErrorCodes.Program.ValueOutsideOfLimit;
                    resultmessage = string.Format("\n\t  BLEVersion: Expected: {0}  Found: {1}", bleVersion, bleVRead);
                    return status;
                }
                if (firsttime)
                {
                    UpdateStatusWindow("Looking for BLE version.");
                    firsttime = false;
                }
                else
                    UpdateStatusWindow(".");
                Thread.Sleep(2000);
            } while ((startTime.AddSeconds(timeoutsecs) > DateTime.Now) & (timeoutsecs != 0));
            // if it gets here it did not find a valid version number
            return (int)ErrorCodes.Program.ValueOutsideOfLimit;
        }

        //--------------------------------
        // SetPcbaManData
        ///<summary>This will set the pcba manufacturing data.
        /// </summary>
        /// <remarks>
        /// It will first see if the unit is blank.
        /// If it had data programmed in it already, it will verify that it matches the passed data.
        ///   if it does not, the test will fail.
        /// If there is nothing programmed,
        ///   if there is not a mac assigned to this SN
        ///     generate a mac
        ///   if this a new sn/mac
        ///     program it
        ///   else it is a old one(one that was found in the database)
        ///     Ask if this is a repaired unit
        ///       if yes, program it
        ///       else possible dup SN so fail it.
        /// </remarks>
        ///<param name="mac">string - mac to save in the dut. Format xxxxxxxxxxxx.</param>
        ///<param name="pn">string - part number to save in the dut.</param>
        ///<param name="sn">string - serial number to save in the dut.</param>
        ///<param name="newdata">bool - true if this was generated MAC, not found in the database.</param>
        ///<param name="reporttext">out string - description of the error if there is a failure.</param>
        ///<returns>
        ///     ErrorCodes.Program.Success        0
        ///     ErrorCodes.Program.DUTComError    2
        ///     ErrorCodes.Program.DupicateSN     5
        ///</returns>
        ///
        public int SetPcbaManData(LimitsData.stepsItem parameters, FixtureDatabase.ResultData testDB,  MainWindow.uutdata uutData, bool newdata, out string reporttext)
        {
            int status;

            testDB.CreateStepRecord(parameters.name, parameters.step_number);
            status = setPcbaManData(parameters, uutData, newdata, out reporttext);
            testDB.AddStepResult((status == (int)ErrorCodes.Program.Success) ? "PASS" : "FAIL", reporttext, status);
            if (status == (int)ErrorCodes.Program.Success)
            {
                testDB.AddStepRef("PCBA_SN", uutData.pcbaSN, "", "", "");
                testDB.AddStepRef("PCBA_PN", uutData.pcbaPN, "", "", "");
                testDB.AddStepRef("PCBA_MAC", uutData.mac, "", "", "");
            }
            UpdateStatusWindow(reporttext + "\n");
            return status;
        }

        //--------------------------------
        // SetHlaManData
        ///<summary>This will set the HLA manufacturing data. Verifies the data.</summary>
        ///<param name="reporttext">status of the test</param>
        ///<param name="testDB">test result database instance</param>
        ///<param name="testToRun">test parameters</param>
        ///<param name="uutData">data for the uut</param>
        public int SetHlaManData(LimitsData.stepsItem testToRun, FixtureDatabase.ResultData testDB, MainWindow.uutdata uutData, bool newdata, out string reporttext)
        {
            int status;


            testDB.CreateStepRecord(testToRun.name, testToRun.step_number);
            status = setHlaData(testToRun, uutData, newdata, out reporttext);

            testDB.AddStepResult((status == (int)ErrorCodes.Program.Success) ? "PASS" : "FAIL", reporttext, status);

            return (int)ErrorCodes.Program.Success;
        }

        //---------------------------------
        // setAllManData
        ///<summary>This will set the all the manufacturing data.
        /// </summary>
        public int SetAllManData(LimitsData.stepsItem testToRun, FixtureDatabase.ResultData testDB, MainWindow.uutdata uutData, bool newPcbaData, bool newHlaData, out string reporttext)
        {
            int status;
            string tempout = string.Empty;

            testDB.CreateStepRecord(testToRun.name, testToRun.step_number);
            status = setPcbaManData(testToRun, uutData, newPcbaData, out reporttext);   // program the pcba data
            if (status == (int)ErrorCodes.Program.Success)                              // if programed pcba ok
                status = setHlaData(testToRun, uutData, newHlaData, out reporttext);               // program hla data
            if (status == (int)ErrorCodes.Program.Success)                              // if programed hla ok
                UpdateStatusWindow(string.Format("Unit set to:\n\tPCBA SN: {0}  PN: {1}  MAC: {2}\n\tHLA  SN: {3}  PN: {4}  Model: {5}\n",
                                            uutData.pcbaSN, uutData.pcbaPN, uutData.mac, uutData.topSN, uutData.topPN, uutData.model));


            testDB.AddStepResult((status == (int)ErrorCodes.Program.Success) ? "PASS" : "FAIL", reporttext, status);

            return status;

        }


        //---------------------------------------
        // TestNetworks
        ///<summary>
        /// Tests the ethernet and radio networks. Will record the test results
        ///</summary>
        /// <remarks>
        /// It will set the channel to configTestRadioTestChannel.
        /// Will use the test radio at:
        ///      configTestRadioMac
        /// At the end of the test, it will be set back to the defaults(s d a).
        /// </remarks>
        ///<param name="parameters">LimitsData.stepsItem - list of the test parameters. Assumed to have two parameters: RadioLQIm and RadioLQIt</param>
        ///<param name="testDB">FixtureDatabase.ResultDatabase - database to log the data</param>
        ///<param name="resultmessage">failmsg - if there is a failure, this string will have the failure message.</param>
        ///<returns>
        ///    ErrorCodes.Program.Success         0 
        ///    ErrorCodes.Program.DUTComError     2
        ///    ErrorCodes.System.InvalidParameter     100026
        ///    ErrorCodes.Program.ValueOutsideOfLimit     1
        ///</returns>
        public int TestNetworks(LimitsData.stepsItem parameters, FixtureDatabase.ResultData testDB, MainWindow.uutdata uutData, ref string resultmessage)
        {
            string rcvbuf = string.Empty;
            string[] nums;
            string stepFailmsg = string.Empty;
            int stepStatus = (int)ErrorCodes.Program.Success;
            int testStatus = (int)ErrorCodes.Program.Success;
            bool status = true;
            int retryCount = 0;
            int retests = 0;                    // defaults to no retests if option is not present

            string dutTestPrompt;               // prompt to use for the test

            double radioLQIm = 0;              // set up as no response was found
            double radioLQIt = 0;
            double radioLQImLimit = 0;
            double radioLQItLimit = 0;

            double testvalue;
            int testNumber = parameters.step_number;

            dutTestPrompt = uutData.dvtmanPrompt;       // default to unit being in DVTMAN

            if (testDB != null)
                testDB.CreateStepRecord(parameters.name, testNumber);      // create the test record header.
            resultmessage = string.Empty;

            if (fixture.refRadio.MAC.Length != 17)
            {
                testStatus = (int)ErrorCodes.System.InstrumentNotSetUp;
                resultmessage = "The test radio mac is not the correct lenght. Found '" + testRadioConsole.MAC + "'";
                return testStatus;
            }

            foreach (var item in parameters.parameters)
            {
                if (item.name.ToUpper().Contains("RETRYS"))
                    retests = Convert.ToInt32(item.value);
                if (item.name.ToUpper().Contains("MODE"))       // if there is a code mode constant
                {
                    if (item.value.ToUpper().Contains("APP"))   // if in App mode, use the App prompt
                        dutTestPrompt = uutData.appPrompt;
                }
            }

            foreach (var pitem in parameters.limits)
            {
                if (pitem.name.Equals("RADIOLQIM", StringComparison.InvariantCultureIgnoreCase))
                    radioLQImLimit = Convert.ToDouble(pitem.value1);
                if (pitem.name.Equals("RADIOLQIT", StringComparison.InvariantCultureIgnoreCase))
                    radioLQItLimit = Convert.ToDouble(pitem.value1);
            }
            if ((radioLQImLimit == 0) | (radioLQItLimit == 0))
            {
                resultmessage = "System Error: Did not find both RadioLQIm and RadioLQIt in limits.";
                if (testDB != null)
                    testDB.AddStepResult("FAIL", resultmessage, (int)ErrorCodes.System.MissingParameter);
                return (int)ErrorCodes.System.MissingParameter;
            }

            // make sure radio at a know state
            if (!sensorConsole.WriteLine("s d a", 1000))               // if error sending to the uut
            {
                resultmessage = "Error sending 's d a' command.";
                if (testDB != null)
                    testDB.AddStepResult("FAIL", resultmessage, (int)ErrorCodes.Program.DUTComError + (testNumber * 100));
                return (int)ErrorCodes.Program.DUTComError + (testNumber * 100);
            }
            sensorConsole.WaitForPrompt(2000, dutTestPrompt, out rcvbuf);
            UpdateStatusWindow("Setting to channel " + uutData.testChannel.ToString() + "\n");
            string cmd = "s r c=" + uutData.testChannel;
            sensorConsole.WriteLine(cmd, 1000);                            // set the test channel
            sensorConsole.WaitForPrompt(2000, dutTestPrompt, out rcvbuf);

            do
            {
                testStatus = (int)ErrorCodes.Program.Success;
                cmd = "p w " + fixture.refRadio.MAC.Substring(9);     // run the radio test
                status = sensorConsole.WriteLine(cmd, 2000);
                if (sensorConsole.WaitForPrompt(10000, "Got LQI resp", out rcvbuf))            // wait for the LQI value
                {
                    sensorConsole.ReadLine(ref rcvbuf, 1000);                                  // get the value part of the prompt
                    // it will have the format '(m=x,t=x)' 2.9.9 and above or 'onse(m=x,t=x) below 2.9.9
                    nums = Regex.Split(rcvbuf.Substring(rcvbuf.IndexOf("(m=") + 3), @"\D+");
                    radioLQIm = int.Parse(nums[0]);
                    radioLQIt = int.Parse(nums[1]);
                    sensorConsole.WaitForPrompt(1000, dutTestPrompt, out rcvbuf);               // eat the prompt


                    foreach (var item in parameters.limits)
                    {
                        stepStatus = (int)ErrorCodes.Program.Success;
                        if (item.name.Equals("RadioLQIm", StringComparison.CurrentCultureIgnoreCase))
                            testvalue = radioLQIm;
                        else
                            testvalue = radioLQIt;
                        try
                        {
                            if (!CheckResultValue(testvalue, item, ref stepFailmsg))
                                stepStatus = (int)ErrorCodes.Program.ValueOutsideOfLimit + (testNumber * 100);
                            resultmessage = resultmessage + "\t  " + stepFailmsg;
                        }
                        catch (Exception ex)       // got a system error of some sort
                        {
                            if (testDB != null)
                                testDB.AddStepResult("FAIL", stepFailmsg, (int)ErrorCodes.System.InvalidParameter);
                            resultmessage = "System Error: " + ex.Message;
                            return (int)ErrorCodes.System.InvalidParameter;
                        }

                        if (stepStatus != (int)ErrorCodes.Program.Success)                    // if failed the test
                        {
                            testStatus = (int)ErrorCodes.Program.ValueOutsideOfLimit + (testNumber * 100);
                            if (testDB != null)
                                testDB.AddStepResult(item.name, ((retryCount <= retests) ? "RETEST" : "FAIL"), item.operation, Convert.ToDouble(item.value1), testvalue,
                                                    "", "", string.Format("Try {0}", retryCount + 1), testStatus);
                        }
                        else                                // passed the test
                        {
                            if (testDB != null)
                                testDB.AddStepResult(item.name, "PASS", item.operation, Convert.ToDouble(item.value1), testvalue, "LQI", "", "", (int)ErrorCodes.Program.Success);
                        }
                    }
                }
                else            // did not get the 'Got LQI response(m=' response
                {
                    sensorConsole.WaitForPrompt(1000, dutTestPrompt, out rcvbuf);       // so eat the prompt if there
                    testStatus = (int)ErrorCodes.Program.DUTComError + (testNumber * 100);
                    if (testDB != null)
                        testDB.AddStepResult(((retryCount <= retests) ? "RETEST" : "FAIL"), "Did not get 'Got LQI response(m'. Got:" + rcvbuf, (int)ErrorCodes.Program.DUTComError);
                    resultmessage = resultmessage + "\t  Did not get 'Got LQI response(m'. Got:" + "\n" + rcvbuf + "\n";
                }
                Thread.Sleep(1000);
            } while ((testStatus != (int)ErrorCodes.Program.Success) && (retryCount++ <= retests));

            UpdateStatusWindow("Resetting to default\n");
            sensorConsole.WriteLine("s d a", 1000);                    // make sure radio is back in its default state
            sensorConsole.WaitForPrompt(3000, dutTestPrompt, out rcvbuf);
            UpdateStatusWindow(resultmessage);
            //Thread.Sleep(5000);
            return testStatus;
        }

        //-------------------------------------------------
        // CheckContolOutput
        ///<summary>Will verify that the control voltage output is working. Will record the results.
        ///</summary>
        ///<remarks>
        ///  Will send the command below and check to see if
        ///  they are within the limits set by the low and high values.
        ///  param      command
        ///  OutputXV   set voltage X
        ///  OutputXVW  set warm X
        ///  OutputXVC  set cold X
        ///  
        /// The first time a voltage is set, it sometime takes time to set. So the
        /// FirstSetSettleTime will delay x seconds after the first voltage is set to let
        /// it settle out.
        ///</remarks>
        ///<param name="parameters">test stucture</param>
        ///<param name="testDB">object of result recording</param>
        ///<param name="resultmessage">error message</param>
        ///<returns>
        ///     ErrorCodes.Program.Success            0
        ///     ErrorCodes.Program.DUTComError    2
        ///     ErrorCodes.Program.ValueOutsideOfLimit    1
        ///     ErrorCodes.System.LabJackReadError    100028
        ///     ErrorCodes.System.InvalidParameter    100026
        ///     ErrorCodes.System.MissingParameter
        ///</returns>
        public int CheckControlOutput(LimitsData.stepsItem parameters, FixtureDatabase.ResultData testDB, MainWindow.uutdata uutData, ref string resultmessage)
        {
            int testStatus = (int)ErrorCodes.Program.Success;
            int stepStatus = (int)ErrorCodes.Program.Success;
            string cmd = string.Empty;
            string cmdVolts = string.Empty;
            string outmsg = string.Empty;
            string reportText = string.Empty;
            double readvolts = 0;
            int settleTime = 1000;
            string singleFailmsg = string.Empty;
            string[] paramDecode;
            int outputToRead = 1;                       // which output to read: 1 or 2
            bool firstOutputSet = false;                // set to true once one ouput has been set
            int firstSettleTime = 2000;                 // default settle time

            int testNumber = parameters.step_number;
            resultmessage = string.Empty;
            testDB.CreateStepRecord(parameters.name, testNumber);      // create the test record header.

            foreach (var item in parameters.parameters)
            {
                if (item.name.ToUpper() == "FIRSTSETSETTLETIMEMS")
                    if (!Int32.TryParse(item.value, out firstSettleTime))    // if bad number format
                    {
                        testStatus = (int)ErrorCodes.System.InvalidParameter;
                        resultmessage = "Invalid FirstSetSettleTimeMS value. Can only be interger. " + item.value;
                        testDB.AddStepResult("FAIL", resultmessage, testStatus);
                        return testStatus;
                    }
                if (item.name.ToUpper() == "SETSETTLETIMEMS")
                    if (!Int32.TryParse(item.value, out settleTime))    // if bad number format
                    {
                        testStatus = (int)ErrorCodes.System.InvalidParameter;
                        resultmessage = "Invalid SetSettleTimeMS value. Can only be interger. " + item.value;
                        testDB.AddStepResult("FAIL", resultmessage, testStatus);
                        return testStatus;
                    }
            }

            testStatus = (int)ErrorCodes.Program.Success;
            foreach (var item in parameters.limits)
            {
                stepStatus = (int)ErrorCodes.Program.Success;
                paramDecode = Regex.Split(item.name, @"\D+");      // get the voltage
                cmdVolts = string.Empty;
                foreach (var vitem in paramDecode)               
                {
                    if (!string.IsNullOrEmpty(vitem))
                        cmdVolts = vitem;
                }
                if (cmdVolts == string.Empty)                           // if no number found
                {
                    testStatus = (int)ErrorCodes.System.InvalidParameter;
                    resultmessage = "Invalid param, no voltage: " + item.name;
                    testDB.AddStepResult("FAIL", resultmessage, testStatus);
                    return testStatus;
                }
                if (item.name.Contains("VW"))
                {
                    cmd = "set warm " + cmdVolts;
                    outputToRead = 2;
                }
                else if (item.name.Contains("VC"))
                {
                    cmd = "set cold " + cmdVolts;
                    outputToRead = 1;
                    outputToRead = 1;
                }
                else if (item.name.Contains("V1"))
                {
                    cmd = "set dim1 " + cmdVolts;
                    outputToRead = 2;
                }
                else if (item.name.Contains("V2"))
                {
                    cmd = "set dim2 " + cmdVolts;
                    outputToRead = 1;
                }
                else if (item.name.Contains("V"))
                {
                    cmd = "set volt " + cmdVolts;
                    outputToRead = 1;
                }
                else
                {
                    testStatus = (int)ErrorCodes.System.InvalidParameter;
                    resultmessage = "Invalid param, output spec(V, VW, VC, V1, V2): " + item.name;
                    testDB.AddStepResult("FAIL", resultmessage, testStatus);
                    return testStatus;
                }
                if (!sensorConsole.WriteLine(cmd, 1000))
                {
                    resultmessage = "Error: Dut com error sending cmd " + cmd;
                    testDB.AddStepResult(item.name, "FAIL", resultmessage, (int)ErrorCodes.Program.DUTComError + (testNumber * 100));
                    return (int)ErrorCodes.Program.DUTComError + (testNumber * 100);
                }
                sensorConsole.WaitForPrompt(15000, uutData.dvtmanPrompt, out outmsg);   // this set to a long time because the first time the command is
                                                                                        // is issue(DVTMAN 3.0.7) it takes a long time for the voltage
                                                                                        // to set. The prompt comes back after the voltage sets
                if (firstOutputSet)
                    Thread.Sleep(settleTime);    // let the output settle
                else
                {
                    firstOutputSet = true;
                    Thread.Sleep(firstSettleTime);
                }
                try
                {
                    if (outputToRead == 1)
                        readvolts = fixture.ReadControlVoltage();
                    else
                        readvolts = fixture.ReadControlVoltage2();
                }
                catch (Exception ex)
                {
                    resultmessage = "Labjack error: " + ex.Message;
                    testDB.AddStepResult("FAIL", resultmessage, (int)ErrorCodes.System.LabJackError);
                    return (int)ErrorCodes.System.LabJackError;
                }
                try
                {
                    if (!CheckResultValue(readvolts, item, ref singleFailmsg))
                        stepStatus = (int)ErrorCodes.Program.ValueOutsideOfLimit + (testNumber * 100);
                    testDB.AddStepResult(item.name, (testStatus == (int)ErrorCodes.Program.Success) ? "PASS" : "FAIL", item.operation, Convert.ToDouble(item.value1),
                                            Convert.ToDouble(item.value2), readvolts, "V", "N2", singleFailmsg, stepStatus);
                    if (stepStatus != (int)ErrorCodes.Program.Success)
                        testStatus = stepStatus;
                    UpdateStatusWindow(singleFailmsg + "\t>>> " + ((stepStatus == (int)ErrorCodes.Program.Success) ? "PASS\n":"FAIL\n"),
                                        (stepStatus == (int)ErrorCodes.Program.Success) ? StatusType.passed : StatusType.failed);
                }
                catch (Exception ex)
                {
                    resultmessage = "Limit file error: " + ex.Message;
                    return (int)ErrorCodes.System.InvalidParameter;
                }
                resultmessage = string.Format("{0}\t   {1}: Expected: {2} - {3}   Read: {4:G2}\t ---- {5} ----\n", resultmessage, item.name,
                                                    item.value1, item.value2, readvolts, (stepStatus == (int)ErrorCodes.Program.Success) ? "PASS" : "FAIL");
            }

            return testStatus;
        }

        /// <summary>
        /// Test all the sensors on the board.
        /// <remarks>
        ///  Turn off the light
        ///  Gather the sensor data with the light off
        ///  Gather the sensor data with the light on
        ///  Turn off the light
        ///  Test the various sensors according to the list of limits
        ///  
        ///     Limit param="PIRIROff"
        ///     Limit param="PIRIROn"
        ///     Limit param="PIRTemp"
        ///     Limit param="PIRMotion"
        ///     Limit param="PIRIROnDiff"
        ///     Limit param="AMBVisableOff"
        ///     Limit param="AMBVisableOn"
        ///     Limit param="AMBIROff"
        ///     Limit param="AMBIROn"
        ///     Limit param="AMBLuxOff"
        ///     Limit param="AMBLuxOn"
        ///     
        /// If the AMBSource is LED, then the ambient sensor test will be done with the
        /// Lamp off. If it is LAMP then both ambient and PIR will be done at the same time.
        /// </remarks>
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="testDB"></param>
        /// <param name="resultmessage"></param>
        /// <returns></returns>
        public int AllSensorTest(LimitsData.stepsItem parameters, FixtureDatabase.ResultData testDB, MainWindow.uutdata uutData, ref string resultmessage)
        {
            SUSensorData sdataOff = new SUSensorData();
            SUSensorData sdataOn1 = new SUSensorData();
            SUSensorData sdataOn2 = new SUSensorData();

            string outmsg;
            int testStatus = (int)ErrorCodes.Program.Success;
            int stepStatus = (int)ErrorCodes.Program.Success;
            double testvalue = 0;
            string stepresult = string.Empty;

            resultmessage = string.Empty;

            int testNumber = parameters.step_number;
            testDB.CreateStepRecord(parameters.name, testNumber);    // make the header for the test result

            // first get all the sensor data with lamp and led off
            if (!fixture.ControlPIRSource(0))                                        // just make sure it is off
            {
                resultmessage = "Problem turn off test lamp.";
                return (int)ErrorCodes.System.LabJackError;
            }
            if (!fixture.ControlAmbientSensorLightSource(0))
            {
                resultmessage = "Problem turn off test led.";
                return (int)ErrorCodes.System.LabJackError;
            }
            if (!sensorConsole.WriteLine("s l 0 0", 2000))          // make sure LEDs are off
            {
                resultmessage = "Problem issuing the led command to the dut";
                return (int)ErrorCodes.Program.DUTComError + (testNumber * 100);
            }
            sensorConsole.WaitForPrompt(1000, uutData.dvtmanPrompt , out outmsg);
            Thread.Sleep(1000);                                         // just in case it was on
            stepStatus = ReadSensors(ref sdataOff, uutData, out resultmessage);            // get the off sensor data
            if (stepStatus != (int)ErrorCodes.Program.Success)
                return stepStatus + (testNumber * 100);

            // get ambient detector data
            fixture.ControlAmbientSensorLightSource(1);
            stepStatus = ReadSensors(ref sdataOn1, uutData, out resultmessage);  // get the LED on sensor data
            fixture.ControlAmbientSensorLightSource(0);
            if (stepStatus != (int)ErrorCodes.Program.Success)
                return stepStatus + (testNumber * 100);

            // PIR is source is always the lamp, so turn on the lamp and read
            fixture.ControlPIRSource(1);
            stepStatus = ReadSensors(ref sdataOn2, uutData, out resultmessage);            // get the lamp on sensor data
            fixture.ControlPIRSource(0);
            if (stepStatus != (int)ErrorCodes.Program.Success)
                return stepStatus + (testNumber * 100);

            // now is the time to figure out what results are need
            foreach (var item in parameters.limits)
            {
                if (item.name.ToUpper().Contains("PIRIROFF"))
                    testvalue = sdataOff.PIRIR;
                if (item.name.ToUpper().Contains("PIRIRON"))
                    testvalue = sdataOn2.PIRIR;
                if (item.name.ToUpper().Contains("PIRTEMP"))
                    testvalue = sdataOn2.PIRTemperature;
                if (item.name.ToUpper().Contains("PIRMOTION"))
                    testvalue = sdataOn2.Motion;
                if (item.name.ToUpper().Contains("PIRIRONDIFF"))
                    testvalue = Math.Abs(sdataOn2.PIRIR - sdataOff.PIRIR);
                if (item.name.ToUpper().Contains("AMBVISABLEOFF"))
                    testvalue = sdataOff.AmbVisable;
                if (item.name.ToUpper().Contains("AMBVISABLEON"))
                    testvalue = sdataOn1.AmbVisable;
                if (item.name.ToUpper().Contains("AMBIROFF"))
                    testvalue = sdataOff.AmbVisable;
                if (item.name.ToUpper().Contains("AMBIRON"))
                    testvalue = sdataOn1.AmbIR;
                if (item.name.ToUpper().Contains("AMBLUXOFF"))
                    testvalue = sdataOff.AmbLux;
                if (item.name.ToUpper().Contains("AMBLUXON"))
                    testvalue = sdataOn1.AmbLux;

                if (!CheckResultValue(testvalue, item, ref stepresult))
                    stepStatus = (int)ErrorCodes.Program.ValueOutsideOfLimit + (testNumber * 100);
                else
                    stepStatus = (int)ErrorCodes.Program.Success;
                resultmessage = resultmessage + "\t  " + stepresult;
                if (stepStatus != (int)ErrorCodes.Program.Success)      // if step fail, set the test as fail
                    testStatus = stepStatus;
                if (item.type.ToUpper().Equals("RANGE"))
                    testDB.AddStepResult(item.name, (stepStatus == (int)ErrorCodes.Program.Success) ? "PASS" : "FAIL", item.operation,
                                    Convert.ToDouble(item.value1), Convert.ToDouble(item.value2), testvalue, "", "", stepresult, testStatus);
                else
                    testDB.AddStepResult(item.name, (stepStatus == (int)ErrorCodes.Program.Success) ? "PASS" : "FAIL", item.operation,
                                    Convert.ToDouble(item.value1), testvalue, "", "", stepresult, testStatus);
            }
            UpdateStatusWindow(resultmessage + "\n");
            
            return testStatus;
        }

        //-------------------------------------------------
        // CalibrateCPUTemperature
        /// <summary>
        /// Will calibrate the CPU Temperature to match the inputed temperature.
        /// If the first limit in a limit list is TempDiff, then the cpu temp will
        /// be read and compared to the probe temp.
        /// </summary>
        /// <param name="parameters">test parameters</param>
        /// <param name="testDB">result database</param>
        /// <param name="reporttext">messages if the test fail</param>
        /// <returns>
        ///     0           ErrorCodes.Program.Success
        ///     2           ErrorCodes.Program.DUTComError
        ///     100025      ErrorCodes.System.ResultDBFailure
        ///     100028      ErrorCodes.System.LabJackReadError
        /// </returns>
        public int CalibrateCPUTemperature(LimitsData.stepsItem parameters, FixtureDatabase.ResultData testDB, MainWindow.uutdata uutData, ref string reporttext)
        {
            double probeTemp = 0;
            int readCpuTemp = 0;
            double cpuTemp = 0;
            int cpuTempDiv = 1;

            int stepStatus = (int)ErrorCodes.Program.Success;

            reporttext = string.Empty;
            int testNumber = parameters.step_number;
            testDB.CreateStepRecord(parameters.name, testNumber);      // create the test record header.

            stepStatus = fixture.ReadTemperatureProbe(out probeTemp, out reporttext);
            if (stepStatus != (int)ErrorCodes.Program.Success)    // system error
            {
                testDB.AddStepResult("FAIL", reporttext, stepStatus);
                return stepStatus;
            }

            if (!sensorConsole.WriteLine("s tempcal " + Convert.ToInt32(probeTemp * 1000).ToString(), 1000))
            {
                reporttext = "Dut com error: Error writing 's tempcal' command.";
                stepStatus = (int)ErrorCodes.Program.DUTComError + (testNumber * 100);
                testDB.AddStepResult("FAIL", reporttext, stepStatus);
                return stepStatus;
            }
            if (!sensorConsole.WaitForPrompt(5000, uutData.dvtmanPrompt, out reporttext))
            {
                reporttext = "Dut com error: Error waiting for prompt after 's tempcal' command.";
                stepStatus = (int)ErrorCodes.Program.DUTComError + (testNumber * 100);
                testDB.AddStepResult("FAIL", reporttext, stepStatus);
                return stepStatus;
            }

            testDB.AddStepResult((stepStatus == (int)ErrorCodes.Program.Success) ? "PASS" : "FAIL", reporttext, stepStatus);
            testDB.AddStepRef("CalTemp", probeTemp.ToString(), "C", "N1", "");
            UpdateStatusWindow(string.Format("Caled to probe temp {0:N1} C\n", probeTemp));
           
            // if the first item in the limit list is TempDiff, then do the test
            if (parameters.limits.Count != 0)
            {
                if (parameters.limits[0].name.ToUpper().Equals("TEMPDIFF"))     // assuming cpu temp in degrees
                    cpuTempDiv = 1;
                else if (parameters.limits[0].name.ToUpper().Equals("TEMPX10DIFF")) // assuming cpu temp in degrees * 10
                    cpuTempDiv = 10;
                else
                {
                    reporttext = "Only TempDiff or Tempx10Diff are valid limits.";
                    return (int)ErrorCodes.System.InvalidParameter;
                }
                stepStatus = ReadCPUTemperature(out readCpuTemp, uutData, out reporttext);
                if (stepStatus != (int)ErrorCodes.Program.Success)        // error reading the cpu temp
                {
                    stepStatus = stepStatus + (testNumber * 100);
                    testDB.AddStepResult("FAIL", reporttext, stepStatus);
                    return stepStatus;
                }
                cpuTemp = (double)readCpuTemp / cpuTempDiv;

                if (!CheckResultValue(probeTemp - cpuTemp, parameters.limits[0], ref reporttext))
                    stepStatus = (int)ErrorCodes.Program.ValueOutsideOfLimit + (testNumber * 100);
                testDB.AddStepResult(parameters.limits[0].name, (stepStatus == (int)ErrorCodes.Program.Success) ? "PASS" : "FAIL", parameters.limits[0].operation,
                                        Convert.ToDouble(parameters.limits[0].value1), probeTemp - cpuTemp, "C", "N1", reporttext, stepStatus);
                testDB.AddStepRef("CPUTemperature", cpuTemp.ToString(), "C", "N1", "");
                UpdateStatusWindow(string.Format("Probe temp={0:N1} CPU temp={1:N1} Diff={2:N1}\n", probeTemp, cpuTemp, probeTemp - cpuTemp));
            }
            reporttext = string.Format("\t  CPU Temp Cal point = {0:N0}", probeTemp * 1000);
            return stepStatus;
        }

        //----------------------------------------
        // LEDTestManual
        /// <summary>
        /// Will do a manual LED test. It will require the operator to open the
        /// box and verify that the LEDs are working.
        /// </summary>
        /// <param name="name">string - name of the test to record in the result database.</param>
        /// <param name="testDB">FixtureDatabase.ResultDatabase - result database</param>
        /// <param name="reportText">string - if failure, details of failure</param>
        /// <returns>bool - true = passed</returns>
        public int LEDTestManual(LimitsData.stepsItem parameters, FixtureDatabase.ResultData testDB, out string reportText)
        {
            int status = (int)ErrorCodes.Program.Success;

            reportText = string.Empty;
            int testNumber = parameters.step_number;
            testDB.CreateStepRecord(parameters.name, testNumber);

            if (!sensorConsole.WriteLine("s l 1 0", 2000))
            {
                reportText = "Dut Com Error: Error sending out the command 's l 1 0'.";
                testDB.AddStepResult("FAIL", reportText, (int)ErrorCodes.Program.DUTComError);
                return (int)ErrorCodes.Program.DUTComError + (testNumber * 100);
            }
            var resultR = MessageBox.Show("Open the test box and look at the RED LED. Is it on?", "RED LED Test", MessageBoxButtons.YesNo);
            if (!sensorConsole.WriteLine("s l 0 1", 2000))
            {
                reportText = "Dut Com Error: Error sending out the command 's l 0 1'.";
                testDB.AddStepResult("FAIL", reportText, (int)ErrorCodes.Program.DUTComError);
                return (int)ErrorCodes.Program.DUTComError + (testNumber * 100);
            }
            var resultG = MessageBox.Show("Is the GREEN LED on?", "Greem LED Test", MessageBoxButtons.YesNo);
            if ((resultR == DialogResult.No) | (resultG == DialogResult.No))
                status = (int)ErrorCodes.Program.ValueOutsideOfLimit + (testNumber * 100);
            if (resultR == DialogResult.No)
                reportText = "Red LED failed. ";
            else
                reportText = "Red LED passed. ";
            if (resultG == DialogResult.No)
                reportText = reportText + "Green LED failed.";
            else
                reportText = reportText + "Green LED passed.";
            UpdateStatusWindow(reportText + "\n");
            testDB.AddStepResult((status == (int)ErrorCodes.Program.Success) ? "PASS" : "FAIL", reportText, status);
            return status;
        }

        //----------------------------------------
        // LEDTest
        /// <summary>
        /// Will do a LED test.
        /// </summary>
        /// <remarks>
        /// 
        /// </remarks>
        /// <param name="name">string - name of the test to record in the result database.</param>
        /// <param name="testDB">FixtureDatabase.ResultDatabase - result database</param>
        /// <param name="reportText">string - if failure, details of failure</param>
        /// <returns>bool - true = passed</returns>

        public int LEDTest(LimitsData.stepsItem testParams, FixtureDatabase.ResultData testDB, MainWindow.uutdata uutData,
                            string LEDDetectorType, out string reportText)
        {
            int testStatus = (int)ErrorCodes.Program.Success;
            int stepStatus = (int)ErrorCodes.Program.Success;
            int freqStatus = (int)ErrorCodes.Program.Success;
            int intensityStatus = (int)ErrorCodes.Program.Success;
            int i;
            int lowFvalue = -1;
            int highFvalue = -1;
            int lowIvalue = -1;
            int highIvalue = -1;
            string statusString = string.Empty;
            int iledFreq = 0;
            int iledIntensity = 0;
            string testName = testParams.name;
            string ledcmd = string.Empty;
            string outmsg = string.Empty;
            string[,] ledCommands = 
            {   {"DUTRED", "s led 1 0"},
                {"DUTGREEN", "s led 0 1"},
                {"BLEBLUE", "s ble 1 0"},
                {"BLEGREEN", "s ble 0 1"}
            };
            bool[,] ledTests = 
            {   {false, false},         // dutRed - color, intensity
                {false, false},         // dutGreen
                {false, false},         // bleBlue
                {false, false}          // lbeGreen
            };
            int[,] paraNumber =
            {
                {-1, -1},               // dutRed - color, intensity
                {-1, -1},               // dutGreen
                {-1, -1},               // bleBlue
                {-1, -1}                // bleGreen
            };

            int[,] colorLimits =       // will hold the color limits
            {
                {-1, -1 },
                {-1, -1 },
                {-1, -1 },
                {-1, -1 }
            };

            string[] svalues;

            bool found = false;
            string suseFixtureColorValues = "NO";

            //LimitFileFunctions.LimitFileFunctions limitFunctions = new LimitFileFunctions.LimitFileFunctions();

            int testNumber = testParams.step_number;
            testDB.CreateStepRecord(testParams.name, testNumber);

            sensorConsole.WriteLine("s l 0 0", 1000);
            sensorConsole.WaitForPrompt(1000, uutData.dvtmanPrompt, out outmsg);
            sensorConsole.WriteLine("s ble 0 0", 1000);     // this may not be a valid command, but do it anyway
            sensorConsole.WaitForPrompt(1000, uutData.dvtmanPrompt, out outmsg);

            // get any test constants
            suseFixtureColorValues = testParams.GetParameterValue("UseFixtureColorValues");
            if (suseFixtureColorValues.ToUpper() == "YES")      // if configed to use the fixture colors instead of limit colors
            {
                svalues = Regex.Split(stationConfig.ConfigLedDetector.ColorRED, @"\D+");
                colorLimits[0, 0] = Int32.Parse(svalues[0]);
                colorLimits[0, 1] = Int32.Parse(svalues[1]);
                svalues = Regex.Split(stationConfig.ConfigLedDetector.ColorGREEN, @"\D+");
                colorLimits[1, 0] = Int32.Parse(svalues[0]);
                colorLimits[1, 1] = Int32.Parse(svalues[1]);
                svalues = Regex.Split(stationConfig.ConfigLedDetector.ColorBLUE, @"\D+");
                colorLimits[2, 0] = Int32.Parse(svalues[0]);
                colorLimits[2, 1] = Int32.Parse(svalues[1]);
                svalues = Regex.Split(stationConfig.ConfigLedDetector.ColorGREEN, @"\D+");
                colorLimits[3, 0] = Int32.Parse(svalues[0]);
                colorLimits[3, 1] = Int32.Parse(svalues[1]);
            }

            // figure out what test need to be done
            i = 0;  // parameter number
            foreach (var item in testParams.limits)
	        {
                found = false;
                for (int j = 0; j < 4; j++)
			    {
			        if (item.name.ToUpper().Contains(ledCommands[j,0]))    // if found a led type/color
                    {
                        if (item.name.ToUpper().Contains("COLOR"))
                        {
                            ledTests[j,0] = true;
                            found = true;
                            paraNumber[j, 0] = i;
                            if (suseFixtureColorValues.ToUpper() != "YES")      // if not config to use the fixture colors
                            {
                                colorLimits[j, 0] = Convert.ToInt32(item.value1);   // get limit from limits
                                colorLimits[j, 1] = Convert.ToInt32(item.value2);
                            }
                        }
                        if (item.name.ToUpper().Contains("INT"))
                        {
                            ledTests[j,1] = true;
                            paraNumber[j, 1] = i;
                            found = true;
                        }
                    }
			    }
                if (!found)
                {
                    reportText = "Parameter " + item.name + " is invalid.";
                    return (int)ErrorCodes.System.InvalidParameter;
                }
                i++;
	        }


            reportText = string.Empty;
            for (i = 0; i < 4; i++)
            {
                string lclreportText = string.Empty;
                stepStatus = (int)ErrorCodes.Program.Success;

                if (ledTests[i, 0] | ledTests[i, 1])      // if this color is to test color or intensity
                {
                    UpdateStatusWindow("Tesing LED " + ledCommands[i, 0]);

                    if (!sensorConsole.WriteLine(ledCommands[i, 1], 2000))
                    {
                        reportText = "\t  Dut Com Error: Error sending out the command '" + ledcmd + "'.";
                        testDB.AddStepResult(ledCommands[i,0], "FAIL", reportText, (int)ErrorCodes.Program.DUTComError);
                        return (int)ErrorCodes.Program.DUTComError + (testNumber * 100);
                    }
                    sensorConsole.WaitForPrompt(1000, uutData.dvtmanPrompt, out outmsg);
                    Thread.Sleep(500);
                    if (ledTests[i, 0])      // if there is a freq test
                    {
                        lowFvalue = colorLimits[i, 0];
                        highFvalue = colorLimits[i, 1];
                    }
                    else
                    {
                        lowFvalue = -1;
                        highFvalue = -1;
                    }
                    if (ledTests[i, 1])      // if there is a intensity test
                    {
                        lowIvalue = Convert.ToInt32(testParams.limits[paraNumber[i, 1]].value1);
                        highIvalue = Convert.ToInt32(testParams.limits[paraNumber[i, 1]].value2);
                    }
                    else
                    {
                        lowIvalue = -1;
                        highIvalue = -1;
                    }

                    stepStatus = fixture.CheckLedValues(lowFvalue, highFvalue, lowIvalue, highIvalue,
                                                        ref freqStatus, ref intensityStatus, ref iledFreq, ref iledIntensity, ref lclreportText);
                    if (reportText == string.Empty)
                        reportText = lclreportText;
                    else
                        reportText = reportText + "\n" + lclreportText;
                    if ((stepStatus > (int)ErrorCodes.System.SystemErrorCodes))         // if there was a system error
                    {
                        testStatus = stepStatus;
                        break;
                    }
                    if (stepStatus != (int)ErrorCodes.Program.Success)
                    {
                        testStatus = stepStatus + (testNumber * 100);
                    }
                    if (ledTests[i, 0])         // color test
                    {
                        UpdateStatusWindow(" - Freq: " + (((int)ErrorCodes.Program.Success == freqStatus) ? "PASS" : "FAIL"),
                                            ((int)ErrorCodes.Program.Success == freqStatus)  ? StatusType.passed : StatusType.failed);
                        testDB.AddStepResult(testParams.limits[paraNumber[i, 0]].name, ((int)ErrorCodes.Program.Success == freqStatus) ? "PASS" : "FAIL",
                            testParams.limits[paraNumber[i, 0]].operation,
                            lowFvalue, highFvalue,
                            iledFreq, "", "", reportText, freqStatus);
                    }
                    if (ledTests[i, 1])
                    {
                        UpdateStatusWindow(" - Intensity: " + (((int)ErrorCodes.Program.Success == intensityStatus) ? "PASS" : "FAIL"),
                                            ((int)ErrorCodes.Program.Success == intensityStatus) ? StatusType.passed : StatusType.failed);
                        testDB.AddStepResult(testParams.limits[paraNumber[i, 1]].name, ((int)ErrorCodes.Program.Success == intensityStatus) ? "PASS" : "FAIL",
                            testParams.limits[paraNumber[i, 1]].operation,
                            lowIvalue, highIvalue,
                            iledIntensity, "", "", reportText, intensityStatus);
                    }
                    // make sure all leds are off at end of the test
                    if (ledTests[0, 0] | ledTests[0, 1] | ledTests[1, 0] | ledTests[1, 1])
                    {
                        sensorConsole.WriteLine("s l 0 0", 1000);
                        sensorConsole.WaitForPrompt(1000, uutData.dvtmanPrompt, out outmsg);
                    }
                    if (ledTests[2, 0] | ledTests[2, 1] | ledTests[2, 0] | ledTests[2, 1])
                    {
                        sensorConsole.WriteLine("s ble 0 0", 1000);
                        sensorConsole.WaitForPrompt(1000, uutData.dvtmanPrompt, out outmsg);
                    }
                    UpdateStatusWindow("\n");
                }
            }

            return testStatus;
        }


        //-----------------------------------------
        /// <summary>
        /// Will verfy that the CU serial port is working. Set testDB=null to not record
        /// in the database.
        /// </summary>
        /// <remarks>
        /// Optional constants:
        ///  Retrys - default 0
        ///  CUResponse - default CUver=0020
        /// </remarks>
        /// <param name="parameters">test structure</param>
        /// <param name="testDB">result object</param>
        /// <param name="reporttext">test result text</param>
        /// <returns>
        ///     0           ErrorCodes.Program.Success
        ///     1           ErrorCodes.Program.ValueOutsideOfLimit
        ///     2           ErrorCodes.Program.DUTComError
        ///     4           ErrorCodes.Program.CUComError
        /// </returns>
        public int CUPortTest(LimitsData.stepsItem parameters, FixtureDatabase.ResultData testDB, MainWindow.uutdata uutData, 
                                string dumpFileBasename, out string reporttext)
        {

            int stepStatus = (int)ErrorCodes.Program.Success;
            int maxretrycnt = 0;                        // default to 0 retrys if not defined
            int retrycnt = 0;
            string cuResponse = "CUver=0020";      // default CU response
            string rdchars = string.Empty;
            char[] rd_string = new char[6];
            byte[] rd_versionRequest = new byte[6];
            byte[] rd_versionReply = new byte[6];

            rd_versionRequest[0] = 0x00; rd_versionRequest[1] = 0xff; rd_versionRequest[2] = 0x02; rd_versionRequest[3] = 0; rd_versionRequest[4] = 0x00;
            rd_versionRequest[5] = (byte)CrcCalc(rd_versionRequest, 5);

            rd_versionReply[0] = 0x80; rd_versionReply[1] = 0xff; rd_versionReply[2] = 0x02; rd_versionReply[3] = 0; rd_versionReply[4] = 0x20;
            rd_versionReply[5] = (byte)CrcCalc(rd_versionReply, 5);

            reporttext = string.Empty;
            int testNumber = parameters.step_number;

            using (CUConsole cuConsole = new CUConsole())                               // CU serial
            {
                // open CU port
                cuConsole.DisplayRcvChar = stationConfig.ConfigDisplayRcvChar;
                cuConsole.DisplaySendChar = stationConfig.ConfigDisplaySendChar;
                cuConsole.StatusUpdated += new EventHandler<StatusUpdatedEventArgs>(StatusUpdated);
                cuConsole.SetDumpFileName(stationConfig.ConfigReportDirectory, dumpFileBasename);
                cuConsole.Open(stationConfig.ConfigCUComPort);


                if (parameters.parameters != null)
                {
                    foreach (var item in parameters.parameters)
                    {
                        if (item.name.ToUpper().Contains("RETRYS"))
                            maxretrycnt = Convert.ToInt32(item.value);
                        if (item.name.ToUpper().Contains("CURESPONSE"))
                            cuResponse = item.value;
                    }
                }
                if (testDB != null)         // if the datbase is  defined. Set to null if do not want to record
                    testDB.CreateStepRecord(parameters.name, testNumber);    // make the header for the test result

                do
                {
                    cuConsole.ClearSerialBuffers();
                    rdchars = string.Empty;
                    reporttext = string.Empty;
                    if (!sensorConsole.WriteLine(cmd_cutest, 1000))
                    {
                        reporttext = "Dut Com Error: Error writing command 'p cu-test'.";
                        stepStatus = (int)ErrorCodes.Program.DUTComError;
                        break;
                    }
                    cuConsole.RespondToCUTest(rd_versionReply, ref stepStatus, ref reporttext);
                    if (stepStatus != 0)
                        stepStatus = (int)ErrorCodes.Program.CUComError;
                    //Debug.WriteLine("Wait for cmd send." + DateTime.Now + ":" + DateTime.Now.Millisecond);
                    //if (!cuConsole.ReadChars(ref rdchars, 6, 1000))
                    //{
                    //    reporttext = "CU Com Error: CU read error. Got: " + rdchars;
                    //    stepStatus = (int)ErrorCodes.Program.CUComError + (testNumber * 100);
                    //    break;
                    //}
                    //Debug.WriteLine("send response1. " + DateTime.Now + ":" + DateTime.Now.Millisecond);
                    //if (!cuConsole.WriteChars(rd_versionReply, 6))
                    //{
                    //    reporttext = "CU Com Error: CU write error.";
                    //    stepStatus = (int)ErrorCodes.Program.CUComError + (testNumber * 100);
                    //    break;
                    //}
                    //rd_string = rdchars.ToArray();
                    if (!sensorConsole.ReadLine(ref rdchars, 1000))
                    {
                        reporttext = "Dut Com Error: Dut read of the cu test results failed.";
                        stepStatus = (int)ErrorCodes.Program.DUTComError + (testNumber * 100);
                        break;
                    }
                    if (rdchars.Contains(cuResponse))
                        stepStatus = (int)ErrorCodes.Program.Success;
                    else
                    {
                        stepStatus = (int)ErrorCodes.Program.ValueOutsideOfLimit + (testNumber * 100);
                    }
                    if (!sensorConsole.WaitForPrompt(1000, uutData.dvtmanPrompt, out rdchars))
                    {
                        reporttext = "Dut Com Error: Dut did not get the prompt.";
                        stepStatus = (int)ErrorCodes.Program.DUTComError + (testNumber * 100);
                        break;
                    }
                } while ((retrycnt++ < maxretrycnt) & (stepStatus != (int)ErrorCodes.Program.Success));

            }

            if (reporttext == string.Empty)
                if (retrycnt > 1)
                    reporttext = string.Format("Retrys = {0}", retrycnt);
            if (testDB != null)
                testDB.AddStepResult("CUResponse", ((stepStatus == (int)ErrorCodes.Program.Success) ? "PASS" : "FAIL"), "EQ", cuResponse, rdchars,
                    reporttext, stepStatus);


            return stepStatus;
        }

        //-----------------------------------------
        /// <summary>
        /// Will verfy that the CU serial port is working.
        /// </summary>
        /// <remarks>
        /// Optional constants:
        ///  Retrys - default 0
        ///  CUResponse - default CUver=0020
        /// </remarks>
        /// <param name="parameters">test structure</param>
        /// <param name="testDB">result object</param>
        /// <param name="reporttext">test result text</param>
        /// <returns>
        ///     0           ErrorCodes.Program.Success
        ///     1           ErrorCodes.Program.ValueOutsideOfLimit
        ///     2           ErrorCodes.Program.DUTComError
        ///     4           ErrorCodes.Program.CUComError
        /// </returns>
        public int DaliPortTest(LimitsData.stepsItem parameters, StationConfigure stationConfig, FixtureDatabase.ResultData testDB, MainWindow.uutdata uutData, out string reporttext)
        {

            int stepStatus = (int)ErrorCodes.Program.Success;
            string cuResponse = "CUver=" + stationConfig.ConfigCUVersionTag;
            string outmsg = string.Empty;


            reporttext = string.Empty;
            testDB.CreateStepRecord(parameters.name, parameters.step_number);    // make the header for the test result

            doDaliPortTest(parameters, ref uutData, ref reporttext, ref stepStatus, cuResponse);

            testDB.AddStepResult("CUResponse", ((stepStatus == (int)ErrorCodes.Program.Success) ? "PASS" : "FAIL"), "EQ", cuResponse, outmsg,
                reporttext, stepStatus);


            return stepStatus;
        }

        /// <summary>
        /// Dali Port test without the database stuff
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="uutData"></param>
        /// <param name="reporttext"></param>
        /// <param name="stepStatus"></param>
        /// <param name="maxretrycnt"></param>
        /// <param name="retrycnt"></param>
        /// <param name="cuResponse"></param>
        /// <param name="outmsg"></param>
        public void doDaliPortTest(LimitsData.stepsItem parameters, ref MainWindow.uutdata uutData, ref string reporttext, ref int stepStatus, string cuResponse)
        {
            int maxretrycnt = 0;                        // default to 0 retrys if not defined
            int retrycnt = 0;
            string outmsg = string.Empty;

            foreach (var item in parameters.parameters)
            {
                if (item.name.ToUpper().Contains("RETRYS"))
                    maxretrycnt = Convert.ToInt32(item.value);
            }

            do
            {
                reporttext = string.Empty;

                if (!sensorConsole.WriteLine("p cu-test", 1000))
                {
                    reporttext = "Dut Com Error: Error writing command 'p cu-test'.";
                    stepStatus = (int)ErrorCodes.Program.DUTComError + (parameters.step_number * 100);
                    break;
                }
                if (!sensorConsole.WaitForPrompt(2000, uutData.dvtmanPrompt, out outmsg))
                {
                    reporttext = "DUT Com Error: Did not get prompt. Got: " + outmsg;
                    stepStatus = (int)ErrorCodes.Program.CUComError + (parameters.step_number * 100);
                    break;
                }
                if (outmsg.Contains(cuResponse))
                    stepStatus = (int)ErrorCodes.Program.Success;
                else
                {
                    stepStatus = (int)ErrorCodes.Program.ValueOutsideOfLimit + (parameters.step_number * 100);
                    reporttext = "Did not read a response of: " + cuResponse + ".\nGot: " + outmsg;
                }
            } while ((retrycnt++ < maxretrycnt) & (stepStatus != (int)ErrorCodes.Program.Success));
            if (retrycnt != 0)
                reporttext = string.Format("{0}\nRetrys = {1}", reporttext, retrycnt);
        }


        //-----------------------------------------
        // CheckPOST
        /// <summary>
        /// Will verify that the POST message 'POST PASS' is found.
        /// Return codes:
        ///     0           ErrorCodes.Program.Success
        ///     1           ErrorCodes.Program.ValueOutsideOfLimit
        ///     2           ErrorCodes.Program.DUTComError
        /// </summary>
        /// <param name="reporttext">string - returns string.Empty if POST PASS is found. Returns
        /// the failure message if there is a failure.</param>
        /// <returns>bool - true = POST PASS was found.</returns>
        public int CheckPOST(LimitsData.stepsItem testToRun, FixtureDatabase.ResultData testDB, MainWindow.uutdata uutData, out string reporttext)
        {
            string outmsg = string.Empty;
            int status = (int)ErrorCodes.Program.Success;

            testDB.CreateStepRecord(testToRun.name, testToRun.step_number);
            reporttext = string.Empty;
            if (!sensorConsole.WriteLine("p post", 1000))
            {
                reporttext = "Dut Com Error: Error sending the 'p post' command.";
                status = (int)ErrorCodes.Program.DUTComError + (testToRun.step_number * 100);
            }
            if (status == (int)ErrorCodes.Program.Success)
            {
                if (!sensorConsole.WaitForPrompt(10000, uutData.dvtmanPrompt, out outmsg))
                {
                    reporttext = "Dut Com Error: Error waiting for 'p post' post prompt." + outmsg;
                    status = (int)ErrorCodes.Program.DUTComError + (testToRun.step_number * 100);
                }
                else
                {
                    if (!outmsg.Contains("POST PASS"))
                    {
                        reporttext = "POST Failure: POST message is: " + outmsg;
                        status = (int)ErrorCodes.Program.ValueOutsideOfLimit + (testToRun.step_number * 100);
                    }
                }
            }
            testDB.AddStepResult((status == (int)ErrorCodes.Program.Success) ? "PASS" : "FAIL", reporttext, status);

            return status;
        }

        //-------------------------------------------------
        // ChangeOver
        /// <summary> 
        /// Will do the change over function. Checks for the following messages:
        ///     INFO: copying new app to internal flash ... done
        ///     SU (image=2) version: ---loadedversion---
        ///     HW INIT OK
        ///     SU>
        /// If the text 'Error'(case insensitive) is found, the test will fail.
        /// Will look for the string in the 'Find' operation limits.
        /// </summary>
        /// <remarks>
        /// Typical change over looks like below:
        /// ---- if EnableBLEChangeover = YES
        ///DVTMAN>p ble-spi 1
        ///p ble-spi 1			     response
        ///BLE-spi: Copy IMG 2		 response
        ///RD = 13, ER, WR = 1,Done    response
        /// --- will verify that 1 is written. retry once.
        ///DVTMAN>				     response
        ///---- end
        /// DVTMAN> p change
        /// 
        /// 
        /// enLighted Boot Version 02.07
        /// 
        /// INFO: copying new app to internal flash ... done
        /// 
        /// 
        /// enLighted Boot Version 02.07
        /// 
        /// 
        /// SU (image=2) version: 2.7.5.3127, built by jenkins, on Tue May 26 16:26:36 PDT 2015
        /// 
        /// Reset by WDOG
        /// 
        /// HW INIT OK
        /// 
        /// Smart serial: YES
        /// Product code: S4U
        /// Oscillator trim: 0xFB
        /// FCV calibration: OFF
        /// FCV WGM mode: FAST PWM, 10-BIT
        /// Motion scaling: 5/5ths
        /// Ambient scaling: OFF
        /// Networking Behavior: STANDARD
        /// Sensors: AMB-RGB, MOT-PIR
        /// Controller IFC: DALI
        /// CPU ID: 2
        /// Motion Algo: V2
        /// BLE: YES
        /// SPI WP PIN: PB4
        /// SPI CS PIN: PD7
        /// 
        /// SU>
        /// </remarks>
        /// <param name="reporttext">string - output of the dut if there was a error.</param>
        /// <returns>bool - true = all messages found</returns>
        public int ChangeOver(LimitsData.stepsItem testToRun, FixtureDatabase.ResultData testDB, MainWindow.uutdata uutData, out string reporttext)
        {
            int retryCnt = 0;
            string outmsg = string.Empty;
            string fullBootString = string.Empty;
            int status = (int)ErrorCodes.Program.Success;
            string loadVersion = string.Empty;
            int waitTimeSec = 10;
            int offDelaySec = 10;
            bool enableBLEChangeover = false;           // set with test constant EnableBLEChangeover
            LimitsData.limitsItem paramStruct = null;
            DateTime changeOverStartTime = DateTime.Now;                // time when p changeover was issued

            reporttext = string.Empty;

            testDB.CreateStepRecord(testToRun.name, testToRun.step_number);

            if (testToRun.GetParameterValue("SUTimeoutSec") == string.Empty)
            {
                reporttext = "Error reading SUTimeoutSec." + outmsg;
                status = (int)ErrorCodes.System.MissingParameter;
            }
            else
            {
                if (!int.TryParse(testToRun.GetParameterValue("SUTimeoutSec"), out waitTimeSec))
                {
                    reporttext = "Error try to convert SUTimeoutSec(" + testToRun.GetParameterValue("SUTimeoutSec") + ") to a number.";
                    status = (int)ErrorCodes.System.InvalidParameter;
                }
            }
            if (testToRun.GetParameterValue("DelayOffSec") == string.Empty)
            {
                reporttext = "Error reading DelayOffSec." + outmsg;
                status = (int)ErrorCodes.System.MissingParameter;
            }
            else
            {
                if (!int.TryParse(testToRun.GetParameterValue("DelayOffSec"), out offDelaySec))
                {
                    reporttext = "Error try to convert DelayOffSec(" + testToRun.GetParameterValue("DelayOffSec") + ") to a number.";
                    status = (int)ErrorCodes.System.InvalidParameter;
                }
            }
            if (testToRun.GetParameterValue("EnableBLEChangeover") != string.Empty)
            {
                if (testToRun.GetParameterValue("EnableBLEChangeover").ToUpper().Equals("YES"))
                    enableBLEChangeover = true;
            }
            if (testToRun.GetLimitValues("AppVersion") == null)
            {
                reporttext = "List parameter AppVersion is missing.";
                status = (int)ErrorCodes.System.MissingParameter;
            }
            else
                paramStruct = testToRun.GetLimitValues("AppVersion");

            // if there are errors reading the limit file
            if (status != (int)ErrorCodes.Program.Success)
                return status;

            reporttext = string.Empty;
            // if the ble is enabled to changeover
            if (enableBLEChangeover)
            {
                UpdateStatusWindow("  Enable BLE changeover..");
                // will set the flash to make the start command load from ble image 2
                status = (int)ErrorCodes.Program.Success;
                if (!sensorConsole.WriteLine("p ble-spi 1", 2000))
                {
                    reporttext = "Error sending 'p ble-spi 1' command.";
                    status = (int)ErrorCodes.Program.DutUnexpectedCmdReturn;
                    UpdateStatusWindow(" Send FAIL\n", StatusType.failed);
                }
                if (status == (int)ErrorCodes.Program.Success)
                {
                    if (!sensorConsole.WaitForDVTMANPrompt(10000, out outmsg))
                    {
                        reporttext = "Error waiting for the  'p ble-spi 1' reponse prompt.";
                        status = (int)ErrorCodes.Program.DutUnexpectedCmdReturn;
                        UpdateStatusWindow(" Send FAIL\n", StatusType.failed);
                    }
                }
                if (status == (int)ErrorCodes.Program.Success)
                {
                    sensorConsole.WriteLine("p ble-spi", 2000);       // verify it wrote correctlly
                    sensorConsole.WaitForDVTMANPrompt(2000, out outmsg);
                    if (!outmsg.Contains("RD=1"))                       // if it does not have the correct value
                    {
                        reporttext = "p ble-spi 1 did not set the correct value. Read:" + outmsg;
                        status = (int)ErrorCodes.Program.DutUnexpectedCmdReturn;
                        UpdateStatusWindow(" Readback FAIL\n", StatusType.failed);
                    }
                    else
                    {
                        UpdateStatusWindow(" PASS\n");
                    }
                }
            }
            if (status == (int)ErrorCodes.Program.Success)
            {
                changeOverStartTime = DateTime.Now;                         // save starting time for overall timeout
                UpdateStatusWindow("  Issued 'p change' command.. ");
                if (!sensorConsole.WriteLine("p change", 1000))
                {
                    reporttext = "Dut Com Error: Error sending the 'p change' command.";
                    status = (int)ErrorCodes.Program.DUTComError + (testToRun.step_number * 100);
                    UpdateStatusWindow(" FAILED\n", StatusType.failed);
                }
                UpdateStatusWindow("  PASSED\n");
            }
            if (status == (int)ErrorCodes.Program.Success)
            {
                UpdateStatusWindow("  Waiting for flash copy to finish");
                                                                                // the time out is configured timeout - time passed since p change issued
                if (!sensorConsole.WaitForPrompt((waitTimeSec - (DateTime.Now - changeOverStartTime).Seconds) * 1000, "INFO: copying new app to internal flash ... done", out outmsg))
                {
                    reporttext = "Dut Com Error: Error waiting for '... done' post prompt. Found:\n" + sensorConsole.GetCopyOfSerialBuffer();
                    status = (int)ErrorCodes.Program.DutUnexpectedCmdReturn;
                    UpdateStatusWindow(" Copy wait fail\n", StatusType.failed);
                }
                else
                {
                    UpdateStatusWindow(" Copy done\n");
                }
                fullBootString = outmsg;
            }
            if (status == (int)ErrorCodes.Program.Success)
            {
                UpdateStatusWindow("  Waiting for version info..");
                if (!sensorConsole.WaitForPrompt((waitTimeSec - (DateTime.Now - changeOverStartTime).Seconds) * 1000, "SU (image=2) version: ", out outmsg))
                {
                    reporttext = "Dut Com Error: Error waiting for 'SU (image=2)' post prompt." + outmsg;
                    status = (int)ErrorCodes.Program.DUTComError;
                    UpdateStatusWindow(" Info not found\n", StatusType.failed);
                }
                fullBootString = fullBootString + outmsg;
            }
            if (status == (int)ErrorCodes.Program.Success)
            {
                sensorConsole.ReadLine(ref outmsg, 500);       // get the rest of the line which will have version
                if (!outmsg.Contains(paramStruct.value1))
                {
                    reporttext = "App version is wrong. Found " + outmsg;
                    status = (int)ErrorCodes.Program.ValueOutsideOfLimit;
                    UpdateStatusWindow(" Info bad\n", StatusType.failed);
                }
                else
                {
                    UpdateStatusWindow(" Info good\n");
                }
                fullBootString = fullBootString + outmsg;
            }
            if (status == (int)ErrorCodes.Program.Success)
            {
                UpdateStatusWindow("  Waiting for HW INT OK message..");
               if (!sensorConsole.WaitForPrompt((waitTimeSec - (DateTime.Now - changeOverStartTime).Seconds) * 1000, "HW INIT OK", out outmsg))
                {
                    reporttext = "Dut Com Error: Error waiting for 'HW INIT OK' post prompt." + outmsg;
                    status = (int)ErrorCodes.Program.DUTComError;
                    UpdateStatusWindow("FAILED\n", StatusType.failed);
                }
                else
                {
                    UpdateStatusWindow("Found\n");
                }
               fullBootString = fullBootString + outmsg;
            }
            if (status == (int)ErrorCodes.Program.Success)
            {
                UpdateStatusWindow("  Waiting for finial prompt.. ");
                if (!sensorConsole.WaitForPrompt((waitTimeSec - (DateTime.Now - changeOverStartTime).Seconds) * 1000, uutData.appPrompt, out outmsg))
                {
                    reporttext = "Dut Com Error: Error waiting for '" + uutData.appPrompt + "' post prompt.\n" + outmsg;
                    status = (int)ErrorCodes.Program.DUTComError;
                    UpdateStatusWindow(" Prompt not found\n", StatusType.failed);
                }
                else
                {
                    UpdateStatusWindow(" Prompt found\n");
                }
                fullBootString = fullBootString + outmsg;
            }

            if (status == (int)ErrorCodes.Program.Success)
            {
                // Look for the 'Find' operation strings
                foreach (var item in testToRun.limits)
                {
                    if (item.operation.ToUpper().Equals("FIND"))
                    {
                        UpdateStatusWindow("Looking for string:" + item.value1 + "..");
                        if (!fullBootString.Contains(item.value1))
                        {
                            reporttext = "Did not find the string " + item.value1 + " at bootup";
                            status = (int)ErrorCodes.Program.ValueOutsideOfLimit;
                            UpdateStatusWindow(" String not found\n", StatusType.failed);
                            break;
                        }
                        UpdateStatusWindow(" String found\n");
                    }
                }
            }

            // see if there are any Error text.
            if (fullBootString.ToUpper().Contains("ERROR"))
            {
                int starttextindex = fullBootString.ToUpper().IndexOf("ERROR");
                int endtextindex = fullBootString.IndexOf('\n', starttextindex);
                reporttext = "'Error' was found at bootup. " + fullBootString.Substring(starttextindex, endtextindex - starttextindex);
                status = (int)ErrorCodes.Program.ValueOutsideOfLimit;

            }

            if (status == (int)ErrorCodes.Program.Success)
            {
                UpdateStatusWindow("  Delay to let unit initialize\n");
                Thread.Sleep(offDelaySec * 1000);
            }

            if (status != (int)ErrorCodes.Program.Success)
                if (status < (int)ErrorCodes.System.SystemErrorCodes)
                    status = status + (testToRun.step_number * 100);

            testDB.AddStepResult((status == (int)ErrorCodes.Program.Success) ? "PASS":"FAIL", reporttext, status);

            return status;
        }

        /// <summary>
        /// print a label.
        /// </summary>
        /// <param name="testToRun"></param>
        /// <param name="testDB"></param>
        /// <param name="uutData"></param>
        /// <param name="printerName"></param>
        /// <param name="resultmessage"></param>
        /// <param name="lastMacBarcodeFileName"></param>
        /// <returns></returns>
        public int PrintPcbaLabel(LimitsData.stepsItem testToRun, FixtureDatabase.ResultData testDB, MainWindow.uutdata uutData, string printerName,
                                    out string resultmessage, ref string lastMacBarcodeFileName)
        {
            string outmsg = string.Empty;
            int stepStatus = (int)ErrorCodes.Program.Success;
            string fullLabelName = lastMacBarcodeFileName;
            string printedData = string.Empty;
            int numLabelsToPrint = 1;
            string snumLabelsToPrint = "1";

            resultmessage = string.Empty;
            testDB.CreateStepRecord(testToRun.name, testToRun.step_number);
            lastMacBarcodeFileName = testToRun.GetParameterValue("BarcodeFile");
            if (testToRun.GetParameterValue("NumberOfLabelsToPrint") != string.Empty)
            {
                if (!int.TryParse(testToRun.GetParameterValue("NumberOfLabelsToPrint"), out numLabelsToPrint))
                {
                    stepStatus = (int)ErrorCodes.System.FileOpenError;
                    resultmessage = "System Error: The NumberOfLabelsToPrint value is not a interger. Found:" + snumLabelsToPrint;
                    testDB.AddStepResult("FAIL", resultmessage, stepStatus);
                }
            }

            //--------------------------------------------------------------------------//
            // read the test label file                                                 //
            // the fixture label directory will override the station limit directory.   //
            //--------------------------------------------------------------------------//
            if (stationConfig.ConfigFixtureLabelFileDirectory == string.Empty)
            {
                if (stationConfig.ConfigLabelFileDirectory == string.Empty)     // if no directory is defined
                {
                    String strAppDir = Path.GetDirectoryName(Application.ExecutablePath);
                    fullLabelName = Path.Combine(strAppDir, lastMacBarcodeFileName);
                }
                else                                                            // directory is defined
                {
                    fullLabelName = Path.Combine(stationConfig.ConfigLabelFileDirectory, lastMacBarcodeFileName);
                }
            }
            else
                fullLabelName = Path.Combine(stationConfig.ConfigFixtureLabelFileDirectory, lastMacBarcodeFileName);


            if (!File.Exists(fullLabelName))
            {
                stepStatus = (int)ErrorCodes.System.FileOpenError;
                resultmessage = "System Error: The barcode file " + fullLabelName + " is missing.";
                testDB.AddStepResult("FAIL", resultmessage, stepStatus);
            }
            if (stepStatus == (int)ErrorCodes.Program.Success)
            {
                try
                {
                    var btengine = new Engine(true);
                    var btformat = btengine.Documents.Open(fullLabelName);
                    btformat.PrintSetup.PrinterName = printerName;
                    btformat.PrintSetup.IdenticalCopiesOfLabel = numLabelsToPrint;
                    foreach (var item in btformat.SubStrings.ToList())
                    {
                        if (item.Name == "MAC")
                        {
                            btformat.SubStrings["MAC"].Value = uutData.mac;
                            printedData = uutData.mac;
                        }
                        else if (item.Name == "SN")
                        {
                            btformat.SubStrings["SN"].Value = uutData.pcbaSN;
                            if (printedData != string.Empty)
                                printedData = printedData + ":";
                            printedData = printedData + uutData.pcbaSN;
                        }
                        else if (item.Name == "PCBA_SN")
                        {
                            btformat.SubStrings["PCBA_SN"].Value = uutData.pcbaSN;
                            if (printedData != string.Empty)
                                printedData = printedData + ":";
                            printedData = printedData + uutData.pcbaSN;
                        }
                        else if (item.Name == "Product_SN")
                        {
                            btformat.SubStrings["Product_SN"].Value = uutData.topSN;
                            if (printedData != string.Empty)
                                printedData = printedData + ":";
                            printedData = printedData + uutData.topSN;
                        }
                        else
                        {
                            stepStatus = (int)ErrorCodes.System.LabelPrintError;
                            resultmessage = "System Error: There is a invalid field(" + item.Name + ") in the barcode file.";
                            testDB.AddStepResult("FAIL", resultmessage, stepStatus);
                            return stepStatus;
                        }

                    }
                    btformat.Save();
                    Result result = btformat.Print();
                    btformat.Close(SaveOptions.DoNotSaveChanges);
                    btengine.Dispose();
                    resultmessage = "Label printed: " + printedData;
                    stepStatus = (int)ErrorCodes.Program.Success;
                    testDB.AddStepResult("PASS", resultmessage, stepStatus);
                }
                catch (Exception ex)
                {
                    stepStatus = (int)ErrorCodes.System.LabelPrintError;
                    resultmessage = "System Error: " + ex.Message;
                    testDB.AddStepResult("FAIL", resultmessage, stepStatus);
                }
            }
            if ((stepStatus < (int)ErrorCodes.System.SystemErrorCodes) & (stepStatus != (int)ErrorCodes.Program.Success))
                stepStatus = stepStatus + (testToRun.step_number * 100);
            return stepStatus;
        }

        public int PrintProductLabel(LimitsData.stepsItem testToRun, FixtureDatabase.ResultData testDB, MainWindow.uutdata uutData, string printerName,
                                    out string resultmessage, ref string lastMacBarcodeFileName)
        {
            string outmsg = string.Empty;
            int stepStatus = (int)ErrorCodes.Program.Success;
            string fullLabelName = lastMacBarcodeFileName;
            string printedData = string.Empty;
            string snumLabelsToPrint = "1";
            int numLabelsToPrint = 1;

            resultmessage = string.Empty;
            testDB.CreateStepRecord(testToRun.name, testToRun.step_number);
            if (!testToRun.DoesParameterExist("BarcodeFile"))
            {
                stepStatus = (int)ErrorCodes.System.MissingParameter;
                resultmessage = "System Error: Missing parameter BarcodeFile";
                testDB.AddStepResult("FAIL", resultmessage, stepStatus);
                return stepStatus;
            }
            lastMacBarcodeFileName = testToRun.GetParameterValue("BarcodeFile");
            if (testToRun.DoesParameterExist("NumberOfLabelsToPrint"))
            {
                if (!int.TryParse(testToRun.GetParameterValue("NumberOfLabelsToPrint"), out numLabelsToPrint))
                {
                    stepStatus = (int)ErrorCodes.System.InvalidParameter;
                    resultmessage = "System Error: The NumberOfLabelsToPrint value is not a interger. Found:" + snumLabelsToPrint;
                    testDB.AddStepResult("FAIL", resultmessage, stepStatus);
                    return stepStatus;
                }
            }
            else
            {
                stepStatus = (int)ErrorCodes.System.MissingParameter;
                resultmessage = "System Error: Missing parameter NumberOfLabelsToPrint";
                testDB.AddStepResult("FAIL", resultmessage, stepStatus);
                return stepStatus;
            }

            //--------------------------------------------------------------------------//
            // read the test label file                                                 //
            // the fixture label directory will override the station limit directory.   //
            //--------------------------------------------------------------------------//
            if (stationConfig.ConfigLabelFileDirectory == string.Empty)     // if no directory is defined
            {
                String strAppDir = Path.GetDirectoryName(Application.ExecutablePath);
                fullLabelName = Path.Combine(strAppDir, lastMacBarcodeFileName);
            }
            else                                                            // directory is defined
            {
                fullLabelName = Path.Combine(stationConfig.ConfigLabelFileDirectory, lastMacBarcodeFileName);
            }

            if (!File.Exists(fullLabelName))
            {
                stepStatus = (int)ErrorCodes.System.FileOpenError;
                resultmessage = "System Error: The barcode file " + fullLabelName + " is missing.";
                testDB.AddStepResult("FAIL", resultmessage, stepStatus);
                return stepStatus;
            }
            if (stepStatus == (int)ErrorCodes.Program.Success)
            {
                try
                {
                    var btengine = new Engine(true);
                    var btformat = btengine.Documents.Open(fullLabelName);
                    btformat.PrintSetup.PrinterName = printerName;
                    btformat.PrintSetup.IdenticalCopiesOfLabel = numLabelsToPrint;
                    foreach (var item in btformat.SubStrings.ToList())
                    {
                        if (item.Name == "MAC")
                        {
                            btformat.SubStrings["MAC"].Value = uutData.mac;
                            printedData = uutData.mac;
                        }
                        else if (item.Name == "SN")
                        {
                            btformat.SubStrings["SN"].Value = uutData.topSN;
                            if (printedData != string.Empty)
                                printedData = printedData + ":";
                            printedData = printedData + uutData.topSN;
                        }
                        else if (item.Name == "PCBA_SN")
                        {
                            btformat.SubStrings["PCBA_SN"].Value = uutData.pcbaSN;
                            if (printedData != string.Empty)
                                printedData = printedData + ":";
                            printedData = printedData + uutData.pcbaSN;
                        }
                        else if (item.Name == "Product_SN")
                        {
                            btformat.SubStrings["Product_SN"].Value = uutData.topSN;
                            if (printedData != string.Empty)
                                printedData = printedData + ":";
                            printedData = printedData + uutData.topSN;
                        }
                        else
                        {
                            stepStatus = (int)ErrorCodes.System.LabelPrintError;
                            resultmessage = "System Error: There is a invalid field(" + item.Name + ") in the barcode file.";
                            testDB.AddStepResult("FAIL", resultmessage, stepStatus);
                            return stepStatus;
                        }

                    }
                    btformat.Save();
                    Result result = btformat.Print();
                    btformat.Close(SaveOptions.DoNotSaveChanges);
                    btengine.Dispose();
                    resultmessage = "Label printed: " + printedData;
                    stepStatus = (int)ErrorCodes.Program.Success;
                    testDB.AddStepResult("PASS", resultmessage, stepStatus);
                }
                catch (Exception ex)
                {
                    stepStatus = (int)ErrorCodes.System.LabelPrintError;
                    resultmessage = "System Error: " + ex.Message;
                    testDB.AddStepResult("FAIL", resultmessage, stepStatus);
                }
            }
            if ((stepStatus < (int)ErrorCodes.System.SystemErrorCodes) & (stepStatus != (int)ErrorCodes.Program.Success))
                stepStatus = stepStatus + (testToRun.step_number * 100);
            return stepStatus;
        }

        /// <summary>
        /// Turn the DUT power on and wait for the DVTMAN prompt. Verify firmware version if
        /// configured in the limits.
        /// </summary>
        /// Apply power to the unit and wait for the DVTMAN prompt(uutData.dvtmanPrompt). The timeout will be defined
        /// in a TestConstant param = WaitTimeSecs. If the TestConstant does not exits, 3 seconds will be used.
        /// If there is a Limit with the param = DVTMANVersion, then the
        /// boot up message will be checked for Limit param = DVTMANVersion
        /// <param name="testToRun"></param>
        /// <param name="testDB"></param>
        /// <param name="uutData"></param>
        /// <param name="resultmsg"></param>
        /// <returns></returns>
        public int TurnOnDut(LimitsData.stepsItem testToRun, FixtureDatabase.ResultData testDB, MainWindow.uutdata uutData, ref string resultmsg)
        {
            int status = (int)ErrorCodes.Program.Success;
            int startupDelayMs = 4000;
            string version = string.Empty;
            bool reseat = false;
            int retrys = 0;
            int currentRetryCnt = 0;
            string outbuf = string.Empty;
            double volts = 0;
            LimitsData.limitsItem limitsItem;

            resultmsg = string.Empty;

            if (testToRun.parameters != null)
            {
                if (testToRun.GetParameterValue("WaitTimeSecs") != string.Empty)
                    startupDelayMs = Convert.ToInt32(testToRun.GetParameterValue("WaitTimeSecs")) * 1000;
                if (testToRun.GetParameterValue("Reseat") != string.Empty)
                    reseat = true;
                if (testToRun.GetParameterValue("Retrys") != string.Empty)
                    retrys = Convert.ToInt32(testToRun.GetParameterValue("Retrys"));
            }

            if (testToRun.limits != null)
            {
                if (testToRun.DoesLimitExist("DVTMANVersion"))
                {
                    if ((limitsItem = testToRun.GetLimitValues("DVTMANVersion")).name != null)
                    {
                        version = limitsItem.value1;
                    }
                }
            }

            // if the limit file defines a test number, use it
            testDB.CreateStepRecord(testToRun.name, testToRun.step_number);

            sensorConsole.ClearBuffers();
            fixture.ControlDUTPower(1);             // turn on the DUT power

            do
            {
                status = (int)ErrorCodes.Program.Success;
                Debug.WriteLine("Turn On DUT. Start looking for prompt. " + DateTime.Now + ":" + DateTime.Now.Millisecond);
                if (!sensorConsole.WaitForPrompt(startupDelayMs, uutData.dvtmanPrompt, out resultmsg))
                {
                    Debug.WriteLine("Turn On DUT. Failed looking for DVTMAN prompt. " + DateTime.Now + ":" + DateTime.Now.Millisecond);
                    if (sensorConsole.GetCopyOfSerialBuffer().Contains(uutData.appPrompt))
                    {
                        resultmsg = string.Format("App prompt " + uutData.appPrompt + " found. Unit has done a changeover.\n" + sensorConsole.GetCopyOfSerialBuffer());
                        status = (int)ErrorCodes.Program.ChangedOver + (testToRun.step_number * 100);
                        currentRetryCnt = 999;         // force to end
                        testDB.AddStepResult("FAIL", resultmsg, status);
                    }
                    else if (sensorConsole.GetCopyOfSerialBuffer().Contains("Resetting system"))    // if there is a hardware error dectected
                    {
                        outbuf = sensorConsole.GetCopyOfSerialBuffer();
                        resultmsg = string.Format("Power up hardware error found. {0}", outbuf);
                        status = (int)ErrorCodes.Program.DUTHardwareError + (testToRun.step_number * 100);
                        currentRetryCnt = 999;         // force to end
                        testDB.AddStepResult("FAIL", resultmsg, status);
                    }
                    else
                    {
                        resultmsg = string.Format("{0} prompt not found. Found: <start>{1}<end>",
                                            uutData.dvtmanPrompt, sensorConsole.GetCopyOfSerialBuffer());
                        Debug.WriteLine("Turn On DUT. prompt not found.");
                        Debug.Flush();
                        //sensorConsole.Close();                              // reopen the port just in case
                        //Thread.Sleep(100);
                        //sensorConsole.Open(stationConfig.ConfigDUTComPort);
                        try
                        {
                            sensorConsole.ClearBuffers();
                            if ((currentRetryCnt < retrys) & reseat)
                            {
                                status = (int)ErrorCodes.Program.ValueOutsideOfLimit + (testToRun.step_number * 100);
                                ReloadBanner rlb = new ReloadBanner(fixture);
                                if (rlb.ShowDialog() == DialogResult.Abort)
                                {
                                    currentRetryCnt = 999;                    // force to exit
                                    status = (int)ErrorCodes.Program.OperatorAbort + (testToRun.step_number * 100);
                                    testDB.AddStepResult("FAIL", resultmsg, status);
                                }
                                rlb.Dispose();
                                testDB.AddStepRef("PowerOnRetry", (currentRetryCnt + 1).ToString(), "", "", "");
                            }
                            else  // the prompt was not found after the reseats
                            {
                                status = (int)ErrorCodes.Program.DutUnexpectedCmdReturn + (testToRun.step_number * 100);
                                testDB.AddStepResult("FAIL", resultmsg, status);
                            }

                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("error getting logon stuff." + ex.Message);
                        }
                    }
                }

                volts = fixture.Read3p3Voltage();
                testDB.AddStepRef("PowerOn3p3", volts.ToString("N2"), "V", "N2", "");
                UpdateStatusWindow("DUT 3.3V = " + volts.ToString("N2") + "\n", StatusType.text);

                // if DVTMAN prompt found, check version
                if ((version != string.Empty) && (status == (int)ErrorCodes.Program.Success))
                {
                    if (!resultmsg.Contains(version))
                    {
                        status = (int)ErrorCodes.Program.ValueOutsideOfLimit + (testToRun.step_number * 100);
                        resultmsg = string.Format("Version {0} not found. Found: <start>{1}<end>", version, resultmsg);
                        currentRetryCnt = 999;          // force to end
                    }
                    testDB.AddStepResult("DVTMANVersion", resultmsg.Contains(version) ? "PASS" : "FAIL", "EQ", version, resultmsg, resultmsg, status);
                }

            } while ((status != (int)ErrorCodes.Program.Success) & (currentRetryCnt++ < retrys));
                                                        // look for the DVTMAN prompt
            return status;
        }

        //-----------------------------------------
        // CheckFlash
        /// <summary>
        /// Will verify that the flash can be read.
        /// </summary>
        /// <remarks>
        /// Will do a 'd flash address', where address/length is specified in the constant
        /// Address(ie 0x80000 10), and verify that the text 'Read external flash failed' does not appear,
        /// and that no 'FF' appear.
        /// </remarks>
        ///<param name="reporttext"></param>
        ///<param name="testDB"></param>
        ///<param name="testToRun"></param>
        ///<param name="uutData"></param>
        public int CheckFlash(LimitsData.stepsItem testToRun, FixtureDatabase.ResultData testDB, MainWindow.uutdata uutData, out string reporttext)
        {
            string outmsg = string.Empty;
            int status = (int)ErrorCodes.Program.Success;
            string addressToTest = string.Empty;

            testDB.CreateStepRecord(testToRun.name, testToRun.step_number);
            reporttext = string.Empty;
            addressToTest = testToRun.GetParameterValue("Address");

 
            if (!sensorConsole.WriteLine("d flash " + addressToTest, 1000))
            {
                reporttext = "Dut Com Error: Error sending the 'd flash " + addressToTest + "' command.";
                status = (int)ErrorCodes.Program.DUTComError + (testToRun.step_number * 100);
            }
            if (status == (int)ErrorCodes.Program.Success)
            {
                if (!sensorConsole.WaitForPrompt(10000, uutData.dvtmanPrompt, out outmsg))
                {
                    reporttext = "Dut Com Error: Error waiting for 'd flash " + addressToTest + "' post prompt." + outmsg;
                    status = (int)ErrorCodes.Program.DUTComError + (testToRun.step_number * 100);
                }
                else
                {
                    if (outmsg.Contains("FF"))
                    {
                        reporttext = "Flash Failure: Found a 'FF': " + outmsg;
                        status = (int)ErrorCodes.Program.ValueOutsideOfLimit + (testToRun.step_number * 100);
                    }
                    if (outmsg.Contains("Read external flash failed"))
                    {
                        reporttext = "Flash Failure: Found a 'Read external flash failed': " + outmsg;
                        status = (int)ErrorCodes.Program.ValueOutsideOfLimit + (testToRun.step_number * 100);
                    }
                }
            }
            testDB.AddStepResult((status == (int)ErrorCodes.Program.Success) ? "PASS" : "FAIL", reporttext, status);

            return status;
        }

    }


}
