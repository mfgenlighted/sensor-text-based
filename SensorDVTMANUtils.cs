﻿using System;
using System.Windows.Forms;
using System.Threading;

//using LimitFileFunctions;

namespace SensorManufSY2
{


    public partial class SensorDVTMANTests
    {
        //-----------------------------------------------
        //  ReadSensors
        /// <summary> Will read the sensors and store the values locally.</summary>
        ///<remarks>
        /// Decodes the text as follows:
        ///  T=  pir temperature
        ///  P=  pir ir
        ///  M=  motion
        ///  A=  ambient visable
        ///  I=  ambient IR
        ///  L=  lux
        ///  C=  cpu temperature in C
        ///</remarks> 
        /// <param name="data">returned data. Values will be set to -99
        ///  if there was a error reading or converting.</param>
        /// <param name="reporttext">out string - If there was a error, describes the error.</param>
        /// <returns>test status</returns>
        public int ReadSensors(ref SUSensorData data,  MainWindow.uutdata uutData, out string reporttext)
        {
            string outmsg = string.Empty;

            reporttext = string.Empty;
            data.PIRTemperature = -99;
            data.PIRIR = -99;
            data.Motion = -99;
            data.AmbVisable = -99;
            data.AmbIR = -99;
            data.AmbLux = -99;
            data.CPUTemperature = -99;
            data.AmbBlue = -99;
            data.AmbGreen = -99;
            data.AmbRed = -99;

            // send the command to the UUT
            if (!sensorConsole.WriteLine("d sens", 3000))
            {
                reporttext = "Error issueing 'd sens' command.";
                return (int)ErrorCodes.Program.DUTComError;
            }
            if (!sensorConsole.WaitForPrompt(5000, uutData.dvtmanPrompt, out outmsg))
            {
                reporttext = "Error waiting for echo for the 'd sens' command. ";
                return (int)ErrorCodes.Program.DUTComError;
            }

            // now decode the data
            string[] lines = outmsg.Split('\n');
            try
            {
                foreach (var item in lines)
                {
                    if (item.Contains("T="))
                        data.PIRTemperature = Convert.ToDouble(item.Substring(item.IndexOf('=') + 1));
                    if (item.Contains("P="))
                        data.PIRIR = Convert.ToInt32(item.Substring(item.IndexOf('=') + 1));
                    if (item.Contains("M="))
                        data.Motion = Convert.ToInt32(item.Substring(item.IndexOf('=') + 1));
                    if (item.Contains("A="))
                        data.AmbVisable = Convert.ToInt32(item.Substring(item.IndexOf('=') + 1));
                    if (item.Contains("I="))
                        data.AmbIR = Convert.ToInt32(item.Substring(item.IndexOf('=') + 1));
                    if (item.Contains("L="))
                        data.AmbLux = Convert.ToInt32(item.Substring(item.IndexOf('=') + 1));
                    if (item.Contains("C="))
                        data.CPUTemperature = Convert.ToInt32(item.Substring(item.IndexOf('=') + 1));
                    if (item.Contains("R="))
                        data.AmbRed = Convert.ToInt32(item.Substring(item.IndexOf('=') + 1));
                    if (item.Contains("B="))
                        data.AmbBlue = Convert.ToInt32(item.Substring(item.IndexOf('=') + 1));
                    if (item.Contains("G="))
                        data.AmbGreen = Convert.ToInt32(item.Substring(item.IndexOf('=') + 1));
                }
            }
            catch (Exception ex)
            {
                reporttext = "Error converting 'd sens' command data.\n" + ex.Message;
                return (int)ErrorCodes.Program.DutUnexpectedCmdReturn;
            }

            return (int)ErrorCodes.Program.Success;
        }

        //--------------------------------------
        // ReadCPUTemperature
        //
        /// <summary>
        /// Will read the CPU temperature
        /// </summary>
        //
        // Return:
        //  bool - true = dut command send and prompt found
        //          false = problem with dut or conversion. see reporttext. temperature returned as -99.
        //

        public int ReadCPUTemperature(out int temp, MainWindow.uutdata uutData, out string reporttext)
        {
            string msg = string.Empty;
            int status = 0;
            SUSensorData data = new SUSensorData();

            status = ReadSensors(ref data, uutData, out reporttext);
            temp = data.CPUTemperature;

            return status;
        }


                        //------------------------------------------//
                        //------------------------------------------//
                        //                                          //
                        //      Manufacturing data routines         //
                        //------------------------------------------//
                        //------------------------------------------//

        /// <summary>
        /// Reads the manufacturing data from the unit
        /// </summary>
        /// <remarks>
        /// Assumes a output of:
        /// uutPrompt d man
        ///     MAC=00006854f5db0008
        ///     PCBA Part=02-00239-01
        ///     PCBA Serial=CRSP01131100007
        ///     HLA Part=--not set--
        ///     HLA Serial=--not set--
        ///     s1=Programmed
        ///     s2=Programmed
        ///     model=--not set--
        /// Also verifies the s1 and s2 are set to Programmed.
        /// </remarks>
        private bool readManufData (MainWindow.uutdata uutData, ref MainWindow.uutdata readuutData, ref string outmsg)
        {
            bool status = true;
            string uoutmsg = string.Empty;
            bool olddisplaySerialRcv;
            bool olddisplaySerialSend;
            int foundindex = 0;
            char[] delimiterChars = { '\n', '\r' };

            olddisplaySerialRcv = sensorConsole.DisplayRcvChar;
            olddisplaySerialSend = sensorConsole.DisplaySendChar;

            sensorConsole.DisplaySendChar = false;           // do not want to display this data, ever
            sensorConsole.DisplayRcvChar = false;

            outmsg = string.Empty;
            if (!sensorConsole.WriteLine("d man", 1000))
            {
                outmsg = "Dut Com error: Error while sending 'd man' command.";
                status = false;       // just returns because assuming there is no dut response at all
            }

            if (!sensorConsole.WaitForPrompt(3000, uutData.dvtmanPrompt, out outmsg) && status)
            {
                outmsg = "Dut Com error: Error waiting for " + uutData.dvtmanPrompt + " after the 'd man' command.";
                status = false;
            }
            sensorConsole.DisplaySendChar = olddisplaySerialSend;
            sensorConsole.DisplayRcvChar = olddisplaySerialRcv;

            if (status)
            {
                string[] lines = outmsg.Split(delimiterChars);

                readuutData.mac = "";
                readuutData.model = "";
                readuutData.pcbaPN = "";
                readuutData.pcbaSN = "";
                readuutData.topPN = "";
                readuutData.topSN = "";
                readuutData.s1Set = false;
                readuutData.s2Set = false;

                foreach (var item in lines)
                {
                    // parse out the data
                    if (item.Contains("MAC="))
                        readuutData.mac = item.Substring(item.IndexOf('=') + 1).ToUpper();

                    if (item.Contains("PCBA Part="))
                        readuutData.pcbaPN = item.Substring(item.IndexOf('=') + 1);

                    if (item.Contains("PCBA Serial="))
                        readuutData.pcbaSN = item.Substring(item.IndexOf('=') + 1);

                    if (item.Contains("HLA Part="))
                    {
                        readuutData.topPN = item.Substring(item.IndexOf('=') + 1);
                        if (readuutData.topPN.Contains("--"))   // if it has not been set yet
                            readuutData.topPN = "";
                    }

                    if (item.Contains("HLA Serial="))
                    {
                        readuutData.topSN = item.Substring(item.IndexOf('=') + 1);
                        if (readuutData.topSN.Contains("--"))   // if it has not been set yet
                            readuutData.topSN = "";
                    }

                    if (item.Contains("model="))
                    {
                        readuutData.model = item.Substring(item.IndexOf('=', foundindex) + 1);
                        if (readuutData.model.Contains("--"))   // if it has not been set yet
                            readuutData.model = "";
                    }

                    if (item.Contains("s1=Programmed"))
                        readuutData.s1Set = true;

                    if (item.Contains("s2=Programmed"))
                        readuutData.s2Set = true;
                }
            }

            return status;
        }

        //---------------------------------
        // setPcbaManData
        ///<summary>This will set the pcba manufacturing data.
        /// </summary>
        /// <remarks>
        /// </remarks>
        ///<param name="mac">string - mac to save in the dut. Format xxxxxxxxxxxx.</param>
        ///<param name="pn">string - part number to save in the dut.</param>
        ///<param name="sn">string - serial number to save in the dut.</param>
        ///<param name="newdata">bool - true if this was generated MAC, not found in the database.</param>
        ///<param name="reporttext">out string - description of the error if there is a failure.</param>
        ///<returns>
        ///     ErrorCodes.Program.Success        0
        ///     ErrorCodes.Program.DUTComError    2
        ///     ErrorCodes.Program.DupicateSN     5
        ///</returns>
        ///
        private int setPcbaManData(LimitsDatabase.LimitsData.stepsItem parameters, MainWindow.uutdata uutData, bool newdata, out string reporttext)
        {
            string cmdline;
            bool olddisplaySerialRcv;
            bool olddisplaySerialSend;
            int retests = 2;
            int retestCount = 0;
            bool badEcho = false;

            // ---->>>>> DO NOT EVER DISPLAY THIS COMMAND OR RECORD IT IN A FILE. <<<<<-----
            cmdline = string.Format("s pcba 0000{0} {1} {2} 7CA9997D 656E6C6967", uutData.mac, uutData.pcbaPN, uutData.pcbaSN);

            int stepStatus = (int)ErrorCodes.Program.Success;
            string outmsg = string.Empty;
            string echoReporttext = string.Empty;
            int echoStepStatus = (int)ErrorCodes.Program.Success;

            olddisplaySerialRcv = sensorConsole.DisplayRcvChar;         // this data should never be displayed
            olddisplaySerialSend = sensorConsole.DisplaySendChar;
            sensorConsole.DisplayRcvChar = false;
            sensorConsole.DisplaySendChar = false;
        

            if (parameters.GetParameterValue("Retrys") != string.Empty)
                retests = Convert.ToInt32(parameters.GetParameterValue("Retrys"));

            reporttext = string.Empty;

            do
            {
                sensorConsole.ClearBuffers();
                stepStatus = (int)ErrorCodes.Program.Success;
                badEcho = false;
                if (!sensorConsole.WriteLine(cmdline, 3000))
                {
                    echoReporttext = sensorConsole.LastErrorMessage;
                    echoStepStatus = (int)sensorConsole.LastErrorCode + (parameters.step_number * 100);
                    badEcho = true;
                    // might be a dropped character. so let loop retry
                }
                if (!sensorConsole.WaitForPrompt(5000, uutData.dvtmanPrompt, out cmdline) && (stepStatus == (int)ErrorCodes.Program.Success))
                {
                    // did not see the prompt come back.
                    reporttext = "Error waiting for the prompt for the 's pcb' command.";
                    stepStatus = (int)ErrorCodes.Program.PromptNotFound + (parameters.step_number * 100);
                    break;     // don't try again. something wrong with the commuications
                }
                else            // found dvtman prompt
                {
                    if (!badEcho)       // if there was no bad echo previously, verfiy the programming
                    {
                        // verify that it programed correctly
                        if (!checkPcbaManData(uutData, out reporttext))     // if what is programmed does not match what should be there
                        {
                            reporttext = "\t  Read back data did not match what was programed. " + reporttext;
                            stepStatus = (int)ErrorCodes.Program.PCBADataMismatch + (parameters.step_number * 100);
                        }
                        else
                        {
                            reporttext = string.Format("Programmed SN: {0} \n\tPN: {1} \n\tMAC: {2}", uutData.pcbaSN, uutData.pcbaPN, uutData.mac);
                            stepStatus = (int)ErrorCodes.Program.Success;
                        }
                    }
                    else                // else there was a bad echo, so set errors
                    {
                        reporttext = "\t  Echo bad. " + echoReporttext;
                        stepStatus = echoStepStatus + (parameters.step_number * 100);
                    }
                }

            } while ((stepStatus != (int)ErrorCodes.Program.Success) & (retestCount++ < retests));

            sensorConsole.DisplayRcvChar = olddisplaySerialRcv;         // restore the display
            sensorConsole.DisplaySendChar = olddisplaySerialSend;
            if (stepStatus == (int)ErrorCodes.Program.Success)
            {
                sensorConsole.WriteLine("p reboot", 3000);      // need to reboot so that the pcba sets correctly
                                                                // if the prompt is not found, most likely there was a hardware failure.
                if (!sensorConsole.WaitForPrompt(4000, uutData.dvtmanPrompt, out cmdline))  // if the prompt was not found
                {
                    reporttext = "Did not find prompt. Check message for possible hardware errors.\n" + cmdline;
                    stepStatus = (int)ErrorCodes.Program.DutUnexpectedCmdReturn + (parameters.step_number * 100);
                }
            }
            return stepStatus;
        }


        //----------------------------------------
        // CheckPcbaManData
        /// <summary>
        /// Will verfiy the pcba manufacturing data with what is passed.
        /// <remarks>
        /// Assumes a output of:
        /// uutPrompt d man
        ///     MAC=00006854f5db0008
        ///     PCBA Part=02-00239-01
        ///     PCBA Serial=CRSP01131100007
        ///     HLA Part=--not set--
        ///     HLA Serial=--not set--
        ///     s1=Programmed
        ///     s2=Programmed
        ///     model=--not set--
        /// Also verifies the s1 and s2 are set to Programmed.
        /// </remarks>
        /// </summary>
        /// <param name="mac">string - mac(no leading 0, no :)</param>
        /// <param name="pn">string - pcba pn</param>
        /// <param name="sn">string - pcba sn</param>
        /// <param name="reporttext">string - error string if there was a failure</param>
        /// <returns>bool - true = all is OK</returns>
        private bool checkPcbaManData(MainWindow.uutdata uutData, out string reporttext)
        {
            bool status = true;
            string outmsg = string.Empty;
            string uoutmsg = string.Empty;
            string currentMAC = string.Empty;
            string currentSN = string.Empty;
            string currentPN = string.Empty;
            MainWindow.uutdata readuutData = new MainWindow.uutdata();

            reporttext = string.Empty;
            if (!readManufData(uutData, ref readuutData, ref reporttext))
                status = false;

            // now parse the output
            if (status)
            {
                if (readuutData.mac.ToUpper().Substring(4) != uutData.mac.ToUpper())
                {
                    status = false;
                }
                if (readuutData.pcbaPN != uutData.pcbaPN)
                {
                    status = false;
                }
                if (readuutData.pcbaSN != uutData.pcbaSN)
                {
                    status = false;
                }
                if (!readuutData.s1Set  || !readuutData.s2Set)
                {
                    reporttext = reporttext + "One or more security strings not found in response.\n";
                    status = false;
                }
            }

            reporttext = string.Format("{0}\t  PN Expected: {1} \tRead: {2}\n\t  SN Expected: {3} \tRead: {4}\n\t  MAC Expected: {5} \tRead: {6}\n",
                                            reporttext, uutData.pcbaPN, readuutData.pcbaPN, uutData.pcbaSN, readuutData.pcbaSN, uutData.mac, readuutData.mac);

            return status;
        }

        /// <summary>
        /// Will set the HLA data.
        /// </summary>
        /// <param name="uutData"></param>
        /// <param name="reporttext"></param>
        /// <returns></returns>
        private int setHlaData(LimitsDatabase.LimitsData.stepsItem parameters, MainWindow.uutdata uutData, bool newdata, out string reporttext)
        {
            string cmdline;
            int status;
            int retests = 2;
            int retestCount = 0;

            reporttext = string.Empty;

            cmdline = string.Format("s hla {0} {1} {2}", uutData.topPN, uutData.topSN, uutData.model);

            if (parameters.GetParameterValue("Retrys") != string.Empty)
                retests = Convert.ToInt32(parameters.GetParameterValue("Retrys"));

            reporttext = string.Empty;
            do
            {
                if (!sensorConsole.WriteLine(cmdline, 3000))    // if problem sending the command
                {
                    reporttext = "Dut Com Error: Error sending the 's hla' command.";
                    status = (int)ErrorCodes.Program.DUTComError + (parameters.step_number * 100);
                    break;      // com issue. do not retry
                }
                else        // command sent OK
                {
                    if (!sensorConsole.WaitForPrompt(3000, uutData.dvtmanPrompt, out cmdline))  // if problem waiting for prompt
                    {
                        reporttext = "Dut Com Error: Error waiting for the prompt for the 's hla' command.";
                        status = (int)ErrorCodes.Program.DUTComError + (parameters.step_number * 100);
                        break;      // com issue. do not retry
                    }
                    else        // got prompt
                    {
                        status = checkHlaManData(uutData, out reporttext);
                        if (status != (int)ErrorCodes.Program.Success)      // if the hla data did not match
                        {
                            if (status < (int)ErrorCodes.System.SystemErrorCodes)
                                status = status + (parameters.step_number * 100);
                        }
                        else
                        {
                            reporttext = string.Empty;
                            status = (int)ErrorCodes.Program.Success;

                        }
                    }
                }

            } while ((status != (int)ErrorCodes.Program.Success) & (retestCount++ < retests));

            return status;
        }


        //----------------------------------
        // CheckHlaManData
        /// <summary>
        /// Will check to see if the HLA data matchs what is passed.
        /// Assumes the format is:
        /// uutPrompt d man
        ///     MAC=00006854f5db0008
        ///     PCBA Part=02-00239-01
        ///     PCBA Serial=CRSP01131100007
        ///     HLA Part=01-00280-01
        ///     HLA Serial=EOWS01131300113
        ///     s1=Programmed
        ///     s2=Programmed
        ///     model=RS-2O-SW
        /// </summary>
        /// <param name="pn">string - hla pn</param>
        /// <param name="sn">string - hla sn</param>
        /// <param name="model">string - model</param>
        /// <param name="reporttext">string - error text if there is a error.</param>
        /// <returns>bool - true = everthing matches.</returns>
        private int checkHlaManData(MainWindow.uutdata uutData, out string reporttext)
        {
            int status = (int)ErrorCodes.Program.Success;
            string outmsg = string.Empty;

            reporttext = string.Empty;
            reporttext = string.Empty;
            if (!sensorConsole.WriteLine("d man", 1000))
            {
                reporttext = "Dut Com error: Error while sending 'd man' command.";
                return (int)ErrorCodes.Program.DUTComError;       // just returns because assuming there is no dut response at all
            }

            if (!sensorConsole.WaitForPrompt(3000, uutData.dvtmanPrompt, out outmsg))
            {
                reporttext = "Dut Com error: Error waiting for " + uutData.dvtmanPrompt + " after the 'd man' command.";
                return (int)ErrorCodes.Program.DUTComError;
            }

            // now parse the output
            if (!outmsg.Contains(uutData.topPN))
            {
                if (reporttext == string.Empty)
                    reporttext = "Error: ";
                else
                    reporttext = reporttext + "\n";
                reporttext = reporttext + "PN not found in response.";
                status = (int)ErrorCodes.Program.DutUnexpectedCmdReturn;
            }
            if (!outmsg.Contains(uutData.topSN))
            {
                if (reporttext == string.Empty)
                    reporttext = "Error: ";
                else
                    reporttext = reporttext + "\n";
                reporttext = reporttext + "SN not found in response.";
                status = (int)ErrorCodes.Program.DutUnexpectedCmdReturn;
            }
            if (!outmsg.Contains(uutData.model))
            {
                if (reporttext == string.Empty)
                    reporttext = "Error: ";
                else
                    reporttext = reporttext + "\n";
                reporttext = reporttext + "Model not found in response.";
                status = (int)ErrorCodes.Program.DutUnexpectedCmdReturn;
            }

            return status;
        }



                //--------------------------------------//
                //--------------------------------------//
                //                                      //
                //      Value checking routines         //
                //--------------------------------------//
                //--------------------------------------//

        //-------------------------------------------------
        /// <summary>
        /// This function will check a value as defined in a List type parameters.
        /// </summary>
        /// <param name="testvalue"></param>
        /// <param name="item"></param>
        /// <returns>bool - true = meets the conditions</returns>
        private static bool CheckResultValue(double testvalue, LimitsDatabase.LimitsData.limitsItem item, ref string resultmessage)
        {
            bool status = true;

            resultmessage = string.Empty;
            switch (item.type.ToUpper())
            {
                case "RANGE":
                    switch (item.operation)
                    {
                        case "GTLT":
                            if ((testvalue <= Convert.ToDouble(item.value1)) || (testvalue >= Convert.ToDouble(item.value2)))
                                status = false;
                            resultmessage = string.Format("{0}   Expected: {1} < x < {2}  Found: {3:N2}\n", item.name, item.value1, item.value2, testvalue);
                            break;
                        case "GELE":
                            if ((testvalue < Convert.ToDouble(item.value1)) || (testvalue > Convert.ToDouble(item.value2)))
                                status = false;
                            resultmessage = string.Format("{0}   Expected: {1} <= x <= {2}  Found: {3:N2}\n", item.name, item.value1, item.value2, testvalue);
                            break;
                        default:
                            throw new Exception("Invalid operation: " + item.name.ToString() + ":" + item.operation.ToString());
                    }
                    break;

                case "VALUE":
                    switch (item.operation)
                    {
                        case "GE":      // greater than or equal
                            if (!(testvalue >= Convert.ToDouble(item.value1)))
                                status = false;
                            resultmessage = string.Format("{0}   Expected: x >= {1}  Found: {2:N2}\n", item.name, item.value1, testvalue);
                            break;

                        case "EQ":      // equal
                            if (!(testvalue == Convert.ToDouble(item.value1)))
                                status = false;
                            resultmessage = string.Format("{0}   Expected: x = {1}  Found: {2:N2}\n", item.name, item.value1, testvalue);
                            break;

                        case "LE":      // less than or equal
                            if (!(testvalue <= Convert.ToDouble(item.value1)))
                                status = false;
                            resultmessage = string.Format("{0}   Expected: x <= {1}  Found: {2:N2}\n", item.name, item.value1, testvalue);
                            break;

                        case "LT":      // less than
                            if (!(testvalue < Convert.ToDouble(item.value1)))
                                status = false;
                            resultmessage = string.Format("{0}   Expected: x < {1}  Found: {2:N2}\n", item.name, item.value1, testvalue);
                            break;

                        default:
                            throw new Exception("Invalid operation: " + item.name.ToString() + ":" + item.operation.ToString());
                    }
                    break;

                default:
                    throw new Exception("Invalid type: " + item.name.ToString() + ":" + item.operation.ToString());
            }

            return status;
        }

        //------
        // utility for CU test
        uint CrcCalc(byte[] msg, int len)
        {
            uint k, i;
            uint crc = 0;

            const UInt16 poly = 0x07;
            for (i = 0; i < len; i++)
            {
                crc = crc ^ msg[i];
                for (k = 0; k < 8; k++)
                {
                    if ((crc & 0x80) != 0)
                    {
                        crc <<= 1;
                        crc ^= poly;
                    }
                    else
                    {
                        crc <<= 1;
                    }
                }
            }
            return (crc);
        }


                //--------------------------------------//
                //--------------------------------------//
                //                                      //
                // limit file utils                     //
                //--------------------------------------//
                //--------------------------------------//
        /// <summary>
        /// Will get the data for a given constant in the CList.
        /// </summary>
        /// <param name="testData">structure that has the CList</param>
        /// <param name="constantName">name of the constant to get data for</param>
        /// <param name="constData">returned data. string.empty if none found</param>
        /// <returns>true = name is found</returns>
        //private bool GetConstantValue(TestToRun testData, string constantName, ref string constValue)
        //{
        //    bool tagfound = false;

        //    constValue = string.Empty;
        //    foreach (var item in testData.CList)   // go through the parameters in the test
        //    {
        //        if (item.Name.ToUpper().Equals(constantName.ToUpper()))
        //        {
        //            constValue = item.Value;
        //            tagfound = true;
        //        }
        //    }
        //    return tagfound;
        //}

        //private bool GetParameterStruct(TestToRun testData, string parameter, ref LimitFileFunctions.ListEntry paramStruct)
        //{
        //    bool tagfound = false;

        //    foreach (var item in testData.PList)
        //    {
        //        if (item.Parameter == parameter)
        //        {
        //            tagfound = true;
        //            paramStruct = item;
        //        }
        //    }
        //    return tagfound;
        //}

    }

}