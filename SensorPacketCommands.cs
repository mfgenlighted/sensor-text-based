﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;


/// <summary>
/// This is the sensor commuications using the packet structure.
/// </summary>

namespace SensorManufSY2
{
    class SensorPacketCommands : IDisposable
    {
        //------------------------
        // events
        //  Use UpdateStatusWindow(msg) to display a message on the
        //  MainWindow status window.
        // 
        // From the initializing routine, add
        //     sensor.StatusUpdated += new EventHandler<StatusUpdatedEventArgs>(xxxx);
        //  where xxxx is the routine to update the windows.
        //------------------------
        public event EventHandler<StatusUpdatedEventArgs> StatusUpdated;

        protected virtual void OnStatusUpdated(StatusUpdatedEventArgs e)
        {
            if (StatusUpdated != null)
                StatusUpdated(this, e);
        }

        // call these functions to update the main status window
        private void UpdateStatusWindow(string msg)
        {
            string newStatus = msg;
            StatusUpdatedEventArgs nuea = new StatusUpdatedEventArgs();
            nuea.statusText = msg;
            OnStatusUpdated(nuea);
        }
        private void UpdateStatusWindow(string msg, StatusType status)
        {
            string newStatus = msg;
            StatusUpdatedEventArgs nuea = new StatusUpdatedEventArgs();
            nuea.statusText = msg;
            nuea.statusType = status;
            OnStatusUpdated(nuea);
        }


        // error reporting
        public string LastErrorMessage = string.Empty;
        public int LastErrorCode = 0;

        // com defines
        public string ComPort = string.Empty;
        public uint ComSpeed = 0;

        private SerialPort serial = null;

        // defined commands
        public const byte CMD_Reboot = 0;
        public const byte CMD_ProgramPCBAData = 0;
        public const byte CMD_ProgramHLAData = 0;
        public const byte CMD_TestEnlightedRadio = 0;
        public const byte CMD_TestBLERadio = 0;
        public const byte CMD_ReadSensors = 0;
        public const byte CMD_SetLEDs = 0;
        public const byte CMD_SetVoltages = 0;
        public const byte CMD_ReadPCBAData = 0;
        public const byte CMD_ReadHLAData = 0;
        public const byte CMD_ReadFirmwareVersions = 0;


        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~SensorPacketCommands() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        void IDisposable.Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion

        public bool OpenComPort()
        {
            return true;
        }
        public bool OpenComPort(string comport, uint comspeed)
        {
            ComPort = comport;
            ComSpeed = comspeed;

            return OpenComPort();
        }
        public bool CloseComPort()
        {
            return true;
        }


        public bool Send_Reboot()
        {
            bool status = true;
            // pakage the command
            // send it
            // verify response
            // return status

            return status;
        }

        //---------------------------------------
        // utils
        //---------------------------------------
        bool send_cmd()
        {
            bool status = true;
            // package the command
            // send it
            return status;
        }

        bool receive_response()
        {
            bool status = true;
            //get the response
            return status;
        }

    }
}
