﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;
using System.Threading;

namespace SensorManufSY2
{
    class SnifferConsole
    {
        SerialPort snifferSerialPort;
        string snifferSerialBuffer = string.Empty;

        //------------------------
        // events
        //  Use UpdateStatusWindow(msg) to display a message on the
        //  MainWindow status window.
        //------------------------
        public event EventHandler<StatusUpdatedEventArgs>
            StatusUpdated;

        protected virtual void
            OnStatusUpdated(StatusUpdatedEventArgs e)
        {
            if (StatusUpdated != null)
                StatusUpdated(this, e);
        }

        // call these functions to update the main status window
        private void UpdateStatusWindow(string msg)
        {
            string newStatus = msg;
            StatusUpdatedEventArgs nuea = new StatusUpdatedEventArgs();
            nuea.statusText = msg;
            OnStatusUpdated(nuea);
        }
        private void UpdateStatusWindow(string msg, StatusType status)
        {
            string newStatus = msg;
            StatusUpdatedEventArgs nuea = new StatusUpdatedEventArgs();
            nuea.statusText = msg;
            nuea.statusType = status;
            OnStatusUpdated(nuea);
        }


        //----------------
        // Properties
        //----------------
        public string ComPort = null;
        public bool DisplaySerialSend = false;
        public bool DisplaySerialRcv = false;


        //--------------------------
        // Sets up and opens the Sniffer serial port.
        // Uses the ComPort value for the port.
        //
        // excpetions:
        //          PortNotFound - configured port not in computer ports
        //          Excpetion
        //          SerialPort contruct exceptions
        //          SerialPort.Open exceptions
        public void Open()
        {
            bool found = true;

            if (ComPort == null)                        // if it has not been defined
                throw new Exception("Error: Sniffer com port has not been defined. ComPort must be set.");
            found = CheckForSerialPort(ComPort);

            if (found)
            {
                // port valid on this system, so try to open
                try
                {
                    snifferSerialPort = new SerialPort(ComPort, 115200);
                    snifferSerialPort.DataReceived += new SerialDataReceivedEventHandler(SnifferSerialDataReceived);
                }
                catch (Exception ex)
                {
                    throw new Exception(string.Format("Error opening {0}", ComPort), ex);
                }
                snifferSerialPort.Open();           // open the serial port
            }
            else
                throw new Exception("Sniffer serial port " + ComPort + " is not a valid com port on this computer");
        }

        // Open version passing comport
         public void Open(string comPort)
        {
            ComPort = comPort;
            Open();
        }

        // Verify that the serial port is a valid port
        public bool CheckForSerialPort(string comPort)
        {
            string[] ports = SerialPort.GetPortNames();         // get list of serial ports
            // see if this port is in the list of ports on the computer
            foreach (string i in ports)
            {
                if (i.Equals(comPort))         // if found a match
                    return true;
            }
            return false;
        }


        //-----------------
        // Close the serial port
        public void Close()
        {
            snifferSerialPort.Close();
        }

        //------------------------
        // Clear the serial buffers, both serial driver and serial in buffer.
        // Exceptions:
        //      SerialPort.DiscardInBuffers construct exceptions
        //      SerialPort.DiscardOutBuffers construct exceptions
        public void ClearSerialBuffers()
        {
            try
            {
                snifferSerialPort.DiscardInBuffer();
                snifferSerialPort.DiscardOutBuffer();
                snifferSerialBuffer = string.Empty;
            }
            catch (Exception ex)
            {
                throw new Exception("Error clearing serial buffers", ex);
            }
        }

        /// <summary>
        /// Will set up the sniffer to talk to a unit with the MAC passed.
        /// </summary>
        /// <remarks>
        /// Issues the following commands:
        ///     scan remote on
        ///     remote mac -mac passed in xx:xx:xx format-
        ///     scan start
        /// </remarks>
        /// <param name="mac">MAC of unit to connect to</param>
        /// <param name="outmsg">error message if fails</param>
        /// <returns></returns>
        public int SetupForMAC (string mac, ref string outmsg)
        {
            int status = (int)ErrorCodes.Program.Success;
            string modifiedMAC = string.Empty;

            outmsg = string.Empty;

            // make sure serial port is OK
            if (snifferSerialPort == null)
            {
                outmsg = "Sniffer serial port not initialized.";
                return (int)ErrorCodes.System.ComponetInitFail;
            }
            if (!snifferSerialPort.IsOpen)
            {
                outmsg = "Sniffer serial port not Opened.";
                return (int)ErrorCodes.System.ComponetInitFail;
            }

            // format MAC to xx:xx:xx
            if (mac.Contains(':'))      // if in the form of aa:bb:cc:dd:ee:ff
            {
                if (mac.Length != 17)
                {
                    outmsg = "MAC is not in a accepted format. Should be aa:bb:cc:dd:ee:ff or aabbccddeeff.";
                    return (int)ErrorCodes.System.InvalidParameter;
                }
                modifiedMAC = mac.Substring(8, 8);      // get last 3 parts
            }
            else                        // else in aabbccddeeff format
            {
                if (mac.Length != 12)
                {
                    outmsg = "MAC is not in a accepted format. Should be aa:bb:cc:dd:ee:ff or aabbccddeeff.";
                    return (int)ErrorCodes.System.InvalidParameter;
                }
                modifiedMAC = mac.Substring(6, 2) + ":" + mac.Substring(8, 2) + ":" + mac.Substring(10,2);      // get last 3 parts
            }

            // set up sniffer to talk to unit
            if (!SnifferComWriteLine("scan remote on", 1000))
            {
                status = (int)ErrorCodes.Program.DUTComError;
                outmsg = "Error sending 'scan remote on'";
            }
            else if (!SnifferComWriteLine("remote mac " + modifiedMAC, 1000))
            {
                status = (int)ErrorCodes.Program.DUTComError;
                outmsg = "Error sending 'remote mac " + modifiedMAC + "'";
            }
            else if (!SnifferComWriteLine("scan start", 1000))
            {
                status = (int)ErrorCodes.Program.DUTComError;
                outmsg = "Error sending 'scan start'";
            }

            return status;
        }

        //-------------------------------
        // SnifferComWriteLine
        ///<summary>
        ///  Send a string to Sniffer
        ///  Will send the string to the dut. <cr> will be added to the end of the string.
        ///  After sending the string, it will wait for the echo.
        /// </summary> 
        /// <param name="cmdString">string cmdString - string to send. <cr> will be sent at end.</param>
        /// <param name="timeout">int - number of ms to wait for the echo</param>
        /// <returns>bool - true = command was sent and echo received
        /// false - timeout</returns>
        public bool SnifferComWriteLine(string cmdString, int timeout)
        {
            string rtnstring = string.Empty;
            string msg;
            bool status = true;
            bool echobad = false;
            int timesSent = 0;
            int x;
            char[] cmdBuf;

            cmdBuf = cmdString.ToCharArray();

            do
            {
                try
                {
                    for (x = 0; x < cmdString.Length; x++)
                    {
                        snifferSerialPort.Write(cmdBuf, x, 1);
                        Thread.Sleep(20);
                    }
                    snifferSerialPort.Write("\n");
                    Thread.Sleep(20);
                    if (DisplaySerialSend)         // if display window is enabled
                    {
                            msg = DateTime.Now.ToString("Sniffer HH:mm:ss.fff") + "->" + cmdString + "\n";
                        UpdateStatusWindow(msg, StatusType.snifferSend);
                    }

                }
                catch (TimeoutException)    // return failed if timed out
                {
                    return false;
                }
                catch (Exception)           // throw a exception if anything else
                {
                    throw;
                }

                echobad = true;
                status = SnifferReadLine(ref rtnstring, timeout);   // a line
                while (status)  // as long as there are lines, look for the echo
                {
                    if (rtnstring.Contains(cmdString))             // if the echo is found
                    {
                        echobad = false;
                        break;
                    }
                    Thread.Sleep(timeout);
                    status = SnifferReadLine(ref rtnstring, timeout);   // a line
                }

            } while (echobad && (timesSent <= 1));

            return status;
        }

        //-------------------------------
        // SnifferRemoteWriteLine
        ///<summary>
        ///  Send a string to Sniffer
        ///  Will send the string to the dut. cr will be added to the end of the string.
        ///  After sending the string, it will wait for the echo.
        /// </summary> 
        /// <param name="cmdString">string cmdString - string to send. cr will be sent at end.</param>
        /// <param name="timeout">int - number of ms to wait for the echo</param>
        /// <returns>bool - true = command was sent and echo received
        /// false - timeout</returns>
        public bool SnifferRemoteWriteLine(string cmdString, int timeout)
        {
            string rtnstring = string.Empty;
            string msg;
            bool status = true;
            bool echobad = false;
            int timesSent = 0;
            int x;
            char[] cmdBuf;

            cmdString = "remote " + cmdString;
            cmdBuf = cmdString.ToCharArray();

            do
            {
                timesSent++;
                try
                {
                    for (x = 0; x < cmdString.Length; x++)
                    {
                        snifferSerialPort.Write(cmdBuf, x, 1);
                        Thread.Sleep(20);
                    }
                    snifferSerialPort.Write("\n");
                    Thread.Sleep(20);
                    if (DisplaySerialSend)         // if display window is enabled
                    {
                            msg = DateTime.Now.ToString("Sniffer HH:mm:ss.fff") + "->" + cmdString + "\n";
                            UpdateStatusWindow(msg, StatusType.snifferSend);
                    }

                }
                catch (TimeoutException)    // return failed if timed out
                {
                    return false;
                }
                catch (Exception)           // throw a exception if anything else
                {
                    throw;
                }

                echobad = true;
                status = SnifferReadLine(ref rtnstring, timeout);   // a line
                while (status)  // as long as there are lines, look for the echo
                {
                    if (rtnstring.Contains(cmdString))             // if the echo is found
                    {
                        echobad = false;
                        break;
                    }
                    Thread.Sleep(timeout);
                    status = SnifferReadLine(ref rtnstring, timeout);   // a line
                }

            } while (echobad && (timesSent <= 1));

            return status;
        }


        //--------------------------------------------
        // Method: DutReadLine
        /// <summary>
        ///  Reads the serial input buffer till a newline character is found.
        ///  If the newline character is not found, false is returned.
        /// </summary>
        ///
        ///<param name="rcvCount">int - Number of characters to look for</param>
        ///<param name="returnedLine">ref string - characters found. Will be a empty string
        ///if at least rcvCount characters are not found</param>
        ///<param name="timeoutms">int - Number of ms to wait for the characters</param>
        ///<returns>bool - true = at least rcvCount characters are found and returned in returnedLine
        ///                 false = not enough characters were found in time. Empty string returned.</returns>
        public bool SnifferReadLine(ref string returnedLine, int timeoutms)
        {
            DateTime endTime;
            bool status = true;

            endTime = DateTime.Now.AddMilliseconds(timeoutms);
            while (!snifferSerialBuffer.Contains('\n') && (endTime > DateTime.Now))
            {
                Thread.Sleep(100);
            }

            if (snifferSerialBuffer.Contains('\n'))    // if line feed found
            {
                int nlIndex = snifferSerialBuffer.IndexOf('\n');
                returnedLine = snifferSerialBuffer.Substring(0, nlIndex);
                snifferSerialBuffer = snifferSerialBuffer.Substring(nlIndex + 1, snifferSerialBuffer.Length - nlIndex - 1);
            }
            else
            {
                returnedLine = string.Empty;
                status = false;
            }
            return status;
        }

        //--------------------------------------------
        // Method: DutReadLines
        /// <summary>
        ///  Reads the serial input buffer till a newline character is found.
        ///  If the newline character is not found, false is returned.
        ///  It will try to ready the number of lines asked for.
        /// </summary>
        ///
        ///<param name="rcvCount">int - Number of characters to look for</param>
        ///<param name="returnedLine">ref string[] - characters found. Will be a empty string
        ///if at least rcvCount characters are not found</param>
        ///<param name="timeoutms">int - Number of ms to wait for the characters</param>
        ///<returns>bool - true = at least rcvCount characters are found and returned in returnedLine
        ///                 false = not enough characters were found in time. Empty string returned.</returns>
        public bool SnifferReadLines(ref string[] returnedLines, int numOfLines, int timeoutms)
        {
            DateTime endTime;
            bool status = true;
            int numReadLines = 0;

            endTime = DateTime.Now.AddMilliseconds(timeoutms);
            while (numReadLines < numOfLines)
            {
                while (!snifferSerialBuffer.Contains('\n') && (endTime > DateTime.Now))
                {
                    Thread.Sleep(100);
                }

                if (snifferSerialBuffer.Contains('\n'))     // if line feed found
                {
                    int nlIndex = snifferSerialBuffer.IndexOf('\n');
                    returnedLines[numReadLines] = snifferSerialBuffer.Substring(0, nlIndex);
                    snifferSerialBuffer = snifferSerialBuffer.Substring(nlIndex + 1, snifferSerialBuffer.Length - nlIndex - 1);
                }
                else
                {
                    returnedLines[numReadLines] = string.Empty;
                    status = false;
                }
                numReadLines++;
            }
            for (int i = numOfLines; i < returnedLines.Length; i++)
                returnedLines[i] = string.Empty;
            return status;
        }


        //-------------------------------------------
        // DutWaitForPrompt
        /// <summary>
        /// Read until a prompt is found from the DUT. If no prompt is found, or
        /// no serial data read, a empty string will be returned.
        /// </summary>
        /// 
        /// <param name="outputData">Serial buffer from last read up till and including
        ///  prompt string. Will return empty string if prompt not found or no data
        ///  in the serial buffer</param>
        ///  <param name="prompt">String to look for in the serial data</param>
        ///  <param name="timeoutms">Timeout in milliseconds to wait for the prompt string</param>
        ///  
        /// <returns>bool - true = prompt found
        ///                 false = timed out looking for prompt
        ///</returns>
        public bool SnifferWaitForPrompt(int timeoutms, string prompt, out string outputData)
        {
            DateTime endTime;
            int x = 0;

            endTime = DateTime.Now.AddMilliseconds(timeoutms);
            while (!snifferSerialBuffer.Contains(prompt) && (endTime > DateTime.Now))
            {
                Thread.Sleep(10);
                x++;
            }

            if (snifferSerialBuffer.Contains(prompt))     // if prompt is found
            {
                int nlIndex = snifferSerialBuffer.IndexOf(prompt);                          // find where prompt is
                outputData = snifferSerialBuffer.Substring(0, nlIndex + prompt.Length);     // copy all data up to and including prompt
                if ((nlIndex + prompt.Length) == snifferSerialBuffer.Length)                // if prompt last thing in buffer
                    snifferSerialBuffer = string.Empty;                                     // empty the buffer
                else                                                                    // else move any data after prompt to buffer
                    snifferSerialBuffer = snifferSerialBuffer.Substring(nlIndex + prompt.Length,
                            snifferSerialBuffer.Length - nlIndex - prompt.Length);
                return true;
            }
            else                                    // prompt not found in time
            {
                outputData = string.Empty;       // so return what was read, but leave the buffer intact
                return false;
            }
        }



        //-------------------------------------
        // Method: DutSerialDataReceived
        //  Event handler for the dut serial port
        //
        void SnifferSerialDataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            SerialPort sp = (SerialPort)sender;

            string msg = sp.ReadExisting();
            if (DisplaySerialRcv)         // if display window is enabled
            {
                UpdateStatusWindow(msg, StatusType.snifferRcv);
            }
            snifferSerialBuffer += msg;
        }





    }
}
