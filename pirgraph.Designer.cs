﻿namespace SensorManufSY2
{
    partial class pirgraph
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend3 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series9 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series10 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series11 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series12 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.datapoints = new System.Windows.Forms.RichTextBox();
            this.graphYmax = new System.Windows.Forms.NumericUpDown();
            this.graphYMin = new System.Windows.Forms.NumericUpDown();
            this.promptSU = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.numericTimeInterval = new System.Windows.Forms.NumericUpDown();
            this.texttickcnt = new System.Windows.Forms.Label();
            this.buttonStart1sec = new System.Windows.Forms.Button();
            this.buttonLampToggle = new System.Windows.Forms.Button();
            this.promptDVTMAN = new System.Windows.Forms.RadioButton();
            this.lowtickcnt = new System.Windows.Forms.TextBox();
            this.hightickcnt = new System.Windows.Forms.TextBox();
            this.highvalue = new System.Windows.Forms.TextBox();
            this.lowvalue = new System.Windows.Forms.TextBox();
            this.buttonClear = new System.Windows.Forms.Button();
            this.buttonStart = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.radioButtonPIR = new System.Windows.Forms.RadioButton();
            this.radioButtonLED = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.buttonLEDToggle = new System.Windows.Forms.Button();
            this.LEDVoltage = new System.Windows.Forms.NumericUpDown();
            this.promptDVTMANNG = new System.Windows.Forms.RadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.graphYmax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.graphYMin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericTimeInterval)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LEDVoltage)).BeginInit();
            this.SuspendLayout();
            // 
            // datapoints
            // 
            this.datapoints.Location = new System.Drawing.Point(910, 36);
            this.datapoints.Name = "datapoints";
            this.datapoints.Size = new System.Drawing.Size(242, 371);
            this.datapoints.TabIndex = 34;
            this.datapoints.Text = "";
            // 
            // graphYmax
            // 
            this.graphYmax.Location = new System.Drawing.Point(212, 12);
            this.graphYmax.Maximum = new decimal(new int[] {
            20000,
            0,
            0,
            0});
            this.graphYmax.Name = "graphYmax";
            this.graphYmax.Size = new System.Drawing.Size(120, 20);
            this.graphYmax.TabIndex = 33;
            this.graphYmax.Value = new decimal(new int[] {
            15000,
            0,
            0,
            0});
            this.graphYmax.ValueChanged += new System.EventHandler(this.graphYmax_ValueChanged);
            // 
            // graphYMin
            // 
            this.graphYMin.Location = new System.Drawing.Point(243, 387);
            this.graphYMin.Maximum = new decimal(new int[] {
            20000,
            0,
            0,
            0});
            this.graphYMin.Name = "graphYMin";
            this.graphYMin.Size = new System.Drawing.Size(120, 20);
            this.graphYMin.TabIndex = 32;
            this.graphYMin.Value = new decimal(new int[] {
            3000,
            0,
            0,
            0});
            this.graphYMin.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // promptSU
            // 
            this.promptSU.AutoSize = true;
            this.promptSU.Location = new System.Drawing.Point(20, 75);
            this.promptSU.Name = "promptSU";
            this.promptSU.Size = new System.Drawing.Size(40, 17);
            this.promptSU.TabIndex = 31;
            this.promptSU.Text = "SU";
            this.promptSU.UseVisualStyleBackColor = true;
            this.promptSU.CheckedChanged += new System.EventHandler(this.promptChange);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(102, 165);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 13);
            this.label1.TabIndex = 30;
            this.label1.Text = "Time Interval(ms)";
            // 
            // numericTimeInterval
            // 
            this.numericTimeInterval.Location = new System.Drawing.Point(102, 184);
            this.numericTimeInterval.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericTimeInterval.Name = "numericTimeInterval";
            this.numericTimeInterval.Size = new System.Drawing.Size(83, 20);
            this.numericTimeInterval.TabIndex = 29;
            this.numericTimeInterval.ValueChanged += new System.EventHandler(this.numericTimeInterval_ValueChanged);
            // 
            // texttickcnt
            // 
            this.texttickcnt.AutoSize = true;
            this.texttickcnt.Location = new System.Drawing.Point(322, 395);
            this.texttickcnt.Name = "texttickcnt";
            this.texttickcnt.Size = new System.Drawing.Size(19, 13);
            this.texttickcnt.TabIndex = 28;
            this.texttickcnt.Text = "----";
            // 
            // buttonStart1sec
            // 
            this.buttonStart1sec.Location = new System.Drawing.Point(102, 135);
            this.buttonStart1sec.Name = "buttonStart1sec";
            this.buttonStart1sec.Size = new System.Drawing.Size(106, 23);
            this.buttonStart1sec.TabIndex = 27;
            this.buttonStart1sec.Text = "Start, light at 1 sec";
            this.buttonStart1sec.UseVisualStyleBackColor = true;
            this.buttonStart1sec.Click += new System.EventHandler(this.buttonStart1sec_Click);
            // 
            // buttonLampToggle
            // 
            this.buttonLampToggle.Location = new System.Drawing.Point(33, 341);
            this.buttonLampToggle.Name = "buttonLampToggle";
            this.buttonLampToggle.Size = new System.Drawing.Size(96, 23);
            this.buttonLampToggle.TabIndex = 26;
            this.buttonLampToggle.Text = "Turn light ON";
            this.buttonLampToggle.UseVisualStyleBackColor = true;
            this.buttonLampToggle.Click += new System.EventHandler(this.buttonLampToggle_Click);
            // 
            // promptDVTMAN
            // 
            this.promptDVTMAN.AutoSize = true;
            this.promptDVTMAN.Location = new System.Drawing.Point(20, 52);
            this.promptDVTMAN.Name = "promptDVTMAN";
            this.promptDVTMAN.Size = new System.Drawing.Size(71, 17);
            this.promptDVTMAN.TabIndex = 25;
            this.promptDVTMAN.Text = "DVTMAN";
            this.promptDVTMAN.UseVisualStyleBackColor = true;
            this.promptDVTMAN.CheckedChanged += new System.EventHandler(this.promptChange);
            // 
            // lowtickcnt
            // 
            this.lowtickcnt.Location = new System.Drawing.Point(118, 288);
            this.lowtickcnt.Name = "lowtickcnt";
            this.lowtickcnt.Size = new System.Drawing.Size(100, 20);
            this.lowtickcnt.TabIndex = 23;
            // 
            // hightickcnt
            // 
            this.hightickcnt.Location = new System.Drawing.Point(118, 262);
            this.hightickcnt.Name = "hightickcnt";
            this.hightickcnt.Size = new System.Drawing.Size(100, 20);
            this.hightickcnt.TabIndex = 22;
            // 
            // highvalue
            // 
            this.highvalue.Location = new System.Drawing.Point(2, 263);
            this.highvalue.Name = "highvalue";
            this.highvalue.Size = new System.Drawing.Size(100, 20);
            this.highvalue.TabIndex = 21;
            // 
            // lowvalue
            // 
            this.lowvalue.Location = new System.Drawing.Point(1, 289);
            this.lowvalue.Name = "lowvalue";
            this.lowvalue.Size = new System.Drawing.Size(100, 20);
            this.lowvalue.TabIndex = 20;
            // 
            // buttonClear
            // 
            this.buttonClear.Location = new System.Drawing.Point(20, 181);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(75, 23);
            this.buttonClear.TabIndex = 19;
            this.buttonClear.Text = "clear";
            this.buttonClear.UseVisualStyleBackColor = true;
            this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
            // 
            // buttonStart
            // 
            this.buttonStart.Location = new System.Drawing.Point(20, 135);
            this.buttonStart.Name = "buttonStart";
            this.buttonStart.Size = new System.Drawing.Size(75, 23);
            this.buttonStart.TabIndex = 18;
            this.buttonStart.Text = "Start";
            this.buttonStart.UseVisualStyleBackColor = true;
            this.buttonStart.Click += new System.EventHandler(this.buttonStart_Click);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // chart1
            // 
            chartArea3.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea3);
            legend3.Name = "Legend1";
            this.chart1.Legends.Add(legend3);
            this.chart1.Location = new System.Drawing.Point(255, 52);
            this.chart1.Name = "chart1";
            series9.ChartArea = "ChartArea1";
            series9.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series9.Legend = "Legend1";
            series9.Name = "Ambient-Red/PIR";
            series10.ChartArea = "ChartArea1";
            series10.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series10.Legend = "Legend1";
            series10.Name = "Ambient-Green";
            series11.ChartArea = "ChartArea1";
            series11.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series11.Legend = "Legend1";
            series11.Name = "Ambient-Blue";
            series12.ChartArea = "ChartArea1";
            series12.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series12.Legend = "Legend1";
            series12.Name = "Ambient-Visable";
            this.chart1.Series.Add(series9);
            this.chart1.Series.Add(series10);
            this.chart1.Series.Add(series11);
            this.chart1.Series.Add(series12);
            this.chart1.Size = new System.Drawing.Size(604, 312);
            this.chart1.TabIndex = 35;
            this.chart1.Text = "chart1";
            // 
            // radioButtonPIR
            // 
            this.radioButtonPIR.AutoSize = true;
            this.radioButtonPIR.Checked = true;
            this.radioButtonPIR.Location = new System.Drawing.Point(12, 20);
            this.radioButtonPIR.Name = "radioButtonPIR";
            this.radioButtonPIR.Size = new System.Drawing.Size(43, 17);
            this.radioButtonPIR.TabIndex = 36;
            this.radioButtonPIR.TabStop = true;
            this.radioButtonPIR.Text = "PIR";
            this.radioButtonPIR.UseVisualStyleBackColor = true;
            this.radioButtonPIR.CheckedChanged += new System.EventHandler(this.radioButtonPIR_CheckedChanged);
            // 
            // radioButtonLED
            // 
            this.radioButtonLED.AutoSize = true;
            this.radioButtonLED.Location = new System.Drawing.Point(82, 20);
            this.radioButtonLED.Name = "radioButtonLED";
            this.radioButtonLED.Size = new System.Drawing.Size(46, 17);
            this.radioButtonLED.TabIndex = 37;
            this.radioButtonLED.Text = "LED";
            this.radioButtonLED.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioButtonLED);
            this.groupBox1.Controls.Add(this.radioButtonPIR);
            this.groupBox1.Location = new System.Drawing.Point(407, -3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(134, 49);
            this.groupBox1.TabIndex = 38;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Sensor";
            // 
            // buttonLEDToggle
            // 
            this.buttonLEDToggle.Location = new System.Drawing.Point(33, 371);
            this.buttonLEDToggle.Name = "buttonLEDToggle";
            this.buttonLEDToggle.Size = new System.Drawing.Size(96, 23);
            this.buttonLEDToggle.TabIndex = 39;
            this.buttonLEDToggle.Text = "Turn LED ON";
            this.buttonLEDToggle.UseVisualStyleBackColor = true;
            this.buttonLEDToggle.Click += new System.EventHandler(this.buttonLEDToggle_Click);
            // 
            // LEDVoltage
            // 
            this.LEDVoltage.DecimalPlaces = 1;
            this.LEDVoltage.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.LEDVoltage.Location = new System.Drawing.Point(147, 371);
            this.LEDVoltage.Maximum = new decimal(new int[] {
            30,
            0,
            0,
            65536});
            this.LEDVoltage.Name = "LEDVoltage";
            this.LEDVoltage.Size = new System.Drawing.Size(71, 20);
            this.LEDVoltage.TabIndex = 40;
            // 
            // promptDVTMANNG
            // 
            this.promptDVTMANNG.AutoSize = true;
            this.promptDVTMANNG.Location = new System.Drawing.Point(20, 29);
            this.promptDVTMANNG.Name = "promptDVTMANNG";
            this.promptDVTMANNG.Size = new System.Drawing.Size(90, 17);
            this.promptDVTMANNG.TabIndex = 41;
            this.promptDVTMANNG.Text = "DVTMAN-NG";
            this.promptDVTMANNG.UseVisualStyleBackColor = true;
            this.promptDVTMANNG.CheckedChanged += new System.EventHandler(this.promptChange);
            // 
            // pirgraph
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1205, 466);
            this.Controls.Add(this.promptDVTMANNG);
            this.Controls.Add(this.LEDVoltage);
            this.Controls.Add(this.buttonLEDToggle);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.chart1);
            this.Controls.Add(this.datapoints);
            this.Controls.Add(this.graphYmax);
            this.Controls.Add(this.graphYMin);
            this.Controls.Add(this.promptSU);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.numericTimeInterval);
            this.Controls.Add(this.texttickcnt);
            this.Controls.Add(this.buttonStart1sec);
            this.Controls.Add(this.buttonLampToggle);
            this.Controls.Add(this.promptDVTMAN);
            this.Controls.Add(this.lowtickcnt);
            this.Controls.Add(this.hightickcnt);
            this.Controls.Add(this.highvalue);
            this.Controls.Add(this.lowvalue);
            this.Controls.Add(this.buttonClear);
            this.Controls.Add(this.buttonStart);
            this.Name = "pirgraph";
            this.Text = "pirgraph";
            ((System.ComponentModel.ISupportInitialize)(this.graphYmax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.graphYMin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericTimeInterval)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LEDVoltage)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox datapoints;
        private System.Windows.Forms.NumericUpDown graphYmax;
        private System.Windows.Forms.NumericUpDown graphYMin;
        private System.Windows.Forms.RadioButton promptSU;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numericTimeInterval;
        private System.Windows.Forms.Label texttickcnt;
        private System.Windows.Forms.Button buttonStart1sec;
        private System.Windows.Forms.Button buttonLampToggle;
        private System.Windows.Forms.RadioButton promptDVTMAN;
        private System.Windows.Forms.TextBox lowtickcnt;
        private System.Windows.Forms.TextBox hightickcnt;
        private System.Windows.Forms.TextBox highvalue;
        private System.Windows.Forms.TextBox lowvalue;
        private System.Windows.Forms.Button buttonClear;
        private System.Windows.Forms.Button buttonStart;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.RadioButton radioButtonPIR;
        private System.Windows.Forms.RadioButton radioButtonLED;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button buttonLEDToggle;
        private System.Windows.Forms.NumericUpDown LEDVoltage;
        private System.Windows.Forms.RadioButton promptDVTMANNG;
    }
}