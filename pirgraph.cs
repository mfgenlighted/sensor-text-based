﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace SensorManufSY2
{
    public partial class pirgraph : Form
    {
        private SensorConsole senConsole;
        private SensorDVTMANTests senTests;
        private FixtureFunctions fixFunctions;

        private int plowvalue = 0, phighvalue = 0;
        int tickcnt = 0;
        bool oldrcv = true, oldtrx = true;
        int lightStatus;
        double ledStatus;
        private int lastpirread;
        private MainWindow.uutdata uutData = new MainWindow.uutdata();


        public pirgraph(StationConfigure stationConfig, SensorConsole console, SensorDVTMANTests tests, FixtureFunctions fix)
        {
            SUSensorData sdata = new SUSensorData();

            string outmsg;

            InitializeComponent();
            senConsole = console;
            senTests = tests;
            fixFunctions = fix;

           
            uutData.dvtmanPrompt = "DVTMAN> ";
            fixFunctions.ControlDUTPower(1);

            senTests.ReadSensors(ref sdata, uutData, out outmsg);
            timer1.Interval = 500;
            numericTimeInterval.Value = timer1.Interval;
            fixFunctions.ControlPIRSource(0);
            lightStatus = 0;
            fixFunctions.SetAmbientSensorLED(0);
            ledStatus = 0;
        }

        private void buttonStart_Click(object sender, EventArgs e)
        {
            if (timer1.Enabled)
            {
                timer1.Enabled = false;
                buttonStart.Text = "Start";
                buttonStart1sec.Enabled = true;
            }
            else
            {
                timer1.Enabled = true;
                buttonStart.Text = "Stop";
                lastpirread = 0;
            }

        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            chart1.Series["Ambient-Red/PIR"].Points.Clear();
            if (radioButtonLED.Checked)
            {
                chart1.Series["Ambient-Green"].Points.Clear();
                chart1.Series["Ambient-Blue"].Points.Clear();
                chart1.Series["Ambient-Visable"].Points.Clear();
            }
            plowvalue = 100000;
            phighvalue = 0;
            tickcnt = 0;
            datapoints.Clear();
            senConsole.ClearBuffers();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            SUSensorData sdata = new SUSensorData();
            string outmsg;
            senTests.ReadSensors(ref sdata, uutData, out outmsg);
            if (radioButtonPIR.Checked)
            {
                if (sdata.PIRIR != -99)     // if good point
                    lastpirread = sdata.PIRIR;
                chart1.Series["Ambient-Red/PIR"].Points.AddXY(tickcnt * timer1.Interval / 1000.0, lastpirread);
                datapoints.AppendText(string.Format("{0:N1} : {1}\n", tickcnt * timer1.Interval / 1000.0, lastpirread.ToString()));
                if (lastpirread < plowvalue)
                {
                    plowvalue = lastpirread;
                    lowvalue.Text = plowvalue.ToString();
                    lowtickcnt.Text = (tickcnt * timer1.Interval / 1000).ToString();
                }
                if (lastpirread > phighvalue)
                {
                    phighvalue = lastpirread;
                    highvalue.Text = phighvalue.ToString();
                    hightickcnt.Text = (tickcnt * timer1.Interval / 1000).ToString();
                }
            }
            else
            {
                if (sdata.PIRIR != -99)     // if good point
                {
                    chart1.Series["Ambient-Red/PIR"].Points.AddXY(tickcnt * timer1.Interval / 1000.0, sdata.AmbRed);
                    chart1.Series["Ambient-Green"].Points.AddXY(tickcnt * timer1.Interval / 1000.0, sdata.AmbGreen);
                    chart1.Series["Ambient-Blue"].Points.AddXY(tickcnt * timer1.Interval / 1000.0, sdata.AmbBlue);
                    chart1.Series["Ambient-Visable"].Points.AddXY(tickcnt * timer1.Interval / 1000.0, sdata.AmbVisable);
                    datapoints.AppendText(string.Format("{4:N1} R:{0} B:{1} G:{2} V:{3}\n", sdata.AmbRed, sdata.AmbBlue, sdata.AmbGreen, sdata.AmbVisable,
                                               tickcnt * timer1.Interval / 1000.0));
                }
            }
            tickcnt++;
            texttickcnt.Text = tickcnt.ToString();
        }

        private void promptChange(object sender, EventArgs e)
        {
            if (promptDVTMAN.Checked)
            {
                uutData.dvtmanPrompt = "DVTMAN> ";
            }
            else if (promptDVTMANNG.Checked)
            {
                uutData.dvtmanPrompt = "DVTMAN-NG> ";
            }
            else
            {
                uutData.dvtmanPrompt = "SU";
            }
        }

        private void pirgraph_FormClosed(object sender, FormClosedEventArgs e)
        {
            senConsole.DisplaySendChar = oldtrx;
            senConsole.DisplayRcvChar = oldrcv;
            fixFunctions.ControlDUTPower(0);
        }

        private void buttonLampToggle_Click(object sender, EventArgs e)
        {
            if (lightStatus == 0)
            {
                lightStatus = 1;
                buttonLampToggle.Text = "Turn light OFF";
            }
            else
            {
                lightStatus = 0;
                buttonLampToggle.Text = "Turn light ON";
            }
            fixFunctions.ControlPIRSource(lightStatus);
        }

        private void buttonStart1sec_Click(object sender, EventArgs e)
        {
            if (!timer1.Enabled)
            {
                timer1.Enabled = true;
                Thread.Sleep(1000);
                fixFunctions.ControlPIRSource(1);
                buttonStart.Text = "Stop";
                buttonStart1sec.Enabled = false;
                buttonLampToggle.Text = "Turn light OFF";
            }
        }

        private void numericTimeInterval_ValueChanged(object sender, EventArgs e)
        {
            timer1.Interval = (int)numericTimeInterval.Value;
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            chart1.ChartAreas[0].AxisY.Minimum = (double)graphYMin.Value;
        }

        private void graphYmax_ValueChanged(object sender, EventArgs e)
        {
            chart1.ChartAreas[0].AxisY.Maximum = (double)graphYmax.Value;

        }

        private void radioButtonPIR_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonPIR.Checked)
            {
                chart1.Series["Ambient-Red/PIR"].Enabled = true;
                chart1.Series["Ambient-Green"].Enabled = false;
                chart1.Series["Ambient-Blue"].Enabled = false;
                chart1.Series["Ambient-Visable"].Enabled = false;
            }
            else
            {
                chart1.Series["Ambient-Red/PIR"].Enabled = true;
                chart1.Series["Ambient-Green"].Enabled = true;
                chart1.Series["Ambient-Blue"].Enabled = true;
                chart1.Series["Ambient-Visable"].Enabled = true;
            }
        }

        private void buttonLEDToggle_Click(object sender, EventArgs e)
        {
            if (ledStatus == 0)
            {
                ledStatus = Convert.ToDouble(LEDVoltage.Text);
                buttonLEDToggle.Text = "Turn LED OFF";
            }
            else
            {
                ledStatus = 0;
                buttonLEDToggle.Text = "Turn LED ON";
            }
            fixFunctions.SetAmbientSensorLED(ledStatus);

        }

    }
}
